import React from 'react';
import { StyleSheet, Text, View , TextInput,Button, TouchableOpacity,ScrollView,
   Platform, Image,ToastAndroid,Alert,ActivityIndicator,NetInfo} from 'react-native';
import AppNavigator from './AppNavigator';
import firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';
import { NavigationActions ,withNavigation} from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from './NavigationService';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      token:''
    };
}



  componentWillUnmount() {
    this.notificationListener();
    this.notificationOpenedListener();
  }

  async createNotificationListeners() {

            const channel = new firebase.notifications.Android.Channel(
              'notification_channel_name', // To be Replaced as per use
              'Notifications', // To be Replaced as per use
              firebase.notifications.Android.Importance.Max
          ).setDescription('A Channel To manage the notifications related to Application');
          firebase.notifications().android.createChannel(channel);




              /*
  //   * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
  //   * */
  //   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  //     //const { title, body } = notificationOpen.notification;
  //    // this.showAlert(title, body);
  //      ToastAndroid.show("Notfictatoin openingggggg......",ToastAndroid.LONG)
  // //    this.props.navigation.navigate('MOrderScreen')
  // //    const resetAction = StackActions.reset({
  // //    index: 0,
  // //    actions: [NavigationActions.navigate({ routeName: 'OrderHistory' })],
  // //  });
  // //  this.props.navigation.dispatch(resetAction);


  //     //     const navigateAction = NavigationActions.navigate({
  //     //       routeName: 'OrderHistory',
  //     //       params: {},
  //     //   })

  //     // this.props.navigation.dispatch(navigateAction)




  // });



    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
       // this.showAlert(title, body);
       //thgis
 //ToastAndroid.show("Firsssttttw aa",ToastAndroid.LONG)
       this.display(notification)

    });


    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
   // ToastAndroid.show("Called when notification is clicked",ToastAndroid.LONG)

    if (notificationOpen) {
       // const { title, body } = notificationOpen.notification;
        //this.showAlert(title, body);
        // ToastAndroid.show("Called when notification is clicked",ToastAndroid.LONG)



        //Alert.alert("ANdroid")
         // ToastAndroid.show("Called when notification is clicked in background android",ToastAndroid.LONG)

         AsyncStorage.getItem("roles_id").then((item) => {
            if(item){
              if(item == 2){
                NavigationService.navigate('NotificationsListModel');
              }
              else{
                NavigationService.navigate('NotificationsListMember');
              }
            }
        });


    }


    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //const { title, body } = notificationOpen.notification;
   // this.showAlert(title, body);


      //ToastAndroid.show("this is called when app is open in android",ToastAndroid.LONG)

      //Alert.alert("IOS")

AsyncStorage.getItem("roles_id").then((item) => {
   if(item){
     if(item == 2){
       NavigationService.navigate('NotificationsListModel');
     }
     else{
       NavigationService.navigate('NotificationsListMember');
     }
   }
});




});


    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      // console.log(JSON.stringify(message));
   //ToastAndroid.show("hahahahah..."+JSON.stringify(message),ToastAndroid.LONG)

      //in d8bid
      //this is called when app is in foregrpund
      this.displayNotification(message)
    });
  }
  displayNotification = (notification) => {
 //ToastAndroid.show("notification inintalss....",ToastAndroid.LONG)




      if (Platform.OS === 'android') {


          const localNotification = new firebase.notifications.Notification({
              sound: 'default',
              show_in_foreground: true,
          }).setNotificationId(notification._from)
          .setTitle(notification._data.result)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
              .android.setChannelId('notification_channel_name') // e.g. the id you chose above
              .android.setSmallIcon('logo') // create this icon in Android Studio
              .android.setColor('#D3D3D3') // you can set a color here
              .android.setPriority(firebase.notifications.Android.Priority.High);

              //ToastAndroid.show("notification ...",ToastAndroid.LONG)

          firebase.notifications()
              .displayNotification(localNotification)
              .catch(err => ToastAndroid.show(JSON.stringify(err),ToastAndroid.LONG));

      }
      else if (Platform.OS === 'ios') {
        console.log(notification);
       // Alert.alert("IOS 2")
        const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification._from)
            .setTitle(notification._data.result)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
            //.ios.setBadge(notification.ios.badge);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => console.error(err));

    }
  }
  display = (notification) => {

  //ToastAndroid.show(JSON.stringify(notification),ToastAndroid.LONG)



    if (Platform.OS === 'android') {
      const { title, body } = notification;
        const localNotification = new firebase.notifications.Notification({
            sound: 'default',
            show_in_foreground: true,
        }).setNotificationId(notification.notificationId)
        .setTitle(title)
        .setSubtitle(title)
        .setBody(body)
        .setData(notification.data)
            .android.setChannelId('notification_channel_name') // e.g. the id you chose above
            .android.setSmallIcon('logo') // create this icon in Android Studio
            .android.setColor('#D3D3D3') // you can set a color here
            .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase.notifications()
            .displayNotification(localNotification)
            .catch(err => ToastAndroid.show(err.message,ToastAndroid.LONG));

    }
    else if (Platform.OS === 'ios') {
      //Alert.alert("called when app is in foreground ios")
      const { title, body } = notification;
      const localNotification = new firebase.notifications.Notification()
          .setNotificationId(notification.notificationId)
          .setTitle(title)
        //.setSubtitle(title)
        .setBody(body)
        .setData(notification.data)
         // .ios.setBadge(notification.ios.badge);

      firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));

  }
}






       //1
 checkPermission(){
  // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);



       firebase.messaging().hasPermission()
       .then(enabled => {
         if (enabled) {
           // user has permissions
           this.getToken()
         } else {
           // user doesn't have permission
           this.requestPermission()
         }
       });

 }

   //3
 getToken= async () => {
  let fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
      // user has a device token
     // ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
    this.setState({token:fcmToken})
  }
  else{
   //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
  }
 }

   //2
  requestPermission(){

       firebase.messaging().requestPermission()
     .then(() => {
       // User has authorised
       this.getToken();
     })
     .catch(error => {
       // User has rejected permissions
       //ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);



      ToastAndroid.show("Permission Denied", ToastAndroid.SHORT)


     });

 }


  componentDidMount(){
    this.checkPermission()
    this.createNotificationListeners(); //add this line

  }




  render() {
    return (
      <AppNavigator
         ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
