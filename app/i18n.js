import I18n from 'react-native-i18n';
import en from './locales/en';
import fr from './locales/fr';
import hi from './locales/hi';
import th from './locales/th';
import zh from './locales/zh';
import es from './locales/es';
import ru from './locales/ru';
import de from './locales/de';
import ar from './locales/ar';
import ja from './locales/ja';

I18n.fallbacks = true;

I18n.translations = {
  en,
  fr,
  hi,
  th,
  zh,
  es,
  ru,
  de,
  ar,
  ja
};

export default I18n;
