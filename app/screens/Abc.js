import React, {Component} from 'react';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import {Text, View, Image,Dimensions,Button,ScrollView,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import { colors } from './Variables';


export default class Abc extends Component {
    constructor(props) {
        super(props);
        this.state ={
          index:7,
          data:''
         
      };


    }
 
    render() {
        return(
            <View style={{flex:1}}>
          

                <View style={{ flex: 1, marginTop: 50, alignItems: "center" }}>
                            <Button
                            title="OPEN BOTTOM SHEET"
                            onPress={() => {
                                this.RBSheet.open();
                            }}
                            />
                            <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            height={300}
                            duration={250}
                            customStyles={{
                                container: {
                                justifyContent: "center",
                                alignItems: "center"
                                }
                            }}
                            >

                            <View style={{height:'100%',justifyContent:'center',alignItems:'center'}}>
                            <ScrollPicker
                                 dataSource={[
                                      '10',
                                      '20',
                                      '30',
                                      '40',
                                      '50',
                                      '60',
                                      '70',
                                      '80',
                                      '90',
                                      '100',
                                      '110',
                                      '120',
                                      '130',
                                      '140',
                                      '150',
                                      '160',
                                      '170',
                                      '180',
                                 ]}
                                 selectedIndex={10}
                                 renderItem={(data, index, isSelected) => {
                                   
                                     //
                                 }}
                                 onValueChange={(data, selectedIndex) => {
                                     //
                                     this.setState({
                                         index:selectedIndex,
                                         data:data
                                     })
                                     //ToastAndroid.show(data,ToastAndroid.SHORT)
                                    
                                 }}
                                 wrapperHeight={100}
                                 wrapperWidth={190}
                                 wrapperBackground={'#FFFFFF'}
                                 itemHeight={50}
                                 highlightColor={'#d8d8d8'}
                                 highlightBorderWidth={4}
                                 activeItemColor={'#222121'}
                                 itemColor={'#B4B4B4'}
                               />


               
                               </View>

                                                    <TouchableOpacity
                                
                                        style={styles.submitButton}
                                    >
                                        <Text style={styles.submitText}>SELECT</Text>
                                </TouchableOpacity>
                           
                            </RBSheet>
             </View>

      </View>

          
        )
    }
}



let styles = StyleSheet.create({
    container:{
      alignItems: 'center',
      flex:1,
      padding:30,
    },
    submitButton:{
    
      marginBottom:45,
      marginTop:10,
      width:"40%",
      height:50,
      backgroundColor:colors.color_primary,
      borderRadius:10,
      borderWidth: 1,
      borderColor: 'black',
    
     
   },
   submitText:{
     color:'black',
     textAlign:'center',
     fontSize :18,
     paddingTop:10
     
   },

  indicator: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center'
   
  }
    
  
  }
  )