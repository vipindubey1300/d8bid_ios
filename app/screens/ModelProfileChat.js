import React, {Component} from 'react';
import {Text, View, Image,Modal,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,ImageBackground,Platform,ActivityIndicator,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
//import {  } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { Chip } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { Slider,Overlay ,Header} from 'react-native-elements';



export default class ModelProfileChat extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          chips:[],
          gallery:[],
          name:null,
          age:null,
          height:null,
          weight:null,
          user_image:null,
          about:null,
          gender:'',
          state:null,
          live:null,
          user_image:null,
          loading_status:false,
          redeem_modal_visible:false,
          buy_modal_visible:false,
          ethnicities:null,
          body_type:null,
          rating:null



      };


    }

    startChat(){
      var result  = this.props.navigation.getParam('result')
      var model_id = result['model_id']
      var roles = 3
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();



          formData.append('member_id', item);
          formData.append('model_id', model_id);
          formData.append('role_id', roles);

                        this.setState({loading_status:true})
                        let url = urls.base_url +'api/start_chat'
                        fetch(url, {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                    body: formData

                      }).then((response) => response.json())
                            .then((responseJson) => {
                            this.setState({loading_status:false,redeem_modal_visible:!this.state.redeem_modal_visible})

                //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                if(!responseJson.error){

                                  if(responseJson.status == '1'){
                                    //already reddemeed
                                    Platform.OS === 'android'
                                    ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    : Alert.alert(responseJson.message)

                                    this.setState({loading_status:false,redeem_modal_visible:false})

                                    result={}
                                    result["to_id"] = model_id
                                    result["image"] = this.state.user_image
                                    result["name"] = this.state.name
                                    //model chattng to member
                                    this.props.navigation.navigate("ChatBox",{result : result});


                                  }

                                  else if(responseJson.status == '2'){
                                        //reddeem successfullyyy

                                                // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                      Platform.OS === 'android'
                                      ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                      : Alert.alert(responseJson.message)

                                      // this.props.navigation.navigate("Welcome");
                                      //this.props.navigation.navigate("HomeScreen");
                                      AsyncStorage.getItem( 'coins' )
                                      .then( data => {

                                        // the string value read from AsyncStorage has been assigned to data
                                        console.log( data );
                                          var c = parseInt(data)
                                          c= c - 1

                                        //save the value to AsyncStorage again
                                        AsyncStorage.setItem( 'coins', c.toString() );

                                      }).done();



                                      // result={}
                                      // result["member_id"] = item
                                      // result["image"] = this.state.user_image
                                      // result["name"] = this.state.name
                                      // //model chattng to member
                                      // this.props.navigation.navigate("ChatBoxMember",{result : result});


                                          this.setState({loading_status:false,redeem_modal_visible:false})

                                      result={}
                                      result["to_id"] = model_id
                                      result["image"] = this.state.user_image
                                      result["name"] = this.state.name
                                      //model chattng to member
                                      this.props.navigation.navigate("ChatBox",{result : result});

                                  }



                               // this.props.navigation.navigate('HomePage');
                              }
                              else{

                                this.setState({redeem_modal_visible:false,buy_modal_visible:!this.state.buy_modal_visible})
                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(
                  'Buy Coins',
                  'You dont have any coins in your wallett.Are you sure to buy coins ?',
                  [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                  },
                  {
                    text: 'OK',
                    onPress: () => this.props.navigation.navigate("BuyCoins")
                  }
                  ],
                  {
                  cancelable: false
                  }
                );
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})

                                Platform.OS === 'android'
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

    }
//fetch = async () =>
    fetch(model_id){
      this.setState({loading_status:true})

      var formData = new FormData();


      formData.append('model_id', model_id);

                       let url = urls.base_url +'api/models_detail'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type':  'multipart/form-data',
                      },
                      body: formData

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             // var id = responseJson.result.model_detail.id
                             var name = responseJson.result.model_detail.name
                             var live = responseJson.result.model_detail.live
                             var age = responseJson.result.model_detail.age
                             var user_image = responseJson.result.model_detail.user_image
                             var about = responseJson.result.model_detail.about
                             var height = responseJson.result.model_detail.height
                             var weight = responseJson.result.model_detail.weight
                             var state = responseJson.result.model_detail.state
                             var gender = responseJson.result.model_detail.gender


                             var rev = responseJson.result.reviews
                             var stars = responseJson.result.star
                             var reviews = rev.toString().length == 0 ? 0: parseFloat(rev)

                             reviews =  parseFloat(reviews) + parseFloat(stars)
                           //  ToastAndroid.show(stars.toString(),ToastAndroid.LONG)

                             var ethnicities = responseJson.result.ethnicities[0].name
                             var body_type = responseJson.result.body_type[0].name



                             var gallery = responseJson.gallery
                             var len = gallery.length
                             var temp_arr=[]


                             if(len > 0){
                              for(var i = 0 ; i < 5 ; i++){
                                var id = responseJson.gallery[0].id
                                var profile_pic = responseJson.gallery[0].profile_pic

                                var image_one = responseJson.gallery[0].image_one
                                var image_two = responseJson.gallery[0].image_two
                                var image_three = responseJson.gallery[0].image_three
                                var image_four = responseJson.gallery[0].image_four

                                if(i == 0 && profile_pic != null ){
                                  var image = responseJson.gallery[0].profile_pic
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 1 && image_one != null){
                                  var image = responseJson.gallery[0].image_one
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 2 && image_two != null){
                                  var image = responseJson.gallery[0].image_two
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 3 && image_three != null){
                                  var image = responseJson.gallery[0].image_three
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 4 && image_four != null){
                                  var image = responseJson.gallery[0].image_four
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }



                                }
                             }

                               this.setState({gallery : temp_arr});


                             this.setState({
                              name:name,
                              about:about,
                              height:height,
                              weight:weight,
                              age:age,
                              user_image:user_image,
                              live:live,
                              state:state,
                              gender:gender,
                              ethnicities:ethnicities,
                              body_type:body_type,
                              rating:reviews


                            })

                              var interest = responseJson.result.interest_tag
                              var length = interest.length

                              for(var i = 0 ; i < length ; i++){

                                var name_temp = interest[i].name   //interest name

                                      // Create a new array based on current state:
                                      let interests = [...this.state.chips];
                                     // Add item to it
                                      interests.push( name_temp );



                                      this.setState({ chips:interests});




                                }


                            }


                          else{
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Profile not found!", ToastAndroid.SHORT)
                            : Alert.alert("Profile Not found!")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          });



    }

    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    }

    componentWillUnmount() {
       //BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);

    }



    componentDidMount() {
    //  // StatusBar.setBackgroundColor('#0040FF')
     var result  = this.props.navigation.getParam('result')
     var model_id = result['model_id']
     this.fetch(model_id)

    }
    _renderDotIndicator() {
      return <PagerDotIndicator pageCount={this.state.gallery.length} />;
    }


    render() {


      let makeGallery =this.state.gallery.map((gallery) => {
        return (


          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'#d5d5d7'

          }}>

          <Image source={{uri:urls.base_url+gallery.image}}
          resizeMode='contain'
          style={{alignSelf:'center',height:'100%',width:'100%'}} />

        </View>
        );
    })








      let chips = this.state.chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={false}
              selectedColor={'black'}
              onPress={() => {

                 {/*      let ids = [...this.state.interest_boolean];
                      ids[i] =!ids[i];
                      this.setState({interest_boolean: ids });
                      */}
              }}>
              {r}
              </Chip>
        );
})


        return (

         <View
         style={styles.container}>

         <Modal
         visible={this.state.redeem_modal_visible}
         animationType='slide'
         onRequestClose={() => {console.log('Modal has been closed.');}}
         transparent
         >
               <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>




               <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

               <View style={{position:'absolute',right:10,top:10}}>
<TouchableWithoutFeedback onPress={()=> this.setState({
  redeem_modal_visible:false
})}>
<Image source={require('../assets/error.png')} style={{alignSelf:'center',height:25,width:25}} />
</TouchableWithoutFeedback>
</View>

               <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:40,marginBottom:10}}>{I18n.t('redeem_one_coin_chat')}</Text>

               <Text style={{color:'black',fontSize:17,marginTop:10,marginBottom:10,marginLeft:10,marginRight:10}}>{I18n.t('redeem_one_coin_chat_to_lock')}</Text>
               <Image source={require('../assets/coin-small.png')} style={{height:60,width:60,marginBottom:30}} resizeMode='contain'/>
                 <TouchableOpacity
                 style={{backgroundColor:colors.color_primary,width:'100%'}}
                                onPress={() => {
                                    this.setState({redeem_modal_visible : !this.state.redeem_modal_visible})
                     this.startChat()

                       }}
                   >
                   <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>{I18n.t('redeem_now')}</Text>
                   </TouchableOpacity>
               </View>

                 </View>
       </Modal>

  {/*but coins moal*/}

  <Modal
  visible={this.state.buy_modal_visible}
  animationType='slide'
  onRequestClose={() => {console.log('Modal has been closed.');}}
  transparent
  >
        <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>
        <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

        <View style={{position:'absolute',right:10,top:10}}>
        <TouchableWithoutFeedback onPress={()=> this.setState({
          buy_modal_visible:false
        })}>
        <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:25,width:25}} />
        </TouchableWithoutFeedback>
        </View>

        <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:20,marginBottom:10}}>{I18n.t('no_coins_left')}</Text>

        <Text style={{color:'black',fontSize:17,marginTop:10,marginBottom:10,marginLeft:10,marginRight:10}}>{I18n.t('buy_coins_to_connect')}</Text>
        <Image source={require('../assets/coin-small.png')} style={{height:60,width:60,marginBottom:30}} resizeMode='contain'/>
          <TouchableOpacity
          style={{backgroundColor:colors.color_primary,width:'100%'}}
            onPress={() => {
              this.props.navigation.navigate("BuyCoins")
                  this.setState({buy_modal_visible:!this.state.buy_modal_visible})
                }}
            >
            <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>{I18n.t('buy_coins')}</Text>
            </TouchableOpacity>
        </View>

          </View>
</Modal>





         {/*for header*/}
         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>



          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
            centerComponent={{ text: I18n.t('model_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.45,width:'100%',borderBottomColor:'black',borderBottomWidth:1}}>

           <IndicatorViewPager
           style={{ height: '100%' }}
           indicator={this._renderDotIndicator()}>
          {makeGallery}
         </IndicatorViewPager>


      {/*      {
              this.state.user_image == null
              ?  <Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}}  />
              :  <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.user_image}} style={{alignSelf:'center',height:'100%',width:'100%'}}  />

            }
          */}


           </View>


        {/*middle f;oating element */}


              <View style={{flexDirection:'row',justifyContent:'space-around',width:'40%',alignSelf:'flex-end',marginLeft:'57%',marginTop:-45}}>
                  {/*  <TouchableOpacity onPress={()=> {
                      this.setState({
                        redeem_modal_visible:true
                      })
                    }}>*/}
                    <TouchableOpacity onPress={()=> {
                      this.setState({
                        redeem_modal_visible:true
                      })
                    }}>
                              <Image style={{width: 85, height: 85,margin:1}}  source={require('../assets/chat.png')} />
                    </TouchableOpacity>



              </View>


          {/*end */}

           <View style={{flex:0.5,padding:20,marginTop:15,width:'100%'}}>
           <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>

                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Text style={{fontSize:21,marginRight:7,color:'black',fontWeight:'bold'}}>{this.state.name}</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7}}>,</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:17,color:'black'}}>{this.state.age}</Text>

                                        <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />


                    </View>

                    {
                      this.state.rating == null
                      ?
                      <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
                      <Image style={{width: 25, height: 25,marginRight:10}}  source={require('../assets/star.png')} />
                      <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>0 </Text>

                      </View>
                      :
                      <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
                      <Image style={{width: 25, height: 25,marginRight:10}}  source={require('../assets/star.png')} />
                      <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>{this.state.rating} </Text>
                      </View>

                    }

                    </View>
                  <Text style={{alignSelf:'flex-start',fontSize:fonts.font_size}}>{this.state.state}</Text>

                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ? <Text style={{color:'black'}}>{I18n.t('na')}</Text>
                        : <Text style={{color:'black'}}>{this.state.about}</Text>
                      }

                      </View>


                  </View>





                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('height').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>{this.state.height} {I18n.t('cms')}</Text>
                      </View>


                  </View>

                   {/*Weight */}

                   <View style={{marginTop:35,marginLeft:20}}>
                   <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                       <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                       <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('weight').toUpperCase()}</Text>

                   </View>
                   <View style={{marginLeft:30}}>
                   <Text style={{color:'black'}}>{this.state.weight} {I18n.t('kgs')}</Text>
                   </View>


               </View>


               <View style={{marginTop:35,marginLeft:20}}>
               <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                   <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                   <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('gender').toUpperCase()}</Text>

               </View>
               <View style={{marginLeft:30}}>
               <Text style={{color:'black'}}>{this.state.gender} </Text>
               </View>


           </View>


               {/*ethniicty */}

               <View style={{marginTop:35,marginLeft:20}}>
                    <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                        <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                        <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('ethnicity').toUpperCase()}</Text>

                    </View>



                      <View style={{marginLeft:30}}>
                      {
                        this.state.ethnicities == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.ethnicities} </Text>
                      }

                      </View>

           </View>


            {/*bodytype */}


           <View style={{marginTop:35,marginLeft:20}}>
           <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
               <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
               <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('body_type').toUpperCase()}</Text>

           </View>



           <View style={{marginLeft:30}}>
           {
             this.state.body_type == null
             ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
             :  <Text style={{color:'grey'}}>{this.state.body_type} </Text>
           }

           </View>

       </View>



                  {/*interest */}
 {/*
                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/grey.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>INTEREST</Text>

                      </View>



                      <View style={{flexDirection:'row',flexWrap: 'wrap',marginLeft:30}}>
                      {chips}
                      </View>


                  </View>
                  */}

           </View>



         </View>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },

 bid_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',

},


bidButton:{


  width:"100%",
  height:50,
  alignSelf:'center',
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:colors.color_secondary,
  borderRadius:8,
  borderWidth: 1,
  borderColor: 'black',
  marginLeft:10


},
bidText:{
 color:'white',
 textAlign:'center',
 fontSize :18,
 fontWeight : 'bold'
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
