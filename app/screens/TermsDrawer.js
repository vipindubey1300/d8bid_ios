import React, { Component } from 'react';
import { View, WebView, StyleSheet,StatusBar,TouchableWithoutFeedback ,Image,Platform} from 'react-native';
import { colors,urls } from './Variables';
import I18n from '../i18n';
import AsyncStorage from '@react-native-community/async-storage';
import {Header} from 'react-native-elements';




export default class TermsDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
     url:null,
     language:1,


    };

}



  // componentDidMount(){
  //     StatusBar.setBackgroundColor('#32CD32')
  //   }

  componentWillMount(){
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
               if(item == 'en'){
                 //english
                    this.setState({language:1})
               }
               else if(item == 'hi'){
                //hindi
                   this.setState({language:1})
              }
              else if(item == 'th'){
                //thai
                   this.setState({language:1})
              }
              else if(item == 'zh'){
                //chineses
                   this.setState({language:1})
              }
              else if(item == 'de'){
                //germany
                   this.setState({language:1})
              }
              else if(item == 'ru'){
                //russian
                   this.setState({language:1})
              }
              else if(item == 'es'){
                //spanish
                   this.setState({language:1})
              }
              else if(item == 'fr'){
                //french
                   this.setState({language:1})
              }
              else if(item == 'ja'){
                //jaapanses
                   this.setState({language:1})
              }
              else if(item == 'ar'){
                //arabic
                   this.setState({language:1})
              }
            }
            else {
                 I18n.locale = 'en'
              }
    });
  }

  render() {
    return (
       <View style = {styles.container}>


       <Header

       barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

       leftComponent={
        <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomePage")}>
                  <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
            </TouchableWithoutFeedback>
       }


       rightComponent={

        <View></View>

        }


       statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

       //centerComponent={{ text: I18n.t('buy_coins'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
       outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
       containerStyle={{

         backgroundColor: colors.color_primary,
         justifyContent: 'space-around',
         alignItems:'center',
         shadowOpacity:1.7,
         shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
         elevation:7
       }}
       />


         <WebView
         source = {{ uri:
       urls.base_url+'terms-conditions.html' }}
         />
      </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      height: '100%',
   }
})
