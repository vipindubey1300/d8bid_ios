import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { RadioButton } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { colors,urls,fonts} from './Variables';
import {Header} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';



export default class ChooseLanguage extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
         
          language_id:'key0',
        
      };


    }



    isValid() {
      
      var isnum = /^\d+$/.test(this.state.phone);

      let valid = false;

      if (
          this.state.language_id != 'key0'  
         ) {
           valid = true;
           return true;
      }

   if (this.state.language_id < 0 || this.state.language_id ==="key0" || this.state.language_id == 0) {
       
          Platform.OS === 'android' 
          ?   ToastAndroid.show("Enter Language", ToastAndroid.SHORT)
          : Alert.alert("Enter Language")

        return false;
      }
    
    
     

      //return valid;
  }

    onSelect(){
   
      if (this.isValid()) {
       
        AsyncStorage.setItem('lang',this.state.language_id.toString());
        this.props.navigation.navigate("Welcome")
 
 
       }
 
 }



    componentDidMount() {
    
      
    }
   
    render() {

   


     



        return (
          <View 
         style={styles.container}>
     <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableOpacity onPress={()=> {
            const { navigation } = this.props;
            this.props.navigation.navigate("PrivacyPolicy")
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableOpacity>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
           
            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <View style={{justifyContent:'center',alignItems:'center',padding:20,flex:1,marginTop:-30}}>


         

        <Text style={styles.headerText}>CHOOSE LANGUAGE</Text>


       <Text style={styles.input_text}>Language</Text>
        <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker
          
                          mode="dropdown"
                          selectedValue={this.state.language_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              this.setState({language_id: itemValue})
                            
                            }}>
                          <Item label="Select Language" value="key0" />
                          <Item label="English" value="en" />
                          <Item label="French" value="fr" />
                          <Item label="Indian" value="hi" />
                          <Item label="Thai" value="th" />
                          <Item label="Chinese"  value="zh" />
                          <Item label="Spanish" value="es" />
                          <Item label="Russian" value="ru" />
                          <Item label="Germany" value="de" />
                          <Item label="Arabic" value="ar" />
                          <Item label="Japanese"  value="ja" />
                          
                  </Picker>
            </View>

          




          <TouchableOpacity
                 onPress={this.onSelect.bind(this)}
                 style={styles.loginButton}
               
                 underlayColor='#fff'>
                 <Text style={styles.loginText}>SELECT</Text>
         </TouchableOpacity>

       

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         
            </View> 

         

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    
    flex:1,
    backgroundColor: 'white',
    
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'09%',
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
   
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{
  
    marginTop:50,
    marginBottom:15,
    width:"100%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   paddingTop:10,
   textAlign:'center',
   color:'black'
 },
 headerText:{
    color:'black',
    fontWeight:'bold',
    fontSize:20,
    marginBottom:20
 },
 input_text:{
   width:'100%',
   marginBottom:10,
   marginLeft:7
 },
 indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}

  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}