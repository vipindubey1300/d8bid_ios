import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,StatusBar,Platform,TextInput,StyleSheet,
  ImageBackground,TouchableOpacity,ActivityIndicator,SafeAreaView,KeyboardAvoidingView} from 'react-native';
//import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { ScrollView } from 'react-native-gesture-handler';
import { RadioButton } from 'react-native-paper';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";
import TwitterAuth from 'tipsi-twitter';
import FormData from 'FormData';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckBox } from 'react-native-elements'
import firebase from 'react-native-firebase';
import { colors,urls,fonts} from './Variables';
import InstagramLogin from 'react-native-instagram-login'
import I18n from '../i18n';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';


TwitterAuth.init({
  twitter_key: "EKQbVGlRMwOTmjQAQ3NsScLPK",
  twitter_secret: "t5utUVmkDiAmqH3IfcRZRmY3NPjsUMUEo6hMLqzevgJvFl2ZaU"
})

// GoogleSignin.configure({

//   webClientId: '1028850479252-6h3lnm8eljh8dccq1fk09vna4ge3ueuj.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)

// });

GoogleSignin.configure({

  webClientId: '583308626452-gg0ch4knpp6laiea3k53h699cfda0d0l.apps.googleusercontent.com',
  // client ID of type WEB for your server (needed to verify user ID and offline access)

});




export default class SignupMember extends Component {
    constructor(props) {
        super(props);
        this.state ={
          value:'',
          interested_value:'',
            countries:[],
            states:[],
            cities:[],
            country_id:'key0',
            state_id:'key0',
            city_id:'key0',
            checked:false,
            loading_status:false,
            email:'',
            username:'',
            phone:'',
            confirm_password:'',
            password:'',
            first_checked:'',
            second_checked:'',
            token:''

        };

    }

    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }


    fetchCountries = async () =>{
      this.setState({loading_status:true})

      let url = urls.base_url +'api/api_country'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT);
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    getStates(countryId){
      this.setState({loading_status:true,states:[]})

      let url = urls.base_url +'api/state?country_id='+countryId


                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].country_id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], country_id: country_id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({states : temp_arr});

                            }


                          else{
                            ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT);
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    getCity(stateId){
      this.setState({loading_status:true,cities:[]})

      let url = urls.base_url +'api/city?state_id='+stateId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({cities: temp_arr,loading_status:false});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                            this.setState({loading_status:false})
                            ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT);
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    isValid() {

      var isnum = /^\d+$/.test(this.state.phone);

      let valid = false;

      if (this.state.username.trim().length > 0 &&
          this.state.email.trim().length > 0 &&
          this.validateEmail(this.state.email) &&
          //this.state.phone.trim().length > 0 &&
          this.state.password.trim().length > 0 &&
          this.state.password.trim().length > 7 &&
          this.state.password.trim().length < 17  &&
          this.state.confirm_password.trim().length > 0 &&
          // this.state.confirm_password.trim().length > 8 &&
          // this.state.confirm_password.trim().length < 16  &&
          this.state.country_id > 0 &&
        //   this.state.state_id > 0 &&
        //  this.state.city_id > 0 &&
          this.state.first_checked.length > 0 &&
          this.state.second_checked.length > 0 &&
          this.state.checked) {
            valid = true;
           return true;

      }

      if (this.state.username.trim().length === 0) {



        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter username', ToastAndroid.SHORT)
        : Alert.alert('Enter username')


              return false;
      }
      else if(this.state.email.trim().length === 0){

         Platform.OS === 'android'
        ?  ToastAndroid.show('Enter an email', ToastAndroid.SHORT)
        : Alert.alert('Enter an email')


        return false;
      }
      else if(!this.validateEmail(this.state.email)){

          Platform.OS === 'android'
        ?  ToastAndroid.show('Enter valid email', ToastAndroid.SHORT)
        : Alert.alert('Enter valid email')



        return false;
      }
      // else if(this.state.phone.trim().length){

      //   ToastAndroid.show('Enter phone', ToastAndroid.SHORT);
      //   return false;

      // }
      // else if(!isnum){

      //   ToastAndroid.show("Enter Valid Phone Number", ToastAndroid.SHORT);
      //   return false;
      // }

      // else if (this.state.phone.length < 10 || this.state.phone.length > 13) {

      //   ToastAndroid.show('Contact Number is not valid', ToastAndroid.SHORT);
      //   return false;
      // }
      else if (this.state.password.trim().length === 0) {

         Platform.OS === 'android'
        ?  ToastAndroid.show('Enter  password', ToastAndroid.SHORT)
        : Alert.alert('Enter  password')


        return false;
      }

      else if (this.state.password.trim().length < 8 || this.state.password.trim().length > 16) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Password should be 8-16 characters long', ToastAndroid.SHORT)
        : Alert.alert('Password should be 8-16 characters long')


        return false;
      }
      else if (this.state.confirm_password.trim().length === 0) {

             Platform.OS === 'android'
        ?  ToastAndroid.show('Enter confirm password', ToastAndroid.SHORT)
        : Alert.alert('Enter confirm password')



        return false;
      }
      else if (this.state.password.trim().toString() != this.state.confirm_password.trim().toString()) {

       Platform.OS === 'android'
        ?  ToastAndroid.show('Password and confirm password should match', ToastAndroid.SHORT)
        : Alert.alert('Password and confirm password should match')



        return false;
      }
      else if (this.state.country_id < 0 || this.state.country_id ==="key0"|| this.state.country_id == 0) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
        : Alert.alert('Enter Country')


        return false;
      }
      // else if (this.state.state_id < 0 || this.state.state_id ==="key0"|| this.state.state_id == 0) {

      //   ToastAndroid.show('Enter state', ToastAndroid.SHORT);
      //   return false;
      // }
      // else if (this.state.city_id < 0 || this.state.city_id ==="key0" || this.state.city_id == 0) {

      //   ToastAndroid.show('Enter city', ToastAndroid.SHORT);
      //   return false;
      // }
      else if (this.state.first_checked < 0 || this.state.first_checked.length === 0) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter gender', ToastAndroid.SHORT)
        : Alert.alert('Enter gender')


        return false;
      }

      else if (this.state.second_checked < 0 || this.state.second_checked.length === 0) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter Interested gender', ToastAndroid.SHORT)
        : Alert.alert('Enter Interested gender')



        return false;
      }
      else if(!this.state.checked){

        Platform.OS === 'android'
        ?  ToastAndroid.show('Accept Terms & Conditions', ToastAndroid.SHORT)
        : Alert.alert('Accept Terms & Conditions')

        return false;
      }

     return valid;
  }



    onSignup(){

      if (this.isValid()) {

       this.setState({loading_status:true})
       var formData = new FormData();

        formData.append('user_name', this.state.username);
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);
        formData.append('confirm_password', this.state.confirm_password);
        formData.append('country', this.state.country_id);
        //formData.append('city', this.state.city_id);
       //formData.append('city', 7);
      //  formData.append('state', this.state.state_id);
        formData.append('gender', this.state.first_checked);
        formData.append('interested_gender', this.state.second_checked);
        formData.append('terms', 1);
        formData.append('device_token', this.state.token);
        Platform.OS =='android'
        ?  formData.append('device_type',1)
        : formData.append('device_type',2)
        formData.append('roles_id',3);



        let url = urls.base_url +'api/api_signup'

                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false})
                    //Alert.alert(responseJson);
                    if(!responseJson.error){
                          //success in inserting data
                          //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          var id = responseJson.result.id
                          var user_name = responseJson.result.user_name
                          var roles_id = responseJson.result.roles_id
                         // AsyncStorage.setItem('uname', id);


                        //  Platform.OS === 'android'
                        //  ?   ToastAndroid.show("Account Created Sucessfully and is under review", ToastAndroid.SHORT)
                        //  : Alert.alert("Account Created Sucessfully and is under review")

                         if(roles_id === '2'){
                           //means model type
                         }
                         let obj = {
                          "id" : id,

                        } ;
                          //this.setState({username:'',password:''})
                         ///  this.props.navigation.navigate('CreateProfileMember',{result : obj});

                         this.props.navigation.navigate('OTPEmail',{result : obj});


                        }else{


                            Platform.OS === 'android'
                            ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)



                           // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                          }

                    }).catch((error) => {
                      this.setState({loading_status:false})

                      Platform.OS === 'android'
                      ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")
                    });


       }

 }


 _google = async (id,name,email) =>{



  var formData = new FormData();
  this.setState({loading_status:true})

  formData.append('google_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token',this.state.token)
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

 // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);


 let url = urls.base_url +'api/gmail_login'
  fetch(url, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type':  'multipart/form-data',
  },
  body: formData

}).then((response) => response.json())
      .then((responseJson) => {
        this.setState({loading_status:false})
        if(!responseJson.error){
        // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT)


          //success in inserting data
          Platform.OS === 'android'
          ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
          : Alert.alert(responseJson.message)


            let star = 0
            let coin = 0
        var id = responseJson.result.id
        var roles_id = responseJson.result.roles_id
        var user_name = responseJson.result.user_name
        var name = responseJson.result.name
        var user_image = responseJson.result.user_image
        var create_profile_status = responseJson.result.create_profile_status
        // var state_name = responseJson.result.state.name;
        // var country_name = responseJson.result.country.name;
        var age = responseJson.result.age;
        var country_id = responseJson.result.country_id
        var city_id = responseJson.result.city_id
        var state_id = responseJson.result.state_id
        var stars = responseJson.result.stars
        var coins = responseJson.result.coins

       // ToastAndroid.show(country_name,ToastAndroid.LONG)
        if(stars == null){
          stars = star
        }
        else{
          stars = stars
        }


        if(coins == null){
          coins = coin
        }
        else{
          coins = coins
        }

        let obj = {
         "user_id" : id,

       } ;


       let obj_role = {
         "user_id" : id,
         "roles_id":roles_id
       } ;


        if(country_id == null){
          //LocationSocial
          this.props.navigation.navigate('MemberTypeSocial',{result : obj});
        }

        else if(create_profile_status == 0){
             if(roles_id == '2'){

               this.props.navigation.navigate('CreateProfileModel',{result : obj});
             }
             else if(roles_id == '3'){

               this.props.navigation.navigate('CreateProfileMember',{result : obj});
             }
        }

        else{


         if(roles_id == '2'){
          // AsyncStorage.setItem('user_id', id.toString());
          AsyncStorage.multiSet([
            ["user_id",id.toString()],
            ["roles_id", roles_id.toString()],
             ["state",responseJson.result.state.name.toString()],
             ["user_name", name.toString()],
             ["user_image", user_image.toString()],
             ["user_age", age.toString()],
             ["stars", stars.toString()]
           ]);
           this.props.navigation.navigate('HomeModel');
          }
          else if(roles_id == '3'){
         // AsyncStorage.setItem('user_id', id.toString());
            AsyncStorage.multiSet([
              ["user_id",id.toString()],
              ["roles_id", roles_id.toString()],
              ["state", responseJson.result.country.name.toString()],
              ["user_name", name.toString()],
               ["user_image", user_image.toString()],
               ["coins", coins.toString()]
             ]);
           this.props.navigation.navigate('HomeMember');
          }

        }



  }else{

            //this.setState({error:responseJson.message,showProgress:false})
            Platform.OS === 'android'
            ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
            : Alert.alert(responseJson.message)
    }


      }).catch((error) => {
        this.setState({loading_status:false})
        Platform.OS === 'android'
        ?  ToastAndroid.show("Error", ToastAndroid.SHORT)
        : Alert.alert("Error")
      });

}


signIn = async () => {

// ToastAndroid.show("fdf",ToastAndroid.LONG)


      try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            var obj ={
            "name" : userInfo.user.name,
            "email" : userInfo.user.email,
            "id":userInfo.user.id
            }
            //ToastAndroid.show(JSON.stringify(obj),ToastAndroid.LONG)
            this._google(userInfo.user.id,userInfo.user.name,userInfo.user.email)

      } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
            Alert.alert("user cancelled the login flow")
            } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (f.e. sign in) is in progress already
            Alert.alert("f.e. sign in) is in progress already")
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated

            Alert.alert("play services not available or outdatedy")
            } else {
            // some other error happened

            ToastAndroid.show(JSON.stringify(error.code),ToastAndroid.LONG)
            }
      }
};





componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });


}


    componentDidMount() {
      // StatusBar.setBackgroundColor('#0040FF')
      this.checkPermission()
      this.fetchCountries()

     }

      //1
   checkPermission(){
    // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);



         firebase.messaging().hasPermission()
         .then(enabled => {
           if (enabled) {
             // user has permissions
             this.getToken()
           } else {
             // user doesn't have permission
             this.requestPermission()
           }
         });

   }

     //3
   getToken= async () => {
    let fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
        // user has a device token
       // ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
      this.setState({token:fcmToken})
    }
    else{
     //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
    }
   }

     //2
    requestPermission(){

         firebase.messaging().requestPermission()
       .then(() => {
         // User has authorised
         this.getToken();
       })
       .catch(error => {
         // User has rejected permissions
         //ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);


         ToastAndroid.show("Permission Denied", ToastAndroid.SHORT)



       });

   }





   _fb(){



    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      function (result) {

        if (result.isCancelled) {
          //console.log('Login cancelled')
         // ToastAndroid.show('Login cancelled ' ,ToastAndroid.LONG)

          Platform.OS === 'android'
          ?  ToastAndroid.show('Login cancelled ', ToastAndroid.SHORT)
          : Alert.alert('Login cancelled ')


        } else {
            AccessToken.getCurrentAccessToken().then(
           (data) => {
             let accessToken = data.accessToken
             // Alert.alert(accessToken.toString())

             const responseInfoCallback = (error, result) => {
               if (error) {
                Platform.OS === 'android'
                ?  ToastAndroid.show('Error ', ToastAndroid.SHORT)
                : Alert.alert('Error ')

                 //ToastAndroid.show('Error  fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
               } else {
                 //console.log(result)

                  //when success this will call
                 // ToastAndroid.show('Success fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)


                   let id = result["id"]
                   let name = result["name"]
                   let email = result["email"]
                   let first_name = result["first_name"]
                   let last_name = result["last_name"]
                   //this.loginwithfb(id,name,email)

                   var formData = new FormData();
                   this.setState({loading_status:true})

                   formData.append('first_name', first_name);
                   formData.append('last_name', last_name);
                   formData.append('email',email);
                   formData.append('fb_id',id)
                   formData.append('device_token',this.state.token)
                   Platform.OS =='android'
                   ?  formData.append('device_type',1)
                   : formData.append('device_type',2)
                   //ToastAndroid.show(JSON.stringify(formData) ,ToastAndroid.LONG)
                   let url = urls.base_url +'api/api_fb_login'

                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type':  'multipart/form-data',
                   },
                   body: formData

                 }).then((response) => response.json())
                       .then((responseJson) => {
                        this.setState({loading_status:false})
                        //ToastAndroid.showJSON.stringify((responseJson), ToastAndroid.LONG);
                       if(!responseJson.error){
                             //success in inserting data

                             Platform.OS === 'android'
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)


                           //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                           let star = 0

                           var id = responseJson.result.id
                           var roles_id = responseJson.result.roles_id
                           var user_name = responseJson.result.user_name
                           var name = responseJson.result.name
                           var user_image = responseJson.result.user_image
                           var create_profile_status = responseJson.result.create_profile_status
                           var state = responseJson.result.name;
                           var age = responseJson.result.age;
                           var country_id = responseJson.result.country_id
                           var city_id = responseJson.result.city_id
                           var state_id = responseJson.result.state_id
                           var stars = responseJson.result.stars
                           var coins = responseJson.result.coins


                           if(stars == null){
                            stars = star
                          }
                          else{
                            stars = stars
                          }



                           let obj = {
                            "user_id" : id,

                          } ;


                          // let obj_role = {
                          //   "user_id" : id,
                          //   "roles_id":roles_id
                          // } ;




                           if(country_id == null){
                             //LocationSocial
                             this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                           }

                           else if(create_profile_status == 0){
                                if(roles_id == '2'){

                                  this.props.navigation.navigate('CreateProfileModel',{result : obj});
                                }
                                else if(roles_id == '3'){

                                  this.props.navigation.navigate('CreateProfileMember',{result : obj});
                                }
                           }

                           else{

                            if(roles_id == '2'){
                              // AsyncStorage.setItem('user_id', id.toString());
                              AsyncStorage.multiSet([
                                ["user_id",id.toString()],
                                ["roles_id", roles_id.toString()],
                                 ["state",responseJson.result.state.name.toString()],
                                 ["user_name", name.toString()],
                                 ["user_image", user_image.toString()],
                                 ["user_age", age.toString()],
                                 ["stars", stars.toString()]
                               ]);
                               this.props.navigation.navigate('HomeModel');
                              }
                              else if(roles_id == '3'){
                             // AsyncStorage.setItem('user_id', id.toString());
                                AsyncStorage.multiSet([
                                  ["user_id",id.toString()],
                                  ["roles_id", roles_id.toString()],
                                  ["state", responseJson.result.country.name.toString()],
                                  ["user_name", name.toString()],
                                   ["user_image", user_image.toString()],
                                   ["coins", coins.toString()]
                                 ]);
                               this.props.navigation.navigate('HomeMember');
                              }

                           }




                     }else{

                               //this.setState({error:responseJson.message,showProgress:false})
                               Platform.OS === 'android'
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)

                       }


                       }).catch((error) => {


                        this.setState({loading_status:true})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")



                       });
               }
             }

             const infoRequest = new GraphRequest(
               '/me',
               {
                 accessToken: accessToken,
                 parameters: {
                   fields: {
                     string: 'email,name,id,first_name,last_name'
                   }
                 }
               },
               responseInfoCallback
             );

             // Start the graph request.
             let a = new GraphRequestManager().addRequest(infoRequest).start()
            // ToastAndroid.show("Result ..."+ a.toString(),ToastAndroid.LONG)

           }
         )
        }

  }.bind(this),
  function (error) {
   // console.log('Login fail with error: ' + error)
    Platform.OS === 'android'
    ?  ToastAndroid.show("Login fail with error !", ToastAndroid.SHORT)
    : Alert.alert("Login fail with error!")
    //ToastAndroid.show('Login fail with error: ' + JSON.stringify(error),ToastAndroid.LONG)
  }
  )
  }

  _twitter = async () =>{


      try{
            const result = await TwitterAuth.login()
          // ToastAndroid.show(JSON.stringify(result), ToastAndroid.LONG);
           // Alert.alert("ID",result.userId)
            //  Alert.alert("ID",result.authToken)
            //  Alert.alert("ID",result.authTokenSecret)
           // Alert.alert("ID",result.userName)

            //   this.setState({username : result.userId})


              var formData = new FormData();
              this.setState({loading_status:true})

              formData.append('twitter_id', result.userID);
              formData.append('email', result.userName);
              formData.append('name',result.userName)
              formData.append('device_token',this.state.token)
              Platform.OS =='android'
              ?  formData.append('device_type',1)
              : formData.append('device_type',2)

              let url = urls.base_url +'api/twitter_login'

              fetch(url, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type':  'multipart/form-data',
              },
              body: formData

            }).then((response) => response.json())
                  .then((responseJson) => {
                    this.setState({loading_status:false})
                    //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG)
                    if(!responseJson.error){
                      //success in inserting data
                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)


                        let star = 0
                        let coin = 0


                    var id = responseJson.result.id
                    var roles_id = responseJson.result.roles_id
                    var user_name = responseJson.result.user_name
                    var name = responseJson.result.name
                    var user_image = responseJson.result.user_image
                    var create_profile_status = responseJson.result.create_profile_status
                    var state = responseJson.result.name;
                    var age = responseJson.result.age;
                    var country_id = responseJson.result.country_id
                    var city_id = responseJson.result.city_id
                    var state_id = responseJson.result.state_id
                    var stars = responseJson.result.stars
                    var coins = responseJson.result.coins


                    if(stars == null){
                      stars = star
                    }
                    else{
                      stars = stars
                    }

                    if(coins == null){
                      coins = coin
                    }
                    else{
                      coins = coins
                    }

                    let obj = {
                     "user_id" : id,

                   } ;


                   let obj_role = {
                     "user_id" : id,
                     "roles_id":roles_id
                   } ;


                    if(country_id == null){
                      //LocationSocial
                      this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                    }

                    else if(create_profile_status == 0){
                         if(roles_id == '2'){

                           this.props.navigation.navigate('CreateProfileModel',{result : obj});
                         }
                         else if(roles_id == '3'){

                           this.props.navigation.navigate('CreateProfileMember',{result : obj});
                         }
                    }

                    else{


                      if(roles_id == '2'){
                        // AsyncStorage.setItem('user_id', id.toString());
                        AsyncStorage.multiSet([
                          ["user_id",id.toString()],
                          ["roles_id", roles_id.toString()],
                           ["state",responseJson.result.state.name.toString()],
                           ["user_name", name.toString()],
                           ["user_image", user_image.toString()],
                           ["user_age", age.toString()],
                           ["stars", stars.toString()]
                         ]);
                         this.props.navigation.navigate('HomeModel');
                        }
                        else if(roles_id == '3'){
                       // AsyncStorage.setItem('user_id', id.toString());
                          AsyncStorage.multiSet([
                            ["user_id",id.toString()],
                            ["roles_id", roles_id.toString()],
                            ["state", responseJson.result.country.name.toString()],
                            ["user_name", name.toString()],
                             ["user_image", user_image.toString()],
                             ["coins", coins.toString()]
                           ]);
                         this.props.navigation.navigate('HomeMember');
                        }

                    }



              }else{

                        //this.setState({error:responseJson.message,showProgress:false})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                        : Alert.alert(responseJson.message)
                }


                  }).catch((error) => {
                    this.setState({loading_status:false})
                  });

          }
          catch(error){
            Platform.OS === 'android'
                        ?  ToastAndroid.show("Login Cancelled !", ToastAndroid.SHORT)
                        : Alert.alert(" Login Cancelled!")
          }
  }


    instagram(token){
      fetch('https://api.instagram.com/v1/users/self/?access_token='+token, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },


      }).then((response) => response.json())
            .then((responseJson) => {
       this.setState({loading_status:false})
         //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);


                 if(responseJson.meta.code == 200){

                  var id = responseJson.data.id
                  var username = responseJson.data.username
                  var full_name = responseJson.data.full_name


                //   Platform.OS === 'android'
                // ?  ToastAndroid.show('OTP Sent successfully on your email', ToastAndroid.SHORT)
                // : Alert.alert('OTP Sent successfully on your email')

                let obj = {
                  "id" : id,
                  "username":username,
                  "full_name":full_name

                }
               this.props.navigation.navigate("InstagramEmail",{result : obj})
              }
              else{

                Platform.OS === 'android'
              ?   ToastAndroid.show("Error !", ToastAndroid.SHORT)
              : Alert.alert("Error !")
              }
            }).catch((error) => {
              this.setState({loading_status:false})

              Platform.OS === 'android'
              ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
              : Alert.alert("Connection Error !")
            });


    }

    _instagram(token){
      fetch('https://api.instagram.com/v1/users/self/?access_token='+token, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },


      }).then((response) => response.json())
            .then((responseJson) => {
       this.setState({loading_status:false})
         //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);


                 if(responseJson.meta.code == 200){

                  var id = responseJson.data.id
                  var username = responseJson.data.username
                  var full_name = responseJson.data.full_name


                //   Platform.OS === 'android'
                // ?  ToastAndroid.show('OTP Sent successfully on your email', ToastAndroid.SHORT)
                // : Alert.alert('OTP Sent successfully on your email')

                let obj = {
                  "id" : id,
                  "username":username,
                  "full_name":full_name

                }
               this.props.navigation.navigate("InstagramEmail",{result : obj})
              }
              else{

                Platform.OS === 'android'
              ?   ToastAndroid.show("Error !", ToastAndroid.SHORT)
              : Alert.alert("Error !")
              }
            }).catch((error) => {
              this.setState({loading_status:false})

              Platform.OS === 'android'
              ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
              : Alert.alert("Connection Error !")
            });


    }




    onSelect(index, value){
      this.setState({fuel_id:value})


    }

    render() {



              let makecountries =this.state.countries.map((country) => {
                return (
                  <Item label={country.name} value={country.id} key={country.id}/>
                )
            })

       {/*          let makestates =this.state.states.map((state) => {
                  return (
                    <Item label={state.name} value={state.id} key={state.id}/>
                  )
              })


                let makecities =this.state.cities.map((city) => {
                  return (
                    <Item label={city.name} value={city.id} key={city.id}/>
                  )
              })

              */}

        return (
          <SafeAreaView style={{flex:1}}>
          <KeyboardAvoidingView style={{flex:1}}
          behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>
        <ScrollView showsHorizontalScrollIndicator={false}>


        <InstagramLogin
        ref= {ref => this.instagramLogin= ref}
        clientId='557648d7e8324af6bbeefd8bb6a0bef0'
          redirectUrl='http://webmobril.com/auth/instagram/callback'
        scopes={['public_content', 'follower_list']}
        onLoginSuccess={(token) => this._instagram(token)}
        onLoginFailure={(data) => ToastAndroid.show(JSON.stringify(data),ToastAndroid.LONG)}
    />



         <View
         style={styles.container}>

        <Text style={styles.headerText}>{I18n.t('sign_up').toUpperCase()}</Text>

         <Text style={styles.input_text}>{I18n.t('app_user_name')}</Text>
          <TextInput
            value={this.state.username}
            keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
            onChangeText={(username) => this.setState({ username})}
            style={styles.input}
            placeholderTextColor={'black'}
          />

          <Text style={styles.input_text}>Email</Text>
          <TextInput
            value={this.state.email}
            keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
            onChangeText={(email) => this.setState({email : email.trim() })}
            //this will make first chararcter small
            autoCapitalize = 'none'
            style={styles.input}
            placeholderTextColor={'black'}
          />




          <Text style={styles.input_text}>{I18n.t('password')}</Text>
          <TextInput
            value={this.state.password}
            secureTextEntry={true}
            maxLength={20}
            onChangeText={(password) => this.setState({ password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />


          <Text style={styles.input_text}>{I18n.t('confirm_password')}</Text>
          <TextInput
            value={this.state.confirm_password}
            maxLength={20}
            secureTextEntry={true}
            onChangeText={(confirm_password) => this.setState({ confirm_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

       <Text style={styles.input_text}>{I18n.t('country')}</Text>
        <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker

                          mode="dropdown"
                          selectedValue={this.state.country_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              if(itemValue > 0){
                                this.setState({country_id: itemValue})

                              }
                            }}>
                          <Item label="Select Country" value="key0" />
                             {makecountries}
                  </Picker>
            </View>

            {/*

            <Text style={styles.input_text}>State</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                        <Picker

                              mode="dropdown"
                              selectedValue={this.state.state_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {
                                  if(itemIndex > 0){
                                    this.setState({state_id: itemValue})
                                    this.getCity(itemValue)
                                  }
                                }}>
                              <Item label="Select State" value="key0" />
                                {makestates}
                      </Picker>
                </View>


                <Text style={styles.input_text}>City</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                        <Picker

                              mode="dropdown"
                              selectedValue={this.state.city_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {
                                  if(itemIndex > 0){
                                    this.setState({city_id: itemValue})

                                  }
                                }}>
                              <Item label="Select City" value="key0" />
                                {makecities}
                      </Picker>
                </View>
*/}


{/** start of 21st radio */}
    <Text style={{width:'100%',marginTop:20,marginBottom:10,fontSize:fonts.font_size}}>{I18n.t('gender')}</Text>
       <View style={{width:'100%'}}>
       <RadioButton.Group
        onValueChange={value => this.setState({ value })}
        value={this.state.value}
      >

      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

        <View style={{flex:1}}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
         value="male"
         color={colors.color_primary}
         status={this.state.first_checked === 'male' ? 'checked' : 'unchecked'}
         onPress={() => { this.setState({ first_checked: 'male' }); }}/>
         <Text>{I18n.t('male')}</Text>
        </View>
        </View>


        <View  style={{flex:1}}>
           <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
           value="female"
           color={colors.color_primary}
           status={this.state.first_checked === 'female' ? 'checked' : 'unchecked'}
           onPress={() => { this.setState({ first_checked: 'female' }); }} />
           <Text>{I18n.t('female')}</Text>
        </View>
        </View>


        <View style={{flex:1}}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
         value="transgender"
         color={colors.color_primary}
         status={this.state.first_checked === 'transgender' ? 'checked' : 'unchecked'}
         onPress={() => { this.setState({ first_checked: 'transgender' }); }}/>
         <Text>{I18n.t('transgender')}</Text>
        </View>
        </View>

        </View>







      </RadioButton.Group>
       </View>

          {/** end 1st radio */}



       {/** start of 2nd radio */}
       <Text style={{width:'100%',marginTop:10,marginBottom:10,fontSize:fonts.font_size}}>{I18n.t('interested_gender')}</Text>
       <View style={{width:'100%'}}>
       <RadioButton.Group
        onValueChange={interested_value => this.setState({ interested_value })}
        value={this.state.interested_value}
      >

      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

        <View style={{flex:1}}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
         value="second_male"
         color={colors.color_primary}
         status={this.state.second_checked === 'male' ? 'checked' : 'unchecked'}
         onPress={() => { this.setState({ second_checked: 'male' }); }}/>
         <Text>{I18n.t('male')}</Text>
        </View>
        </View>


        <View  style={{flex:1,marginRight:10}}>
           <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
           value="second_female"
           color={colors.color_primary}
           status={this.state.second_checked === 'female' ? 'checked' : 'unchecked'}
           onPress={() => { this.setState({ second_checked: 'female' }); }} />
           <Text>{I18n.t('female')}</Text>
        </View>
        </View>


        <View style={{flex:1}}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton.Android
         value="transgender"
         color={colors.color_primary}
         status={this.state.second_checked === 'transgender' ? 'checked' : 'unchecked'}
         onPress={() => { this.setState({ second_checked: 'transgender' }); }}/>
         <Text>{I18n.t('transgender')}</Text>
        </View>
        </View>



        </View>



      </RadioButton.Group>
       </View>
          {/** end 2nd radio */}


          <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:-10}}>

          {/*
                <CheckBox
                  value={this.state.checked}
                  style={{color:'red'}}
                  onValueChange={() => this.setState({ checked: !this.state.checked })}
                />

                */}

                <CheckBox
                   center
                   checkedColor={colors.color_primary}
                    onPress={() => this.setState({ checked: !this.state.checked })}
                    checked={this.state.checked}
                  />
                <Text style={{marginTop: 18,marginLeft:-15,color:'#5789F4'}}
                 onPress={() => {this.props.navigation.navigate('Terms')}}>{I18n.t('accept_terms_condition')}</Text>
            </View>



          <TouchableOpacity
                onPress={this.onSignup.bind(this)}
                 style={styles.loginButton}
                 underlayColor='#fff'>
                 <Text style={styles.loginText}>{I18n.t('sign_up').toUpperCase()}</Text>
         </TouchableOpacity>

         <Text style={{fontSize:fonts.fontSize,marginBottom:10}}>{I18n.t('or_signup_with')}</Text>





         <View style={styles.socialView}>
         <View style={{flex:1}}>
              <TouchableOpacity onPress={this._fb.bind(this)}>
              <Image source={require('../assets/fb.png')} style={styles.social_image} resizeMode='contain'/>
              </TouchableOpacity>
         </View>
         <View style={{flex:1}}>
         <TouchableOpacity onPress={this._twitter.bind(this)}>
         <Image source={require('../assets/twitter.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>

         <View style={{flex:1}}>
         <TouchableOpacity onPress={()=> this.instagramLogin.show()}>
         <Image source={require('../assets/insta.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>

         <View style={{flex:1}}>
         <TouchableOpacity onPress={()=> this.signIn()}>
          <Image source={require('../assets/google.png')} style={styles.social_image} resizeMode='contain'/>
          </TouchableOpacity>
          </View>




         </View>

         <View style={{flexDirection:'row',marginTop:10}}>
          <Text onPress={()=> this.props.navigation.navigate("Login")} style={{fontSize:fonts.font_size}}>{I18n.t('already_have_account')} ? </Text>
          <Text  onPress={()=> this.props.navigation.navigate("Login")} style={{fontWeight:'bold',fontSize:fonts.font_size}}>{I18n.t('log_in')}</Text>
         </View>


         </View>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         </ScrollView>
         </KeyboardAvoidingView>
         </SafeAreaView>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    padding:20,


  },
  headerText:{
  color:'black',
  fontWeight:'bold',
  fontSize:20,
  marginBottom:20
  },
 logo_image:{
  width:'100%',
 // height:Dimensions.get('height').height * 0.2
 height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,

 },
 input: {
  width: "100%",
  height: 50,
  padding:7,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{

    marginTop:10,
    marginBottom:15,
    width:"100%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',


 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   paddingTop:10,
   textAlign:'center',
   color:'black'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',
   marginBottom:10,
   marginTop:10



 },
 social_image:{
  height:50,
  width:'100%'


},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}


}
)


// const removeEmojis = (string) => {
//   // emoji regex from the emoji-regex library
//   const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g

//   return string.replace(regex, '')
// }
// value={removeEmojis(this.state.text)}
