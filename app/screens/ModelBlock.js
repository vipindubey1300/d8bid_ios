import React, {Component} from 'react';
import {Text, View,Modal, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,Platform,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,urls } from './Variables';
import DismissKeyboard from 'dismissKeyboard';
import {  Card,Item,   Picker,} from "native-base";
import I18n from '../i18n';
import AsyncStorage from '@react-native-community/async-storage';
import {Header} from 'react-native-elements';

export default class ModelBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
         model_id:0,
      };


    }


    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });
    
    }

    componentDidMount() {
    
    
      
    }

    onBlock(){


      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

          var result  = this.props.navigation.getParam('result')
      
         
      var formData = new FormData();


      formData.append('form_id', item);
      formData.append('to_id', result['model_id']);
      formData.append('message', this.state.message);
  
  
  
  
  
                      this.setState({loading_status:true})
                      let url = urls.base_url +'api/block_chat'
  
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                  body: formData
  
                    }).then((response) => response.json())
                          .then((responseJson) => {
                     this.setState({loading_status:false})
                // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                               if(!responseJson.error){
  
                            
                                Platform.OS === 'android' 
                              ?  ToastAndroid.show('Blocked successfully ', ToastAndroid.SHORT)
                              : Alert.alert('Blocked successfully')
  
                              this.props.navigation.navigate("ChatList")
                             
                              
                            }
                            else{
  
                              Platform.OS === 'android' 
                              ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)
  
                              //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})
                            
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });




    
}
    


    render() {

   



        return (
      
         <View 
         style={styles.container}>


        


         {/*for header*/}
         <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
            centerComponent={{ text: I18n.t('block'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />

       {/*for main content*/}


           <View style={styles.body}>
          
        <Text style={{margin:10,color:'black'}}>{I18n.t('choose_block_reason')}</Text>
           <TextInput
           value={this.state.message}
           onChangeText={(message) => this.setState({ message })}
           style={styles.name_input}
           placeholderTextColor={'black'}
         />

         <TouchableOpacity
         onPress={this.onBlock.bind(this)}
               style={styles.saveButton}>
               <Text style={styles.saveText}>{I18n.t('block').toUpperCase()}</Text>
         </TouchableOpacity>
               
    
         </View>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%', 
    paddingTop: Platform.OS === 'android'  ? 0:10, 
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:25,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
 // backgroundColor:this.state.complain_status ? "#D3D3D3" : "#ffffff"
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"90%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }
 
  

}
)


{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}