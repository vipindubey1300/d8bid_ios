import React, {Component} from 'react';
import {Text, View,Modal, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors ,fonts} from './Variables';
//import {  } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";



export default class ModelBidding extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          modal_visible:false,
         
      };


    }




    componentDidMount() {
   // StatusBar.setBackgroundColor('#0040FF')
      
    }
  

    render() {
        return (
      
         <View 
         style={styles.container}>


         {/*for header*/}
          <View style = {styles.header}>

                  <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
                  </TouchableWithoutFeedback>

                  <View>
                    <Text style={{fontSize: 21,fontWeight: 'bold', color: "black",paddingRight:25}}>Bidding</Text>
                  </View>

                  <View>
                  </View>

        </View>

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <Modal
            visible={this.state.modal_visible}
            animationType='slide'
            onRequestClose={() => {console.log('Modal has been closed.');}}
            transparent
            >
                  <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>
                  <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

                  <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:10,marginBottom:10}}>Redeem 1 coin for chat</Text>

                  <Text style={{color:'black',fontSize:17,marginTop:10,marginBottom:10,marginLeft:10,marginRight:10}}>Redeem 1 coin to lock in accepted bid and recieve models private contact details</Text>
                  <Image source={require('../assets/coin-small.png')} style={{height:60,width:60,marginBottom:30}} resizeMode='contain'/>
                    <TouchableOpacity
                    style={{backgroundColor:colors.color_primary,width:'100%'}}
                      onPress={() => {
                            this.setState({modal_visible:!this.state.modal_visible})
                          }}
                      >
                      <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>Redeem now</Text>
                      </TouchableOpacity>
                  </View>
                 
                    </View>
          </Modal>



           <View style={{height:Dimensions.get('window').height * 0.65}}>
          
         

                 <Image source={require('../assets/female.png')} style={{alignSelf:'center',height:'100%',width:'100%'}} />


           </View>


           <View style={{padding:15}}>

             
               


                  <View style={{flexDirection:'row',alignItems:'center',marginBottom:20}}>
                                      <Text style={{fontSize:19,marginRight:7,color:'black'}}>Freda Jones</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black'}}>,</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:17,color:'black'}}>23</Text>
                                      
                                        <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />
                  </View>



                  <Text style={{width:'100%',marginBottom:20,marginTop:10}}>Set Bid Amount</Text>



                   <View style={{flexDirection:'row',alignItems:'center',marginBottom:20}}>

                   <View style={{flex:1.2,margin:3}}>
                   <TextInput
                      value={this.state.email}
                      onChangeText={(email) => this.setState({ email })}
                      placeholder={'Bid Amount '}
                      style={styles.bid_input}
                      placeholderTextColor={'black'}
                    />
                   </View>



                   <View style={{flex:0.8,margin:3}}>
                        <TouchableOpacity onPress={()=> this.setState({modal_visible:!this.state.modal_visible})}
                      style={styles.bidButton}
                        >
                      <Text style={styles.bidText}>BID NOW</Text>
                          </TouchableOpacity>
                   </View>


                   </View>


            

           </View>

               
    
         </View>

         </ScrollView>
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'09%',
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',

  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 bid_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
 
},
about_text:{
  width:'100%',
  marginBottom:10
 },


 
bidButton:{
    
   
     width:"100%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_secondary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  bidText:{
    color:'white',
    textAlign:'center',
    fontSize :18,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}