import React ,  {Component}from 'react';
import { Text, View, Button, StyleSheet,BackHandler,
  SafeAreaView, Dimensions, ScrollView, Image, FlatList, TouchableOpacity,ToastAndroid ,TouchableWithoutFeedback} from 'react-native';
import { colors, urls } from './Variables';
import { StackActions, NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";


  //customizing drawer items here
export default class DrawerMember extends React.Component {


  constructor(props) {
		super(props);
		this.xyz()
    this.state = {
			extra_status :false,
      name:'',
      userId:'',
      userName:'',
      state:'',
      userImage:null,
      coins:'',
      roles_id:'',
      bids_count:'0',
      chats_count:'0',
      notifications_count:'0'
    };
  }


    static navigationOptions = {
      header: null ,
    };

    // let keys = ['email', 'password'];
    // AsyncStorage.multiRemove(keys, (err) => {
    //     console.log('Local storage user info removed!');
    // });

    handleBackWithAlert = () => {
      // const parent = this.props.navigation.dangerouslyGetParent();
        // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
      // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)


                      Alert.alert(
                      'Exit App',
                      'Exiting the application?',
                      [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                      },
                      {
                        text: 'OK',
                        onPress: () => BackHandler.exitApp()
                      }
                      ],
                      {
                      cancelable: false
                      }
                    );


   // return true;

    }

remove = async() =>{
  this.props.navigation.toggleDrawer()
  //ToastAndroid.show("unmount callled " ,ToastAndroid.LONG);
  // this.backhandler = BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
  //     this.backhandler.remove()






  try {
    await AsyncStorage.removeItem("user_id");
    await AsyncStorage.removeItem("roles_id");
    await AsyncStorage.removeItem("state");
    await AsyncStorage.removeItem("user_name");
    await AsyncStorage.removeItem("user_image");
    await AsyncStorage.removeItem("coins");

    LoginManager.getInstance().logOut();

    return true;
  }
  catch(exception) {
    return false;
  }
}

  xyz = async () =>{
   // this.props.navigation.closeDrawer()
    //make sure to save every item in string in asycnncstoarage  otheriws it will not store i it
    AsyncStorage.multiGet(["user_id","roles_id","state","user_name","user_image","coins"]).then(response => {
   // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
      if(response
         && response[0][1] != null
          &&response[1][1] != null
           && response[2][1] != null
           && response[3][1] != null
           && response[4][1] != null
           && response[5][1] != null){
       this.setState({
         userId:response[0][1],
         roles_id:response[1][1],
         state: response[2][1],
         userName:response[3][1],
         userImage:urls.base_url+response[4][1],
         coins:response[5][1]
       })
      }
      else{
        this.setState({
          userId:'',
          roles_id:'',

          state: '',
          userName:'',
          userImage:null,
          coins:"0"
        })
      }


      console.log(response[0][0]) // Key1
      console.log(response[0][1]) // Value1
      console.log(response[1][0]) // Key2
      console.log(response[1][1]) // Value2
    })

  }


componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });
}

getCount(){

//ToastAndroid.show(this.state.userId, ToastAndroid.SHORT)

  var formData = new FormData();


  formData.append('user_id', this.state.userId);
  formData.append('role_id', this.state.roles_id);
  //formData.append('message', this.state.message);




                  let url =
                  urls.base_url +'api/counts?user_id='+this.state.userId+'&role_id='+this.state.roles_id

                 // ToastAndroid.show(url, ToastAndroid.SHORT)

                  fetch(url, {
                  method: 'GET',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              //body: formData

                }).then((response) => response.json())
                      .then((responseJson) => {

           // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           if(!responseJson.error){


                          //   Platform.OS === 'android'
                          // ?  ToastAndroid.show('Blocked successfully ', ToastAndroid.SHORT)
                          // : Alert.alert('Blocked successfully')

                          // this.props.navigation.navigate("ChatList")

                              var bids_count = responseJson.bids
                              var notifications_count = responseJson.notifications
                              var chats_count = responseJson.chats
                              this.setState({
                                notifications_count:notifications_count,
                                bids_count:bids_count,
                                chats_count:chats_count
                              })


                        }
                        else{



                          //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})

                        // Platform.OS === 'android'
                        // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        // : Alert.alert("Connection Error !")
                      });

}



  componentDidMount()
  {
      this.xyz()
      this.getCount()
      this.timer = setInterval(()=> this.xyz(), 5000)
      this.countTimer = setInterval(()=> this.getCount(), 5000)
  }



  componentWillUnmount(){
    //ToastAndroid.show("sdsads",ToastAndroid.LOMG)
    clearInterval(this.timer)
    clearInterval(this.countTimer)
  }


      render() {


          return (
						<SafeAreaView style={{flex:1,backgroundColor:colors.color_primary,padding:0}}>
            <ScrollView style={{flex:1,height:'100%',width:'100%',paddingTop:10,paddingBottom:10}} showsVerticalScrollIndicator={false}>
            <View style={styles.headerView}>
                  <View>
                  {
                    this.state.userImage == null
                    ? null

                    :
                    <TouchableOpacity onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('EditProfile');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'EditProfile',
              actions: [NavigationActions.navigate({ routeName: 'ProfileMember' })],
            });

            this.props.navigation.dispatch(resetAction);

            }}>
                    <Image source={{uri:this.state.userImage}}
                     style={{height:80,width:80,overflow:'hidden',borderRadius:40}} overflow={'hidden'}/>
                     </TouchableOpacity>
                  }



                  </View>


                  <TouchableOpacity onPress={() =>{

              this.props.navigation.navigate('BuyCoins');

              const resetAction = StackActions.reset({
              index: 0,
              key: 'BuyCoins',
              actions: [NavigationActions.navigate({ routeName: 'BuyCoins' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}>
                  <View style={{marginTop:30,marginRight:15,alignItems:'center'}}>
                  <Image source={require('../assets/coin-small.png')} style={{height:40,width:40,marginBottom:10}} resizeMode='contain'/>

                  <Text style={{color:'white'}}>{this.state.coins} {I18n.t('coins')}</Text>
                  </View>
                  </TouchableOpacity>


            </View>

            <View style={{flex:1,marginTop:0}}>
            <Text style={{marginTop:3,color:'white',fontSize:19,marginLeft:10}}>{this.state.userName}</Text>
            <Text style={{marginTop:3,color:'white',marginBottom:20,marginLeft:10}}>{this.state.state}</Text>



            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('HomeScreen');
              {/* key is the key of stacnavogator inside drawer */}
              {/* drawer ke andr jo h wo yaani */}
                const resetAction = StackActions.reset({
                index: 0,
                key: 'HomeScreen',
                actions: [NavigationActions.navigate({ routeName: 'HomePage' })],
              });

              this.props.navigation.dispatch(resetAction);




            }}style={styles.drawertext}>{I18n.t('home')}</Text>


   <Text onPress={() =>{
    this.setState({extra_status:false})
              this.props.navigation.navigate('ChatList');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'ChatList',
              actions: [NavigationActions.navigate({ routeName: 'ChatList' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}
            style={styles.drawertext}>{I18n.t('chats')} ({this.state.chats_count})</Text>




         {/*  <Text style={styles.drawertext}>Chats</Text>  */}



            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('Notifications');
              const resetAction = StackActions.reset({
                index: 0,
                key: 'Notifications',
                actions: [NavigationActions.navigate({ routeName: 'NotificationActivity' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}   style={styles.drawertext}>{I18n.t('notifications')} ({this.state.notifications_count})</Text>


            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('Bids');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'Bids',
              actions: [NavigationActions.navigate({ routeName: 'NotificationsListMember' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}   style={styles.drawertext}>{I18n.t('bids')} ({this.state.bids_count})</Text>




            {/*.navigate('BuyCoins');....navigate('Profile'); */}
            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('BuyCoins');
            {/* key is the key of stacnavogator inside drawer */}
            {/* drawer ke andr jo h wo yaani */}
              const resetAction = StackActions.reset({
              index: 0,
              key: 'BuyCoins',
              actions: [NavigationActions.navigate({ routeName: 'BuyCoins' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}
             style={styles.drawertext}>{I18n.t('coins')}</Text>


          {/*   <Text onPress={() =>{
              this.props.navigation.navigate("EditProfile");

            }} style={styles.drawertext}>Update Profile</Text>*/}

            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('EditProfile');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'EditProfile',
              actions: [NavigationActions.navigate({ routeName: 'ProfileMember' })],
            });

            this.props.navigation.dispatch(resetAction);

            }} style={styles.drawertext}>{I18n.t('update_profile')}</Text>

            <Text onPress={()=> {
              this.setState({extra_status:false})
              this.props.navigation.navigate("ResetPassword");
              const resetAction = StackActions.reset({
              index: 0,
              key:"ResetPassword",
              actions: [NavigationActions.navigate({ routeName:"ResetPassword" })],
            });

            this.props.navigation.dispatch(resetAction);


              //this.props.navigation.navigate("ResetPassword");

            }}style={styles.drawertext}>{I18n.t('change_password')}</Text>


            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate('ChooseLanguageDrawer');
              const resetAction = StackActions.reset({
              index: 0,
              key: 'ChooseLanguageDrawer',
              actions: [NavigationActions.navigate({ routeName: 'ChooseLanguageDrawer' })],
            });

            this.props.navigation.dispatch(resetAction);
            }}
            style={styles.drawertext}>{I18n.t('choose_language')}</Text>



            <Text onPress={() =>{
              this.setState({extra_status:false})
              this.props.navigation.navigate("Contact");
              const resetAction = StackActions.reset({
              index: 0,
              key:"Contact",
              actions: [NavigationActions.navigate({ routeName:"ContactUs" })],
            });

            this.props.navigation.dispatch(resetAction);
              //this.props.navigation.navigate("Contact");

            }}style={styles.drawertext}>{I18n.t('contact_us')}</Text>


            <Text onPress={()=> this.setState({extra_status:!this.state.extra_status})} style={styles.drawertext}>{I18n.t('legal')}</Text>

           {
             this.state.extra_status == false
             ? null
             :  <View>

             <Text onPress={()=> {
              this.props.navigation.navigate("Privacy")
              this.setState({extra_status:false})
             }} style={{marginLeft:23,color:'white',fontSize:17,marginTop:7}}>{I18n.t('privacy_policy')}</Text>
             <Text  onPress={()=> {
              this.props.navigation.navigate("Terms")
              this.setState({extra_status:false})
             }} style={{marginLeft:23,color:'white',fontSize:17,marginTop:7}}>{I18n.t('terms_and_condition')} </Text>




             </View>

           }

           <View style={{flexDirection:'row',marginTop:10,alignItems:'center',marginBottom:30}}>
            <Text onPress={()=> {
             this.remove()

              ToastAndroid.show("Logged Out Successfully !",ToastAndroid.SHORT);

              {/*
              const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Welcome' })],
              });
              this.props.navigation.dispatch(resetAction);
              */}
              this.props.navigation.navigate("Welcome")

            }} style={{marginRight:10,fontSize : 19,color:'white',marginLeft:7}}>{I18n.t('logout')}</Text>
            <Image source={require('../assets/logout.png')} style={{height:40,width:40}} resizeMode='contain'/>

            </View>


            </View>
            </ScrollView>


						</SafeAreaView>

          );
      }
  }

  const styles = StyleSheet.create({

headerView: {
  height:'15%',
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
  marginTop:10,
  marginLeft:5,
  elevation:7,
  shadowOpacity:0.3


},
drawerlayout: {
flexDirection: 'row',
justifyContent: 'flex-start',
alignItems:'center'

},
drawertext:{
fontSize : 17,
color:'white',
marginTop:11,
marginBottom:11,
marginLeft:10
},
drawerimage:{
height:35,
width:35,
marginRight:20,
marginLeft:20
},

});
