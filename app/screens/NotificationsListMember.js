import React, {Component} from 'react';
import {Text, View, Image,Dimensions,Alert,ToastAndroid,ScrollView,StatusBar,StyleSheet,FlatList,Platform,ActivityIndicator,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {Card} from 'native-base';
import { isTSEnumMember } from '@babel/types';
import AsyncStorage from '@react-native-community/async-storage';
import { colors,fonts,urls } from './Variables';
import I18n from '../i18n';
import {Header} from 'react-native-elements';
import { StackActions, NavigationActions} from 'react-navigation';

export default class NotificationsListMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          notifications:[],
          peoples:
        [
          {
            id: 1,
            name: 'Neymar Jr.',
            image:require('../assets/dummy/m1.jpg'),
            price:215,
            status:0 //0 dono button //1 accepted //2 rejected
            
          },
          {
            id: 2,
            name: 'Steve Smitth',
            image:require('../assets/dummy/m2.jpg'),
            price:342,
            status:1
            
          },
         
        

          ]
          
      };


    }

    static navigationOptions = ({ navigation }) => {
      const { params } = navigation.state;
  
      return {
        title: params ? params.screenTitle: 'Lists',
      }
    };


    Fetch(){
     

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();


          formData.append('member_id',item);
          
          //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
                       // this.setState({loading_status:true})
                        let url = urls.base_url +'api/notification_list_member'
                        fetch(url, {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },
                    body: formData

                      }).then((response) => response.json())
                            .then((responseJson) => {
                             //this.setState({loading_status:false})
                          //   ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                if(!responseJson.error){
                                  var temp_arr=[]
                                  var length = responseJson.Notifications.length.toString();
                                 
                                  for(var i = 0 ; i < length ; i++){
                                    var notificaiton_id = responseJson.Notifications[i].id
                                    var bid_price = responseJson.Notifications[i].bid_price
                                    var accept = responseJson.Notifications[i].accept
                                    var reject = responseJson.Notifications[i].reject
                                    var raise = responseJson.Notifications[i].raise
                                    var raise_amount = responseJson.Notifications[i].raise_amount


                                   // ToastAndroid.show(responseJson.Notifications[i].modeluser.id.toString(), ToastAndroid.LONG);



                                    var user_id = responseJson.Notifications[i].modeluser.id
                                   // var user_name = responseJson.Notifications[i].modeluser.user_name
                                   var user_name = responseJson.Notifications[i].modeluser.name
                                    var user_image = responseJson.Notifications[i].modeluser.user_image
                                    
                                   
                                          const array = [...temp_arr];
                                          array[i] = { ...array[i], nid: notificaiton_id };
                                          array[i] = { ...array[i], price: bid_price };
                                          array[i] = { ...array[i], accept: accept };
                                          array[i] = { ...array[i], reject:reject };
                                          array[i] = { ...array[i], raise: raise };
                                          array[i] = { ...array[i], raise_amount: raise_amount };

                                          array[i] = { ...array[i], uid:user_id};
                                          array[i] = { ...array[i], user_name:user_name };
                                          array[i] = { ...array[i], user_image: user_image };
                                         
      
                                          temp_arr = array
                                          
                                          //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                    }
                                    this.setState({ notifications : temp_arr});
                                   
                                      
                              }
                              else{

                               
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})
                              
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

      


    

}

    onFetch(){
     

        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
            var formData = new FormData();


            formData.append('member_id',item);
            
            //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
                          this.setState({loading_status:true})
                          let url = urls.base_url +'api/notification_list_member'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                               this.setState({loading_status:false})
                            //   ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                  if(!responseJson.error){
                                    var temp_arr=[]
                                    var length = responseJson.Notifications.length.toString();
                                   
                                    for(var i = 0 ; i < length ; i++){
                                      var notificaiton_id = responseJson.Notifications[i].id
                                      var bid_price = responseJson.Notifications[i].bid_price
                                      var accept = responseJson.Notifications[i].accept
                                      var reject = responseJson.Notifications[i].reject
                                      var raise = responseJson.Notifications[i].raise
                                      var raise_amount = responseJson.Notifications[i].raise_amount


                                     // ToastAndroid.show(responseJson.Notifications[i].modeluser.id.toString(), ToastAndroid.LONG);



                                      var user_id = responseJson.Notifications[i].modeluser.id
                                     // var user_name = responseJson.Notifications[i].modeluser.user_name
                                     var user_name = responseJson.Notifications[i].modeluser.name
                                      var user_image = responseJson.Notifications[i].modeluser.user_image
                                      
                                     
                                            const array = [...temp_arr];
                                            array[i] = { ...array[i], nid: notificaiton_id };
                                            array[i] = { ...array[i], price: bid_price };
                                            array[i] = { ...array[i], accept: accept };
                                            array[i] = { ...array[i], reject:reject };
                                            array[i] = { ...array[i], raise: raise };
                                            array[i] = { ...array[i], raise_amount: raise_amount };

                                            array[i] = { ...array[i], uid:user_id};
                                            array[i] = { ...array[i], user_name:user_name };
                                            array[i] = { ...array[i], user_image: user_image };
                                           
        
                                            temp_arr = array
                                            
                                            //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                      }
                                      this.setState({ notifications : temp_arr});
                                     
                                        
                                }
                                else{

                                 
                                    Platform.OS === 'android' 
                                    ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    : Alert.alert(responseJson.message)
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                  Platform.OS === 'android' 
                                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                  : Alert.alert("Connection Error !")
                              });
          }
          else{
            ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });

        


      

}


item(accept,reject,raise,item){
  if(accept == 0 && reject == 0 && raise == 0 ){
    return(
      <TouchableWithoutFeedback>
      <View style={{borderColor:'grey',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'grey',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('pending')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      );
  }
  else if(accept == 0 && reject == 0 && raise == 2 ){
    return(
      <TouchableWithoutFeedback>
      <View style={{borderColor:'orange',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'orange',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('raised')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      )

  }
  
  else if(accept == 0 && reject == 0 && raise == 1 ){
    return(
      <TouchableWithoutFeedback>
      <View style={{borderColor:'grey',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'grey',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('pending')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      )

  }
  

  else if(accept == 1){
    //means  accepted

          
    return(
      <TouchableWithoutFeedback>
      <View style={{borderColor:'green',borderRadius:8,borderWidth:2}}>
      
      <Text style={{color:'green',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('accepted')}</Text>
      
      </View>
      </TouchableWithoutFeedback>
      )

  }
  else if(reject == 1){
    return(
    <TouchableWithoutFeedback>
    <View style={{borderColor:'red',borderRadius:8,borderWidth:2}}>
    
    <Text style={{color:'red',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>{I18n.t('rejected')}</Text>
    
    </View>
    </TouchableWithoutFeedback>
    )
  }

  else if(raise == 1){
    return(
      <View style={{flexDirection:'row',justifyContent:'space-around'}}>
      <TouchableOpacity>
                <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
      </TouchableOpacity>

        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />



</View>
);
  }


  
}

    itemView(status){
      if(status === 0){

              return(
                              <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                              <TouchableOpacity>
                                        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
                              </TouchableOpacity>

                                <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />



                        </View>
              );
      }
      else if(status === 1){
        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'red',borderRadius:8,borderWidth:2}}>
        
        <Text style={{color:'red',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Protest</Text>
        
        </View>
        </TouchableWithoutFeedback>
        )
      }

      else if(status === 2){

        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'green',borderRadius:8,borderWidth:2}}>
        
        <Text style={{color:'green',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Accept</Text>
        
        </View>
        </TouchableWithoutFeedback>
        )
      }
    }

    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
                  this.props.navigation.setParams({screenTitle: I18n.t('lists')})
              }
              else {
                   I18n.locale = 'en'
                }
      });


     // StatusBar.setBackgroundColor('#0040FF')
     this.onFetch()
     this.timer = setInterval(()=> this.Fetch(), 4000)
      
    }

    componentWillUnmount(){
      clearInterval(this.timer)
    }

    


    next(member_id,raise,id,price,item){
      // if(raise == 1){
      //   let obj ={
      //     "model_id":member_id,
      //     "id":id,
      //     "price":price,
      //     "raise_amount":item.raise_amount
      //   }
      //   this.props.navigation.navigate("UpdateBid",{result:obj})
      // }
      if(raise == 2){
        let obj ={
          "model_id":member_id,
          "id":id,
          "price":price,
          "raise_amount":item.raise_amount
        }
        this.props.navigation.navigate("UpdateBid",{result:obj})
      }
      else if(item.reject == 1){
         //do nothing 
         ToastAndroid.show("this is ",ToastAndroid.LONG)
       
      }

      else if(item.accept == 0 && item.reject == 0 && item.raise == 0){
        // ToastAndroid.show("this is ",ToastAndroid.LONG)
        result={}
        result["model_id"] = member_id
        result["price"] = price
      
        this.props.navigation.navigate("ProfileDetails",{result : result});
        
      }
      else if(item.accept == 0 && item.reject == 0 && item.raise == 1){

        result={}
        result["model_id"] = member_id
        result["price"] = price
      
        this.props.navigation.navigate("ProfileDetails",{result : result});
      
        
      }
      else{
        let obj ={
          "model_id":member_id
        }
        this.props.navigation.navigate("ModelProfileChat",{result:obj})
      }
      
    }
   

    render() {

    




        return (
      
         <View 
         style={styles.container}>


           {/*for header*/}
           <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
            this.props.navigation.navigate("HomeScreen")
            const resetAction = StackActions.reset({
                index: 0,
                key: 'HomeScreen',
                actions: [NavigationActions.navigate({ routeName: 'HomePage' })],
              });
              this.props.navigation.dispatch(resetAction);
          }}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                
          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
           
        centerComponent={{ text: I18n.t('bids'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />
        
       {/*for main content*/}


           <View style={styles.body}>

           <ScrollView style={{width:'100%',flex:1}} showsVerticalScrollIndicator={false}>

           {
            this.state.notifications.length == 0
            ?
            <Image style={{width: 200, height: 200,marginTop:100,alignSelf:'center'}}  
            resizeMode='contain'
                    source={require('../assets/no_bid.png')} />
            :
            <FlatList
							  style={{marginBottom:10}}
                data={this.state.notifications}
                inverted
							  showsVerticalScrollIndicator={false}
							  scrollEnabled={false}
							  renderItem={({item}) =>

							  
                 <View style={{width:'98%',margin:3}}>
                 
									  <Card style={{width:'100%',padding:10,borderRadius:10}}>
                    <TouchableWithoutFeedback onPress={() =>this.next(item.uid,item.raise,item.nid,item.price,item)}>

                   
                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width: 50, height: 50,borderRadius:25,overflow:'hidden',marginRight:10}} source={{uri:urls.base_url+item.user_image}}  />

                      <View>
                      <Text style={{fontSize:17,color:'black',marginRight:4}}>{item.user_name}</Text>
                      <Text style={{fontSize:14,color:'grey',marginRight:4}}>{I18n.t('bid_price')} : ${item.price}</Text>
                     
                      </View>

                      </View>

                      <View style={{marginTop:0}}>

                      {
                        this.item(item.accept,item.reject,item.raise,item)

                      }
                      </View>

                      </View>
                     

                        </TouchableWithoutFeedback> 
                    </Card>
                    
                    </View>
							 
							  }
							  keyExtractor={item => item.id}
							/>
           }
           
</ScrollView>

                
          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
          }
           </View>

         
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%', 
    paddingTop: Platform.OS === 'android'  ? 0:10, 
    backgroundColor: colors.color_primary

  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    backgroundColor:'#F5F0F0',
    padding:5,
    margin:0
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
 ,
 loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}