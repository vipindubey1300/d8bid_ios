import React, { Component } from 'react';
import { View, WebView, StyleSheet,StatusBar,
  Platform,TouchableWithoutFeedback,Image,Text } from 'react-native';
import { colors,urls } from './Variables';
import I18n from '../i18n';
import {Header} from 'react-native-elements';

export default class Terms extends React.Component {

  // componentDidMount(){
  //     StatusBar.setBackgroundColor('#32CD32')
  //   }

  render() {
    return (
       <View style = {styles.container}>
           {/*for header*/}
           <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

       // centerComponent={{ text: I18n.t('model_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


         <WebView
         source = {{ uri:  urls.base_url+'terms-conditions.html'  }}
         scrollEnabled={false}
         />


      </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      height: '100%',
      flex:1,


   },
   header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary

  },
})
