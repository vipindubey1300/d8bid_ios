import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,
  ToastAndroid,TouchableWithoutFeedback,StatusBar,StyleSheet,
  ImageBackground,TouchableOpacity,TextInput,ActivityIndicator,KeyboardAvoidingView} from 'react-native';
import { colors,fonts,urls} from './Variables';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {Header} from 'react-native-elements';


export default class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          email:'',
          loading_status:false,

      };


    }


    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

    isValid() {
      const { email } = this.state;

      let valid = false;

      if (email.length > 0  && this.validateEmail(email)) {
        valid = true;
      }

      if (email.trim().length === 0) {


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Email', ToastAndroid.SHORT)
      : Alert.alert('Enter Email')

        return false;
      }
      else if(!this.validateEmail(email)){

        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter a valid Email', ToastAndroid.SHORT)
        : Alert.alert('Enter a valid Email')
        return false;
      }


      return valid;
  }


    onForgot(){

      var formData = new FormData();


    formData.append('email', this.state.email);
    if(this.isValid()){
                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/api_forgot_password'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                   this.setState({loading_status:false})
              // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                             if(!responseJson.error){


                              Platform.OS === 'android'
                            ?  ToastAndroid.show('OTP Sent successfully on your email', ToastAndroid.SHORT)
                            : Alert.alert('OTP Sent successfully on your email')

                            let obj = {
                              "email" : this.state.email,

                            }
                           this.props.navigation.navigate("EnterOtp",{result : obj})
                          }
                          else{

                            ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})

                          Platform.OS === 'android'
                          ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                          : Alert.alert("Connection Error !")
                        });


    }
}

componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });


}


    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')


    }

    render() {




        return (

          <KeyboardAvoidingView style={styles.container}  behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>

<Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableOpacity onPress={()=> {
          const { navigation } = this.props;
          navigation.pop()
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableOpacity>
        }


         rightComponent={

          <View></View>



          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <View style={{justifyContent:'center',alignItems:'center',padding:20,flex:1,marginTop:-30}}>


         <View style={{marginTop:-80,width:'100%'}}>




          <Text style={{alignSelf:'center',color:'black',fontSize:21,marginBottom:10}}>{I18n.t('forgot_password')} ?</Text>
          <Text  style={{alignSelf:'center',marginBottom:50,fontSize:fonts.font_size}}>{I18n.t('enter_registered_email')}</Text>


          <Text style={styles.input_text}>{I18n.t('email_id')}</Text>
          <TextInput
            value={this.state.email}
            onChangeText={(email) => this.setState({ email : email.trim()})}
            style={styles.input}
            autoCapitalize='none'
            placeholderTextColor={'black'}
          />



          <TouchableOpacity
          onPress={this.onForgot.bind(this)}
                style={styles.forgotButton}>
                <Text style={styles.forgotText}>{I18n.t('send_my_password')}</Text>
          </TouchableOpacity>

          </View>

          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }

</View>
         </KeyboardAvoidingView>


        )
    }
}

let styles = StyleSheet.create({
  container:{

    flex:1,



  } ,
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size
 },

 input: {
  width: "100%",
  height: 50,
 padding:7,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

forgotButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  forgotText:{
    color:'black',
    textAlign:'center',
    fontSize :19,


  },

  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
 ,
 loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}



}
)
