import React, {Component} from 'react';
import {Text, View,Alert, Image,Dimensions,ScrollView,ToastAndroid,Platform,
  StatusBar,ActivityIndicator,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { CheckBox } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';

import {Header} from 'react-native-elements';

export default class ContactUs extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         image_url:''


      };


    }







componentWillMount() {
  var result  = this.props.navigation.getParam('result')
  this.setState({
    url:result['image_url']
  })

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

}






    render() {
        return (

          <KeyboardAvoidingView
          style={styles.container}  behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>



         {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/back2.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}


         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: 'black',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


       {/*for main content*/}

          <View style={{width:'100%',flex:1,height:'100%',backgroundColor:'black'}}>

          <Image style={{height:Dimensions.get('window').height * 1,width:'100%'}}
        
          source={{uri: urls.base_url +this.state.url}} />


         </View>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


       </KeyboardAvoidingView>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
padding:6,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
 // backgroundColor:this.state.complain_status ? "#D3D3D3" : "#ffffff"
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 120,
  textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
  padding:6
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
