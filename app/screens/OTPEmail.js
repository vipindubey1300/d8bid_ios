import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,ActivityIndicator,TouchableWithoutFeedback} from 'react-native';
import { colors,fonts,urls} from './Variables';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';

export default class OTPEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          otp:'',
          loading_status:false,
          resend_counter:0
         
      };


    }


    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

    isValid() {
     

      let valid = false;
      var isnum = /^\d+$/.test(this.state.otp);

      if (this.state.otp.trim().length > 0 ) {
         valid = true;
      }

      if (this.state.otp.trim().length === 0) {
        
       // ToastAndroid.show('Enter otp', ToastAndroid.SHORT);


      Platform.OS === 'android' 
      ?  ToastAndroid.show('Enter OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter OTP')



        return false;
      }
      if(this.state.otp.toString().includes(".")){
        //ToastAndroid.show("Enter Valid Otp", ToastAndroid.SHORT);


      Platform.OS === 'android' 
      ?  ToastAndroid.show('Enter Valid OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter Valid OTP')



        return false;
      }
      
      if(!isnum){
       // ToastAndroid.show("Enter Valid Otp", ToastAndroid.SHORT);


      Platform.OS === 'android' 
      ?  ToastAndroid.show('Enter Valid OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter Valid OTP')



        return false;
      }

     
     

      return valid;
  }

  onResend(id){

    var formData = new FormData();
  
  
       formData.append('user_id', id);
  
                  this.setState({loading_status:true})
                  let url = urls.base_url +'api/resend_email'
  
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData
  
                }).then((response) => response.json())
                      .then((responseJson) => {
              this.setState({loading_status:false})
            // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                           if(!responseJson.error){
                          
                         // ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT);
                          Platform.OS === 'android' 
                          ?  ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT)
                          : Alert.alert("Successfully sent OTP to Email")
                        //  /// this.props.navigation.navigate("Welcome");
                        //   let obj = {
                        //     "email" : this.state.email,
                          
                        //   }
                        //  this.props.navigation.navigate("EnterOtp",{result : obj})
                        }
                        else{
  
                          
                          Platform.OS === 'android' 
                          ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                          : Alert.alert(responseJson.message)
                        }
                      }).catch((error) => {
                        this.setState({loading_status:false})
                        //ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
  
                        Platform.OS === 'android' 
                        ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")
                      });
  
  
  
  }



onSubmit(){

            if(this.isValid()){
              var formData = new FormData();

                  
              var result  = this.props.navigation.getParam('result')

              
              var id = result["id"]

              formData.append('otp', this.state.otp);
              formData.append('user_id', id);

              this.setState({loading_status:true})
              let url = urls.base_url +'api/email_verify'

              fetch(url, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
              },
          body: formData

            }).then((response) => response.json())
                  .then((responseJson) => {
          this.setState({loading_status:false})
        // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                       if(!responseJson.error){
                      
                     // ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT);
                      // Platform.OS === 'android' 
                      // ?  ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT)
                      // : Alert.alert("Successfully sent OTP to Email")
                    //  /// this.props.navigation.navigate("Welcome");
                    //   let obj = {
                    //     "email" : this.state.email,
                      
                    //   }

                    Platform.OS === 'android' 
                    ?   ToastAndroid.show("Account Created Sucessfully and review within in 30 minutes", ToastAndroid.SHORT)
                    : Alert.alert("Account Created Sucessfully and review within in 30 minutes")




                     this.props.navigation.navigate("Login")
                    }
                    else{

                      
                      Platform.OS === 'android' 
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)
                    }
                  }).catch((error) => {
                    this.setState({loading_status:false})
                    //ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);

                    Platform.OS === 'android' 
                    ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                    : Alert.alert("Connection Error !")
                  });

            }


}



componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

}


    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     if (this.state.loading_status) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }
      
    }

    render() {
     


        return (
      
         <View 
         style={styles.container}>



         <View style={{marginTop:-80,width:'100%'}}>
      

         {/*  
          <Text style={{alignSelf:'center',color:'black',fontSize:21,marginBottom:10}}>Forgot Password?</Text>
          <Text  style={{alignSelf:'center',marginBottom:50,fontSize:fonts.font_size}}>Enter the email address you used to sign in.</Text>
          */}

          <Text style={styles.input_text}>{I18n.t('enter_otp_email')}</Text>
          <TextInput
            value={this.state.otp}
            style={styles.input}
            maxLength={6}
            keyboardType = 'numeric'
            textContentType='telephoneNumber'
            onChangeText={ (otp) => {
              var a = otp.replace(/[^0-9.]/g, '')
              this.setState({otp:a})
            } }
            placeholderTextColor={'black'}
          />

          <Text onPress={()=>{
            var result  = this.props.navigation.getParam('result')
            var email = result["id"]
            let count = this.state.resend_counter
            if(count < 3){
              count = count + 1 
              this.setState({resend_counter:count})
              this.onResend(email);
            }
            else{
              Platform.OS == 'android' ? 
                ToastAndroid.show("OTP can be send only three times",ToastAndroid.SHORT)
                : Alert.alert("OTP can be send only three times")
            }
           
          }} style={{textDecorationStyle:'dotted',marginTop:5,marginBottom:5}}>{I18n.t('resend_otp')}</Text>

        

       
          <TouchableOpacity 
          onPress={this.onSubmit.bind(this)}
                style={styles.forgotButton}>
                <Text style={styles.forgotText}>{I18n.t('enter_otp')}</Text>
          </TouchableOpacity>

          </View>

          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
            }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    alignItems: 'center',
    flex:1,
    padding:30,
  
    
  } ,
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
  color:'black'
 },

 input: {
  width: "100%",
  height: 50,
  padding:4,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
forgotButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  forgotText:{
    color:'black',
    textAlign:'center',
    fontSize :19,
    
    
  },
  
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }
 
 
  

}
)
