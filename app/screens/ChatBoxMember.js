import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,StatusBar,ScrollView,StyleSheet,FlatList,Platform,TouchableWithoutFeedback, BackHandler,TextInput,ImageBackground,TouchableOpacity,ActivityIndicator} from 'react-native';
import { colors,fonts,urls } from './Variables';
import {Card} from 'native-base';
import { withNavigationFocus } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';


export default class ChatBoxMember extends Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          loading_status:false,
           user_image:null,
           user_name:'',
           user_id:0,
           message:''
                  };

    }

    sendMessage(){

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
  
          formData.append('form_id', item);//our id
          formData.append('to_id',this.state.user_id);
          formData.append('message', this.state.message);
      
          
                          // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/chat', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})
                  
                                   if(!responseJson.error){
                                  
                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                                  
                                   
      
                                }
                                else{
          
                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     
  
    }

   
//means this was started y model to chat 
  async componentDidMount(){
    var result  = this.props.navigation.getParam('result')
    //ToastAndroid.show("ID>>>>"+result['id'].toString(),ToastAndroid.LONG)
    this.setState({
      user_id : result['member_id'],
      user_name : result['name'],
      user_image : result['image'],
    
    })

  }


  
	next(model_id){
		if(this.state.models.length > 0){
			//var jobs = this.state.jobs
			for(var i = 0 ; i < this.state.models.length ; i++){
				if(this.state.models[i].key == model_id){
					result={}
					result["model_id"] = this.state.models[i].id
					
					this.props.navigation.navigate("ProfileDetails",{result : result});
				}
			}
		}
  }
  


  onSelect = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
   // ToastAndroid.show("Recieved data is "+ data['selected'],ToastAndroid.LONG);
    //this.setState(data);
  };

    render() {

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }

    
        return (
        
         <View 
         style={styles.container}>

         {/* headrer */}
          <View style = {{flexDirection:'row',justifyContent:'space-between',alignItems: 'center',width:'100%',height:'07%',backgroundColor:'#262626'}}>

                  <TouchableWithoutFeedback onPress={() =>this.props.navigation.goBack()}>
                     <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/back2.png')} />
                  </TouchableWithoutFeedback>

                  <View>
                  <Text style={{fontSize: 19, color: "white",paddingRight:25}}>{this.state.user_name}</Text>
                </View>

                  <View style={{height:40,width:40,borderRadius:20,borderColor:'white',borderWidth:2}}>
                  <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.user_image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:20}} />

                  </View>

            </View>

                     {/* body */}

          <ScrollView style={{width:'100%',padding:10,backgroundColor:'#F5F0F0',flex:1}}>

      
         

          <FlatList
							  style={{marginBottom:10}}
                data={this.state.chats}
                 numColumns={1}
							  showsVerticalScrollIndicator={false}
							  scrollEnabled={false}
							  renderItem={({item}) =>

							  
                 <View style={{width:'100%',margin:3}}>
                 
									  <Card style={{width:'97%',height:null,padding:5}}>
                    <TouchableWithoutFeedback>


                    
                   
                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                              <View style={{flexDirection:'row',alignItems:'center'}}>
                                   <Image style={{width: 60, height: 60,borderRadius:30,overflow:'hidden',marginRight:10}}  source={{uri:"http://webmobril.org/dev/d8bid/"+item.user_image}} />

                                    <View>

                                        <Text style={{fontSize:15,marginRight:4}}>{item.name}</Text>
                          
                                    </View>

                             </View>

                                  <View style={{alignItems:'center'}}>

                                       <Text>{item.time}</Text>
                                  </View>
                       

                      </View>
                     

                        </TouchableWithoutFeedback> 
                    </Card>
                    
                    </View>
							 
							  }
							  keyExtractor={item => item.id}
							/>
            
            </ScrollView>

            <View style={{position:'absolute',bottom:0,left:0,right:0,height:70,backgroundColor:'white',flexDirection:'row',padding:5,alignItems:'center'}}>
                  <View style={{flex:4}}>
                  <TextInput
                      value={this.state.message}
                      style={styles.input}
                      
                     
                      onChangeText={ (message) => {
                     
                        this.setState({message:message})
                      } }
                      placeholderTextColor={'black'}
             />
                  </View>

                  <View style={{flex:1}}>
                  <Image source={require('../assets/fling.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </View>


                  <View style={{flex:1}}>
                  <Image source={require('../assets/cam.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </View>


                  <View style={{flex:1}}>
                  <TouchableWithoutFeedback onPress={()=> this.sendMessage()}>
                  <Image source={require('../assets/send.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </TouchableWithoutFeedback>
                  </View>
            </View>
         </View>

       

        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
   
    flex:1,
  

    
  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,

 } ,input: {
  width: "100%",
  height: 45,
  
  borderRadius: 19,
  borderWidth: 1,
  borderColor: 'black',
  backgroundColor:'#E4E4EE'
  
},
loginButton:{
  
    margin:6,
    width:"100%",
    height:40,
    backgroundColor:'#FFC300',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  
   
 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',
   


 },
 social_image:{
  height:50, 
  width:'100%'
  
  
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
  

}
)


