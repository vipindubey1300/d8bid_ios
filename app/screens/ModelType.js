import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity} from 'react-native';
import { fonts,colors,urls } from './Variables';

export default class ModelType extends React.Component {
    constructor(props) {
        super(props);

    }




    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
      
    }

    render() {
        return (
      
         <View 
         style={styles.container}>

          <View style={{marginTop:-40,width:'100%',marginRight:10}}>

          <Text style={{alignSelf:'center',color:'black',fontSize:20,marginBottom:10}}>What type of model you are ?</Text>
          <Text  style={{alignSelf:'center',marginBottom:60,fontSize:fonts.font_size}}>Please Select Your Type</Text>

          <TouchableOpacity
           onPress={()=> this.props.navigation.navigate("CreateProfile")}
                 style={styles.modelButton}>
                 
                 <Text style={styles.modelText}>DATE</Text>
         </TouchableOpacity>

         <TouchableOpacity
         onPress={()=> this.props.navigation.navigate("CreateProfile")}
                 style={styles.memberButton}>
                 
                 <Text style={styles.memberText}>DANCER</Text>
         </TouchableOpacity>
          </View>


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    alignItems: 'center',
    flex:1,
    padding:20,
    


    
  },
 
  modelButton:{
    
     marginBottom:15,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  modelText:{
    color:colors.color_secondary,
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  memberButton:{
  
    marginTop:15,
    width:"100%",
    height:50,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.color_secondary,
    borderRadius:8,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 memberText:{
   color:colors.color_primary,
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 
  

}
)
