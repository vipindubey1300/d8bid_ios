import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,Platform,TouchableOpacity,TextInput,ActivityIndicator,Alert,TouchableWithoutFeedback} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import { colors ,fonts, urls} from './Variables';
import {Header} from 'react-native-elements'


export default class OtpChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,

          new_password:'',
          confirm_password:'',

      };

    }

    isValid() {


      let valid = false;

      if ( this.state.new_password.trim().length > 0 && this.state.confirm_password.trim().length > 0) {
        valid = true;
      }

    if(this.state.new_password.trim().length === 0){

      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter new password', ToastAndroid.SHORT)
      : Alert.alert('Enter new password')

        //ToastAndroid.show('Enter new password', ToastAndroid.SHORT);
        return false;
      }

      else if (this.state.new_password.trim().length < 8 || this.state.new_password.trim().length > 16) {

        Platform.OS === 'android'
        ?  ToastAndroid.show('Password should be 8-16 characters long', ToastAndroid.SHORT)
        : Alert.alert('Password should be 8-16 characters long')



        return false;
      }
      else if(this.state.confirm_password.trim().length === 0){

        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter confirm password', ToastAndroid.SHORT)
        : Alert.alert('Enter confirm password')


        return false;
      }
      else if (this.state.new_password.trim().toString() != this.state.confirm_password.trim().toString()) {

        Platform.OS === 'android'
        ?  ToastAndroid.show('Password and confirm password should match', ToastAndroid.SHORT)
        : Alert.alert('Password and confirm password should match')




        return false;
      }


      return valid;
  }


    onReset(){

            if(this.isValid()){



              var result  = this.props.navigation.getParam('result')
              var email = result["email"]
              var otp = result["otp"]


                  var formData = new FormData();


                  formData.append('email',email);
                  formData.append('otp', otp);
                  formData.append('password', this.state.new_password);
                  formData.append('confirm_password', this.state.confirm_password);

                                this.setState({loading_status:true})
                                let url = urls.base_url +'api/forgot_new_password'
                                fetch(url, {
                                method: 'POST',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'multipart/form-data',
                                },
                            body: formData

                              }).then((response) => response.json())
                                    .then((responseJson) => {
                            this.setState({loading_status:false})
                          // ToastAndroid.show(JSON.stringify(responseJson.error), ToastAndroid.SHORT);
                                        if(!responseJson.error){


                                        Platform.OS === 'android'
                                        ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                        : Alert.alert(responseJson.message)


                                        this.props.navigation.navigate("Login");
                                      }
                                      else{

                                        Platform.OS === 'android'
                                        ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                        : Alert.alert(responseJson.message)
                                      }
                                    }).catch((error) => {
                                      this.setState({loading_status:false})
                                      Platform.OS === 'android'
                                      ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                                      : Alert.alert("Connection Error!")
                                    });





            }

}

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')

    }

    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });


    }

    render() {




        return (

         <View
         style={styles.container}>





         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableOpacity onPress={()=> {
            const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableOpacity>
        }


         rightComponent={

          <View></View>



          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />


     <View style={{padding:20}}>


          <Text  style={{alignSelf:'center',marginBottom:30}}>{I18n.t('please_change_your_password')}</Text>




         <Text style={styles.input_text}>{I18n.t('new_password')}</Text>
          <TextInput
            value={this.state.new_password}
            secureTextEntry={true}
            onChangeText={(new_password) => this.setState({ new_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />


          <Text style={styles.input_text}>{I18n.t('confirm_password')}</Text>
          <TextInput
            value={this.state.confirm_password}
            onChangeText={(confirm_password) => this.setState({ confirm_password })}
            style={styles.input}
            secureTextEntry={true}
            placeholderTextColor={'black'}
          />

          <TouchableOpacity
          onPress={this.onReset.bind(this)}
                style={styles.resetButton}>
                <Text style={styles.resetText}>{I18n.t('change')}</Text>
          </TouchableOpacity>

          </View>


          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{

    flex:1,



  } ,
 input_text:{
  width:'100%',
  marginBottom:10
 },

 input: {
  width: "100%",
  height: 50,
  padding:6,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

  resetButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  resetText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  logo_image:{
    width:'100%',
    //height:Dimensions.get('window').height * 0.2
    height:'30%'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }




}
)
