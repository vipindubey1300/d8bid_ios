import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,
  Platform,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,
  TouchableWithoutFeedback,ScrollView,Alert,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import I18n from '../i18n';
import {Header} from 'react-native-elements';




export default class EditProfileMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         name:'',
         about:'',
         image:null,
          countries:[],
          country_id:'key0',
          user_id:'',
          photo:null,
          PhotoSource:null,
      };


    }


    fetchCountries = async () =>{
      this.setState({loading_status:true})

                      let url = urls.base_url +'api/api_country'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT);
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     this.fetchCountries()
     var result  = this.props.navigation.getParam('result')

     this.setState({
       name : result["name"],
       about : result["about"],
       image: result["image"],
       user_id:result['user_id'],
       country_id:result['country_id']

     })
    // ToastAndroid.show(JSON.stringify(result),ToastAndroid.LONG)

    }
    componentWillMount(){
      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });
    }


    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
         //  ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };


          this.setState({
            PhotoSource: source ,
            imageData: response.data,
            photo:photo,

          });


          {/*photo is a file object to send to parameters */}
        }
      });
   }




    isValid() {


     // let bid_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.bid_price.toString())
      let valid = false;

      if (
          this.state.name.trim().length > 0
         /// this.state.interest_boolean.includes(true)
          //this.state.interest_boolean.filter(Boolean).length > 0
           ) {
        valid = true;
      }

    if(this.state.name.trim().length === 0){

        //ToastAndroid.show('Enter name', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Name', ToastAndroid.SHORT)
      : Alert.alert('Enter name')



        return false;
      }
      else if (this.state.country_id < 0 || this.state.country_id ==="key0" || this.state.country_id == 0) {



        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
        : Alert.alert('Enter Country')


        return false;
      }


      return valid;
  }



    onCreate(){

      if (this.isValid()) {

        //making paramters for the interest tag
        // var length = this.state.interest_boolean.length;
        // var final= "";
        // for(var i = 0 ; i < length ; i++){
        //   if(this.state.interest_boolean[i] === false){
        //    //do nothing

        //   }
        //   else{
        //     let temp = this.state.interest_id[i].toString()
        //     final = final + temp
        //     final = final + ","
        //   }
        // }
        // var final_interest = final.substring(0, final.length-1);
        //ToastAndroid.show("ToastItems"+final, ToastAndroid.SHORT);
        var arr = [this.state.photoone,this.state.phototwo,this.state.photothree,this.state.photofour]

       this.setState({loading_status:true})
       var formData = new FormData();


       var result  = this.props.navigation.getParam('result')

       //ToastAndroid.show("ToastItems//////........"+this.state.user_id, ToastAndroid.SHORT)


        formData.append('user_id',result["user_id"]);
        formData.append('display_name', this.state.name);
        formData.append('profile_pic', this.state.photo);
        formData.append('country', this.state.country_id);

        if(this.state.about !=null){
          formData.append('about', this.state.about);
        }

          //  ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);
          // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);

            let url = urls.base_url +'api/update_profile'

                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false})
                    //Alert.alert(responseJson);

                  // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                    if(!responseJson.error){

                          //success in inserting data
                          ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          // var id = responseJson.result.id
                          // var roles_id = responseJson.result.roles_id
                          // var state = responseJson.result.state.name;
                          // var name = responseJson.result.name
                          // var user_image = responseJson.result.user_image
                          // var age = responseJson.result.age



                           //  this.props.navigation.navigate('HomeModel');
                           var id = responseJson.result.id
                           var roles_id = responseJson.result.roles_id
                           var country = responseJson.result.country.name;
                           var name = responseJson.result.name
                         //  var city = responseJson.result.city.name;
                           var user_image = responseJson.result.user_image
                           var coins = responseJson.result.coins == null ? 0 : responseJson.result.coins
                           var abc = 0



                           AsyncStorage.multiSet([
                             ['user_id',id.toString()],
                             ['roles_id', roles_id.toString()],
                             ["state", country],
                             ["user_name", name],
                             ["user_image", user_image],
                             ["coins", coins.toString()]

                           ]);
                           //ToastAndroid.show("saved all", ToastAndroid.LONG);
                           this.props.navigation.navigate('EditProfile');
                           const resetAction = StackActions.reset({
                           index: 0,
                           key: 'EditProfile',
                           actions: [NavigationActions.navigate({ routeName: 'ProfileMember' })],
                         });

                          this.props.navigation.dispatch(resetAction);



                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            //this.setState({error:responseJson.message})
                            ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                          }

                    }).catch((error) => {
                      console.error(error);
                      //this.props.navigation.navigate("NoNetwork")
                      // Platform.OS === 'android'
                      // ?   ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT)
                      // : Alert.alert("Connection Error !")


                      Platform.OS === 'android'
                      ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")


                      return;
                    });


       }

 }

 check(){

  if(this.state.PhotoSource === null && this.state.image == null){
    return (<Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}}  />)
  }
  else if(this.state.PhotoSource === null &&  this.state.image != null){
    return (<Image source={{uri:urls.base_url+this.state.image}}
    resizeMode='contain'
    style={{alignSelf:'center',height:'100%',width:'100%'}}  />)
  }
  else{
      return (<Image source={this.state.PhotoSource}  
       resizeMode='contain'
      style={{alignSelf:'center',height:'100%',width:'100%'}} />)
  }


}




    render() {

      let makecountries =this.state.countries.map((country) => {
        return (
          <Item label={country.name} value={country.id} key={country.id}/>
        )
    })


        return (

          <KeyboardAvoidingView style={styles.container}  
          behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>



                  {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('edit_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />

       {/*for main content*/}

       <ScrollView style={{width:'100%',flex:1,height:'100%',backgroundColor:'white'}}>
           <View style={styles.body}>


           <View style={{height:Dimensions.get('window').height * 0.5,width:'100%',
           borderBottomColor:'black',borderBottomWidth:1,backgroundColor:'grey'}}>

           {/*
           {
                 this.state.image == null
                 ?  <Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}}  />
                 :  <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.image}} style={{alignSelf:'center',height:'100%',width:'100%'}}  />
               }

               */}

               {
                 this.check()
               }
                </View>

                <TouchableWithoutFeedback onPress={this.selectPhotoTapped.bind(this)}>
                <Image style={{width: 45, height: 45,marginTop:-60,alignSelf:'flex-end',marginBottom:20,marginRight:20}}  source={require('../assets/pen.png')} />
                </TouchableWithoutFeedback>

          <View style={{width:'100%',height:'100%',padding:20}}>
               <Text style={styles.name_text}>{I18n.t('name')}</Text>
                <TextInput
                  value={this.state.name}
                  onChangeText={(name) => this.setState({ name })}
                  style={styles.name_input}

                  placeholderTextColor={'black'}
                />


                <Text style={{marginBottom:6}}>{I18n.t('country')}</Text>
                <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                            <Picker

                                  mode="dropdown"
                                  selectedValue={this.state.country_id}
                                  onValueChange={(itemValue, itemIndex) =>
                                    {
                                      if(itemIndex > 0){
                                        this.setState({country_id: itemValue})

                                      }
                                    }}>
                                  <Item label="Select Country" value="key0" />
                                     {makecountries}
                          </Picker>
                    </View>

                   <Text style={styles.about_text}>{I18n.t('about_bio')}</Text>
                <TextInput
                  value={this.state.about}
                  onChangeText={(about) => this.setState({about })}
                  style={styles.about_input}
                  multiline={true}
                  placeholderTextColor={'black'}
                />



                <TouchableOpacity
                onPress={()=> this.onCreate()}
                      style={styles.saveButton}>
                      <Text style={styles.saveText}>{I18n.t('save').toUpperCase()}</Text>
                </TouchableOpacity>
     </View>
    </View>

    </ScrollView>

    {this.state.loading_status &&
      <View pointerEvents="none" style={styles.loading}>
        <ActivityIndicator size='large' />
      </View>
  }


         </KeyboardAvoidingView>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:0,

  },
 name_text:{
  width:'100%',
 marginBottom:7,
  fontSize:fonts.font_size

 },

 name_input: {
  width: "100%",
  height: 50,
 borderRadius:8,
 borderColor:'black',
 borderWidth:1,
padding:6,
marginBottom: 15,

},
about_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
 },

 about_input: {
  width: "100%",
  height: 120,
  padding:3,
  textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,

},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',

    fontSize :20,
    fontWeight : 'bold'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }




}
)
