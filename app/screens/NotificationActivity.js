import React, {Component} from 'react';
import {Text, View, Alert,Image,Dimensions,ToastAndroid,StatusBar,ScrollView,StyleSheet,FlatList,TouchableOpacity,TextInput,TouchableWithoutFeedback,Platform,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {Card} from 'native-base';
import { isTSEnumMember } from '@babel/types';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import { colors,fonts,urls } from './Variables';
import {Header} from 'react-native-elements';
import { StackActions, NavigationActions} from 'react-navigation';


export default class NotificationActivity extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:true,
          activities: [],

      };


    }
    // static navigationOptions = ({ navigation }) => {
    //   const { params } = navigation.state;

    //   return {
    //     title: params ? params.screenTitle: 'Activity',
    //   }
    // };

    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
                  this.props.navigation.setParams({screenTitle: I18n.t('activity')})
              }
              else {
                   I18n.locale = 'en'
                }
      });



    }


    onFetch(){


      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {


                        this.setState({loading_status:true})
                        let url = urls.base_url +'api/activity_list?user_id='+item
                        fetch(url, {
                        method: 'GET',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },


                      }).then((response) => response.json())
                            .then((responseJson) => {
                             this.setState({loading_status:false})
                            //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                          //  console.log(responseJson)
                                if(!responseJson.error){
                                  var temp_arr=[]
                                  var length = responseJson.User.length.toString();

                                  for(var i = 0 ; i < length ; i++){
                                    var chat_id = responseJson.User[i].id

                                    var updated_at = responseJson.User[i].updated_at
                                    var time = updated_at.substring(10, 16);

                                    var message = responseJson.User[i].message
                                    var user_id = responseJson.User[i].doneby[0].id
                                   // ToastAndroid.show(user_id.toString(), ToastAndroid.LONG);
                                    //var name = responseJson.User[i].doneby[0].user_name
                                    var name = responseJson.User[i].doneby[0].name
                                    var user_image = responseJson.User[i].doneby[0].user_image


                                          const array = [...temp_arr];
                                          array[i] = { ...array[i], chat_id: chat_id };
                                          array[i] = { ...array[i], time: time };
                                          array[i] = { ...array[i], message:message };
                                          array[i] = { ...array[i], id:user_id};
                                          array[i] = { ...array[i], name:name };
                                          array[i] = { ...array[i], user_image: user_image };


                                          temp_arr = array

                                          //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                    }
                                    this.setState({ activities : temp_arr});


                              }
                              else{


                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show("No Notifications Found", ToastAndroid.SHORT)
                                  : Alert.alert("No Notifications Found")
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})

                                Platform.OS === 'android'
                                ?   ToastAndroid.show('Error', ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });






}





    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     this.onFetch()

    }


    itemView(status){
      if(status === 0){

              return(
                              <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                              <TouchableOpacity>
                                        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
                              </TouchableOpacity>

                                <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />



                        </View>
              );
      }
      else if(status === 1){
        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'red',borderRadius:8,borderWidth:2}}>

        <Text style={{color:'red',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Protest</Text>

        </View>
        </TouchableWithoutFeedback>
        )
      }

      else if(status === 2){

        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'green',borderRadius:8,borderWidth:2}}>

        <Text style={{color:'green',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Accept</Text>

        </View>
        </TouchableWithoutFeedback>
        )
      }
    }



    render() {





        return (

         <View
         style={styles.container}>

   {/*for header*/}

   <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
            this.props.navigation.navigate("HomeScreen")
            const resetAction = StackActions.reset({
                index: 0,
                key: 'HomeScreen',
                actions: [NavigationActions.navigate({ routeName: 'HomePage' })],
              });
              this.props.navigation.dispatch(resetAction);
          }}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('notifications'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />

       {/*for main content*/}


           <View style={styles.body}>
           <ScrollView style={{width:'100%',flex:1}} showsVerticalScrollIndicator={false}>


           {
            this.state.activities.length == 0
            ?
            <Image style={{width: 200, height: 200,marginTop:100,alignSelf:'center'}}
            resizeMode='contain'
                    source={require('../assets/no_notification.png')} />
            :

            <FlatList
							  style={{marginBottom:10}}
                data={this.state.activities}
							  showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                inverted
							  renderItem={({item}) =>


                 <View style={{width:'98%',margin:3}}>

									  <Card style={{width:'100%',padding:10,borderRadius:10}}>
                    <TouchableWithoutFeedback>




                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width: 50, height: 50,borderRadius:25,overflow:'hidden',marginRight:10}}  source={{uri:urls.base_url+item.user_image}} />

                      <View>
                      <Text style={{fontSize:16,color:'black',marginRight:4}}>{item.name}</Text>
                      <Text style={{fontSize:13,marginLeft:4,marginRight:4}}>{item.message}</Text>
                      </View>

                      </View>

                                  <View style={{alignItems:'center'}}>

                                 <Text>{item.time}</Text>
                                  </View>


                      </View>


                        </TouchableWithoutFeedback>
                    </Card>

                    </View>

							  }
							  keyExtractor={item => item.id}
							/>

           }



</ScrollView>


           </View>

           {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    backgroundColor:'#F5F0F0',
    padding:5,
    margin:0
  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  }
  ,
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
