import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,Platform,StyleSheet,
  TextInput,ImageBackground,TouchableOpacity,ActivityIndicator,Alert,KeyboardAvoidingView} from 'react-native';
import { colors,urls,fonts} from './Variables';
import FormData from 'FormData';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";
import TwitterAuth from 'tipsi-twitter';
import InstagramLogin from 'react-native-instagram-login'
import firebase from 'react-native-firebase';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import { thisExpression } from '@babel/types';
import I18n from '../i18n';
import { StackActions, NavigationActions,DrawerActions} from 'react-navigation';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';

// TwitterAuth.init({
//   twitter_key: "H6skhaSBvWYwAbc35lUit4p57",
//   twitter_secret: "pxXQAshaHqJZNqRWOWbsipyF8PpfTxPVqjltHxqAcTR9Oeg5Sw"
// })



TwitterAuth.init({
  twitter_key: "H6skhaSBvWYwAbc35lUit4p57",
  twitter_secret: "pxXQAshaHqJZNqRWOWbsipyF8PpfTxPVqjltHxqAcTR9Oeg5Sw"
})

GoogleSignin.configure({

  webClientId: '583308626452-gg0ch4knpp6laiea3k53h699cfda0d0l.apps.googleusercontent.com',
  // client ID of type WEB for your server (needed to verify user ID and offline access)

});

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state ={
            email:'',
            password:'',
            loading_status:false,
            token:''

        };

    }



    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }


  _google = async (id,name,email) =>{



            var formData = new FormData();
            this.setState({loading_status:true})

            formData.append('google_id', id);
            formData.append('email', email);
            formData.append('name',name)
            formData.append('device_token',this.state.token)
            Platform.OS =='android'
            ?  formData.append('device_type',1)
            : formData.append('device_type',2)

           // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);


           let url = urls.base_url +'api/gmail_login'
            fetch(url, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type':  'multipart/form-data',
            },
            body: formData

          }).then((response) => response.json())
                .then((responseJson) => {
                  this.setState({loading_status:false})
                  if(!responseJson.error){
                  // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT)


                    //success in inserting data
                    Platform.OS === 'android'
                    ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                    : Alert.alert(responseJson.message)


                      let star = 0
                      let coin = 0
                       var id = responseJson.result.id
                      var roles_id = responseJson.result.roles_id
                      var user_name = responseJson.result.user_name
                      var name = responseJson.result.name
                       var user_image = responseJson.result.user_image
                      var create_profile_status = responseJson.result.create_profile_status
                  // var state_name = responseJson.result.state.name;
                  // var country_name = responseJson.result.country.name;
                      var age = responseJson.result.age;
                      var country_id = responseJson.result.country_id
                  var city_id = responseJson.result.city_id
                  var state_id = responseJson.result.state_id
                  var stars = responseJson.result.stars
                  var coins = responseJson.result.coins

                 // ToastAndroid.show(country_name,ToastAndroid.LONG)
                  if(stars == null){
                    stars = star
                  }
                  else{
                    stars = stars
                  }


                  if(coins == null){
                    coins = coin
                  }
                  else{
                    coins = coins
                  }

                  let obj = {
                   "user_id" : id,

                 } ;


                 let obj_role = {
                   "user_id" : id,
                   "roles_id":roles_id
                 } ;


                  if(country_id == null){
                    //LocationSocial
                    this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                  }

                  else if(create_profile_status == 0){
                       if(roles_id == '2'){

                         this.props.navigation.navigate('CreateProfileModel',{result : obj});
                       }
                       else if(roles_id == '3'){

                         this.props.navigation.navigate('CreateProfileMember',{result : obj});
                       }
                  }

                  else{


                   if(roles_id == '2'){
                    // AsyncStorage.setItem('user_id', id.toString());
                    AsyncStorage.multiSet([
                      ["user_id",id.toString()],
                      ["roles_id", roles_id.toString()],
                       ["state",responseJson.result.state.name.toString()],
                       ["user_name", name.toString()],
                       ["user_image", user_image.toString()],
                       ["user_age", age.toString()],
                       ["stars", stars.toString()]
                     ]);
                     this.props.navigation.navigate('HomeModel');
                    }
                    else if(roles_id == '3'){
                   // AsyncStorage.setItem('user_id', id.toString());
                      AsyncStorage.multiSet([
                        ["user_id",id.toString()],
                        ["roles_id", roles_id.toString()],
                        ["state", responseJson.result.country.name.toString()],
                        ["user_name", name.toString()],
                         ["user_image", user_image.toString()],
                         ["coins", coins.toString()]
                       ]);
                     this.props.navigation.navigate('HomeMember');
                    }

                  }



            }else{

                      //this.setState({error:responseJson.message,showProgress:false})
                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)
              }


                }).catch((error) => {
                  this.setState({loading_status:false})
                  Platform.OS === 'android'
                  ?  ToastAndroid.show("Error", ToastAndroid.SHORT)
                  : Alert.alert("Error")
                });

}


signIn = async () => {

 // ToastAndroid.show("fdf",ToastAndroid.LONG)


  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    var obj ={
      "name" : userInfo.user.name,
      "email" : userInfo.user.email,
      "id":userInfo.user.id
    }
    console.log("GMAIL...",JSON.stringify(obj))
      //ToastAndroid.show(JSON.stringify(obj),ToastAndroid.LONG)
      this._google(userInfo.user.id,userInfo.user.name,userInfo.user.email)

  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        Alert.alert("user cancelled the login flow")
    } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        Alert.alert("f.e. sign in) is in progress already")
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated

        Alert.alert("play services not available or outdatedy")
    } else {
        // some other error happened

        ToastAndroid.show(JSON.stringify(error.code),ToastAndroid.LONG)
    }
  }
};


  componentWillMount() {

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });

  }

  componentDidMount() {
    // StatusBar.setBackgroundColor('#0040FF')
    this.checkPermission()

   }

    //1
 checkPermission(){
  // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);



       firebase.messaging().hasPermission()
       .then(enabled => {
         if (enabled) {
           // user has permissions
           this.getToken()
         } else {
           // user doesn't have permission
           this.requestPermission()
         }
       });

 }

   //3
 getToken= async () => {
  let fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
      // user has a device token
     // ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
    this.setState({token:fcmToken})
  }
  else{
   //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
  }
 }

   //2
  requestPermission(){

       firebase.messaging().requestPermission()
     .then(() => {
       // User has authorised
       this.getToken();
     })
     .catch(error => {
       // User has rejected permissions
       //ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);

       console.log("HHH",error.message)

         ToastAndroid.show("Permission Denied", ToastAndroid.SHORT)



     });

 }



    isValid() {
      const { email, password } = this.state;

      let valid = false;

      if (email.length > 0 && password.length > 0 && this.validateEmail(email)) {
        valid = true;
      }

      if (email.trim().length === 0) {

        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter an email', ToastAndroid.SHORT)
        : Alert.alert('Enter an email')


        return false;
      }
      else if(!this.validateEmail(email)){

        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter valid email', ToastAndroid.SHORT)
        : Alert.alert('Enter valid email')
        return false;
      }
      else if (password.trim().length === 0) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter password', ToastAndroid.SHORT)
        : Alert.alert('Enter password')


        return false;
      }

      return valid;
  }


  _fb(){



    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      function (result) {

        if (result.isCancelled) {
          //console.log('Login cancelled')
         // ToastAndroid.show('Login cancelled ' ,ToastAndroid.LONG)

          Platform.OS === 'android'
          ?  ToastAndroid.show('Login cancelled ', ToastAndroid.SHORT)
          : Alert.alert('Login cancelled ')


        } else {
            AccessToken.getCurrentAccessToken().then(
           (data) => {
             let accessToken = data.accessToken
             // Alert.alert(accessToken.toString())

             const responseInfoCallback = (error, result) => {
               if (error) {
                Platform.OS === 'android'
                ?  ToastAndroid.show('Error ', ToastAndroid.SHORT)
                : Alert.alert('Error ')

                 //ToastAndroid.show('Error  fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
               } else {
                 //console.log(result)

                  //when success this will call
                 // ToastAndroid.show('Success fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)


                   let id = result["id"]
                   let name = result["name"]
                   let email = result["email"]
                   let first_name = result["first_name"]
                   let last_name = result["last_name"]
                   //this.loginwithfb(id,name,email)

                   var formData = new FormData();
                   this.setState({loading_status:true})

                   formData.append('first_name', first_name);
                   formData.append('last_name', last_name);
                   formData.append('email',email);
                   formData.append('fb_id',id)
                   formData.append('device_token',this.state.token)
                   Platform.OS =='android'
                   ?  formData.append('device_type',1)
                   : formData.append('device_type',2)
                  // ToastAndroid.show(JSON.stringify(formData) ,ToastAndroid.LONG)
                   let url = urls.base_url +'api/api_fb_login'
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type':  'multipart/form-data',
                   },
                   body: formData

                 }).then((response) => response.json())
                       .then((responseJson) => {
                        this.setState({loading_status:false})
                      //  ToastAndroid.showJSON.stringify((responseJson), ToastAndroid.LONG);
                       if(!responseJson.error){
                        //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                             //success in inserting data

                             Platform.OS === 'android'
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)


                           //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                           let star = 0
                           let coin = 0

                           var id = responseJson.result.id
                           var roles_id = responseJson.result.roles_id
                           var user_name = responseJson.result.user_name
                           var name = responseJson.result.name
                           var user_image = responseJson.result.user_image
                           var create_profile_status = responseJson.result.create_profile_status
                          //  var state_name = responseJson.result.state.name;
                          //  var country_name = responseJson.result.country.name;
                           var age = responseJson.result.age;
                           var country_id = responseJson.result.country_id
                           var city_id = responseJson.result.city_id
                           var state_id = responseJson.result.state_id
                           var stars = responseJson.result.stars
                           var coins = responseJson.result.coins


                           if(stars == null){
                            stars = star
                          }
                          else{
                            stars = stars
                          }

                          if(coins == null){
                            coins = coin
                          }
                          else{
                            coins = coins
                          }



                           let obj = {
                            "user_id" : id,

                          };


                          // let obj_role = {
                          //   "user_id" : id,
                          //   "roles_id":roles_id
                          // } ;




                           if(country_id == null){
                             //LocationSocial
                             this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                           }

                           else if(create_profile_status == 0){
                                if(roles_id == '2'){

                                  this.props.navigation.navigate('CreateProfileModel',{result : obj});
                                }
                                else if(roles_id == '3'){

                                  this.props.navigation.navigate('CreateProfileMember',{result : obj});
                                }
                           }

                           else{

                            if(roles_id == '2'){
                              // AsyncStorage.setItem('user_id', id.toString());
                              AsyncStorage.multiSet([
                                ["user_id",id.toString()],
                                ["roles_id", roles_id.toString()],
                                 ["state",responseJson.result.state.name.toString()],
                                 ["user_name", name.toString()],
                                 ["user_image", user_image.toString()],
                                 ["user_age", age.toString()],
                                 ["stars", stars.toString()]
                               ]);

              //                  this.props
              //  .navigation
              //  .dispatch(StackActions.reset(
              //    {
              //       index: 0,
              //       actions: [
              //         NavigationActions.navigate({ routeName: 'HomeModel'})
              //       ]
              //     }));
                           this.props.navigation.navigate('HomeModel');
                              }
                              else if(roles_id == '3'){
                             // AsyncStorage.setItem('user_id', id.toString());
                                AsyncStorage.multiSet([
                                  ["user_id",id.toString()],
                                  ["roles_id", roles_id.toString()],
                                  ["state", responseJson.result.country.name.toString()],
                                  ["user_name", name.toString()],
                                   ["user_image", user_image.toString()],
                                   ["coins", coins.toString()]
                                 ]);

                                //  this.props
                                //  .navigation
                                //  .dispatch(StackActions.reset(
                                //    {
                                //       index: 0,
                                //       actions: [
                                //         NavigationActions.navigate({ routeName:'HomeMember'})
                                //       ]
                                //     }));



                              this.props.navigation.navigate('HomeMember');
                              }

                           }




                     }else{

                               //this.setState({error:responseJson.message,showProgress:false})
                               Platform.OS === 'android'
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)

                       }


                       }).catch((error) => {


                        this.setState({loading_status:true})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show(error.message, ToastAndroid.SHORT)
                        : Alert.alert("Connection Error !")



                       });
               }
             }

             const infoRequest = new GraphRequest(
               '/me',
               {
                 accessToken: accessToken,
                 parameters: {
                   fields: {
                     string: 'email,name,id,first_name,last_name'
                   }
                 }
               },
               responseInfoCallback
             );

             // Start the graph request.
             let a = new GraphRequestManager().addRequest(infoRequest).start()
            // ToastAndroid.show("Result ..."+ a.toString(),ToastAndroid.LONG)

           }
         )
        }

  }.bind(this),
  function (error) {
   // console.log('Login fail with error: ' + error)
    Platform.OS === 'android'
    ?  ToastAndroid.show(error.message, ToastAndroid.SHORT)
    : Alert.alert("Login fail with error!")
    //ToastAndroid.show('Login fail with error: ' + JSON.stringify(error),ToastAndroid.LONG)
  }
)
  }

  _twitter = async () =>{


      try{
            const result = await TwitterAuth.login()
         // ToastAndroid.show(JSON.stringify(result), ToastAndroid.LONG);
           // Alert.alert("ID",result.userId)
            //  Alert.alert("ID",result.authToken)
            //  Alert.alert("ID",result.authTokenSecret)
            //Alert.alert(JSON.stringify(result))

            //   this.setState({username : result.userId})
          //  ToastAndroid.show(JSON.stringify(result), ToastAndroid.LONG);

              var formData = new FormData();
              this.setState({loading_status:true})

              formData.append('twitter_id', result.userID);
              formData.append('email', result.userName);
              formData.append('name',result.userName)
              formData.append('device_token',this.state.token)
              Platform.OS =='android'
              ?  formData.append('device_type',1)
              : formData.append('device_type',2)

             // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);


             let url = urls.base_url +'api/twitter_login'
              fetch(url, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type':  'multipart/form-data',
              },
              body: formData

            }).then((response) => response.json())
                  .then((responseJson) => {
                    this.setState({loading_status:false})
                    if(!responseJson.error){
                    // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT)


                      //success in inserting data
                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)


                        let star = 0
                        let coin = 0
                    var id = responseJson.result.id
                    var roles_id = responseJson.result.roles_id
                    var user_name = responseJson.result.user_name
                    var name = responseJson.result.name
                    var user_image = responseJson.result.user_image
                    var create_profile_status = responseJson.result.create_profile_status
                    // var state_name = responseJson.result.state.name;
                    // var country_name = responseJson.result.country.name;
                    var age = responseJson.result.age;
                    var country_id = responseJson.result.country_id
                    var city_id = responseJson.result.city_id
                    var state_id = responseJson.result.state_id
                    var stars = responseJson.result.stars
                    var coins = responseJson.result.coins

                   // ToastAndroid.show(country_name,ToastAndroid.LONG)
                    if(stars == null){
                      stars = star
                    }
                    else{
                      stars = stars
                    }


                    if(coins == null){
                      coins = coin
                    }
                    else{
                      coins = coins
                    }

                    let obj = {
                     "user_id" : id,

                   } ;


                   let obj_role = {
                     "user_id" : id,
                     "roles_id":roles_id
                   } ;


                    if(country_id == null){
                      //LocationSocial
                      this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                    }

                    else if(create_profile_status == 0){
                         if(roles_id == '2'){

                           this.props.navigation.navigate('CreateProfileModel',{result : obj});
                         }
                         else if(roles_id == '3'){

                           this.props.navigation.navigate('CreateProfileMember',{result : obj});
                         }
                    }

                    else{


                     if(roles_id == '2'){
                      // AsyncStorage.setItem('user_id', id.toString());
                      AsyncStorage.multiSet([
                        ["user_id",id.toString()],
                        ["roles_id", roles_id.toString()],
                         ["state",responseJson.result.state.name.toString()],
                         ["user_name", name.toString()],
                         ["user_image", user_image.toString()],
                         ["user_age", age.toString()],
                         ["stars", stars.toString()]
                       ]);
                       this.props.navigation.navigate('HomeModel');
                      }
                      else if(roles_id == '3'){
                     // AsyncStorage.setItem('user_id', id.toString());
                        AsyncStorage.multiSet([
                          ["user_id",id.toString()],
                          ["roles_id", roles_id.toString()],
                          ["state", responseJson.result.country.name.toString()],
                          ["user_name", name.toString()],
                           ["user_image", user_image.toString()],
                           ["coins", coins.toString()]
                         ]);
                       this.props.navigation.navigate('HomeMember');
                      }

                    }



              }else{

                        //this.setState({error:responseJson.message,showProgress:false})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                        : Alert.alert(responseJson.message)
                }


                  }).catch((error) => {
                    this.setState({loading_status:false})
                  });

          }
          catch(error){
            Platform.OS === 'android'
                        ?  ToastAndroid.show("Login Cancelled !", ToastAndroid.SHORT)
                        : Alert.alert(" Login Cancelled!")
          }
  }






    _instagram(token){
      fetch('https://api.instagram.com/v1/users/self/?access_token='+token, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },


      }).then((response) => response.json())
            .then((responseJson) => {
       this.setState({loading_status:false})
         //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);


                 if(responseJson.meta.code == 200){

                  var id = responseJson.data.id
                  var username = responseJson.data.username
                  var full_name = responseJson.data.full_name


                //   Platform.OS === 'android'
                // ?  ToastAndroid.show('OTP Sent successfully on your email', ToastAndroid.SHORT)
                // : Alert.alert('OTP Sent successfully on your email')

                let obj = {
                  "id" : id,
                  "username":username,
                  "full_name":full_name

                }
               this.props.navigation.navigate("InstagramEmail",{result : obj})
              }
              else{

                Platform.OS === 'android'
              ?   ToastAndroid.show("Error !", ToastAndroid.SHORT)
              : Alert.alert("Error !")
              }
            }).catch((error) => {
              this.setState({loading_status:false})

              Platform.OS === 'android'
              ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
              : Alert.alert("Connection Error !")
            });


    }

  onLogin() {

     if (this.isValid()) {
      this.setState({loading_status:true})
      var formData = new FormData();
        var token = this.checkPermission()
       formData.append('email', this.state.email);
       formData.append('password', this.state.password);
       formData.append('device_token', this.state.token);
       Platform.OS =='android'
       ?  formData.append('device_type',1)
       : formData.append('device_type',2)
      // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)

      console.log("DATASSSS",formData)

               let url = urls.base_url +'api/api_login'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData

             }).then((response) => response.json())
                   .then((responseJson) => {
                       this.setState({loading_status:false})

                  //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);

                   if(!responseJson.error){
                     this.setState({email:'',password:''})

                          let age = 0;
                          let coin = 0;
                          let star = 0;
                         //success in inserting data
                         //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                         var id = responseJson.result.id
                         var roles_id = responseJson.result.roles_id
                         var user_name = responseJson.result.user_name
                         var name = responseJson.result.name
                         var user_image = responseJson.result.user_image
                         var create_profile_status = responseJson.result.create_profile_status
                         var state = responseJson.result.state
                         var country = responseJson.result.country
                         var stars = responseJson.result.stars
                         var coins = responseJson.result.coins


                         if(responseJson.result.age == null){
                           age = 0
                         }
                         else{
                           age = responseJson.result.age
                         }

                         if(coins == null){
                           coins = coin
                         }
                         else{
                           coins = coins
                         }


                         if(stars == null){
                          stars = star
                        }
                        else{
                          stars = stars
                        }

                         //0 means not created profile
                         //1 means created orofike pictyre

                         let obj = {
                          "user_id" : id,
                        } ;

                         if(create_profile_status == 0){
                          if(roles_id == 2){
                            //AsyncStorage.setItem('user_id', id.toString());
                            this.props.navigation.navigate('CreateProfileModel',{result : obj});
                           }
                           else if(roles_id == 3){
                           // AsyncStorage.setItem('user_id', id.toString());
                            this.props.navigation.navigate('CreateProfileMember',{result : obj});
                           }
                         }
                         else{


                          if(roles_id == 2){
                           // AsyncStorage.setItem('user_id', id.toString());
                           AsyncStorage.multiSet([
                             ["user_id",id.toString()],
                             ["roles_id", roles_id.toString()],
                              ["state", state.name.toString()],
                              ["user_name", name.toString()],
                              ["user_image", user_image.toString()],
                              ["user_age", age.toString()],
                              ["stars", stars.toString()]
                            ]);
                            this.props.navigation.navigate('HomeModel');
                           }
                           else if(roles_id == 3){
                          // AsyncStorage.setItem('user_id', id.toString());
                             AsyncStorage.multiSet([
                               ["user_id",id.toString()],
                               ["roles_id", roles_id.toString()],
                               ["state", country.name],
                               ["user_name", name.toString()],
                                ["user_image", user_image.toString()],
                                ["coins", coins.toString()]
                              ]);
                            this.props.navigation.navigate('HomeMember');
                           }

                         }





                       }else{
                           //errror in insering data
                           //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                           this.setState({loading_status:false})


                           Platform.OS === 'android'
                           ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                           : Alert.alert(responseJson.message)



                         }

                   }).catch((error) => {
                             this.setState({loading_status:false})

                             Platform.OS === 'android'
                             ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                             : Alert.alert("Connection Error !")


                   });


      }

}

    render() {



        return (

          <View style={styles.container}>






              <InstagramLogin
              ref= {ref => this.instagramLogin= ref}
              clientId='557648d7e8324af6bbeefd8bb6a0bef0'
                redirectUrl='http://webmobril.com/auth/instagram/callback'
             // scopes={['public_content', 'follower_list']}
              onLoginSuccess={(token) => this._instagram(token)}
              onLoginFailure={(data) => ToastAndroid.show(JSON.stringify(data),ToastAndroid.LONG)}
          />
         <Image source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>

         <Text style={styles.input_text}>{I18n.t('email_id')}</Text>
          <TextInput
            value={this.state.email}
            keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
            onChangeText={(email) => this.setState({ email : email.trim() })}
            style={styles.input}
            autoCapitalize='none'
            placeholderTextColor={'black'}
          />

          <Text style={styles.input_text}>{I18n.t('password')}</Text>
          <TextInput
            value={this.state.password}

            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

          <Text onPress={()=> {
            this.setState({
              email:'',
              password:'',
            })
            this.props.navigation.navigate("ForgotPassword")
          }} style={{marginBottom:15,alignSelf:'flex-end',fontSize:fonts.font_size}} >{I18n.t('forgot_password')} ?</Text>

          <TouchableOpacity
          onPress={this.onLogin.bind(this)}
                 style={styles.loginButton}
            >
                 <Text style={styles.loginText}>{I18n.t('login').toUpperCase()}</Text>
         </TouchableOpacity>

         <Text style={{marginBottom:15,fontSize:fonts.font_size}} >{I18n.t('login_with')}</Text>

         <View style={styles.socialView}>
         <View style={{flex:1}}>
         <TouchableOpacity onPress={this._fb.bind(this)}>
         <Image source={require('../assets/fb.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>
         <View style={{flex:1}}>
         <TouchableOpacity onPress={this._twitter.bind(this)}>
         <Image source={require('../assets/twitter.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>
         <View style={{flex:1}}>
        {/* <TouchableOpacity onPress={()=> this.instagramLogin.show()}>*/}
        <TouchableOpacity onPress={()=> this.instagramLogin.show()}>
         <Image source={require('../assets/insta.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>

         <View style={{flex:1}}>
         <TouchableOpacity onPress={()=> this.signIn()}>
          <Image source={require('../assets/google.png')} style={styles.social_image} resizeMode='contain'/>
          </TouchableOpacity>
          </View>

         </View>

         <View style={{flexDirection:'row'}}>
          <Text onPress={()=> this.props.navigation.navigate("MemberType")} style={{fontSize:fonts.font_size}}>{I18n.t('dont_have_account')} ? </Text>
          <Text onPress={()=> this.props.navigation.navigate("MemberType")} style={{fontWeight:'bold',fontSize:fonts.font_size}}>{I18n.t('sign_up')}</Text>
         </View>


         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


      </View>






        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    padding:30,


  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'30%'
 },
 input_text:{
  width:'100%',
  marginBottom:7,
  fontSize:fonts.font_size,

 },
 input: {
  width: "100%",
  height: 50,
 padding:7,
  borderRadius: 10,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{

    marginBottom:15,
    marginTop:10,
    width:"100%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',


 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :18,
   paddingTop:10

 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',
   marginBottom:15,



 },
 social_image:{
  height:50,
  width:'100%'


},
indicator: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'

},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}


}
)



// {
//   this.state.loading_status
//   ?
//   <View style={{backgroundColor: 'rgba(128,128,128,0.5)',flex:1,position: 'absolute',
//   left: 0,
//   right: 0,
//   top: 0,
//   bottom: 0,
//   alignItems: 'center',
//   justifyContent: 'center'}}>
//   <ActivityIndicator
//   animating={true}
//   style={styles.indicator}
//   size="large"
// />
//   </View>

//   :
//   null

// }
