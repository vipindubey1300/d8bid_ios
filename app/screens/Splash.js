import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { colors } from './Variables';
import firebase from 'react-native-firebase';


export default class Splash extends React.Component {
    constructor(props) {
        super(props);

    }

abc = async () =>{
	//make sure to save every item in string in asycnncstoarage  otheriws it will not store i it
		await AsyncStorage.getItem('privacy_status')
								.then((item) => {
						 if (item) {
               //means user alerady accepted
                         AsyncStorage.multiGet(["user_id","roles_id","state","user_name","user_image","user_age"]).then(response => {
                          //ToastAndroid.show("FIRst......"+JSON.stringify(response),ToastAndroid.LONG)
                        if(response  && response[0][1] != null &&response[1][1] != null){
                                  if(response[1][1] == 2){
                                    this.props.navigation.navigate('HomeModel')
                                  }
                                  else{
                                    this.props.navigation.navigate('HomeMember')
                               }
                     }
                    else{
                      this.props.navigation.navigate('Welcome')
                    }
           
               
               console.log(response[0][0]) // Key1
               console.log(response[0][1]) // Value1
               console.log(response[1][0]) // Key2
               console.log(response[1][1]) // Value2
             })
					   }
					   else {
							  //do something else means go to login
               // this.next()
							  this.props.navigation.navigate('PrivacyPolicy')
						  }
					});

}

xyz = async () =>{
	//make sure to save every item in string in asycnncstoarage  otheriws it will not store i it
  await AsyncStorage.getItem('privacy_status')
  .then((item) => {
    if (item) {
            AsyncStorage.getItem('lang')
            .then((value) => {
              if(value){
                //means lang accepted
                AsyncStorage.multiGet(["user_id","roles_id","state","user_name","user_image","user_age"]).then(response => {
                  //ToastAndroid.show("FIRst......"+JSON.stringify(response),ToastAndroid.LONG)
                if(response  && response[0][1] != null &&response[1][1] != null){
                          if(response[1][1] == 2){
                            this.props.navigation.navigate('HomeModel')
                          }
                          else{
                            this.props.navigation.navigate('HomeMember')
                       }
             }
            else{
              this.props.navigation.navigate('Welcome')
            }
   
       
       console.log(response[0][0]) // Key1
       console.log(response[0][1]) // Value1
       console.log(response[1][0]) // Key2
       console.log(response[1][1]) // Value2
     })
              }

              else{
                this.props.navigation.navigate('ChooseLanguage')

              }
            });
}
else {
  //do something else means go to login
 // this.next()
  this.props.navigation.navigate('PrivacyPolicy')
}
});

}




check = async () =>{
	 setTimeout(() => {
            //go to main to check if login or not
            //abc was main
			 this.xyz()


        }, 1000)


  }

  //1
 checkPermission = async()=> {
  //ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
      this.getToken();
  } else {
      this.requestPermission();
  }
}

  //3
 getToken= async()=>{
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
          // user has a device token
          //ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
          console.log("TOKENSSSSSSSSS",fcmToken)
      }
  }
}

  //2
requestPermission= async()=> {
  try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
  } catch (error) {
      // User has rejected permissions
    ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);
  }
}

componentWillMount(){
  this.checkPermission();
}

    componentDidMount() {
      StatusBar.setBackgroundColor(colors.status_bar_color)
       this.check()
       
    }

    render() {
        return (
          <View
            style={styles.container}>

			     <Image source={require('../assets/logo.png')} style={styles.splash_image} resizeMode='contain'/>

          </View>

        )
    }
}

let styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height:"100%",
    backgroundColor:'white'
  },
  splash_image:{
    height:"35%", 
    width:'50%',
    marginTop:-20
  }

}
)


// AsyncStorage.multiSet([['asfjdbasdfjk',data1], ['sadasdd', gotCode]]);
// AsyncStorage.multiGet(['email', 'password']).then((data) => {
//   let email = data[0][1];
//   let password = data[1][1];

//   if (email !== null)
//       //Your logic
// });
