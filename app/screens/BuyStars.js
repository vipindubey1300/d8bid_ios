import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,Platform,StatusBar,StyleSheet,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,urls } from './Variables';
import { ScrollView } from 'react-native-gesture-handler';
import {  Card,Item,   Picker,} from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
 ProductPurchase,
 PurchaseError,
 Product
} from 'react-native-iap';
import {Header} from 'react-native-elements';


const itemSkus = Platform.select({
  ios: [
    'com.d8bid.1stars','com.d8bid.2stars',
    'com.d8bid.3stars','com.d8bid.4stars',
    'com.d8bid.5stars'
  ],
  android: [
    'com.example.coins100'
  ]
});



export default class BuyStars extends React.Component {
    constructor(props) {
        super(props);
        this.state ={

          stars:[],
          loading_status:false,
          star_id:'key0',
          price:0,
          textstatus:false,
          star_name:'',
          products:null


      };


    }

    purchaseUpdateSubscription = null
    purchaseErrorSubscription = null


    iap = async(sku) =>{
      try {
        let a = await RNIap.requestPurchase(sku, false);
       // Alert.alert(JSON.stringify(a))
        console.log(JSON.stringify(a))

       



      } catch (err) {
        console.log("ERROROROROROORORORORO")
        console.warn(err.code, err.message);
      }
    }



    componentWillUnmount(){
      this.setState({
        stars:[],products:null
      })

      if (this.purchaseUpdateSubscription) {
        this.purchaseUpdateSubscription.remove();
        this.purchaseUpdateSubscription = null;
      }
      if (this.purchaseErrorSubscription) {
        this.purchaseErrorSubscription.remove();
        this.purchaseErrorSubscription = null;
      }
    }

    fetchStars = async () =>{
      this.setState({loading_status:true})

                        let url = urls.base_url +'api/api_star'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].star_name
                              var price = responseJson.result[i].price

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };
                                    array[i] = { ...array[i], price: price };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ stars : temp_arr});

                            }


                          else{
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT)
                            : Alert.alert("Cant Connect to Server")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          });



    }


    async componentWillMount() {

      try {

        const products: Product[] = await RNIap.getProducts(itemSkus);


        if(products[0].productId.toString().includes('coins')){

          
          let pro = products.slice(2, products.length)

          console.log("YYYYYYYYYYYYYYYYYYYYYYY",JSON.stringify(pro))
          this.setState({
            products:pro
          })
        }
        else{
          this.setState({
            products:products
          })
        }
      
       

       console.log("STARS",JSON.stringify(products))
//
      } catch(err) {
        console.warn(err); // standardized err.code and err.message available
      }


      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    }
    convertDate(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString();
      var dd  = date.getDate().toString();

      var mmChars = mm.split('');
      var ddChars = dd.split('');

      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    }


 IapServer(star_id,price,transactionId){
   AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

          //ToastAndroid.show("price"+price+"bokingid...."+bid+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
          var formData = new FormData();
          //txnid and payal id should have same value
          formData.append('user_id',item);
          formData.append('star',star_id);
          formData.append('price', price);
          formData.append('txn_id',transactionId);

          var date = this.convertDate(new Date())
          let star = this.state.star_name.toString().substring(0, 1)

          //product_type 1=star, 2=coin
          formData.append('user_id',item);
          formData.append('product_id',star_id);
          formData.append('product_type',1);
          formData.append('role_id',2);
          formData.append('txn_amt', price);
          formData.append('txn_date', date);
          formData.append('txn_status', 'success');
          formData.append('txn_id',transactionId);
          formData.append('stars',star);
         

          //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
            this.setState({loading_status:true})

            console.log("FORM",JSON.stringify(formData))



       let url = urls.base_url +'api/api_apple_pay'
          fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type':  'multipart/form-data',
          },
          body: formData

        }).then((response) => response.json())
              .then((responseJson) => {
                this.setState({loading_status:false})
               //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
              if(!responseJson.error){
                    //success in inserting data

                    var star = this.state.star_name.toString().substring(0, 1)
                    AsyncStorage.getItem( 'stars' )
                          .then( data => {
            
                            // the string value read from AsyncStorage has been assigned to data
                            console.log( data );
                              var c = parseInt(data)
                              c= c + parseInt(star)
            
                            //save the value to AsyncStorage again
                            AsyncStorage.setItem( 'stars', c.toString() );
            
                          }).done();
            
            
                    this.props.navigation.navigate('HomeScreen');
            
                   // console.log("PURCHASEeeee",purchase.transactionId)


                    Platform.OS === 'android'
                    ?  ToastAndroid.show("Payment Successful ", ToastAndroid.SHORT)
                    : Alert.alert("Payment Successful ")


                 


            }else{



                      Platform.OS === 'android'
                      ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)
              }


              }).catch((error) => {
               // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                Platform.OS === 'android'
                ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                : Alert.alert("Connection Error !")

              });
        }
        else{
          //ToastAndroid.show("User not found !",ToastAndroid.LONG)
          // Platform.OS === 'android'
          // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
          // : Alert.alert("User not found !")
        }
      });
    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')

     AsyncStorage.getItem('stars')
     .then((item) => {
               if (item) {

                       //means model
                       this.setState({prev_stars:parseInt(item)})


             }
             else {
               ToastAndroid.show("Error !",ToastAndroid.LONG)
               }
     });




     this.fetchStars()


     this.purchaseUpdateSubscription = purchaseUpdatedListener((purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase ) => {
      console.log('purchaseUpdatedListener', purchase);
      const receipt = purchase.transactionReceipt;
      if (receipt) {

        RNIap.finishTransactionIOS(purchase.transactionId);
        //Alert.alert("Sucessfully purchased stars")

        var star_id = this.state.star_id
        var star_price = this.state.price
        var transactionId = purchase.transactionId

        this.IapServer(star_id,star_price,transactionId)

       
      }
    });
 
    this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
      console.warn('purchaseErrorListener', error);
    });

    }

    starSelect(itemValue){
       // console.log(JSON.stringify(this.state.products))
      if(this.state.stars.length > 0){
        //var jobs = this.state.jobs
        for(var i = 0 ; i < this.state.stars.length ; i++){
          if(this.state.stars[i].id == itemValue){

            this.setState({star_id: this.state.stars[i].id,
              price:this.state.products[i].price,
              textstatus:true,star_name:this.state.stars[i].name})
            //.setState({coin_id: this.state.stars[i].id,price:this.state.stars[i].price,coin_name:this.state.coins[i].name,textstatus:true})
          }
        }
      }

    }


    chooseIAP(){
      var temp_star = this.state.star_name.toString().substring(0, 1)
      var total = parseInt(temp_star) + parseInt(this.state.prev_stars)

    if(this.state.star_id.length === 0 || this.state.star_id ==="key0" || this.state.star_id == 0){

      //ToastAndroid.show("Choose Star !", ToastAndroid.SHORT);

      Platform.OS === 'android'
      ?  ToastAndroid.show("Choose Star !", ToastAndroid.SHORT)
      : Alert.alert("Choose Star !")



      return
    }
    else if(total > 5){

      Platform.OS === 'android'
      ?  ToastAndroid.show("You can have max 5 stars !", ToastAndroid.SHORT)
      : Alert.alert("You can have max 5 stars !")



      return
    }

       else{
        let star = this.state.star_name.toString().substring(0, 1)

        console.log("VALUE",star)
            if(star == 1){
                let sku = this.state.products[0].productId
                this.iap(sku)
                return 
            }
            else if(star == 2){
              let sku = this.state.products[1].productId
              this.iap(sku)
              return 
          }
          else if(star == 3){
            let sku = this.state.products[2].productId
            this.iap(sku)
            return 
        }
        else if(star == 4){
          let sku = this.state.products[3].productId
          this.iap(sku)
          return 
         }

         else if(star == 5){
          let sku = this.state.products[4].productId
          this.iap(sku)
          return 
      }


       }
       
}

    continue(){

       var temp_star = this.state.star_name.toString().substring(0, 1)
        var total = parseInt(temp_star) + parseInt(this.state.prev_stars)

      if(this.state.star_id.length === 0 || this.state.star_id ==="key0" || this.state.star_id == 0){

        //ToastAndroid.show("Choose Star !", ToastAndroid.SHORT);

        Platform.OS === 'android'
        ?  ToastAndroid.show("Choose Star !", ToastAndroid.SHORT)
        : Alert.alert("Choose Star !")



        return
      }
      else if(total > 5){

        Platform.OS === 'android'
        ?  ToastAndroid.show("You can have max 5 stars !", ToastAndroid.SHORT)
        : Alert.alert("You can have max 5 stars !")



        return
      }

      else{
        let obj = {
          "price" : this.state.price,
          "role_id": 2,
          "star":this.state.star_name.toString().substring(0, 1),
          "star_id":this.state.star_id

        }

        this.props.navigation.navigate('PaymentMode',{result :obj});
      }



    }

    render() {

      let makestars =this.state.stars.map((star) => {
        return (
          <Item label={star.name} value={star.id} key={star.id}/>
        )
    })


        return (

         <View
         style={styles.container}>


         {/*for header*/}
			 
         <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomePage")}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                
          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
           
        centerComponent={{ text: I18n.t('buy_stars'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


       {/*for main content*/}

            <ScrollView style={{flex:1, padding:25,width:'100%'}}>
           <View style={styles.body}>

           <View style={{borderRadius:10,borderWidth:1,borderColor:'grey',padding:20}}>


           <Text style={{fontSize:22,color:'black',marginBottom:20}}>{I18n.t('buy_star_rating_visible_partner')}</Text>
           <View style={{width:'100%',height:1,borderRadius:10,borderWidth:1,borderColor:'grey',marginTop:15,marginBottom:23}}>
           </View>

           <Image source={require('../assets/stars.jpg')} style={{alignSelf:'center',height:190,width:240,marginBottom:30}} resizeMode='contain'/>

             
           

           {
             this.state.textstatus
             ?  <Text style={{marginBottom:8,fontSize:17,marginTop:-20,
             color: colors.color_primary,
             textAlign:'center'}}>{I18n.t('total_price')} : {this.state.products[0].currency} {this.state.price} </Text>
             : null
           }


          


             <Text style={styles.about_text}>{I18n.t('choose_number_of_stars')}</Text>
             <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker

                          mode="dropdown"
                          selectedValue={this.state.star_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              if(itemValue > 0){

                                this.starSelect(itemValue)


                              }
                              else{
                                this.setState({star_id: 0,price:0,textstatus:false,star_name:''})
                              }
                            }}>
                         
                             {makestars}
                  </Picker>
            </View>



            <TouchableOpacity onPress={()=> this.chooseIAP(0)}
                  style={styles.saveButton}>
                  <Text style={styles.saveText}>{I18n.t('buy_now')}</Text>
            </TouchableOpacity>
           </View>







         </View>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,

  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_secondary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:colors.color_primary,
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
