import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,ToastAndroid,TouchableWithoutFeedback,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,ActivityIndicator} from 'react-native';
import { colors,fonts,urls} from './Variables';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {Header} from 'react-native-elements';


export default class InstagramEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          email:'',
          loading_status:false,
         
      };


    }


componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });


}

    componentDidMount(){
      this.checkPermission()
    }


        //1
 checkPermission(){
  // ToastAndroid.show("Permission checkingg....",ToastAndroid.SHORT);

  

       firebase.messaging().hasPermission()
       .then(enabled => {
         if (enabled) {
           // user has permissions
           this.getToken()
         } else {
           // user doesn't have permission
           this.requestPermission()
         } 
       });
   
 }
 
   //3
 getToken= async () => {
  let fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {
      // user has a device token
     // ToastAndroid.show("Token.."+fcmToken,ToastAndroid.SHORT);
    this.setState({token:fcmToken})
  }
  else{
   //ToastAndroid.show("no toeke.....",ToastAndroid.SHORT);
  }
 }
 
   //2
  requestPermission(){

       firebase.messaging().requestPermission()
     .then(() => {
       // User has authorised  
       this.getToken();
     })
     .catch(error => {
       // User has rejected permissions  
       //ToastAndroid.show("Permission Denied",ToastAndroid.SHORT);

        
       Platform.OS === 'android' 
       ?  ToastAndroid.show("Permission Denied", ToastAndroid.SHORT)
       : Alert.alert("Permission Denied")


     });
       
 }



    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

    isValid() {
      const { email } = this.state;

      let valid = false;

      if (email.length > 0  && this.validateEmail(email)) {
        valid = true;
      }

      if (email.trim().length === 0) {
        
        
      Platform.OS === 'android' 
      ?  ToastAndroid.show('Enter Email', ToastAndroid.SHORT)
      : Alert.alert('Enter Email')

        return false;
      }
      else if(!this.validateEmail(email)){

        Platform.OS === 'android' 
        ?  ToastAndroid.show('Enter a valid Email', ToastAndroid.SHORT)
        : Alert.alert('Enter a valid Email')
        return false;
      } 
     

      return valid;
  }


    onInsta(){

    var formData = new FormData();
    var result  = this.props.navigation.getParam('result')



    formData.append('email', this.state.email);
    formData.append('device_token', this.state.token);
    Platform.OS =='android'
    ?  formData.append('device_type',1)
    : formData.append('device_type',2)
    formData.append('instagram_id', result["id"]);
    formData.append('name', result["full_name"]);



    if(this.isValid()){
                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/instagram_login'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                   this.setState({loading_status:false})
             // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                             if(!responseJson.error){

                              //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                              Platform.OS === 'android' 
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)


                          let star = 0
                          let coin = 0
                          var id = responseJson.result.id
                          var roles_id = responseJson.result.roles_id
                          var user_name = responseJson.result.user_name
                          var name = responseJson.result.name
                          var user_image = responseJson.result.user_image
                          var create_profile_status = responseJson.result.create_profile_status
                          var state = responseJson.result.name;
                          var age = responseJson.result.age;
                          var country_id = responseJson.result.country_id
                          var city_id = responseJson.result.city_id
                          var state_id = responseJson.result.state_id
                          var stars = responseJson.result.stars
                          var coins = responseJson.result.coins
      
      
                          if(stars == null){
                            stars = star
                          }
                          else{
                            stars = stars
                          }


                          if(coins == null){
                            coins = coin
                          }
      
                          let obj = {
                           "user_id" : id,
                           
                         } ;
      
      
                         let obj_role = {
                           "user_id" : id,
                           "roles_id":roles_id
                         } ;
      
                 
                          if(country_id == null){
                            //LocationSocial
                            this.props.navigation.navigate('MemberTypeSocial',{result : obj});
                          }
      
                          else if(create_profile_status == 0){
                               if(roles_id == '2'){
                                 
                                 this.props.navigation.navigate('CreateProfileModel',{result : obj});
                               }
                               else if(roles_id == '3'){
                               
                                 this.props.navigation.navigate('CreateProfileMember',{result : obj});
                               }
                          }
      
                          else{
      
                             //ToastAndroid.show("ID.."+age.toString()+"..role..."+user_image.toString(),ToastAndroid.LONG)
                           if(roles_id == '2'){
                            // AsyncStorage.setItem('user_id', id.toString());
                            AsyncStorage.multiSet([
                              ["user_id",id.toString()], 
                              ["roles_id", roles_id.toString()],
                               ["state", responseJson.result.state.name.toString()],
                               ["user_name", name.toString()],
                               ["user_image", user_image.toString()],
                               ["user_age", age.toString()],
                               ["stars", stars.toString()]
                             ]);
                             this.props.navigation.navigate('HomeModel');
                            }
                            else if(roles_id == '3'){
                           // AsyncStorage.setItem('user_id', id.toString());
                              AsyncStorage.multiSet([
                                ["user_id",id.toString()],
                                ["roles_id", roles_id.toString()],
                                ["state", responseJson.result.country.name.toString()],
                                ["user_name", name.toString()],
                                 ["user_image", user_image.toString()],
                                 ["coins", coins.toString()]
                               ]);
                             this.props.navigation.navigate('HomeMember');
                            }
      
                          }
                          
                           
                          
                          }
                          else{

                            Platform.OS === 'android' 
                            ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)

                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})
                          
                          Platform.OS === 'android' 
                          ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                          : Alert.alert("Connection Error !")
                        });


    }
}




    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
    
      
    }

    render() {

   

      
        return (
      
         <View 
         style={styles.container}>
<Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
           
            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <View style={{justifyContent:'center',alignItems:'center',padding:20,flex:1,marginTop:-30}}>


         <View style={{marginTop:-80,width:'100%'}}>

        

          
        
          <Text  style={{alignSelf:'center',marginBottom:50,fontSize:fonts.font_size}}>{I18n.t('enter_your_email')}</Text>


          <Text style={styles.input_text}>{I18n.t('email_id')}</Text>
          <TextInput
            value={this.state.email}
            onChangeText={(email) => this.setState({ email : email.trim()})}
            style={styles.input}
            placeholderTextColor={'black'}
          />

       

          <TouchableOpacity 
          onPress={this.onInsta.bind(this)}
                style={styles.forgotButton}>
                <Text style={styles.forgotText}>{I18n.t('submit').toUpperCase()}</Text>
          </TouchableOpacity>

          </View>

          {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }
</View>

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    alignItems: 'center',
    flex:1,
    padding:30,
  
    
  } ,
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size
 },

 input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
forgotButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  forgotText:{
    color:'black',
    textAlign:'center',
    fontSize :19,
    
    
  },
  
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  }
 ,
 loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}
 
  

}
)
