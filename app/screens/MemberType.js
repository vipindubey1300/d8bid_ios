import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity} from 'react-native';
import { fonts,colors,urls } from './Variables';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';



export default class MemberType extends React.Component {
    constructor(props) {
        super(props);

    }


    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });
    
    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
      
    }

    next(id){
      //2 model
      //3 memeber

      let obj = {
        "roles_id" : id,
       
      }

      if(id == 2){
        this.props.navigation.navigate("SignupModel",{result : obj})
      }
      else if(id == 3){
        this.props.navigation.navigate("SignupMember",{result : obj})
      }
     
    }

    render() {
        return (
      
         <View 
         style={styles.container}>

          <View style={{marginTop:-40,width:'100%',marginRight:10}}>

          <Text style={{alignSelf:'center',color:'black',fontSize:20,marginBottom:10}}>{I18n.t('who_you_are')} ?</Text>
          <Text  style={{alignSelf:'center',marginBottom:60,fontSize:fonts.font_size}}>{I18n.t('please_select_type')}</Text>

          <TouchableOpacity
           onPress={()=> this.next(2)}
                 style={styles.modelButton}>
                 
                 <Text style={styles.modelText}>{I18n.t('enter_as_model').toUpperCase()}</Text>
         </TouchableOpacity>

         <TouchableOpacity
         onPress={()=> this.next(3)}
                 style={styles.memberButton}>
                 
                 <Text style={styles.memberText}>{I18n.t('enter_as_member').toUpperCase()}</Text>
         </TouchableOpacity>
          </View>


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    alignItems: 'center',
    flex:1,
    padding:20,
    


    
  },
 
  modelButton:{
    
     marginBottom:15,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  modelText:{
    color:colors.color_secondary,
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  memberButton:{
  
    marginTop:15,
    width:"100%",
    height:50,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.color_secondary,
    borderRadius:8,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 memberText:{
   color:colors.color_primary,
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 
  

}
)
