import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,StatusBar,
  ScrollView,StyleSheet,FlatList,Platform,Modal,TouchableWithoutFeedback,
   BackHandler,TextInput,ImageBackground,TouchableOpacity,ActivityIndicator} from 'react-native';
import { colors,fonts,urls } from './Variables';
import {Card} from 'native-base';
import { withNavigationFocus } from 'react-navigation';
import I18n from '../i18n';
import { PermissionsAndroid } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Slider,Overlay ,Header} from 'react-native-elements';
import Video from 'react-native-video';
import RBSheet from "react-native-raw-bottom-sheet";
import VideoPlayer from 'react-native-video-controls';



 class MemberHomePage extends Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          models:[],
          models_server:[],
          search_status:false,
          search:'',
          latitude:0.0,
             longitude: 0.0,
             banner_image:null,
             overlay_location:false,
             locationvalue:5,
             overlay_video:false,
             model_id:0,
             videoSource:null,
             // video states
            paused: false,
            played: 0,
            duration: 0,
            isFullscreen: false,
            filters_data:null
        };

    }


    sendLocation(latitude,longtitude){

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

         var text = this.state.message
          formData.append('user_id', item);//our id
          formData.append('latitude',latitude);
          formData.append('longitude', longtitude);


                          // this.setState({loading_status:true})
                          let url = urls.base_url +'api/user_location'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})

                                   if(!responseJson.error){

                                       // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);





                                }
                                else{

                                       // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                // Platform.OS === 'android'
                                // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                // : Alert.alert("Connection Error !")
                              });
        }
        else{
         // ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });


    }


  async  requestLocationPermission(){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'D8Bid App',
          'message': 'D8Bid App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {


        navigator.geolocation.getCurrentPosition(
          position => {
            this.setState({

             latitude: position.coords.latitude,
             longitude: position.coords.longitude,


            });
            this.sendLocation(position.coords.latitude,position.coords.longitude)
          },
          error => {
          //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

            Platform.OS === 'android'
            ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
            : Alert.alert("Check your internet speed connection")


            //this.setState({loading_status:false})
          },
          { enableHighAccuracy: false, timeout: 10000}
        );


      } else {
        console.log("location permission denied")
       Alert.alert("location permission denied");
        //alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
        Alert.alert(JSON.stringify(err.message));
    }
  }


    async  requestLocationPermissionIOS(){


        navigator.geolocation.getCurrentPosition(
          position => {
            this.setState({

             latitude: position.coords.latitude,
             longitude: position.coords.longitude,


            });

            this.sendLocation(position.coords.latitude,position.coords.longitude)

          },
          error => {
          //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);


           console.log("LOCATION ERROR",error.message)

            Platform.OS === 'android'
            ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
            : Alert.alert("Cannot fetch Location")


            this.setState({loading_status:false})
          },
          { enableHighAccuracy: false, timeout: 10000 }
        );
  }



  onSearch(){


    var results = [];

    this.setState({ loading_status : true});

    var toSearch = this.state.search;


    var model = this.state.models_server
  var length = this.state.models_server.length.toString()
  var name = ''
  //ToastAndroid.show("fgdgfdgd......"+length.toString(),ToastAndroid.SHORT)
    for(var i=0; i< length  ; i++) {

         name = model[i].name
        //ToastAndroid.show("Result.."+name.includes("t"),ToastAndroid.SHORT)
        if(name  != null ){
                var check = name.indexOf(toSearch) > -1
              if(check) {
                results.push(model[i]);
              // ToastAndroid.show("Result.."+model[i].name,ToastAndroid.SHORT)
              }
        }

    }
    //ToastAndroid.show("Result.."+JSON.stringify(results),ToastAndroid.LONG)

    if(results.length == 0){
     // ToastAndroid.show("No models found",ToastAndroid.LONG)

      Platform.OS === 'android'
      ?  ToastAndroid.show("No models found", ToastAndroid.SHORT)
      : Alert.alert("No models found")
    }

    this.setState({models:results,loading_status : false})


}
fetchNearLocation = async (location) =>{
  this.setState({loading_status:true})
  var formData = new FormData();
  formData.append('latitude', this.state.latitude);//our id
   formData.append('longitude',this.state.longitude);
   formData.append('dist',location);
 // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);
 let url = urls.base_url +'api/near_model'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                  }).then((response) => response.json())
                      .then((responseJson) => {
                        console.log(responseJson)
                      // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                        this.setState({loading_status:false})
                        if(!responseJson.error){

                          var length = responseJson.result.length.toString();
                          var temp_arr=[]
                          var index = 0
                          for(var i = 0 ; i < length ; i++){

                                index = index + 1

                                var id = responseJson.result[i].model_id
                                var name = responseJson.result[i].name
                                var age = responseJson.result[i].age
                                var state = responseJson.result[i].state
                                var live = responseJson.result[i].live
                                var user_image= responseJson.result[i].user_image


                                let array = [...temp_arr];
                                      //	const array = [...this.state.data];
                                array[i] = { ...array[i], key:id };
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], state: state};
                                array[i] = { ...array[i], age: age};
                                array[i] = { ...array[i], name:name};
                                array[i] = { ...array[i], live:live };
                                array[i] = { ...array[i], user_image:urls.base_url+user_image };



                                temp_arr = array
                          }
                          this.setState({ models : temp_arr,models_server:temp_arr});
                          Platform.OS === 'android'
                          ?
                          ToastAndroid.show("Total Models "+ index.toString(), ToastAndroid.SHORT)
                          : Alert.alert("Total Models "+ index.toString())

                        }


                      else{
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Empty!", ToastAndroid.SHORT)
                        : Alert.alert("Empty!")
                      }

                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error!")
                      });



}




fetchBanner = async () =>{
  this.setState({loading_status:true})
  var formData = new FormData();
  formData.append('role_id', 3);//our id

  let url = urls.base_url +'api/advert_list'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                  }).then((response) => response.json())
                      .then((responseJson) => {

                      // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                        this.setState({loading_status:false})
                        if(!responseJson.error){

                          var image = responseJson.result.advert_image
                          this.setState({banner_image:urls.base_url+image})

                        }


                      else{
                        // Platform.OS === 'android'
                        // ?  ToastAndroid.show("Empty!", ToastAndroid.SHORT)
                        // : Alert.alert("Empty!")
                      }

                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
                        // Platform.OS === 'android'
                        // ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                        // : Alert.alert("Connection Error!")
                      });



}




filters = async (data) =>{
  this.setState({loading_status:true})
  var formData = new FormData();
  var arr=[];

  var male = data['male']
  var female = data['female']
  var transgender = data['transgender']

  if(male == true && female == true && transgender == true){
      arr=['male','female','transgender']
  }
  else if(male == true && female == false && transgender == true){
    arr=['male','transgender']
  }
  else if(male == true && female == false && transgender == false){
    arr=['male']
  }
  else if(male == true && female == true && transgender == false){
    arr=['male','female']
  }

  else if(male == false && female == true && transgender == false){
    arr=['female']
  }
  else if(male == false && female == true && transgender == true){
    arr=['female','transgender']
  }
  else if(male == false && female == false && transgender == true){
    arr=['transgender']
  }
    var a = 0
  if(data['stars'] != null){
   if(data['stars']== 'key0'){
     a = null
   }
   else{
    a= data['stars']
   }
  }

  formData.append('star',a);
  for(var i = 0 ; i < arr.length ; i++){
    formData.append('int_gender[' +i+']',arr[i]);
 }

    for(var i = 0 ; i < data['height'].length ; i++){
      formData.append('height[' +i+']',data['height'][i]);
    }

    for(var i = 0 ; i < data['weight'].length ; i++){
      formData.append('weight[' +i+']',data['weight'][i]);
    }
    for(var i = 0 ; i < data['cities'].length ; i++){
      formData.append('cities[' +i+']',data['cities'][i]);
    }
    for(var i = 0 ; i < data['age'].length ; i++){
      formData.append('age[' +i+']',data['age'][i]);
    }

    formData.append("ethnicities",data['ethnicity'])
    formData.append("body_type",data['body_type'])



 // formData.append('int_gender',arr);
  // formData.append('star',a);
 // formData.append('height',data['height']);
 // formData.append('weight',data['weight']);
 // formData.append('city',data['cities']);

  // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.show)

                  let url = urls.base_url +'api/filter'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
                   body: formData

                  }).then((response) => response.json())
                      .then((responseJson) => {
console.log("RESSSSSSS",responseJson)
                      // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                        this.setState({loading_status:false})
                        if(!responseJson.error){
                          var length = responseJson.search_result.length.toString();
                          var temp_arr=[]
                          for(var i = 0 ; i < length ; i++){


                            var id = responseJson.search_result[i].id
                            var name = responseJson.search_result[i].name
                            var age = responseJson.search_result[i].age
                            var state = responseJson.search_result[i].state
                            var live = responseJson.search_result[i].live
                            var user_image= responseJson.search_result[i].user_image
                            var user_video= responseJson.search_result[i].user_video


                                let array = [...temp_arr];
                                      //	const array = [...this.state.data];
                                array[i] = { ...array[i], key:id };
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], state: state};
                                array[i] = { ...array[i], age: age};
                                array[i] = { ...array[i], user_video : user_video};
                                array[i] = { ...array[i], name:name};
                                array[i] = { ...array[i], live:live };
                                array[i] = { ...array[i], user_image:urls.base_url+user_image };



                                temp_arr = array
                          }
                          this.setState({ models : temp_arr,models_server:temp_arr});

                        }


                      else{
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("No Models found!", ToastAndroid.SHORT)
                        : Alert.alert("No Models found!")

                        this.setState({ models : [],models_server:[]});
                      }

                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})

                        console.log("Errror",JSON.stringify(error.message))
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error!")
                      });



}

fetch = async () =>{
  this.setState({loading_status:true})

 // ToastAndroid.show("fethngggggg", ToastAndroid.LONG);

 let url = urls.base_url +'api/models_list'
                  fetch(url, {
                  method: 'GET',

                  }).then((response) => response.json())
                      .then((responseJson) => {

                        console.log("SS",responseJson)

                      // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                        this.setState({loading_status:false})
                        if(!responseJson.error){
                          var length = responseJson.result.length.toString();
                          var temp_arr=[]
                          var index = 0
                          for(var i = 0 ; i < length ; i++){

                                index = index + 1

                                var id = responseJson.result[i].model_id
                                var name = responseJson.result[i].name
                                var age = responseJson.result[i].age
                                var state = responseJson.result[i].state
                                var live = responseJson.result[i].live
                                var user_image= responseJson.result[i].user_image
                                var user_video= responseJson.result[i].user_video


                                let array = [...temp_arr];
                                      //	const array = [...this.state.data];
                                array[i] = { ...array[i], key:id };
                                array[i] = { ...array[i], id: id };
                                array[i] = { ...array[i], state: state};
                                array[i] = { ...array[i], age: age};
                                array[i] = { ...array[i], user_video : user_video};
                                array[i] = { ...array[i], name:name};
                                array[i] = { ...array[i], live:live };
                                array[i] = { ...array[i], user_image:urls.base_url+user_image };



                                temp_arr = array
                          }
                          this.setState({ models : temp_arr,models_server:temp_arr});
                          // Platform.OS === 'android'
                          // ?
                          // ToastAndroid.show("Total Models "+ index.toString(), ToastAndroid.SHORT)
                          // : Alert.alert("Total Models "+ index.toString())

                        }


                      else{
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Empty!", ToastAndroid.SHORT)
                        : Alert.alert("Empty!")
                      }

                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                        : Alert.alert("Connection Error!")
                      });



}


 async componentDidMount(){
    this.fetch()
    this.fetchBanner()
    Platform.OS === 'android'
    ?  await this.requestLocationPermission()
    :  await this.requestLocationPermissionIOS()
  }




  handleBackWithAlert = () => {
    // const parent = this.props.navigation.dangerouslyGetParent();
      // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
    // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)

    if (this.props.isFocused) {

            if(this.state.loading_status){
                 this.setState({loading_status:false})
            }

          else{
                  Alert.alert(
                  'Exit App',
                  'Exiting the application?',
                  [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                      },
                        {
                          text: 'OK',
                          onPress: () => BackHandler.exitApp()
                        }
                  ],
                  {
                  cancelable: false
                  }
                );
          }

          return true;
        }
}


	next(model_id){
    //ToastAndroid.show(model_id.toString(), ToastAndroid.LONG);
		if(this.state.models.length > 0){
			//var jobs = this.state.jobs
			for(var i = 0 ; i < this.state.models.length ; i++){
				if(this.state.models[i].key == model_id){
					result={}
					result["model_id"] = this.state.models[i].id

					this.props.navigation.navigate("ProfileDetails",{result : result});
				}
			}
		}
  }



  componentWillMount() {

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                //ToastAndroid.show(item.toString(),ToastAndroid.LONG)
                I18n.locale = item.toString()
            }
            else {
              //ToastAndroid.show(item.toString(),ToastAndroid.LONG)
                 I18n.locale = 'en'
              }
    });


    BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);

  }

  componentWillUnmount() {
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
    // ToastAndroid.show("unmount callled " ,ToastAndroid.LONG);

  }
  onSelect = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
  //  ToastAndroid.show("Recieved data is "+JSON.stringify(data) ,ToastAndroid.LONG);
    //this.setState(data);

    console.log("DATA",JSON.stringify(data))

    this.setState({filters_data:data})


    this.filters(data)
  };

    render() {




        return (

         <View
         style={styles.container}>

         <Overlay
         isVisible={this.state.overlay_location}
         windowBackgroundColor="rgba(0, 0, 0, .5)"
         overlayBackgroundColor="white"
         width={300}
         height='auto'

       >

         <View style={{height:null,width:null,backgroundColor:'white',alignItems:'center',justifyContent:'center',padding:5}}>



         <View style={{position:'absolute',right:5,top:5}}>
        <TouchableWithoutFeedback onPress={()=> this.setState({
          overlay_location:false
        })}>
        <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:20,width:20}} />
        </TouchableWithoutFeedback>
        </View>

        <Text style={{margin:20,flexWrap:'nowrap',fontSize:17,color:'black',fontWeight:'bold'}}>{I18n.t('choose_distance')}</Text>
        <View style={{ width:'90%', justifyContent: 'center',marginBottom:20 }}>
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
          <Text style={{color:'black',fontSize:15}}>5</Text>
          <Text style={{color:'black',fontSize:15}}>50</Text>
        </View>
        <Slider
          value={this.state.locationvalue}
          thumbTintColor={colors.color_primary}
          maximumValue={50}
          minimumValue={5}
          onValueChange={locationvalue => this.setState({ locationvalue })}
        />
        <Text style={{marginBottom:10,alignSelf:'center',marginTop:5,color:colors.color_primary,fontSize:16}}>{I18n.t('distance')} : {parseInt(this.state.locationvalue)} km</Text>
        <TouchableOpacity
        style={{backgroundColor:colors.color_primary,width:'80%',height:40,alignSelf:'center',marginTop:5}}
          onPress={() => {

                this.setState({overlay_location:!this.state.overlay_location})
                this.fetchNearLocation(parseInt(this.state.locationvalue))
              }}
          >
          <Text style={{color:'black',fontSize:19,fontWeight:'bold',alignSelf:'center',marginTop:5}}>{I18n.t('choose')}</Text>
          </TouchableOpacity>
      </View>


         </View>



         </Overlay>


         <RBSheet
         ref={ref => {
          this.RBSheet = ref;
      }}
            height={300}

            customStyles={{
                container: {
                justifyContent: "center",
                alignItems: "center"
                }
            }}

       >

         <View style={{height:'100%',width:'100%',backgroundColor:'white',alignItems:'center',
         justifyContent:'center',padding:25,backgroundColor:'white'}}>



         <View style={{position:'absolute',right:10,top:10}}>
        <TouchableWithoutFeedback onPress={()=> {
          this.RBSheet.close()
          this.setState({
            overlay_video:false
          })
        }}>
        <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:30,width:30}} />
        </TouchableWithoutFeedback>
        </View>

        {/*
         <Video source={{uri:urls.base_url+this.state.videoSource}}  // Can be a URL or a local file.

           rate={1.0}
           volume={1.0}
           muted={false}
           resizeMode={"cover"}
           onEnd={() => { console.log('Done!') }}
           repeat={true}                                     // Store reference
          style={{height:'70%',width:'90%',marginTop:10}} />*/}

        {
          this.state.videoSource == null
          ?
           <Text>{I18n.t('no_video_found')}</Text>
           :
           <VideoPlayer source={{uri:urls.base_url+this.state.videoSource}}  // Can be a URL or a local file.
                                 // Store reference
          style={{height:'50%',width:'95%',marginTop:10}} />
        }


         </View>



         </RBSheet>





         {/* headrer */}



            <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <View style={{flexDirection:'row',alignItems:'center'}}>
                  <TouchableWithoutFeedback onPress={() =>this.props.navigation.toggleDrawer()}>
                     <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/man-user.png')} />
                  </TouchableWithoutFeedback>

                  {/*<TouchableWithoutFeedback onPress={() =>this.fetchNearLocation()}>*/}
                  <TouchableWithoutFeedback onPress={() =>this.setState({overlay_location:!this.state.overlay_location})}>
                  <Image style={{width: 25, height: 25,marginLeft:6}}  source={require('../assets/locationsearch.png')} />
               </TouchableWithoutFeedback>

                  </View>
        }


         rightComponent={

          <View style={{flexDirection:'row',alignItems:'center'}}>
                        {
                          this.state.search_status
                          ?
                        null
                        : <TouchableWithoutFeedback onPress={() =>{
                          if(this.state.search_status){}
                          else{
                            this.setState({search_status:!this.state.search_status})
                          }
                        }}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/search.png')} />
                        </TouchableWithoutFeedback>
                        }
                       {/*  <TouchableWithoutFeedback onPress={() =>this.props.navigation.navigate("Filters", { onSelect: this.onSelect })} >*/}
                       <TouchableWithoutFeedback onPress={() =>this.props.navigation.navigate("Filters", { onSelect: this.onSelect,filters_data:this.state.filters_data })} >
                             {/* <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/filters.png')} />  */}

                             <Text style={{marginRight:5,fontSize:17,color:'black',fontWeight:'bold'}}>{I18n.t('filters')}</Text>
                        </TouchableWithoutFeedback>

                  </View>
          }




         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}

         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


                     {/* body */}

          <ScrollView style={{width:'100%',paddingBottom:50,backgroundColor:'#F5F0F0',flex:1}}>


     {/*


          {
            this.state.search_status
            ?
            <View style={{flexDirection:'row',alignItems:'center',flex:4,borderColor:'black',borderWidth:3,marginLeft:4,marginRight:2,borderRadius:10,marginTop:10}}>


            <View style={{flex:2.2}}>
            <TextInput
            value={this.state.search}
            onSubmitEditing={() => this.onSearch()}
            keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
            onChangeText={(search) => this.setState({ search : search.trim() })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

            </View>


            <View style={{flex:0.28}}>

               <TouchableWithoutFeedback onPress={() =>this.onSearch()}>
                  <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/search.png')} />
                  </TouchableWithoutFeedback>
            </View>




            <View style={{flex:0.28}}>
            <TouchableWithoutFeedback onPress={() =>this.setState({search_status:!this.state.search_status})}>
            <Image style={{width: 22, height: 22,margin:10}}  source={require('../assets/error.png')} />
            </TouchableWithoutFeedback>
      </View>

            </View>
            : null

          }


        */}



          <FlatList
							  style={{marginBottom:10}}
                data={this.state.models}
                 numColumns={2}
							  showsVerticalScrollIndicator={false}
							  scrollEnabled={false}
							  renderItem={({item}) =>


                 <View style={{width:'48%',margin:3}}>

									  <Card style={{width:'100%',height:320}}>
                    <TouchableWithoutFeedback  onPress={() => this.next(item.key)}>



                      <View style={{width:'100%',flex:1}}>
                        <View style={{flex:0.7}}>

                        <Image style={{width:'100%',height:'100%'}} source={{uri:item.user_image}}></Image>
                        </View>


                        <View style={{flex:0.3,alignSelf:'center',marginTop:0,alignItems:'center',backgroundColor:'#fcf5b5'}}>
                              <View style={{flexDirection:'row',alignItems:'center',
                              flex:1,height:null,justifyContent:'space-evenly',width:'100%'}}>


                                    <Text allowFontScaling={true}
                                    style={{fontSize:16,color:'black',
                                    marginRight:4,textAlign:'center',
                                    flexWrap:'wrap',flex:0.8,marginLeft:15,
                                    height:'auto',fontWeight:'bold'}}>{item.name}</Text>

                                    <View style={{flexDirection:'row',flex:0.2,
                                   justifyContent:'flex-end',alignItems:'center'}}>

                                        <Text style={{fontSize:14,color:'black',marginLeft:4,marginRight:5}}>{item.age}</Text>
                                        {item.live == 1 ?
                                        <Image style={{width: 15, height: 15,margin:0}}  source={require('../assets/green.png')} />
                                        :
                                        <Image style={{width: 15, height: 15,margin:0}}  source={require('../assets/red.png')} />
                                        }

                                    </View>
                              </View>
                        {
                          item.state == null
                          ?  <Text style={{fontSize:13,marginLeft:4,marginRight:5,flex:0.5}}></Text>
                          :  <Text style={{fontSize:13,marginLeft:4,marginRight:5,flex:0.5}}>{item.state}</Text>
                        }
                        <Text style={{fontSize:13,alignSelf:'center',
                        fontWeight:'bold',color:'blue',marginBottom:7,flex:0.5}}
                        onPress={()=> {
                          console.log("VVV",urls.base_url+item.user_video)
                          this.RBSheet.open()
                          this.setState({videoSource:item.user_video})
                        }}>{I18n.t('see_video')}</Text>

                        </View>
                        </View>


                        </TouchableWithoutFeedback>
                    </Card>

                    </View>

							  }
							  keyExtractor={item => item.id}
							/>


            </ScrollView>


            {
              this.state.search_status
              ?
              <View style={{position:'absolute',top:Platform.OS == 'ios' ? (Dimensions.get('window').height * 0.1)  : 70,left:0,right:0,height:55,width:'100%',backgroundColor:'white',padding:5,shadowOffset: {
                 width: 0,
                 height: 4,
             },
             shadowOpacity: 0.4,
             shadowRadius: 3,elevation:9}}>
              <View style={{flexDirection:'row',alignItems:'center',flex:4,borderColor:'black',borderWidth:2,marginLeft:4,marginRight:2,borderRadius:10,marginTop:0}}>


              <View style={{flex:2.2}}>
              <TextInput
              value={this.state.search}
              onSubmitEditing={() => this.onSearch()}
              keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
              onChangeText={(search) => this.setState({ search : search.trim() })}
              style={styles.input}
              placeholderTextColor={'black'}
              placeholder={'Search for models here '}
            />

              </View>


              <View style={{flex:0.28}}>
                 {/*}   <TouchableWithoutFeedback onPress={() =>this.props.navigation.navigate("Filters", { onSelect: this.onSelect })}> */}
                 <TouchableWithoutFeedback onPress={() =>this.onSearch()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/search.png')} />
                    </TouchableWithoutFeedback>
              </View>




              <View style={{flex:0.28}}>
              <TouchableWithoutFeedback onPress={() =>this.setState({search_status:!this.state.search_status})}>
              <Image style={{width: 22, height: 22,margin:10}}  source={require('../assets/error.png')} />
              </TouchableWithoutFeedback>
        </View>

              </View>
              </View>
              : null

            }





               {/* banner */}
                {/*
 {
   this.state.banner_image == null
   ?
   null:

   <View style={{position:'absolute',bottom:0,left:0,right:0,height:'5%',width:'100%',backgroundColor:'white'}}>
 <Image style={{width:'100%',height:'100%'}} source={{uri:this.state.banner_image}}></Image>

 </View>

 }

  */}

            {this.state.loading_status &&
              <View style={styles.loading}>
                <ActivityIndicator size='large' />
              </View>
          }
         </View>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',

    flex:1,



  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,

 },
 input: {
  width: "100%",
  height: 44,
  textAlign: 'left',


},
loginButton:{

    margin:6,
    width:"100%",
    height:40,
    backgroundColor:'#FFC300',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',


 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',



 },
 social_image:{
  height:50,
  width:'100%'


},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}


}
)


export default withNavigationFocus(MemberHomePage);
