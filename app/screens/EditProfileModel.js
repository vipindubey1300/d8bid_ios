import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,
  StyleSheet,ActivityIndicator,ImageBackground,Platform,TouchableOpacity,TextInput,
  TouchableWithoutFeedback,ScrollView,Alert,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import I18n from '../i18n';
import { Slider,Overlay } from 'react-native-elements';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
import VideoPlayer from 'react-native-video-controls';
import Axios from 'axios';
import {Header} from 'react-native-elements';




export default class EditProfileModel extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          countries:[],
          states:[],
          cities:[],
          gallery:[],
          name:'',
          height:'',
          weight:'',
          age:'',
          id:'',
          about:'',
          image:null,
          password:'',
          user_id:'',

          age_id:'key0',
          country_id:'key0',
          city_id:'key0',
          state_id:'key0',
          photo:null,
          imageData:'',
          mainphoto:null,
          mainPhotoSource:null,
          photoone:null,
          phototwo:null,
          photothree:null,
          photofour:null,
          photocounter:0,
          imageSourceOne:null,
          imageSourceTwo:null,
          imageSourceThree:null,
          imageSourceFour:null,

          ethnicity_id:'key0',
          ethnicity:[],
          ethnicity_name:'',
          body:[],
          body_type_id:'key0',
          body_name:'',
          videoSource:null,
          overlay_video:false

      };


    }

    fetchCountries = async () =>{
      this.setState({loading_status:true})

                    let url = urls.base_url +'api/api_country'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    getStates(countryId){
      this.setState({loading_status:true,states:[],city:[],city_id:0,state_id:0})

                       let url = urls.base_url +'api/state?country_id='+countryId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].country_id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], country_id: country_id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({states : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    getCity(stateId){
     // ToastAndroid.show("State id is ..."+stateId, ToastAndroid.LONG);
      this.setState({loading_status:true,cities:[],city_id:0})

                    let url = urls.base_url +'api/city?state_id='+stateId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].id
                              var name = responseJson.result[i].name

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({cities: temp_arr,loading_status:false});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                            this.setState({loading_status:false})
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }



    fetchBodyType = async () =>{
      this.setState({loading_status:true})

               let url = urls.base_url +'api/api_body_type'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name
                              const array = [...temp_arr];
                              array[i] = { ...array[i], id: id };
                              array[i] = { ...array[i], name: name };

                              temp_arr = array
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ body : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    fetchEthnicity = async () =>{
      this.setState({loading_status:true})

                       let url = urls.base_url +'api/api_ethnicity'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name


                              const array = [...temp_arr];
                              array[i] = { ...array[i], id: id };
                              array[i] = { ...array[i], name: name };

                              temp_arr = array


                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ ethnicity : temp_arr});

                            }


                          else{
                           // Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                           // ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    isValid() {

      let age_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.age.toString())
      let height_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.height.toString())
      let weight_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.weight.toString())
     // let bid_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.bid_price.toString())
      let valid = false;

      if (
          this.state.name.trim().length > 0
         /// this.state.interest_boolean.includes(true)
          //this.state.interest_boolean.filter(Boolean).length > 0
           ) {
        valid = true;
      }

    if(this.state.name.trim().length === 0){

        //ToastAndroid.show('Enter name', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Name', ToastAndroid.SHORT)
      : Alert.alert('Enter name')



        return false;
      }
      else if(this.state.age_id < 0 || this.state.age_id ==="key0" || this.state.age_id == 0){

      //  ToastAndroid.show('Enter age', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Age', ToastAndroid.SHORT)
      : Alert.alert('Enter Age')



        return false;
      }

      // else if(!age_valid){
      //   ToastAndroid.show("Enter valid age", ToastAndroid.SHORT);
      //   return false;
      // }
      else if(this.state.height.toString().trim().length === 0){

      //  ToastAndroid.show('Enter height', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter height', ToastAndroid.SHORT)
      : Alert.alert('Enter height')



        return false;
      }

      else if(!height_valid){
      //  ToastAndroid.show("Enter valid height", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter valid height', ToastAndroid.SHORT)
      : Alert.alert('Enter valid height')



        return false;
      }
      else if(this.state.height < 100 || this.state.height == 0){

       // ToastAndroid.show('Enter height greater than 10', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter height greater than 100', ToastAndroid.SHORT)
      : Alert.alert('Enter height greater than 100')



        return false;
      }
      else if(this.state.height > 220 ){

        // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


       Platform.OS === 'android'
       ?  ToastAndroid.show('Enter height less than 220', ToastAndroid.SHORT)
       : Alert.alert('Enter height less than 220')



         return false;
       }
      else if(this.state.weight.toString().trim().length === 0){

       // ToastAndroid.show('Enter weight', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter weight', ToastAndroid.SHORT)
      : Alert.alert('Enter weight')



        return false;
      }
      else if(!weight_valid){
        //ToastAndroid.show("Enter valid height", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter valid height', ToastAndroid.SHORT)
      : Alert.alert('Enter valid height')



        return false;
      }
      else if(this.state.weight < 40 || this.state.weight == 0){

       // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter weight greater than 40', ToastAndroid.SHORT)
      : Alert.alert('Enter weight greater than 40')



        return false;
      }
      else if(this.state.weight > 150 ){

        // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


       Platform.OS === 'android'
       ?  ToastAndroid.show('Enter weight less than 150', ToastAndroid.SHORT)
       : Alert.alert('Enter weight less than 150')



         return false;
       }

      else if (this.state.country_id < 0 || this.state.country_id ==="key0" || this.state.country_id == 0) {



        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter Country', ToastAndroid.SHORT)
        : Alert.alert('Enter Country')


        return false;
      }
      else if (this.state.state_id < 0 || this.state.state_id ==="key0" || this.state.state_id == 0) {



        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter state', ToastAndroid.SHORT)
        : Alert.alert('Enter state')


        return false;
      }
      else if (this.state.city_id < 0 || this.state.city_id ==="key0" || this.state.city_id == 0) {


        Platform.OS === 'android'
        ?  ToastAndroid.show('Enter city', ToastAndroid.SHORT)
        : Alert.alert('Enter city')


        return false;
      }


      else if (this.state.ethnicity_id < 0 || this.state.ethnicity_id ==="key0" || this.state.ethnicity_id == 0) {

        //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Select Ethnicity tags!', ToastAndroid.SHORT)
      : Alert.alert('Select Ethnicity tags!')



        return false;
      }

      else if (this.state.body_type_id < 0 || this.state.body_type_id ==="key0" || this.state.body_type_id == 0 ) {

        //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Select Body Type tags!', ToastAndroid.SHORT)
      : Alert.alert('Select Body Type tags!')



        return false;
      }
      // else if(this.state.bid_price.trim().length === 0){

      //  // ToastAndroid.show('Enter bid price', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter bid price', ToastAndroid.SHORT)
      // : Alert.alert('Enter bid price')



      //   return false;
      // }

      // else if(!bid_valid){
      //   //ToastAndroid.show("Enter valid bid price", ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter valid bid price', ToastAndroid.SHORT)
      // : Alert.alert('Enter valid bid price')



      //   return false;
      // }
      // else if(this.state.bid_price < 10 || this.state.bid_price == 0){

      //  // ToastAndroid.show('Enter bid price greater than 10', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter bid price greater than 10', ToastAndroid.SHORT)
      // : Alert.alert('Enter bid price greater than 10')



      //   return false;
      // }
      // else if (this.state.interest_boolean.filter(Boolean).length < 0 || this.state.interest_boolean.filter(Boolean).length == 0  ) {

      //   //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT)
      // : Alert.alert('Select Interest tags!')



      //   return false;
      // }

      return valid;
  }



    onCreate(){

      if (this.isValid()) {

        //making paramters for the interest tag
        // var length = this.state.interest_boolean.length;
        // var final= "";
        // for(var i = 0 ; i < length ; i++){
        //   if(this.state.interest_boolean[i] === false){
        //    //do nothing

        //   }
        //   else{
        //     let temp = this.state.interest_id[i].toString()
        //     final = final + temp
        //     final = final + ","
        //   }
        // }
        // var final_interest = final.substring(0, final.length-1);
        //ToastAndroid.show("ToastItems"+final, ToastAndroid.SHORT);
       // var arr = [this.state.photoone,this.state.phototwo,this.state.photothree,this.state.photofour]

       this.setState({loading_status:true})
       var formData = new FormData();


       var result  = this.props.navigation.getParam('result')

     //  ToastAndroid.show("ToastItems//////........"+this.state.user_id, ToastAndroid.SHORT)


        formData.append('user_id',result["user_id"]);
        formData.append('display_name', this.state.name);
        formData.append('weight', this.state.weight);

        formData.append('city',this.state.city_id);
        formData.append('state', this.state.state_id);
        formData.append('country', this.state.country_id);


        //formData.append('bid_price', this.state.bid_price);
        formData.append('height', this.state.height);
        formData.append('age', this.state.age_id);
        formData.append('image_one',this.state.photoone);
        formData.append('image_two',this.state.phototwo);
        formData.append('image_three',this.state.photothree);
        formData.append('image_four',this.state.photofour);
        if(this.state.about !=null){
          formData.append('about', this.state.about);
        }
        // else{
        //   formData.append('about', this.state.about);
        // }
       // formData.append('interest_tag', final_interest);
        formData.append('profile_pic', this.state. mainphoto);
        formData.append('ethnicities',this.state.ethnicity_id);
        formData.append('body_type', this.state.body_type_id);
   //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);

                 let url = urls.base_url +'api/update_profile'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false})
                   // Alert.alert(responseJson);
                   // console.log(responseJson)
                    if(!responseJson.error){
                          //success in inserting data
                          ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                           //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                           var id = responseJson.result.id
                           var roles_id = responseJson.result.roles_id
                           var state = responseJson.result.state.name;
                           var name = responseJson.result.name
                           var user_image = responseJson.result.user_image
                           var age = responseJson.result.age



                             AsyncStorage.multiSet([
                               ['user_id',id.toString()],
                               ['roles_id', roles_id.toString()],
                               ["state", state],
                               ["user_name", name],
                               ["user_image", user_image],
                               ["user_age", age.toString()]

                             ]);


                             this.props.navigation.navigate('HomeScreen');
                             const resetAction = StackActions.reset({
                             index: 0,
                             key: 'HomeScreen',
                             actions: [NavigationActions.navigate({ routeName: 'HomePage' })],
                           });

                           this.props.navigation.dispatch(resetAction);

                           //  this.props.navigation.navigate('HomeModel');




                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            //this.setState({error:responseJson.message})
                            ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                          }

                    }).catch((error) => {
                      console.error(error);
                      //this.props.navigation.navigate("NoNetwork")
                      // Platform.OS === 'android'
                      // ?   ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT)
                      // : Alert.alert("Connection Error !")


                      Platform.OS === 'android'
                      ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")


                      return;
                    });


       }

 }




    async componentWillMount() {
     // StatusBar.setBackgroundColor('#0040FF')

     AsyncStorage.getItem('lang')
     .then((item) => {
               if (item) {
                 I18n.locale = item.toString()
             }
             else {
                  I18n.locale = 'en'
               }
     });



     this.fetchCountries()

     var result  = this.props.navigation.getParam('result')

    //ToastAndroid.show(JSON.stringify(result),ToastAndroid.LONG)

     var age = result ['age']

     await this.setState({
       name : result["name"],
       about : result["about"],
       image: result["image"],
       height: result["height"].toString(),
       weight: result["weight"].toString(),
       age: result["age"],
       about: result["about"],
       gallery : result['gallery'],
       user_id : result['user_id'],
        country_id:result['country_id'],
        city_id:result['city_id'],
        state_id:result['state_id'],
        age_id:age.toString(),




     })

     await this.fetchEthnicity()
     await this.fetchBodyType()

     await this.getStates(result['country_id'])
     await this.getCity(result['state_id'])


   // ToastAndroid.show(result['gallery'].length.toString(),ToastAndroid.LONG)
     await this.setState({
      city_id:result['city_id'],
      state_id:result['state_id'],
      ethnicity_id:result['ethnicity_id'],
        body_type_id:result['body_type_id']

     })





    }




downloadFile() {
  let dirs = RNFetchBlob.fs.dirs;
  const filePath = `${dirs.DocumentDir}`;
  var filename = this.state.invoiceUrl.substring(this.state.invoiceUrl.lastIndexOf('/')+1);
  RNFetchBlob.config({
      path:`${dirs.DownloadDir}/${filename}`,
      fileCache:false
      // addAndroidDownloads: {
      //     notification : true,
      //     useDownloadManager : true,
      //     description: 'TaxiJo Payment Invoice',
      //     mime:'application/pdf',
      //     mediaScannable:true,
      //     path:`${dirs.DownloadDir}/${filename}`
      // },
  })
  .fetch('GET',this.state.invoiceUrl,{
      'Cache-Control' : 'no-store'
  })
  .progress({ interval: 250 },(received,total)=>{
      console.log('progress',received/total);
      this.setState({
          downloadProgress:(received/total)*100
      })
  })
  .then(res=>{
      // RNFetchBlob.fs.stat(res.path()).then(stats=>{
      //     console.log(stats);
      // }).catch(err=>{
      //     console.log('error while getting mimetypes');
      // })
      this.setState({
          downloadProgress:0
      })
      // RNFetchBlob.fs.exists(res.path()).then(exist=>{
      //     console.log(`file ${exist ? '' : 'not'} exists`)
      // }).catch(
      //     err=>console.log('error while checking existance',err)
      // );
      if(Platform.OS === 'ios'){
          RNFetchBlob.ios.openDocument(res.path());
      }else{
          RNFetchBlob.android.actionViewIntent(res.path(),"application/pdf");
      }
  })
  .catch((errorMessage,statusCode)=>{
      console.log("error with downloading file",errorMessage)
  })
}

    consume(stream, total = 0) {
      while (stream.state === "readable") {
        var data = stream.read()
        total += data.byteLength;
        console.log("received " + data.byteLength + " bytes (" + total + " bytes in total).")
      }
      if (stream.state === "waiting") {
        stream.ready.then(() => consume(stream, total))
      }
      return stream.closed
    }

    uploadVideo = (videoUri) => {
     var formData = new FormData();

      var video={
        name: 'name.mp4',
        uri: videoUri.uri,
        type: 'video/mp4'
      }



       var result  = this.props.navigation.getParam('result')

         formData.append('user_id',result["user_id"]);
         formData.append('video', video);
        // formData.append('user_id',5);
        // formData.append('video', video);
        // formData.append('media_type',2)
        //  formData.append('title', 'aaaa');
        //  formData.append('description','sdfsdfsdfsdfsdfsdfsdfsdfdsf');






         console.log("para-----",JSON.stringify(formData))

                      this.setState({loading_status:true,
                        overlay_video:false})

               let url = urls.base_url +'api/video_upload'
                     //let url = 'http://webmobril.org/dev/cannabis/api/create_post'




                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'multipart/form-data',
                      },
                      body: formData

                    }, (progressEvent) => {
                      const progress = progressEvent.loaded / progressEvent.total;
                      console.log("progeessss",progress);
                  }).then((response) =>response.json())
                          .then((responseJson) => {
                            console.log("RESONSE---",responseJson)
                     this.setState({loading_status:false})

                             if(!responseJson.error){

                                Platform.OS === 'android'
                              ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
                              : Alert.alert('Video added successfully')

                              this.props.navigation.navigate('EditProfile');
                              const resetAction = StackActions.reset({
                              index: 0,
                              key: 'EditProfile',
                              actions: [NavigationActions.navigate({ routeName: 'ProfileModel' })],
                            });

                            this.props.navigation.dispatch(resetAction);

                            }
                            else{

                              Platform.OS === 'android'
                              ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)
                            }
                          }).catch((error) => {
                            this.setState({loading_status:false})

                            console.log("RESONSE---",error)

                            Platform.OS === 'android'
                            ?   ToastAndroid.show("Upload failed ! Try Again", ToastAndroid.SHORT)
                            : Alert.alert("Upload failed ! Try Again")
                          });


                  //RN FETCH BLOB



  //   var tempParam =
  //   [{
  //     name: 'video', filename: 'vid.mp4', data: RNFetchBlob.wrap(videoUri.uri), type:'video/mp4'
  //   },
  //   {
  //     name:'user_id',data:'1060'
  //   }]

  //  console.log(JSON.stringify(tempParam))

  //    RNFetchBlob.config({timeout:300000}).fetch('POST', url, {
  //     'Content-Type' : 'multipart/form-data'
  //   },
  //   [{
  //     name: 'video',
  //     filename: 'vid.mp4',
  //     data: RNFetchBlob.wrap(videoUri.uri),
  //      type:'video/mp4'
  //   },
  //   {
  //     name:'user_id',
  //     data:'1060'
  //   }]
  //   )
  //   .uploadProgress((written, total) => {
  //     var perc = ((((written / total)*100).toFixed(2))*1) +  "%"
  //     console.log('uploaded', perc)

  //     Platform.OS === 'android'
  //     ?  ToastAndroid.show('Uploaded..'+perc.toString(), ToastAndroid.SHORT)
  //     : Alert.alert('Uploaded..'+perc.toString())
  //   })
  //   .then((response) => response.json())
  //   .then((responseJson) => {
  //     if(!responseJson.error){
  //       this.setState({loading_status:false})

  //                   Platform.OS === 'android'
  //                 ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
  //                 : Alert.alert('Video added successfully')

  //               }
  //               else{

  //                 Platform.OS === 'android'
  //                 ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
  //                 : Alert.alert(responseJson.message)
  //               }
  //   })
  //   .catch((err) => {
  //     this.setState({loading_status:false})
  //     Platform.OS === 'android'
  //                           ?   ToastAndroid.show("Try Again", ToastAndroid.SHORT)
  //                           : Alert.alert("Try Again !")
  //     console.log(err);
  //   })



    //AXIOS


//     Axios.post(url, formData, {onUploadProgress: progressEvent => console.log(progressEvent.loaded)},{
//       headers: {

//           'accept': 'application/json',
//           'Content-Type': `multipart/form-data`
//       }
//   }
// ).then(res => {
//   this.setState({loading_status:false})
//   console.log("ERRRORR",res.data.error)
//       if(!res.data.error){


//                     Platform.OS === 'android'
//                   ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
//                   : Alert.alert('Video added successfully')

//                 }
//                 else{

//                   Platform.OS === 'android'
//                   ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
//                   : Alert.alert(responseJson.message)
//                 }


// })
// .catch(err => {
//   this.setState({loading_status:false})
//   console.log("error1234567",err);
//         Platform.OS === 'android'
//                             ?   ToastAndroid.show("Try Again", ToastAndroid.SHORT)
//                             : Alert.alert("Try Again !")
// });


//XMLREQUESY

//   var xhr = new XMLHttpRequest();


//   xhr.onreadystatechange = (e) => {
//     if (xhr.readyState !== 4) {
//       return;
//     }

//     if (xhr.status === 200) {
//       console.log('SUCCESS', xhr.responseText);
//       callback(JSON.parse(xhr.responseText));
//     } else {
//       console.warn('request_error0',xhr.status.toString());
//     }
//   };

//   xhr.open('POST', url);
//   console.log('OPENED', xhr.status);
//   xhr.send(formData);



//   xhr.onprogress = function () {
//       console.log('LOADING', xhr.status);
//   };

//   xhr.onload = function () {
//       console.log('DONE', xhr.status);
//   };

//  // xhr.setRequestHeader('authorization', this.state.token);

//   if (xhr.upload) {
//   xhr.upload.onprogress = ({ total, loaded }) => console.log("PPP",loaded / total);
//   }



    }



    addVideo() {
      const options = {
        title: 'Video Picker',
        mediaType: 'video',
        videoQuality: 'medium',
        durationLimit: 30,
        takePhotoButtonTitle: 'Take Video...',
        allowsEditing: true,
       // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
         // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);

         // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};
          console.log("VIDEO>>>>",JSON.stringify(response))

          this.setState({
            videoSource: source ,
            overlay_video:true

          });

        //  this.uploadVideo(response.uri)




        }
      });
   }

    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
         // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };


          this.setState({
            mainPhotoSource: source ,
            imageData: response.data,
            mainphoto:photo,

          });


          {/*photo is a file object to send to parameters */}
        }
      });
   }


   selectPhotoTappedOne() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };


        this.setState({

          photoone:photo,
          photocounter:1,
          imageSourceOne: source ,

        });


        {/*photo is a file object to send to parameters */}
      }
    });
 }

 selectPhotoTappedTwo() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        phototwo:photo,
        photocounter:2,
        imageSourceTwo: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}

selectPhotoTappedThree() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        photothree:photo,
        photocounter:3,
        imageSourceThree: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}

selectPhotoTappedFour() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
      //ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        photofour:photo,
        photocounter:4,
        imageSourceFour: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}



selectPhotoRuntime() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
      ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};


      const some_array = [...this.state.gallery]




      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      if(this.state.photoone == null && this.state.gallery[1]  == null){


        //ToastAndroid.show("One",ToastAndroid.LONG)

        some_array[1] = ''
       // this.setState({gallery:some_array,phototwo:null})



        this.setState({

          photoone:photo,
          photocounter:4,
          imageSourceOne: source ,
          gallery:some_array

        });

      }
      else if(this.state.phototwo == null && this.state.gallery[2]  == null ){
        some_array[2] = ''

        this.setState({

          phototwo:photo,
          photocounter:4,
          imageSourceTwo: source ,
          gallery:some_array

        });
      }
      else if(this.state.photothree == null && this.state.gallery[3]  == null ){

        some_array[3] = ''

        this.setState({

          photothree:photo,
          photocounter:4,
          imageSourceThree: source ,
          gallery:some_array

        });
      }
      else if(this.state.photofour == null && this.state.gallery[4]  == null){


        some_array[4] = ''


        this.setState({

          photofour:photo,
          photocounter:4,
          imageSourceFour: source ,
          gallery:some_array


        });
      }




      {/*photo is a file object to send to parameters */}
    }
  });
}




  gallery(){
          var length = this.state.gallery.length
          for(var i = 0 ; i < length ; i++){
          //  ToastAndroid.show("Index..."+length,ToastAndroid.LONG)
           return (
             <View>

             <View style={{height:80,width:80,borderRadius:40}}>
             <Image source={{uri:urls.base_url+this.state.gallery[i].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
             </View>

             <View style={{position:'absolute',left:50}}>
             <TouchableWithoutFeedback>
                <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
                </TouchableWithoutFeedback>

             </View>


             <View style={{position:'absolute',left:50,bottom:7}}>
             <TouchableWithoutFeedback>
                <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
                </TouchableWithoutFeedback>

             </View>


             </View>
           );
          }
        };



        check(){

          if(this.state.mainPhotoSource === null && this.state.gallery.length == 0){
            return (<Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}}  />)
          }
          else if(this.state.mainPhotoSource === null && this.state.gallery.length > 0){
            return (<Image source={{uri:urls.base_url+this.state.gallery[0].image}}
            resizeMode='contain'
            style={{alignSelf:'center',height:'100%',width:'100%'}}  />)
          }
          else{
              return (<Image source={this.state.mainPhotoSource} 
              resizeMode='contain'
               style={{alignSelf:'center',height:'100%',width:'100%'}} />)
          }


        }

        ethnicityname(value){
          for(var i = 0 ; i < this.state.ethnicity.length ; i++){
            if(this.state.ethnicity[i].id === value){
              //ToastAndroid.show("IDDDDD.."+ this.state.data[i].key, ToastAndroid.LONG);
              this.setState({ethnicity_name:this.state.ethnicity[i].name})
            }
          }
        }

        bodyname(value){
          for(var i = 0 ; i < this.state.body.length ; i++){
            if(this.state.body[i].id === value){
              //ToastAndroid.show("IDDDDD.."+ this.state.data[i].key, ToastAndroid.LONG);
              this.setState({body_name:this.state.body[i].name})
            }
          }
        }

        checkImage(){
          var counter = 0;


              if(this.state.photoone == null && this.state.gallery[1] == null ){

              }
              else{
                counter = counter + 1
              }
              if(this.state.phototwo == null  && this.state.gallery[2] == null ){

              }
              else{
                counter = counter + 1
              }

              if(this.state.photothree == null  && this.state.gallery[3] == null ){

              }
              else{
                counter = counter + 1
              }

              if(this.state.photofour == null  && this.state.gallery[4] == null ){

              }
              else{
                counter = counter + 1
              }





                    if(counter > 3){
                      Platform.OS == 'android'?

                      ToastAndroid.show("You can't upload more images.",ToastAndroid.SHORT)
                      :
                      Alert.alert("You can't upload more images.")
                    }
                    else{
                      this.selectPhotoRuntime()
                    }

        }

    render() {
      const age =[
        {
          "id":18,
          "value":18
        },
        {
          "id":19,
          "value":19
        },
        {
          "id":20,
          "value":20
        },
        {
          "id":21,
          "value":21
        },
        {
          "id":22,
          "value":22
        },
        {
          "id":23,
          "value":23
        },
        {
          "id":24,
          "value":24
        },
        {
          "id":25,
          "value":25
        },
        {
          "id":26,
          "value":26
        },
        {
          "id":27,
          "value":27
        },
        {
          "id":28,
          "value":28
        },
        {
          "id":29,
          "value":29
        },
        {
          "id":30,
          "value":30
        },
        {
          "id":31,
          "value":31
        },
        {
          "id":32,
          "value":32
        },
        {
          "id":33,
          "value":33
        },
        {
          "id":34,
          "value":34
        },
        {
          "id":35,
          "value":35
        },
        {
          "id":36,
          "value":36
        },
        {
          "id":37,
          "value":37
        },
        {
          "id":38,
          "value":38
        },
        {
          "id":39,
          "value":39
        },
        {
          "id":40,
          "value":40
        },
        {
          "id":41,
          "value":41
        },
        {
          "id":42,
          "value":42
        },
        {
          "id":43,
          "value":43
        },
        {
          "id":44,
          "value":44
        },
        {
          "id":45,
          "value":45
        },
        {
          "id":46,
          "value":47
        },
        {
          "id":48,
          "value":48
        },
        {
          "id":49,
          "value":49
        },
        {
          "id":50,
          "value":50
        },
        {
          "id":51,
          "value":51
        },
        {
          "id":52,
          "value":52
        },
        {
          "id":53,
          "value":53
        },
        {
          "id":54,
          "value":54
        },
        {
          "id":55,
          "value":55
        },
        {
          "id":56,
          "value":56
        },
        {
          "id":57,
          "value":57
        },
        {
          "id":58,
          "value":58
        },
        {
          "id":59,
          "value":59
        },
        {
          "id":60,
          "value":60
        },
        {
          "id":61,
          "value":61
        },
        {
          "id":62,
          "value":62
        },
        {
          "id":63,
          "value":63
        },
        {
          "id":64,
          "value":64
        },
        {
          "id":65,
          "value":65
        },
        {
          "id":66,
          "value":66
        },
        {
          "id":67,
          "value":67
        },
        {
          "id":68,
          "value":68
        },
        {
          "id":69,
          "value":69
        },
        {
          "id":70,
          "value":70
        },
        {
          "id":71,
          "value":71
        },
        {
          "id":72,
          "value":72
        },
        {
          "id":73,
          "value":73
        },
        {
          "id":74,
          "value":74
        },
        {
          "id":75,
          "value":75
        },
        {
          "id":76,
          "value":76
        },
        {
          "id":77,
          "value":77
        },
        {
          "id":78,
          "value":78
        },
        {
          "id":79,
          "value":79
        },
        {
          "id":80,
          "value":80
        },
        {
          "id":81,
          "value":81
        },
        {
          "id":82,
          "value":82
        },
        {
          "id":83,
          "value":83
        },
        {
          "id":84,
          "value":84
        },
        {
          "id":85,
          "value":85
        },
        {
          "id":86,
          "value":86
        },
        {
          "id":87,
          "value":87
        },
        {
          "id":88,
          "value":88
        },
        {
          "id":89,
          "value":89
        },
        {
          "id":90,
          "value":90
        },

      ];

      let makeages =age.map((age) => {
        return (
          <Item label={age.value.toString()} value={age.id.toString()} key={age.id}/>
        )
    })



      let makecountries =this.state.countries.map((country) => {
        return (
          <Item label={country.name} value={country.id} key={country.id}/>
        )
    })

        let makestates =this.state.states.map((state) => {
          return (
            <Item label={state.name} value={state.id} key={state.id}/>
          )
      })


        let makecities =this.state.cities.map((city) => {
          return (
            <Item label={city.name} value={city.id} key={city.id}/>
          )
      })


      let makeethnicity =this.state.ethnicity.map((ethnicity) => {
        return (
          <Item label={ethnicity.name} value={ethnicity.id} key={ethnicity.id}/>
        )
      })

      let makebody =this.state.body.map((body) => {
        return (
          <Item label={body.name} value={body.id} key={body.id}/>
        )
      })


        return (

          <KeyboardAvoidingView style={styles.container}  behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>



         <Overlay
         isVisible={this.state.overlay_video}
         windowBackgroundColor="rgba(0, 0, 0, .5)"
         overlayBackgroundColor="white"
         width={300}
         height='auto'

       >




         <View style={{height:400,width:null,backgroundColor:'white',alignItems:'center',justifyContent:'center',padding:25}}>

         <VideoPlayer source={this.state.videoSource}  // Can be a URL or a local file.

                                           // Store reference
        style={{height:'50%',width:'100%'}} />

          <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

          <View style={{marginTop:15,borderColor:'black',borderWidth:1,
        borderRadius:10,backgroundColor:colors.color_primary,flex:1,justifyContent:'center',alignItems:'center',padding:10}}>

        <Text onPress={()=> this.uploadVideo(this.state.videoSource)
        }>{I18n.t('upload')}</Text>
        </View>


          </View>

         </View>

         <View style={{position:'absolute',right:5,top:5}}>
         <TouchableWithoutFeedback onPress={()=> this.setState({
           overlay_video:false
         })}>
         <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:20,width:20}} />
         </TouchableWithoutFeedback>
         </View>



         </Overlay>


                  {/*for header*/}
                  <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('edit_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />
       {/*for main content*/}

       <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

                <View style={{height:Dimensions.get('window').height * 0.5,
                width:'100%',borderBottomColor:'black',borderBottomWidth:1,backgroundColor:'grey'}}>

              {
                this.check()
              }

                </View>

                <TouchableWithoutFeedback onPress={this.selectPhotoTapped.bind(this)}>
                <Image style={{width: 45, height: 45,marginTop:-60,alignSelf:'flex-end',marginBottom:20,marginRight:20}}  source={require('../assets/pen.png')} />
                </TouchableWithoutFeedback>

          <View style={{width:'100%',height:'100%',padding:20}}>





          <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
          {
            this.state.gallery[1] == null
            ? null
            :   <View>

            <View style={{height:70,width:70,borderRadius:35}}>
                {
                  this.state.photoone == null
                  ? <Image source={{uri:urls.base_url +this.state.gallery[1].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
                  : <Image source={this.state.imageSourceOne} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
                }

            </View>

            <View style={{position:'absolute',left:50}}>
            <TouchableWithoutFeedback onPress={this.selectPhotoTappedOne.bind(this)}>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

 {/*
            <View style={{position:'absolute',left:50,bottom:7}}>
            <TouchableWithoutFeedback onPress={
              ()=>{
                  const some_array = [...this.state.gallery]
                  some_array[1] = null
                  this.setState({gallery:some_array,photoone:null})
              }
            }>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

            */}



            </View>

          }


          {
            this.state.gallery[2] == null
            ? null
            :   <View>

            <View style={{height:70,width:70,borderRadius:35}}>

            {
              this.state.phototwo == null
              ? <Image source={{uri:urls.base_url+this.state.gallery[2].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
              : <Image source={this.state.imageSourceTwo} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
            }


            </View>

            <View style={{position:'absolute',left:50}}>
            <TouchableWithoutFeedback onPress={this.selectPhotoTappedTwo.bind(this)}>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>


          {/*
            <View style={{position:'absolute',left:50,bottom:7}}>
            <TouchableWithoutFeedback onPress={
              ()=>{
                  const some_array = [...this.state.gallery]
                  some_array[2] = null
                  this.setState({gallery:some_array,phototwo:null})
              }
            }>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

          */}



            </View>

          }

          {
            this.state.gallery[3] == null
            ? null
            :   <View>

            <View style={{height:70,width:70,borderRadius:35}}>
            {
              this.state.photothree == null
              ? <Image source={{uri:urls.base_url+this.state.gallery[3].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
              : <Image source={this.state.imageSourceThree} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
            }
            </View>

            <View style={{position:'absolute',left:50}}>
            <TouchableWithoutFeedback onPress={this.selectPhotoTappedThree.bind(this)}>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>


         {/*             <View style={{position:'absolute',left:50,bottom:7}}>
            <TouchableWithoutFeedback onPress={
              ()=>{
                  const some_array = [...this.state.gallery]
                  some_array[3] = null
                  this.setState({gallery:some_array,photothree:null})
              }
            }>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

            */}




            </View>

          }

          {
            this.state.gallery[4] == null
            ? null
            :   <View>

            <View style={{height:70,width:70,borderRadius:35}}>
            {
              this.state.photofour == null
              ? <Image source={{uri:urls.base_url + this.state.gallery[4].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
              : <Image source={this.state.imageSourceFour} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
            }
            </View>

            <View style={{position:'absolute',left:50}}>
            <TouchableWithoutFeedback onPress={this.selectPhotoTappedFour.bind(this)}>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

{/*
            <View style={{position:'absolute',left:50,bottom:7}}>
            <TouchableWithoutFeedback onPress={
              ()=>{
                  const some_array = [...this.state.gallery]
                  some_array[4] = null
                  this.setState({gallery:some_array,photofour:null})
              }
            }>
               <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/pen.png')} />
               </TouchableWithoutFeedback>

            </View>

          */}



            </View>

          }




          </View>


          <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15,marginBottom:10}}>
          <View style={{borderColor:'black',borderRadius:10,borderWidth:1,width:null,alignSelf:'flex-end',margin:10,backgroundColor:colors.color_primary}}>
          <Text style={{margin:10,fontWeight:'bold',size:18}} onPress={()=> this.checkImage()}>{I18n.t('add_images')}</Text>
          </View>

          {/*

          <View style={{borderColor:'black',borderRadius:10,borderWidth:1,width:null,alignSelf:'flex-start',margin:10,backgroundColor:colors.color_primary}}>
          <Text style={{margin:10,fontWeight:'bold',size:18}} onPress={()=> this.addVideo()}>{I18n.t('add_video')}</Text>
          </View>

          */}
          </View>



               <Text style={styles.name_text}>{I18n.t('name')}</Text>
                <TextInput
                  value={this.state.name}
                  onChangeText={(name) => this.setState({ name })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />



          <Text style={styles.name_text}>{I18n.t('height_cms')}</Text>
           <TextInput
             value={this.state.height}
             onChangeText={(height) => this.setState({ height })}
             keyboardType = 'numeric'
             maxLength = {3}
            textContentType='telephoneNumber'
             style={styles.name_input}
             placeholderTextColor={'black'}
           />



          <Text style={styles.name_text}>{I18n.t('weight_kgs')}</Text>
           <TextInput
             value={this.state.weight}
             keyboardType = 'numeric'
             maxLength = {3}
            textContentType='telephoneNumber'
             onChangeText={(weight) => this.setState({weight })}
             style={styles.name_input}
             placeholderTextColor={'black'}
           />


                <Text style={styles.name_text}>{I18n.t('age')}</Text>
                <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                            <Picker

                                  mode="dropdown"
                                  selectedValue={this.state.age_id}
                                  onValueChange={(itemValue, itemIndex) =>
                                    {

                                        this.setState({age_id: itemValue})


                                    }}>
                                  <Item label="Select age" value="key0" />
                                    {makeages}
                          </Picker>
                    </View>


                <Text style={styles.name_text}>{I18n.t('country')}</Text>
        <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker

                          mode="dropdown"
                          selectedValue={this.state.country_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              this.setState({country_id: itemValue,state_id:0,city_id:0,cities:[],states:[]})
                              if(itemIndex > 0){

                                this.getStates(itemValue)
                              }
                            }}>
                          <Item label="Select Country" value="key0" />
                             {makecountries}
                  </Picker>
            </View>

            <Text style={styles.name_text}>{I18n.t('state')}</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                        <Picker

                              mode="dropdown"
                              selectedValue={this.state.state_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {
                                  this.setState({state_id: itemValue,city_id:0})
                                  if(itemIndex > 0){

                                    this.getCity(itemValue)
                                  }
                                }}>
                              <Item label="Select State" value="key0" />
                                {makestates}
                      </Picker>
                </View>


                <Text style={styles.name_text}>{I18n.t('city')}</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                        <Picker

                              mode="dropdown"
                              selectedValue={this.state.city_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {

                                    this.setState({city_id: itemValue})


                                }}>
                              <Item label="Select City" value="key0" />
                                {makecities}
                      </Picker>
                </View>


                <Text style={styles.name_text}>{I18n.t('ethnicity')}</Text>
                <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker

                          mode="dropdown"
                          selectedValue={this.state.ethnicity_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              this.setState({ethnicity_id: itemValue})
                              this.ethnicityname(itemValue)

                            }}>
                          <Item label="Select Ethnicity" value="key0" />
                             {makeethnicity}
                  </Picker>
            </View>


                <Text style={{ width:'100%', marginBottom:10,fontSize:fonts.font_size,marginTop:10}}>{I18n.t('body_type')}</Text>
                <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                <Picker

                      mode="dropdown"
                      selectedValue={this.state.body_type_id}
                      onValueChange={(itemValue, itemIndex) =>
                        {
                          this.setState({body_type_id: itemValue})
                          this.bodyname(itemValue)


                        }}>
                      <Item label="Select Body Type" value="key0" />
                         {makebody}
              </Picker>
        </View>

                   <Text style={styles.about_text}>{I18n.t('about_bio')}</Text>
                <TextInput
                  value={this.state.about}
                  onChangeText={(about) => this.setState({ about })}
                  style={styles.about_input}
                  multiline={true}
                  placeholderTextColor={'black'}
                />



                <TouchableOpacity
                onPress={()=> this.onCreate()}
                      style={styles.saveButton}>
                      <Text style={styles.saveText}>{I18n.t('save').toUpperCase()}</Text>
                </TouchableOpacity>
     </View>
    </View>

    </ScrollView>

    {this.state.loading_status &&
      <View pointerEvents="none" style={styles.loading}>
        <ActivityIndicator size='large' />
      </View>
  }


        </KeyboardAvoidingView>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:0,

  },
 name_text:{
  width:'100%',
 marginBottom:10,
  fontSize:fonts.font_size

 },

 name_input: {
  width: "100%",
  height: 50,
padding:5,
 borderColor:'black',
 borderWidth:1,
borderRadius:8,
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
 },

 about_input: {
  width: "100%",
  height: 120,
  padding:3,
  textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }




}
)
