import React, {Component} from 'react';
import {Text, View,Modal, Image,Dimensions,ToastAndroid,StatusBar,
  StyleSheet,Platform,ActivityIndicator,ImageBackground,TouchableOpacity,
  TextInput,TouchableWithoutFeedback,Alert,PermissionsAndroid,FlatList,ScrollView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,urls } from './Variables';
import DismissKeyboard from 'dismissKeyboard';
import {  Card,Item,   Picker,} from "native-base";
import I18n from '../i18n';
import AsyncStorage from '@react-native-community/async-storage';

import {Header} from 'react-native-elements';



import Geocoder from 'react-native-geocoder';


const mapStyle =[
  [
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.government",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.medical",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.place_of_worship",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.school",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ]
]

export default class SendLocation extends React.Component {
    constructor(props) {
        super(props);
        this.state ={

          loading_status:false,
          region: {
            latitude: 0.0,
            longitude: 0.0,
            latitudeDelta: 0.2,
            longitudeDelta: 0.9
          },
          coordinates:{
            latitude: 0.0,
            longitude: 0.0,
          },
          current_latitude:0.00,
          current_longitude:0.00,
          nearby_latitude: 0.0,
          nearby_longitude: 0.0,
          current_address:'',
          nearby_address:'',
            myPlaces:[]

      };


    }




    getNearby(){



     //https://maps.googleapis.com/maps/api/place/nearbysearch/json?
      //location=28.8670,77.1957&radius=3500&types=restaurant&key=AIzaSyBx5f8NnFiA2kEv7ZcFJVtUs0_6TfZaMPw

          let url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+this.state.current_latitude+','+this.state.current_longitude+'&radius=3500&types=restaurant,bar,food&key=AIzaSyBx5f8NnFiA2kEv7ZcFJVtUs0_6TfZaMPw'
             fetch(url, {
             method: 'GET',
             headers: {
               'Accept': 'application/json',
               'Content-Type':  'multipart/form-data',
             },


           }).then((response) => response.json())
                 .then((responseJson) => {
                   this.setState({loading_status:false})
                  //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                  var places = responseJson.results

                  this.setState({
                    myPlaces:places
                  })
                  //result[0].name
                  //result[0].icon
                   //result[0].geometry.location.lat
                    //result[0].geometry.location.lng



                 }).catch((error) => {
                  // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                   Platform.OS === 'android'
                   ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                   : Alert.alert("Connection Error !")


                 })


                }
       //this one current
  getaddress(lat,long){
    // Position Geocoding
              var NY = {
                lat:lat,
                lng: long
              };



              Geocoder.geocodePosition(NY).then(res => {
                //  ToastAndroid.show(JSON.stringify(res), ToastAndroid.LONG);
                var addr  = res[0].formattedAddress
                  this.setState({current_address: addr.toString()})
                  this.getNearby()


              })
              .catch(err =>{
               // ToastAndroid.show("Cannot get this location !",ToastAndroid.SHORT);
                Platform.OS === 'android'
                ? ToastAndroid.show("Cannot get this location !", ToastAndroid.SHORT)
                : Alert.alert("Cannot get this location !")
              this.setState({loading_status:false});
              })
  }

       async  requestLocationPermission(){
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              'title': 'Example App',
              'message': 'Example App access to your location '
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {


            navigator.geolocation.getCurrentPosition(
              position => {
                this.setState({
                  region: {
                   latitude: position.coords.latitude,
                   longitude: position.coords.longitude,
                   latitudeDelta: 0.23,
                   longitudeDelta: 0.5,
                   accuracy: position.coords.accuracy
                 },
                 coordinates:{
                   latitude: position.coords.latitude,
                   longitude: position.coords.longitude
                 },
                 current_latitude: position.coords.latitude,
                 current_longitude: position.coords.longitude,


                });
                this.getaddress(position.coords.latitude, position.coords.longitude)
              },
              error => {
              //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

                Platform.OS === 'android'
                ? ToastAndroid.show(error.message, ToastAndroid.SHORT)
                : Alert.alert("Check your internet speed connection")


                this.setState({loading_status:false})
              },
              { enableHighAccuracy: false, timeout: 10000 }

            );


          } else {
            console.log("location permission denied")
            Alert.alert("location permission denied");
            //alert("Location permission denied");
          }
        } catch (err) {
          console.warn(err)
          Alert.alert(err.message);
        }
      }


      //https://maps.googleapis.com/maps/api/place/nearbysearch/json?
      //location=28.8670,77.1957&radius=3500&types=restaurant&key=AIzaSyBx5f8NnFiA2kEv7ZcFJVtUs0_6TfZaMPw

        async  requestLocationPermissionIOS(){


            navigator.geolocation.getCurrentPosition(
              position => {
                this.setState({
                  region: {
                   latitude: position.coords.latitude,
                   longitude: position.coords.longitude,
                   latitudeDelta: 0.23,
                   longitudeDelta: 0.5,
                   accuracy: position.coords.accuracy
                 },
                 coordinates:{
                   latitude: position.coords.latitude,
                   longitude: position.coords.longitude
                 },
                 current_latitude: position.coords.latitude,
                 current_longitude: position.coords.longitude,

                });
                this.getaddress(position.coords.latitude, position.coords.longitude)
              },
              error => {
              //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

                Platform.OS === 'android'
                ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
                : Alert.alert("Check your internet speed connection")


                this.setState({loading_status:false})
              },
              { enableHighAccuracy: false, timeout: 10000 }
            );
      }



async componentDidMount(){
  Platform.OS === 'android'
  ?  await this.requestLocationPermission()
  :  await this.requestLocationPermissionIOS()


  AsyncStorage.getItem('user_id')
  .then((item) => {
            if (item) {

                    this.setState({user_id:item})

          }
          else {
            ToastAndroid.show("Error !")
            }
  });


}


      sendLocation(addr,lat,lng){

        const { navigation } = this.props;
              navigation.goBack();

       // ToastAndroid.show(addr+',,'+lat+'...'+lng,ToastAndroid.SHORT)
       navigation.state.params.onSelect({ address: addr,
        latitude:lat,
        longtitude:lng,
    })

      }



    render() {

        return (

         <View
         style={styles.container}>

         {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('send_location'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />






       {/*for main content*/}


           <ScrollView style={styles.body}>





          <Text style={{margin:10,fontSize:19,fontWeight:'bold',color:colors.color_secondary}}
          >{I18n.t('your_current_location')}</Text>


        <View style={{flexDirection:'row',alignSelf:'center',flex:5,padding:3}}>
         <View style={{width:null,height:'auto',padding:10,flex:4,
         borderColor:'black',borderRadius:10,borderWidth:1,alignItems:'center',justifyContent:'center',backgroundColor:'silver'}}>

                <Text style={{color:'black'}}>{this.state.current_address}</Text>
         </View>

          <TouchableOpacity onPress={()=> {
            this.sendLocation(this.state.current_address,this.state.current_latitude,this.state.current_longitude)
          }}>
            <Image style={{width: 35, height: 35,margin:10}}  source={require('../assets/location-send.png')} />


         </TouchableOpacity>


         </View>


         <Text style={{margin:10,fontSize:19,fontWeight:'bold',color:colors.color_secondary}}>{I18n.t('nearby_favourite_places')}</Text>


         <FlatList
							  style={{marginBottom:10}}
                data={this.state.myPlaces}

							  showsVerticalScrollIndicator={false}
							  scrollEnabled={false}
							  renderItem={({item}) =>


                 <View style={{width:'98%',margin:5,flexDirection:'row',justifyContent:'space-between',
                 alignItems:'center',borderColor:'black',borderRadius:10,borderWidth:0.4,padding:7}}>

                        <View style={{flexDirection:'row',alignItems:'center'}}>
                          <Image source={{uri:item.icon}} resizeMode='contain'
                           style={{height:30,width:30,marginLeft:5,marginRight:9}}/>
                          <Text style={{width:'65%',color:'black'}}>{item.name}</Text>
                        </View>


                        <TouchableOpacity onPress={()=> this.sendLocation(item.name,item.geometry.location.lat,item.geometry.location.lng)}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/location-send.png')} />
                        </TouchableOpacity>




                    </View>

							  }
							  keyExtractor={item => item.id}
							/>







           </ScrollView>










         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.8

  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)


{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
