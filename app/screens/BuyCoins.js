import React, {Component} from 'react';
import {Text, View,Modal, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,Platform,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,urls } from './Variables';
import DismissKeyboard from 'dismissKeyboard';
import {  Card,Item,   Picker,} from "native-base";
import I18n from '../i18n';
import AsyncStorage from '@react-native-community/async-storage';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
 ProductPurchase,
 PurchaseError,
 Product
} from 'react-native-iap';
import {Header} from 'react-native-elements';

const itemSkusCoins = Platform.select({
  ios: [
    'com.d8bid.3coins','com.d8bid.5coins'
  ],
  android: [
    'com.example.coins100'
  ]
});

export default class CreateProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          password:'',
          modal_visible:false,
          choose_status:false,
          coins:[],
          coin_id:'key0',
          coin_name:'',
          price:'',
          textstatus:false,
          products:null
      };


    }

    purchaseUpdateSubscription = null
    purchaseErrorSubscription = null

    fetchCoins = async () =>{
      this.setState({loading_status:true})

                      let url = urls.base_url +'api/api_coin'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].no_coins
                              var price = responseJson.result[i].price

                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };
                                    array[i] = { ...array[i], price: price };

                                    temp_arr = array

                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ coins : temp_arr});

                            }


                          else{

                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Cant Connect to Server", ToastAndroid.SHORT)
                            : Alert.alert("Cant Connect to Server")



                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})

                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")

                          });



    }


    iap = async(sku) =>{
      try {
        let a = await RNIap.requestPurchase(sku, false);
       // Alert.alert(JSON.stringify(a))
        console.log(JSON.stringify(a))

       



      } catch (err) {
        console.log("ERROROROROROORORORORO")
        console.warn(err.code, err.message);
      }
    }


    chooseIAP(){

      if(this.state.coin_id.length === 0 || this.state.coin_id ==="key0" || this.state.coin_id == 0){

        // ToastAndroid.show("Choose Coins !", ToastAndroid.SHORT);
 
         Platform.OS === 'android'
         ?  ToastAndroid.show("Choose Coins !", ToastAndroid.SHORT)
         : Alert.alert("Choose Coins !")
         return
       }


       else{
        let coin = this.state.coin_name.toString().substring(0, 1)
            if(coin == 3){
                let sku = this.state.products[0].productId
                this.iap(sku)
                return 
            }
            else if(coin == 5){
                let sku = this.state.products[1].productId
                this.iap(sku)
                return 
            }
      
       }
       
}



    componentWillUnmount() {

      this.setState({
        products:null
      })
      if (this.purchaseUpdateSubscription) {
        this.purchaseUpdateSubscription.remove();
        this.purchaseUpdateSubscription = null;
      }
      if (this.purchaseErrorSubscription) {
        this.purchaseErrorSubscription.remove();
        this.purchaseErrorSubscription = null;
      }
    }
  

    async componentWillMount() {
      try {

        const products: Product[] = await RNIap.getProducts(itemSkusCoins);
      // Alert.alert(JSON.stringify(products))
       console.log("COINS",JSON.stringify(products))


       if(products[0].productId.toString().includes('stars')){
         //console.log("YYYYYYYYYYYYYYYYYYYYYYY")
        let pro = products.slice(5, products.length)
        console.log("YYYYYYYYYYYYYYYYYYYYYYY",JSON.stringify(pro))
        this.setState({
          products:pro
        })
      }
      else{
        this.setState({
          products:products
        })
      }

      //  this.setState({
      //    products:products
      //  })

      // val product_id = products[0].productId;
      // val price = products[0].localizedPrice;
//
      } catch(err) {
        console.warn(err); // standardized err.code and err.message available
      }

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    }

 convertDate(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString();
      var dd  = date.getDate().toString();

      var mmChars = mm.split('');
      var ddChars = dd.split('');

      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    }

    IapServer(coin_id,price,transactionId){
      AsyncStorage.getItem("user_id").then((item) => {
           if (item) {
   
             //ToastAndroid.show("price"+price+"bokingid...."+bid+"uiddd..."+uid+"date..."+date+"....pid"+pid, ToastAndroid.SHORT);
             var formData = new FormData();
             var date = this.convertDate(new Date())
             let coin = this.state.coin_name.toString().substring(0, 1)

             //product_type 1=star, 2=coin
             formData.append('user_id',item);
             formData.append('product_id',coin_id);
             formData.append('product_type',2);
             formData.append('role_id',3);
             formData.append('txn_amt', price);
             formData.append('txn_date', date);
             formData.append('txn_status', 'success');
             formData.append('txn_id',transactionId);
             formData.append('coins',coin);
            
   console.log("FORM",JSON.stringify(formData))
             //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
               this.setState({loading_status:true})
   
   
          let url = urls.base_url +'api/api_apple_pay'
             fetch(url, {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type':  'multipart/form-data',
             },
             body: formData
   
           }).then((response) => response.json())
                 .then((responseJson) => {
                   this.setState({loading_status:false})
                  //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                 if(!responseJson.error){
                       //success in inserting data
   
                                  let coin =  this.state.coin_name.toString().substring(0, 1)

                        AsyncStorage.getItem( 'coins' )
                        .then( data => {

                          // the string value read from AsyncStorage has been assigned to data
                          console.log( data );
                            var c = parseInt(data)
                            c= c + parseInt(coin)

                          //save the value to AsyncStorage again
                            AsyncStorage.setItem( 'coins', c.toString() );

                        }).done();
                          
                              this.props.navigation.navigate('HomePage');
                
   
   
                       Platform.OS === 'android'
                       ?  ToastAndroid.show("Payment Successful ", ToastAndroid.SHORT)
                       : Alert.alert("Payment Successful ")
   
   
                    
   
   
               }else{
   
   
   
                         Platform.OS === 'android'
                         ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                         : Alert.alert(responseJson.message)
                 }
   
   
                 }).catch((error) => {
                  // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                   Platform.OS === 'android'
                   ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                   : Alert.alert("Connection Error !")
   
                 });
           }
           else{
             //ToastAndroid.show("User not found !",ToastAndroid.LONG)
             // Platform.OS === 'android'
             // ?  ToastAndroid.show("User not found !", ToastAndroid.SHORT)
             // : Alert.alert("User not found !")
           }
         });
       }





    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     this.fetchCoins()


     this.purchaseUpdateSubscription = purchaseUpdatedListener((purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase ) => {
      console.log('purchaseUpdatedListener', purchase);
      const receipt = purchase.transactionReceipt;
      if (receipt) {

        RNIap.finishTransactionIOS(purchase.transactionId);
       // Alert.alert("Sucessfully purchased coins !");


       var coin_id = this.state.coin_id
       var coin_price = this.state.price
       var transactionId = purchase.transactionId

       this.IapServer(coin_id,coin_price,transactionId)


    



        // yourAPI.deliverOrDownloadFancyInAppPurchase(purchase.transactionReceipt)
        // .then((deliveryResult) => {
        //   if (isSuccess(deliveryResult)) {
        //     // Tell the store that you have delivered what has been paid for.
        //     // Failure to do this will result in the purchase being refunded on Android and
        //     // the purchase event will reappear on every relaunch of the app until you succeed
        //     // in doing the below. It will also be impossible for the user to purchase consumables
        //     // again untill you do this.
        //     if (Platform.OS === 'ios') {
        //       RNIap.finishTransactionIOS(purchase.transactionId);
        //     } else if (Platform.OS === 'android') {
        //       // If consumable (can be purchased again)
        //       RNIap.consumePurchaseAndroid(purchase.purchaseToken);
        //       // If not consumable
        //       RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
        //     }
        //   } else {
        //     // Retry / conclude the purchase is fraudulent, etc...
        //   }
        // });

        console.log("PURCHASEeeee",purchase.transactionId)
      }
    });
 
    this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
      console.warn('purchaseErrorListener', error);
    });

    }
    continue(){


      if(this.state.coin_id.length === 0 || this.state.coin_id ==="key0" || this.state.coin_id == 0){

       // ToastAndroid.show("Choose Coins !", ToastAndroid.SHORT);

        Platform.OS === 'android'
        ?  ToastAndroid.show("Choose Coins !", ToastAndroid.SHORT)
        : Alert.alert("Choose Coins !")
        return
      }

      else{
        let obj = {
          "price" : this.state.price,
          "role_id": 3,
          "coin":this.state.coin_name.toString().substring(0, 1),
          "coin_id":this.state.coin_id

        }
        this.props.navigation.navigate('PaymentMode',{result :obj});
      }

    }

    coinSelect(itemValue){

      console.log(JSON.stringify(this.state.products))

      if(this.state.coins.length > 0){
        //var jobs = this.state.jobs
        for(var i = 0 ; i < this.state.coins.length ; i++){
          if(this.state.coins[i].id == itemValue){
            this.setState({coin_id: this.state.coins[i].id,
              price:this.state.products[i].price,
              coin_name:this.state.coins[i].name,textstatus:true})
          }
        }
      }

    }


    render() {

      let makecoins =this.state.coins.map((coin) => {
        return (
          <Item label={coin.name} value={coin.id} key={coin.id}/>
        )
    })




        return (

         <View
         style={styles.container}>

         {
           this.state.choose_status == true
           ?
           <View style={{position:'absolute',top:0,bottom:0,right:0,left:0,backgroundColor:'red',flex:1,height:'100%'}}>
           <Text>asdfsdfsd</Text>
           </View>
           :

           null
         }




         {/*for header*/}

         <Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomePage")}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                
          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
           
        centerComponent={{ text: I18n.t('buy_coins'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />






       {/*for main content*/}


           <View style={styles.body}>

           <Modal
            visible={this.state.modal_visible}
            animationType='slide'
            onRequestClose={() => {console.log('Modal has been closed.');}}
            transparent
            >
                  <View style={{height:'100%',width:'100%',backgroundColor:'rgba(0, 0, 0, 0.7)',alignItems:'center',justifyContent:'center'}}>
                  <View style={{height:null,width:300,backgroundColor:'white',alignItems:'center'}}>

                  <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:10,marginBottom:10}}>Redeem 1 coin for chat</Text>

                  <Text style={{color:'black',fontSize:17,marginTop:10,marginBottom:10,marginLeft:10,marginRight:10}}>Redeem 1 coin to lock in accepted bid and recieve models private contact details</Text>
                  <Image source={require('../assets/coin-small.png')} style={{height:60,width:60,marginBottom:30}} resizeMode='contain'/>
                    <TouchableOpacity
                    style={{backgroundColor:colors.color_primary,width:'100%'}}
                      onPress={() => {
                            this.setState({modal_visible:!this.state.modal_visible})
                          }}
                      >
                      <Text style={{color:'black',fontSize:19,fontWeight:'bold',marginTop:14,marginBottom:14,alignSelf:'center'}}>Redeem now</Text>
                      </TouchableOpacity>
                  </View>

                    </View>
          </Modal>

           <View style={{borderRadius:10,borderWidth:1,borderColor:'grey',padding:20}}>


           <Text style={{fontSize:20,color:'black'}}>{I18n.t('buy_coins_to_unlock_conversation')}</Text>
           <View style={{width:'100%',height:1,borderRadius:10,borderWidth:1,borderColor:'grey',marginTop:15,marginBottom:23}}>
           </View>

           <Image source={require('../assets/coin-big.png')} style={{alignSelf:'center',height:130,width:130,marginBottom:30}} resizeMode='contain'/>


              
          


           
           {
            this.state.textstatus
            ?  <Text style={{marginBottom:8,fontSize:17,marginTop:-20,
            color: colors.color_primary,textAlign:'center'}}>{I18n.t('total_price')} : {this.state.products[0].currency} {this.state.price}</Text>
            : null
          }


             <Text style={styles.about_text}>{I18n.t('choose_number_of_coins')}</Text>
             <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker

                          mode="dropdown"
                          selectedValue={this.state.coin_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              if(itemValue > 0){

                                this.coinSelect(itemValue)

                              }
                              else{
                                this.setState({coin_id: 0,price:0,textstatus:false})
                              }
                            }}>
                          
                             {makecoins}
                  </Picker>
            </View>

{/* onPress={this.continue.bind(this) */}

            <TouchableOpacity onPress={()=> this.chooseIAP()}
                  style={styles.saveButton}>
                  <Text style={styles.saveText}>{I18n.t('buy_now')}</Text>
            </TouchableOpacity>
           </View>






         </View>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.8

  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:25,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)


{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
