import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,FlatList,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {Card} from 'native-base';
import { isTSEnumMember } from '@babel/types';
import { StackActions, NavigationActions} from 'react-navigation';



export default class NotificationsActivityMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          peoples:
        [
          {
            id: 1,
            name: 'Neymar Jr.',
            image:require('../assets/dummy/m1.jpg'),
            price:215,
            status:0 //0 dono button //1 accepted //2 rejected
            
          },
          {
            id: 2,
            name: 'Steve Smitth',
            image:require('../assets/dummy/m2.jpg'),
            price:342,
            status:1
            
          },
          {
            id: 3,
            name: 'Lionel Messi',
            image:require('../assets/dummy/m3.jpg'),
            price:212,
            status:2
           
          },
          {
            id: 4,
            name: 'Christiano Ronaldo',
            image:require('../assets/dummy/m4.jpg'),
            price:150,
            status:1
           
          },
        

          ]
          
      };


    }


    itemView(status){
      if(status === 0){

              return(
                              <View style={{flexDirection:'row',justifyContent:'space-around'}}>
                              <TouchableOpacity>
                                        <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/accept.png')} />
                              </TouchableOpacity>

                                <Image style={{width: 45, height: 45,margin:1}}  source={require('../assets/reject.png')} />



                        </View>
              );
      }
      else if(status === 1){
        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'red',borderRadius:8,borderWidth:2}}>
        
        <Text style={{color:'red',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Protest</Text>
        
        </View>
        </TouchableWithoutFeedback>
        )
      }

      else if(status === 2){

        return(
        <TouchableWithoutFeedback>
        <View style={{borderColor:'green',borderRadius:8,borderWidth:2}}>
        
        <Text style={{color:'green',fontSize:17,marginLeft:15,marginRight:15,marginBottom:5,marginTop:5}}>Accept</Text>
        
        </View>
        </TouchableWithoutFeedback>
        )
      }
    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
      
    }
   

    render() {

     



        return (
      
         <View 
         style={styles.container}>


         {/*for header*/}
        

       {/*for main content*/}


           <View style={styles.body}>

              
           <FlatList
							  style={{marginBottom:10}}
                data={this.state.peoples}
							  showsVerticalScrollIndicator={false}
							  scrollEnabled={false}
							  renderItem={({item}) =>

							  
                 <View style={{width:'98%',margin:3}}>
                 
									  <Card style={{width:'100%',padding:10,borderRadius:10}}>
                    <TouchableWithoutFeedback onPress={() =>{
                       result={}
                       result["model_id"] = 714
                      this.props.navigation.navigate("ModelProfileChat",{result:result})}}>


                    
                   
                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image style={{width: 50, height: 50,borderRadius:25,overflow:'hidden',marginRight:10}}  source={item.image} />

                      <View>
                      <Text style={{fontSize:17,color:'black',marginRight:4}}>{item.name}</Text>
                      <Text style={{fontSize:14,color:'black',marginLeft:4,marginRight:5}}>Bid Amount : {item.price}$</Text>
                      </View>

                      </View>

                                  <View style={{alignSelf:'flex-end'}}>

                                  {
                                    this.itemView(item.status)

                                  }
                                  </View>
                       

                      </View>
                     

                        </TouchableWithoutFeedback> 
                    </Card>
                    
                    </View>
							 
							  }
							  keyExtractor={item => item.id}
							/>


                
    
           </View>

         
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'09%',
    backgroundColor: '#D2AC45',
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:10,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
 
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}