import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,
  ScrollView,StatusBar,StyleSheet,ImageBackground,Platform,Alert,BackHandler,TouchableOpacity,
  TextInput,TouchableWithoutFeedback,ActivityIndicator,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import { Chip } from 'react-native-paper';
import { withNavigationFocus } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import I18n from '../i18n';

import {Header} from 'react-native-elements';



class CreateProfileModel extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
         imageSource:null,
         imageData:null,
          interest_boolean: [],
          interest_id:[],
          interest_names:[],
          ethnicity_boolean: [],
          ethnicity_id:[],
          ethnicity_names:[],
          bodytype_boolean: [],
          bodytype_id:[],
          bodytype_names:[],
          photo:null,
          name:'',
          about:'',
          age:'',
          height:'',
          weight:'',
          age_id:'key0',
          bid_price:'',
          photoone:null,
          phototwo:null,
          photothree:null,
          photofour:null,
          photocounter:0,
          imageSourceOne:null,
          imageSourceTwo:null,
          imageSourceThree:null,
          imageSourceFour:null,
          ethnicity_id:'key0',
          ethnicity:[],
          ethnicity_name:'',
          body:[],
          body_id:'key0',
          body_name:'',

      };


    }

    //regex only onge dot in a time
    //let valid = /^\d*\.?(?:\d{1,2})?$/;

    // if (!isNaN(value) && value.toString().indexOf('.') != -1)
    // {
    //     alert('this is a numeric value and I\'m sure it is a float.');
    // }​

    isValid() {

      let age_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.age.toString())
      let height_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.height.toString())
      let weight_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.weight.toString())
      let bid_valid = /^\d*\.?(?:\d{1,2})?$/.test(this.state.bid_price.toString())
      let valid = false;

      if (this.state.imageSource != null &&
          this.state.name.trim().length > 0
         /// this.state.interest_boolean.includes(true)
          //this.state.interest_boolean.filter(Boolean).length > 0
           ) {
        valid = true;
      }

      if (this.state.imageSource === null) {

      //  ToastAndroid.show('Upload Display Picture', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Upload Display Picture', ToastAndroid.SHORT)
      : Alert.alert('Upload Display Picture')



        return false;
      }
      else if(this.state.name.trim().length === 0){

        //ToastAndroid.show('Enter name', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Name', ToastAndroid.SHORT)
      : Alert.alert('Enter name')



        return false;
      }
      else if(this.state.age_id < 0 || this.state.age_id ==="key0" || this.state.age_id == 0){

      //  ToastAndroid.show('Enter age', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Age', ToastAndroid.SHORT)
      : Alert.alert('Enter Age')



        return false;
      }

      // else if(!age_valid){
      //   ToastAndroid.show("Enter valid age", ToastAndroid.SHORT);
      //   return false;
      // }
      else if(this.state.height.trim().length === 0){

      //  ToastAndroid.show('Enter height', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter height', ToastAndroid.SHORT)
      : Alert.alert('Enter height')



        return false;
      }

      else if(this.state.height.toString().includes(".")){
      //  ToastAndroid.show("Enter valid height", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter valid height', ToastAndroid.SHORT)
      : Alert.alert('Enter valid height')



        return false;
      }
      else if(this.state.height < 100 || this.state.height == 0){

       // ToastAndroid.show('Enter height greater than 10', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter height greater than 100', ToastAndroid.SHORT)
      : Alert.alert('Enter height greater than 100')



        return false;
      }
      else if(this.state.height > 220 ){

        // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


       Platform.OS === 'android'
       ?  ToastAndroid.show('Enter height less than 220', ToastAndroid.SHORT)
       : Alert.alert('Enter height less than 220')



         return false;
       }
      else if(this.state.weight.trim().length === 0){

       // ToastAndroid.show('Enter weight', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter weight', ToastAndroid.SHORT)
      : Alert.alert('Enter weight')



        return false;
      }
      else if(this.state.weight.toString().includes(".")){
        //ToastAndroid.show("Enter valid height", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter valid height', ToastAndroid.SHORT)
      : Alert.alert('Enter valid height')



        return false;
      }
      else if(this.state.weight < 40 || this.state.weight == 0){

       // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter weight greater than 40', ToastAndroid.SHORT)
      : Alert.alert('Enter weight greater than 40')



        return false;
      }
      else if(this.state.weight > 150 ){

        // ToastAndroid.show('Enter weight greater than 10', ToastAndroid.SHORT);


       Platform.OS === 'android'
       ?  ToastAndroid.show('Enter weight less than 150', ToastAndroid.SHORT)
       : Alert.alert('Enter weight less than 150')



         return false;
       }

      else if (this.state.ethnicity_id < 0 || this.state.ethnicity_id ==="key0" || this.state.ethnicity_id == 0) {

          //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


        Platform.OS === 'android'
        ?  ToastAndroid.show('Select Ethnicity tags!', ToastAndroid.SHORT)
        : Alert.alert('Select Ethnicity tags!')



          return false;
        }

        else if (this.state.body_id < 0 || this.state.body_id ==="key0" || this.state.body_id == 0 ) {

          //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


        Platform.OS === 'android'
        ?  ToastAndroid.show('Select Body Type tags!', ToastAndroid.SHORT)
        : Alert.alert('Select Body Type tags!')



          return false;
        }



      // else if(this.state.bid_price.trim().length === 0){

      //  // ToastAndroid.show('Enter bid price', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter bid price', ToastAndroid.SHORT)
      // : Alert.alert('Enter bid price')



      //   return false;
      // }

      // else if(!bid_valid){
      //   //ToastAndroid.show("Enter valid bid price", ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter valid bid price', ToastAndroid.SHORT)
      // : Alert.alert('Enter valid bid price')



      //   return false;
      // }
      // else if(this.state.bid_price < 10 || this.state.bid_price == 0){

      //  // ToastAndroid.show('Enter bid price greater than 10', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Enter bid price greater than 10', ToastAndroid.SHORT)
      // : Alert.alert('Enter bid price greater than 10')



      //   return false;
      // }
      // else if (this.state.interest_boolean.filter(Boolean).length < 0 || this.state.interest_boolean.filter(Boolean).length == 0  ) {

      //   //ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);


      // Platform.OS === 'android'
      // ?  ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT)
      // : Alert.alert('Select Interest tags!')



      //   return false;
      // }

      return valid;
  }



    onCreate(){

      if (this.isValid()) {

        //making paramters for the interest tag
        // var length = this.state.interest_boolean.length;
        // var final= "";
        // for(var i = 0 ; i < length ; i++){
        //   if(this.state.interest_boolean[i] === false){
        //    //do nothing

        //   }
        //   else{
        //     let temp = this.state.interest_id[i].toString()
        //     final = final + temp
        //     final = final + ","
        //   }
        // }


        // //making parameter for ethnicity

        // var length = this.state.ethnicity_boolean.length;
        // var final_ethnicity= "";
        // for(var i = 0 ; i < length ; i++){
        //   if(this.state.ethnicity_boolean[i] === false){
        //    //do nothing

        //   }
        //   else{
        //     let temp = this.state.ethnicity_id[i].toString()
        //     final_ethnicity = final_ethnicity + temp
        //     final_ethnicity = final_ethnicity + ","
        //   }
        // }


        // final_ethnicity = final_ethnicity.substring(0, final_ethnicity.length-1);



        //  //making parameter for body type

        //  var length = this.state.bodytype_boolean.length;
        //  var final_bodytype = "";
        //  for(var i = 0 ; i < length ; i++){
        //    if(this.state.bodytype_boolean[i] === false){
        //     //do nothing

        //    }
        //    else{
        //      let temp = this.state.bodytype_id[i].toString()
        //      final_bodytype = final_bodytype + temp
        //      final_bodytype = final_bodytype + ","
        //    }
        //  }


        //  final_bodytype = final_bodytype.substring(0, final_bodytype.length-1);




        //ToastAndroid.show("ToastItems"+final, ToastAndroid.SHORT);
        var arr = [this.state.photoone,this.state.phototwo,this.state.photothree,this.state.photofour]

       this.setState({loading_status:true})
       var formData = new FormData();
       var result  = this.props.navigation.getParam('result')
        formData.append('user_id',result["user_id"]);
        formData.append('display_name', this.state.name);
        formData.append('weight', this.state.weight);
        //formData.append('bid_price', this.state.bid_price);
        formData.append('height', this.state.height);
        formData.append('age', this.state.age_id);
        formData.append('image_one',this.state.photoone);
        formData.append('image_two',this.state.phototwo);
        formData.append('image_three',this.state.photothree);
        formData.append('image_four',this.state.photofour);
        if(this.state.about !=null){
          formData.append('about', this.state.about);
        }
       // formData.append('interest_tag', final_interest);
        formData.append('profile_pic', this.state.photo);
        formData.append('ethnicities',this.state.ethnicity_id);
        formData.append('body_type', this.state.body_id);
     //Alert.alert(JSON.stringify(formData));

     //ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)
             let url = urls.base_url +'api/create_profile'

                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false})
                    //Alert.alert(responseJson);
                   //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                    if(!responseJson.error){
                          //success in inserting data
                          //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          var id = responseJson.result.id
                          var roles_id = responseJson.result.roles_id
                          var state = responseJson.result.state.name;
                          var name = responseJson.result.name
                          var user_image = responseJson.result.user_image
                          var age = responseJson.result.age
                          var stars = responseJson.result.stars
                          var abc = 0



                            AsyncStorage.multiSet([
                              ['user_id',id.toString()],
                              ['roles_id', roles_id.toString()],
                              ["state", state],
                              ["user_name", name],
                              ["user_image", user_image],
                              ["user_age", age.toString()],
                              ["stars", abc.toString()]

                            ]);
                             this.props.navigation.navigate('HomeModel');




                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            //this.setState({error:responseJson.message})

                            Platform.OS === 'android'
                            ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)

                          }

                    }).catch((error) => {
                      console.error(error);
                      //this.props.navigation.navigate("NoNetwork")
                      // Platform.OS === 'android'
                      // ?   ToastAndroid.show(JSON.stringify(error), ToastAndroid.SHORT)
                      // : Alert.alert("Connection Error !")


                      Platform.OS === 'android'
                      ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")


                      return;
                    });


       }

 }

    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
         // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };


          this.setState({
            imageSource: source ,
            imageData: response.data,
            photo:photo,

          });


          {/*photo is a file object to send to parameters */}
        }
      });
   }

   selectPhotoTappedOne() {
    const options = {
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
       // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
       //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
        let source = { uri: response.uri};

        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };


        this.setState({

          photoone:photo,
          photocounter:1,
          imageSourceOne: source ,

        });


        {/*photo is a file object to send to parameters */}
      }
    });
 }

 selectPhotoTappedTwo() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        phototwo:photo,
        photocounter:2,
        imageSourceTwo: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}

selectPhotoTappedThree() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
     // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        photothree:photo,
        photocounter:3,
        imageSourceThree: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}

selectPhotoTappedFour() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
      ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
     //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };


      this.setState({

        photofour:photo,
        photocounter:4,
        imageSourceFour: source ,

      });


      {/*photo is a file object to send to parameters */}
    }
  });
}





    fetchInterest = async () =>{
      this.setState({loading_status:true})

                    let url = urls.base_url +'api/api_interest'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name

                                    // Create a new array based on current state:
                                    let interests = [...this.state.interest_names];
                                      // Add item to it
                                    interests.push( name );


                                    let interests_ids = [...this.state.interest_id];
                                    interests_ids.push(id );


                                    let interests_bool = [...this.state.interest_boolean];
                                    interests_bool.push( false );

                                    // Set state
                                    this.setState({ interest_id:interests_ids,interest_names:interests,interest_boolean:interests_bool });



                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                             // this.setState({ countries : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    fetchBodyType = async () =>{
      this.setState({loading_status:true})

                        let url = urls.base_url +'api/api_body_type'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name
                              const array = [...temp_arr];
                              array[i] = { ...array[i], id: id };
                              array[i] = { ...array[i], name: name };

                              temp_arr = array
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ body : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    fetchEthnicity = async () =>{
      this.setState({loading_status:true})

                     let url = urls.base_url +'api/api_ethnicity'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name


                              const array = [...temp_arr];
                              array[i] = { ...array[i], id: id };
                              array[i] = { ...array[i], name: name };

                              temp_arr = array


                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ ethnicity : temp_arr});

                            }


                          else{
                           // Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                           // ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    next(){
      var length = this.state.interest_boolean.length;
      var final= "";
      for(var i = 0 ; i < length ; i++){
        if(this.state.interest_boolean[i] === false){
         //do nothing

        }
        else{
          let temp = this.state.interest_id[i].toString()
          final = final + temp
          final = final + ","
        }
      }
     // ToastAndroid.show("Final Items"+final, ToastAndroid.SHORT);


    }

    ethnicityname(value){
      for(var i = 0 ; i < this.state.ethnicity.length ; i++){
        if(this.state.ethnicity[i].id === value){
          //ToastAndroid.show("IDDDDD.."+ this.state.data[i].key, ToastAndroid.LONG);
          this.setState({ethnicity_name:this.state.ethnicity[i].name})
        }
      }
    }

    bodyname(value){
      for(var i = 0 ; i < this.state.body.length ; i++){
        if(this.state.body[i].id === value){
          //ToastAndroid.show("IDDDDD.."+ this.state.data[i].key, ToastAndroid.LONG);
          this.setState({body_name:this.state.body[i].name})
        }
      }
    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     //this.fetchInterest()
     this.fetchEthnicity()
     this.fetchBodyType()

    }

    handleBackWithAlert = () => {
      // const parent = this.props.navigation.dangerouslyGetParent();
        // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
      // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)

      if (this.props.isFocused) {

                if(this.state.loading_status){
                       this.setState({loading_status:false})
                }

              else{
                this.props.navigation.navigate("Welcome")
              }

    return true;
    }
    }



    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
    //this.props.navigation.dispatch(DrawerActions.closeDrawer());


    }

    componentWillUnmount() {
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);

    }

    photoStatus(){
      if(this.state.photocounter == 0){
        //choose first extra image
        this.selectPhotoTappedOne()
      }
      else if (this.state.photocounter == 1){
        //choose first extra image
        this.selectPhotoTappedTwo()
      }
      else if (this.state.photocounter == 2){
        //choose first extra image
        this.selectPhotoTappedThree()
      }
      else if (this.state.photocounter == 3){
        //choose first extra image
        this.selectPhotoTappedFour()
      }
      else if (this.state.photocounter == 4){
        //choose first extra image
        ToastAndroid.show("You cannot upload more images",ToastAndroid.SHORT)
      }
    }

    check(){

      if(this.state.imageSource === null){
        return ( <Image source={require('../assets/avatar.png')}
        style={styles.image} />
         )
      }

      else{
          return (<Image source={this.state.imageSource}
             style={styles.image}/>
             )
      }


    }


    render() {
      const age =[
        {
          "id":18,
          "value":18
        },
        {
          "id":19,
          "value":19
        },
        {
          "id":20,
          "value":20
        },
        {
          "id":21,
          "value":21
        },
        {
          "id":22,
          "value":22
        },
        {
          "id":23,
          "value":23
        },
        {
          "id":24,
          "value":24
        },
        {
          "id":25,
          "value":25
        },
        {
          "id":26,
          "value":26
        },
        {
          "id":27,
          "value":27
        },
        {
          "id":28,
          "value":28
        },
        {
          "id":29,
          "value":29
        },
        {
          "id":30,
          "value":30
        },
        {
          "id":31,
          "value":31
        },
        {
          "id":32,
          "value":32
        },
        {
          "id":33,
          "value":33
        },
        {
          "id":34,
          "value":34
        },
        {
          "id":35,
          "value":35
        },
        {
          "id":36,
          "value":36
        },
        {
          "id":37,
          "value":37
        },
        {
          "id":38,
          "value":38
        },
        {
          "id":39,
          "value":39
        },
        {
          "id":40,
          "value":40
        },
        {
          "id":41,
          "value":41
        },
        {
          "id":42,
          "value":42
        },
        {
          "id":43,
          "value":43
        },
        {
          "id":44,
          "value":44
        },
        {
          "id":45,
          "value":45
        },
        {
          "id":46,
          "value":47
        },
        {
          "id":48,
          "value":48
        },
        {
          "id":49,
          "value":49
        },
        {
          "id":50,
          "value":50
        },
        {
          "id":51,
          "value":51
        },
        {
          "id":52,
          "value":52
        },
        {
          "id":53,
          "value":53
        },
        {
          "id":54,
          "value":54
        },
        {
          "id":55,
          "value":55
        },
        {
          "id":56,
          "value":56
        },
        {
          "id":57,
          "value":57
        },
        {
          "id":58,
          "value":58
        },
        {
          "id":59,
          "value":59
        },
        {
          "id":60,
          "value":60
        },
        {
          "id":61,
          "value":61
        },
        {
          "id":62,
          "value":62
        },
        {
          "id":63,
          "value":63
        },
        {
          "id":64,
          "value":64
        },
        {
          "id":65,
          "value":65
        },
        {
          "id":66,
          "value":66
        },
        {
          "id":67,
          "value":67
        },
        {
          "id":68,
          "value":68
        },
        {
          "id":69,
          "value":69
        },
        {
          "id":70,
          "value":70
        },
        {
          "id":71,
          "value":71
        },
        {
          "id":72,
          "value":72
        },
        {
          "id":73,
          "value":73
        },
        {
          "id":74,
          "value":74
        },
        {
          "id":75,
          "value":75
        },
        {
          "id":76,
          "value":76
        },
        {
          "id":77,
          "value":77
        },
        {
          "id":78,
          "value":78
        },
        {
          "id":79,
          "value":79
        },
        {
          "id":80,
          "value":80
        },
        {
          "id":81,
          "value":81
        },
        {
          "id":82,
          "value":82
        },
        {
          "id":83,
          "value":83
        },
        {
          "id":84,
          "value":84
        },
        {
          "id":85,
          "value":85
        },
        {
          "id":86,
          "value":86
        },
        {
          "id":87,
          "value":87
        },
        {
          "id":88,
          "value":88
        },
        {
          "id":89,
          "value":89
        },
        {
          "id":90,
          "value":90
        },

      ];

      let makeages =age.map((age) => {
        return (
          <Item label={age.value.toString()} value={age.id.toString()} key={age.id}/>
        )
    })


      let imageResult = this.check()



//       let chips = this.state.interest_names.map((r, i) => {

//         return (
//               <Chip
//               style={{width:null,margin:4}}
//               mode={'outlined'}
//               selected={this.state.interest_boolean[i]}
//               selectedColor={'black'}
//               onPress={() => {

//                       let ids = [...this.state.interest_boolean];
//                       ids[i] =!ids[i];
//                       this.setState({interest_boolean: ids });
//               }}>
//               {r}
//               </Chip>
//         );
// })



// let ethnicity_chips = this.state.ethnicity_names.map((r, i) => {

//   return (
//         <Chip
//         style={{width:null,margin:4}}
//         mode={'outlined'}
//         selected={this.state.ethnicity_boolean[i]}
//         selectedColor={'black'}
//         onPress={() => {



//                 var length = this.state.ethnicity_boolean.length;

//                 var temp =[]
//                 for(var j = 0 ; j < length ; j++){


//                     // ToastAndroid.show("SA",ToastAndroid.LONG)
//                     let ids = [...temp];
//                     ids[j] = false;
//                     temp = ids



//                 }
//                 //ToastAndroid.show(JSON.stringify(temp),ToastAndroid.LONG)
//                 this.setState({ethnicity_boolean: temp });
//                // ToastAndroid.show("SsssssssA",ToastAndroid.LONG)

//                 let ids = [...this.state.ethnicity_boolean];
//                 ids[i] = !ids[i];
//                 this.setState({ethnicity_boolean: ids });



//                 // if( this.state.ethnicity_boolean.filter(Boolean).length < 0  || this.state.ethnicity_boolean.filter(Boolean).length == 0){
//                 //   ToastAndroid.show("SA",ToastAndroid.LONG)
//                 //   let ids = [...this.state.ethnicity_boolean];
//                 //     ids[i] =!ids[i];
//                 //   this.setState({ethnicity_boolean: ids });
//                 // }

//                 // else{
//                 //   ToastAndroid.show("nnnnnA",ToastAndroid.LONG)

//                 //   let val = i
//                   // var length = this.state.ethnicity_boolean.length;

//                   // for(var i = 0 ; i < length ; i++){
//                   //   if(this.state.ethnicity_boolean[i] === true){

//                   //     let ids = [...this.state.ethnicity_boolean];
//                   //     ids[i] = false;
//                   //   }
//                   //   else{

//                   //   }
//                   // }

//                 //      let ids = [...this.state.ethnicity_boolean];
//                 //       ids[val] = true;

//                 // }
//         }}>
//         {r}
//         </Chip>
//   );
// })



// let bodytype_chips = this.state.bodytype_names.map((r, i) => {

//   return (
//         <Chip
//         style={{width:null,margin:4}}
//         mode={'outlined'}
//         selected={this.state.bodytype_boolean[i]}
//         selectedColor={'black'}
//         onPress={() => {

//                 let ids = [...this.state.bodytype_boolean];
//                 ids[i] =!ids[i];
//                 this.setState({bodytype_boolean: ids });
//         }}>
//         {r}
//         </Chip>
//   );
// })

      let makeethnicity =this.state.ethnicity.map((ethnicity) => {
        return (
          <Item label={ethnicity.name} value={ethnicity.id} key={ethnicity.id}/>
        )
      })

      let makebody =this.state.body.map((body) => {
        return (
          <Item label={body.name} value={body.id} key={body.id}/>
        )
      })


        return (
          <View 
          //keyboardVerticalOffset={Platform.select({ios: 80, android: 500})}
        
                style={{ flex: 1 }}>


         {/*for header*/}
         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("Welcome")}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('create_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />

       {/*for main content*/}

       <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <KeyboardAvoidingView 
     
             behavior={Platform.OS === 'ios' ? 'padding' : undefined} enabled
           style={styles.body}>



           <View style={{height:160,width:160,overflow:'hidden',alignSelf:'center'}}>
           <TouchableWithoutFeedback onPress={this.selectPhotoTapped.bind(this)}>
                   {imageResult}
         </TouchableWithoutFeedback>
         </View>

         <View style={{alignSelf:'flex-end',marginTop:-10,marginBottom:10}}>
          <Text onPress={this.photoStatus.bind(this)}>{I18n.t('add_images')}</Text>
          </View>

          <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center'}}>
          {
            this.state.photoone == null
            ? null
            : <Image source={this.state.imageSourceOne} style={styles.imagesmall} />
          }
          {
            this.state.phototwo == null
            ? null
            : <Image source={this.state.imageSourceTwo} style={styles.imagesmall}/>
          }
          {
            this.state.photothree == null
            ? null
            : <Image source={this.state.imageSourceThree} style={styles.imagesmall}/>
          }
          {
            this.state.photofour == null
            ? null
            : <Image source={this.state.imageSourceFour} style={styles.imagesmall} />
          }

          </View>


                 <Text style={styles.name_text}>{I18n.t('model_display_name')}</Text>
                <TextInput
                  value={this.state.name}
                  maxLength={30}
                  keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
                  onChangeText={(name) => this.setState({ name })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />


            <Text style={styles.name_text}>{I18n.t('age')}</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                        <Picker

                              mode="dropdown"
                              selectedValue={this.state.age_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {

                                    this.setState({age_id: itemValue})


                                }}>
                              <Item label="Select age" value="key0" />
                                {makeages}
                      </Picker>
                </View>



                <Text style={styles.name_text}>{I18n.t('height_cms')}</Text>
                <TextInput
                  value={this.state.height}
                  keyboardType = 'numeric'
                  maxLength = {3}
                  textContentType='telephoneNumber'
                  onChangeText={(height) => this.setState({ height})}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />

                <Text style={styles.name_text}>{I18n.t('weight_kgs')}</Text>
                <TextInput
                  value={this.state.weight}
                  maxLength = {3}
                  keyboardType = 'numeric'
                  textContentType='telephoneNumber'
                  onChangeText={(weight) => this.setState({ weight })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />

 {/*
                <Text style={styles.name_text}>Bid Price (in dollars)</Text>
                <TextInput
                  value={this.state.bid_price}
                  maxLength = {5}
                  keyboardType = 'numeric'
                  textContentType='telephoneNumber'
                  onChangeText={(bid_price) => this.setState({ bid_price })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />

 */}

                  <Text style={styles.name_text}>{I18n.t('ethnicity')}</Text>
                  <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                      <Picker

                            mode="dropdown"
                            selectedValue={this.state.ethnicity_id}
                            onValueChange={(itemValue, itemIndex) =>
                              {
                                this.setState({ethnicity_id: itemValue})
                                this.ethnicityname(itemValue)

                              }}>
                            <Item label="Select Ethnicity" value="key0" />
                                {makeethnicity}
                    </Picker>
                  </View>

                  <Text style={{ width:'100%', marginBottom:10,fontSize:fonts.font_size,marginTop:10}}>{I18n.t('body_type')}</Text>
                  <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                  <Picker

                        mode="dropdown"
                        selectedValue={this.state.body_id}
                        onValueChange={(itemValue, itemIndex) =>
                          {
                            this.setState({body_id: itemValue})
                            this.bodyname(itemValue)


                          }}>
                        <Item label="Select Body Type" value="key0" />
                          {makebody}
                  </Picker>
                  </View>

                  <Text style={styles.about_text}>{I18n.t('about_bio')}</Text>
                <TextInput
                  value={this.state.about}
                  keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
                  onChangeText={(about) => this.setState({ about })}
                  style={styles.about_input}
                  multiline={true}
                  placeholderTextColor={'black'}
                />
{/*
                <Text style={styles.name_text}>Interest</Text>
                <View style={{flexDirection:'row',flexWrap: 'wrap'}}>
                     {chips}
                </View>

     */}









                <TouchableOpacity
                onPress={this.onCreate.bind(this)}
                      style={styles.saveButton}>
                      <Text style={styles.saveText}>{I18n.t('save').toUpperCase()}</Text>
                </TouchableOpacity>

    </KeyboardAvoidingView>

       </ScrollView>
       {this.state.loading_status &&
        <View pointerEvents="none" style={styles.loading}>
          <ActivityIndicator size='large' />
        </View>
    }


        </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:30,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size

 },

 name_input: {
  width: "100%",
  height: 50,
  padding:4,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
 },

 about_input: {
  width: "100%",
  height: 120,
 padding:3,
 textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  image:{
    // height:160,
    // width:160,
    // alignSelf:'center',
    // marginBottom:30,
    // borderRadius: 220 / 2,
    // overflow: "hidden",


    height:Platform.OS === 'android' ? 160 :140,
    width:Platform.OS === 'android' ? 160 :140,
    alignSelf:'center',
    marginBottom:30,
  //  borderRadius: 140 / 2,
   borderRadius: Platform.OS === 'android' ? 110 : 70,
    overflow: "hidden",

  },
  imagesmall:{
    height:60,
    width:60,
    alignSelf:'center',
    marginBottom:30,
    borderRadius: Platform.OS === 'android' ? 90 / 2 : 60/2,
    overflow: "hidden",
  },

indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}




}
)

export default withNavigationFocus(CreateProfileModel);
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
