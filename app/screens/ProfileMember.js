import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,Platform,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import AsyncStorage from '@react-native-community/async-storage';
//import {  } from 'react-native-gesture-handler';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import I18n from '../i18n';

import {Header} from 'react-native-elements';

export default class ProfileMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={

         loading_status : false,
         name:'',
         about:null,
         age:'',
         user_id:0,
         user_image:null,
         location:'',
         country_id:''
      };


    }



    fetch(member_id){
      this.setState({loading_status:true})

                    let url = urls.base_url +'api/api_user_profile?id='+ member_id
                      fetch(url, {
                      method: 'GET',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type':  'multipart/form-data',
                      },


                      }).then((response) => response.json())
                          .then((responseJson) => {

                    // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                    console.log(JSON.stringify(responseJson))
                            this.setState({loading_status:false})

                            if(!responseJson.error){
                                    var id = responseJson.result.id
                                  var name = responseJson.result.name
                                 // var live = responseJson.result.live
                                //  var age = responseJson.result.age
                                  var user_image = responseJson.gallery[0].profile_pic
                                  var about = responseJson.result.about
                                  var location = responseJson.result.country.name
                                  var country_id = responseJson.result.country.id
                                  // var height = responseJson.result.height
                                  // var weight = responseJson.result.weight


                             this.setState({
                              name:name,
                              about:about,
                              user_id:id,
                              user_image:user_image,
                              location:location,
                              country_id:country_id



                            })


                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Error !", ToastAndroid.SHORT);
                          });



    }


    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });


    }



    componentDidMount() {
   // StatusBar.setBackgroundColor('#0040FF')
          AsyncStorage.getItem('user_id')
          .then((item) => {
                    if (item) {
                    this.fetch(item)
                  }
                  else {
                    ToastAndroid.show("Error !")
                    }
          });


    }

    next(){
      result={}
      result["name"] = this.state.name
      result["image"] = this.state.user_image
      result["about"] = this.state.about
      result["user_id"] = this.state.user_id
      result['country_id'] = this.state.country_id


      this.props.navigation.navigate('EditProfileMember',{result : result});


    }


    render() {


        return (

         <View
         style={styles.container}>


         {/*for header*/}
         <Header

barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

leftComponent={
 <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomeScreen")}>
           <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
     </TouchableWithoutFeedback>
}


rightComponent={

 <View></View>

 }


statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

centerComponent={{ text: I18n.t('profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
containerStyle={{

  backgroundColor: colors.color_primary,
  justifyContent: 'space-around',
  alignItems:'center',
  shadowOpacity:1.7,
  shadowOffset: {
     width: 0,
     height: 3,
 },
 shadowOpacity: 0.3,
 shadowRadius: 2,
  elevation:7
}}
/>

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%',backgroundColor:'white'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.5,width:'100%',
           borderBottomColor:'black',borderBottomWidth:1,backgroundColor:'grey'}}>


                {
                  this.state.user_image == null
                  ?  <Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}} />
                  :  <Image source={{uri:urls.base_url+this.state.user_image}}
                  resizeMode='contain'
                   style={{alignSelf:'center',height:'100%',width:'100%'}} />
                }



           </View>


           <View style={{padding:15}}>

               {/*for above part*/}
               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                  <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <Text style={{fontSize:fonts.font_size,marginRight:0,color:'black',fontWeight: "bold"}}>{this.state.name}</Text>


                  </View>

                   {/* <TouchableWithoutFeedback onPress={()=> this.next()}>*/}
                  <TouchableWithoutFeedback onPress={()=> this.next()}>
                  <View style={{borderColor:'black',borderRadius:8,borderWidth:2,backgroundColor:colors.color_primary}}>

                  <Text style={{color:'black',fontSize:17,marginLeft:20,marginRight:20,marginBottom:5,marginTop:5,fontWeight:'bold'}}>{I18n.t('edit')}</Text>

                  </View>
                  </TouchableWithoutFeedback>


               </View>


                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight:'bold',color:'black'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ?  <Text style={{color:'black'}}>{I18n.t('na')}</Text>
                        : <Text style={{color:'black'}}>{this.state.about}</Text>
                      }

                      </View>


                  </View>







                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight:'bold',color:'black'}}>{I18n.t('location').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>{this.state.location}</Text>
                      </View>


                  </View>



           </View>



         </View>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
   indicator: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     height: 80
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
