import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,Platform,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator,Alert} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
//import {  } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { Chip } from 'react-native-paper';
import ScrollPicker from 'react-native-wheel-scroll-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import DismissKeyboard from 'dismissKeyboard';
import AsyncStorage from '@react-native-community/async-storage';
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import I18n from '../i18n';
import {Header} from 'react-native-elements';


export default class UpdateBid extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          chips:[],
          gallery:[],
          bodytype_chips:[],
          bodytype_chips_boolean:[],
          ethnicity_chips:[],
          ethnicity_chips_boolean:[],
          name:null,
          age:null,
          height:null,
          weight:null,
          user_image:null,
          about:null,
          state:null,
          live:null,
          user_image:null,
          loading_status:false,
          bid_data:0,
          price:'',
          raise_amount:'',
          rating:null


      };


    }





    getUserId= async () =>{
      var id = await AsyncStorage.getItem('user_id')
      return id
    }


    onBid(){

      var result  = this.props.navigation.getParam('result')
      var model_id = result['model_id']
      var id = result['id']

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

      var formData = new FormData();
     // ToastAndroid.show("wait..."+ this.state.bid_data, ToastAndroid.SHORT);

      formData.append('model_id',model_id);
      formData.append('updated_bid_amount', this.state.bid_data);
      formData.append('member_id', item);
      formData.append('id', id);

                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/change_bid'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                          this.setState({loading_status:false})
                      // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                  //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                            if(!responseJson.error){

                              Platform.OS === 'android'
                              ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)

                            this.props.navigation.navigate('HomePage')



                             // this.props.navigation.navigate("Welcome");
                            //this.props.navigation.navigate("HomeScreen");
                          }
                          else{


                              Platform.OS === 'android'
                              ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})

                            Platform.OS === 'android'
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                        });
        }
        else{
                  //ToastAndroid.show("User not found !",ToastAndroid.LONG)
                }
              });





}

isValid() {




  if (this.state.bid_data.toString().trim().length > 0  ) {
     valid = true;
  }

  if (this.state.bid_data.toString().trim().length === 0 || this.state.bid_data== 0) {

   // ToastAndroid.show('Enter otp', ToastAndroid.SHORT);


  Platform.OS === 'android'
  ?  ToastAndroid.show('Enter Bid Price', ToastAndroid.SHORT)
  : Alert.alert('Enter Bid Price')



    return false;
  }





  return valid;
}


next()
{
  if(this.isValid()){
    this.onBid()
  }
}



//fetch = async () =>
    fetch(model_id){
      this.setState({loading_status:true})

      var formData = new FormData();


      formData.append('model_id', model_id);

                  let url = urls.base_url +'api/models_detail'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type':  'multipart/form-data',
                      },
                      body: formData

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             // var id = responseJson.result.model_detail.id
                             var name = responseJson.result.model_detail.name
                             var live = responseJson.result.model_detail.live
                             var age = responseJson.result.model_detail.age
                             var user_image = responseJson.result.model_detail.user_image
                             var about = responseJson.result.model_detail.about
                             var height = responseJson.result.model_detail.height
                             var weight = responseJson.result.model_detail.weight
                             var state = responseJson.result.model_detail.state

                             var gallery = responseJson.gallery
                             var len = gallery.length
                             var temp_arr=[]


                             var rev = responseJson.result.reviews
                             var stars = responseJson.result.star
                             var reviews = rev.toString().length == 0 ? 0: parseFloat(rev)

                             reviews =  parseFloat(reviews) + parseFloat(stars)

                            if(len > 0){
                              for(var i = 0 ; i < 5 ; i++){
                                var id = responseJson.gallery[0].id
                                var profile_pic = responseJson.gallery[0].profile_pic

                                var image_one = responseJson.gallery[0].image_one
                                var image_two = responseJson.gallery[0].image_two
                                var image_three = responseJson.gallery[0].image_three
                                var image_four = responseJson.gallery[0].image_four

                                if(i == 0 && profile_pic != null ){
                                  var image = responseJson.gallery[0].profile_pic
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 1 && image_one != null){
                                  var image = responseJson.gallery[0].image_one
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 2 && image_two != null){
                                  var image = responseJson.gallery[0].image_two
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 3 && image_three != null){
                                  var image = responseJson.gallery[0].image_three
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }
                                else if(i == 4 && image_four != null){
                                  var image = responseJson.gallery[0].image_four
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], image: image};
                                  temp_arr = array
                                }



                                }
                            }

                               this.setState({gallery : temp_arr});


                             this.setState({
                              name:name,
                              about:about,
                              height:height,
                              weight:weight,
                              age:age,
                              user_image:user_image,
                              live:live,
                              state:state,
                              rating:reviews


                            })

                              var interest = responseJson.result.interest_tag
                              var length = interest.length

                              for(var i = 0 ; i < length ; i++){

                                var name_temp = interest[i].name   //interest name

                                      // Create a new array based on current state:
                                      let interests = [...this.state.chips];
                                     // Add item to it
                                      interests.push( name_temp );



                                      this.setState({ chips:interests});




                                }



                                //making chips for ethnicity
                      var length = responseJson.result.ethnicities.length
                      //ToastAndroid.show(length,ToastAndroid.LONG)
                                            for(var i = 0 ; i < length ; i++){
                                           //   var id = responseJson.result.interest[i].id  //interest id
                                              var name_temp = responseJson.result.ethnicities[i].name   //interest name

                                                    // Create a new array based on current state:
                                                    let ethnicitys = [...this.state.ethnicity_chips];
                                                   // Add item to it
                                                   ethnicitys.push( name_temp );


                                                    let ethnicitys_bool = [...this.state.ethnicity_chips_boolean];
                                                    ethnicitys_bool.push( false );

                                                    // Set state
                                                    this.setState({ ethnicity_chips:ethnicitys,ethnicity_chips_boolean:ethnicitys_bool });




                                              }


                                               //making chips for bodytype
                                            var length =  responseJson.result.body_type.length

                                            for(var i = 0 ; i < length ; i++){
                                           //   var id = responseJson.result.interest[i].id  //interest id
                                              var name_temp = responseJson.result.body_type[i].name   //interest name

                                                    // Create a new array based on current state:
                                                    let bodytypes = [...this.state.bodytype_chips];
                                                   // Add item to it
                                                   bodytypes.push( name_temp );


                                                    let bodytypes_bool = [...this.state.bodytype_chips_boolean];
                                                    bodytypes_bool.push( false );

                                                    // Set state
                                                    this.setState({ bodytype_chips:bodytypes,bodytype_chips_boolean:bodytypes_bool });


                                              }


                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})

                                        Platform.OS === 'android'
                                        ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                        : Alert.alert("Connection Error !")
                          });



    }


    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     var result  = this.props.navigation.getParam('result')
     var model_id = result['model_id']
     var price = result['price']
     var raise_amount = result['raise_amount']
     //ToastAndroid.show("HAAHA"+raise_amount,ToastAndroid.LONG)
     this.setState({
       price:price,
       raise_amount:raise_amount
     })
     this.fetch(model_id)

    }


    _renderDotIndicator() {
      return <PagerDotIndicator pageCount={this.state.gallery.length} />;
    }


    render() {

      let ethnicity_chips = this.state.ethnicity_chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={this.state.ethnicity_chips_boolean[i]}
              selectedColor={'black'}
              >
              {r}
              </Chip>
        );
      })


      let bodytype_chips = this.state.bodytype_chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={this.state.bodytype_chips_boolean[i]}
              selectedColor={'black'}
              >
              {r}
              </Chip>
        );
      })




      let makeGallery =this.state.gallery.map((gallery) => {
        return (


          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'#d5d5d7'
          }}>

          <Image source={{uri:urls.base_url+gallery.image}}
          resizeMode='contain'
          style={{alignSelf:'center',height:'100%',width:'100%'}} />

        </View>
        );
    })




      let chips = this.state.chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={false}
              selectedColor={'black'}
              onPress={() => {

                 {/*      let ids = [...this.state.interest_boolean];
                      ids[i] =!ids[i];
                      this.setState({interest_boolean: ids });
                      */}
              }}>
              {r}
              </Chip>
        );
})


        return (

         <View
         style={styles.container}>


         {/*for header*/}
         <Header

barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

leftComponent={
 <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
           <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
     </TouchableWithoutFeedback>
}


rightComponent={

 <View></View>

 }


statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

centerComponent={{ text: I18n.t('model_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
containerStyle={{

  backgroundColor: colors.color_primary,
  justifyContent: 'space-around',
  alignItems:'center',
  shadowOpacity:1.7,
  shadowOffset: {
     width: 0,
     height: 3,
 },
 shadowOpacity: 0.3,
 shadowRadius: 2,
  elevation:7
}}
/>

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.4,width:'100%'}}>



           <IndicatorViewPager
           style={{ height: '100%' }}
           indicator={this._renderDotIndicator()}>
          {makeGallery}
         </IndicatorViewPager>


           </View>


        {/*middle f;oating element */}
         {/*

              <View style={{flexDirection:'row',justifyContent:'space-around',width:'40%',alignSelf:'flex-end',marginLeft:'50%',marginTop:-25}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ModelBidding")}>
                              <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/accept.png')} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ModelProtest")}>
                      <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/reject.png')} />
                      </TouchableOpacity>


              </View>
              */}

          {/*end */}

           <View style={{flex:0.5,padding:20,marginTop:15,width:'100%'}}>
           <Text style={{marginBottom:10,color:'black'}}>{I18n.t('your_previous_bid_amount')} : {this.state.price}$</Text>
           <Text style={{marginBottom:10,color:'black'}}>{I18n.t('raised_bid_amount')} : {this.state.raise_amount}$</Text>
           <View style={{marginBottom:25}}>
           <Text style={{marginBottom:10}}>{I18n.t('set_bid_amount')}</Text>
           <View style={{width:'70%',flexDirection:'row',justifyContent:'space-around'}}>
                <View style={{flex:1}}>
                {/*
                <TextInput
                value={this.state.bid_data}
                onChangeText={(bid_data) => this.setState({ bid_data })}
                placeholder={'Bid Amount '}
                style={styles.bid_input}
                keyboardType={ Platform.OS === 'android' ?'none' : 'default'}
                onFocus={ () => {
                  DismissKeyboard()
                  this.RBSheet.open();
                } }
                placeholderTextColor={'black'}
              />

              */}


              <TouchableWithoutFeedback onPress={()=>  this.RBSheet.open()}>
              <View style={{backgroundColor:'white',height:50,width:null,justifyContent:'center',alignItems:'center',borderColor:'black',borderRadius:10,borderWidth:2}}>


                <Text style={{color:'black'}}>{this.state.bid_data}</Text>

              </View>
              </TouchableWithoutFeedback>




                </View>
                <View style={{flex:1}}>
                <TouchableOpacity onPress={()=> {
                  this.next()
                  // result={}
                  // result["model_id"] = 714
                  // this.props.navigation.navigate("Abc");
                  // ToastAndroid.show("ASFSADFSADF",ToastAndroid.SHORT)

                  {/*this.props.navigation.navigate("ModelProfileChat",{result : result}); 8  */}
                }}
                style={styles.bidButton}
                  >
                <Text style={styles.bidText}>{I18n.t('bid_now').toUpperCase()}</Text>
                    </TouchableOpacity>
                </View>

           </View>
           </View>


           <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Text style={{fontSize:18,marginRight:7,color:'black',fontWeight:'bold'}}>{this.state.name}</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7}}>,</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:17,color:'black'}}>{this.state.age}</Text>

                                        <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />


                    </View>


                    {
                      this.state.rating == null
                      ?
                      <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
                      <Image style={{width: 25, height: 25,marginRight:10}}  source={require('../assets/star.png')} />
                      <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>0 </Text>

                      </View>
                      :
                      <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}}>
                      <Image style={{width: 25, height: 25,marginRight:10}}  source={require('../assets/star.png')} />
                      <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>{this.state.rating.toString().substring(0,3)} </Text>
                      </View>

                    }

                    </View>
                  <Text style={{alignSelf:'flex-start',fontSize:13}}>{this.state.state}</Text>

                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        : <Text style={{color:'grey'}}>{this.state.about}</Text>
                      }

                      </View>


                  </View>





                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('height').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'grey'}}>{this.state.height} {I18n.t('cms')}</Text>
                      </View>


                  </View>

                   {/*Weight */}

                   <View style={{marginTop:35,marginLeft:20}}>
                   <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                       <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                       <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('weight').toUpperCase()}</Text>

                   </View>
                   <View style={{marginLeft:30}}>
                   <Text style={{color:'grey'}}>{this.state.weight} {I18n.t('kgs')}</Text>
                   </View>


               </View>



                {/*ethniicty */}

                <View style={{marginTop:35,marginLeft:20}}>
                <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                    <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                    <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('ethnicity').toUpperCase()}</Text>

                </View>



                  <View style={{flexDirection:'row',flexWrap: 'wrap',marginLeft:30}}>
                        {ethnicity_chips}
                  </View>

            </View>


             {/*bodytype */}


            <View style={{marginTop:35,marginLeft:20}}>
            <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('body_type').toUpperCase()}</Text>

            </View>



              <View style={{flexDirection:'row',flexWrap: 'wrap',marginLeft:30}}>
                    {bodytype_chips}
              </View>

        </View>



                  {/*interest */}
{/*
                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/grey.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>INTEREST</Text>

                      </View>



                      <View style={{flexDirection:'row',flexWrap: 'wrap',marginLeft:30}}>
                      {chips}
                      </View>


                  </View>

                  */}

           </View>



         </View>

         <RBSheet
         ref={ref => {
             this.RBSheet = ref;
         }}
         height={400}
         duration={250}
         customStyles={{
             container: {
             justifyContent: "center",
             alignItems: "center"
             }
         }}
         >

         <View style={{height:'100%',justifyContent:'center',alignItems:'center',width:'100%'}}>
         <View style={{height:'75%'}}>

         <Text style={{fontSize:18,margin:10,color:'black',marginTop:-5,marginBottom:20}}>{I18n.t('choose_bid_amount_dollars')}</Text>
         <ScrollPicker
              dataSource={[
                '10',
                '20',
                '30',
                '40',
                '50',
                '60',
                '70',
                '80',
                '90',
                '100',
                '110',
                '120',
                '130',
                '140',
                '150',
                '160',
                '170',
                '180',
                '190',
                '200',
                '220',
                '240',
                '260',
                '280',
                '300',
                '320',
                '340',
                '360',
                '380',
                '400',
                '420',
                '440',
                '460',
                '480',
                '500',
                '520',
                '540',
                '560',
                '580',
                '600',
                '620',
                '640',
                '660',
                '680',
                '700',
                '720',
                '740',
                '760',
                '760',
                '780',
                '800',
                '820',
                '840',
                '860',
                '880',
                '900',
                '920',
                '940',
                '960',
                '980',
                '1000',
                '1020',
                '1040',
                '1060',
                '1080',
                '1100',
                '1120',
                '1140',
                '1160',
                '1180',
                '1200',
                '1220',
                '1240',
                '1260',
                '1280',
                '1300',
                '1320',
                '1340',
                '1360',
                '1380',
                '1400',
                '1420',
                '1440',
                '1460',
                '1480',
                '1500',
                '1520',
                '1540',
                '1560',
                '1580',
                '1600',
                '1620',
                '1640',
                '1660',
                '1680',
                '1700',
                '1720',
                '1740',
                '1760',
                '1760',
                '1780',
                '1800',
                '1820',
                '1840',
                '1860',
                '1880',
                '1900',
                '1920',
                '1940',
                '1960',
                '1980',
                '2000',
                '2020',
                '2040',
                '2060',
                '2080',
                '2100'





              ]}
              selectedIndex={10}
              renderItem={(data, index, isSelected) => {

                  //
              }}
              onValueChange={(data, selectedIndex) => {
                  //
                  this.setState({
                      index:selectedIndex,
                      bid_data:data
                  })
                  //ToastAndroid.show(data,ToastAndroid.SHORT)

              }}
              wrapperHeight={70}
              wrapperWidth={150}
              wrapperBackground={'#FFFFFF'}
              itemHeight={40}
              highlightColor={'#d8d8d8'}
              highlightBorderWidth={4}
              activeItemColor={'#222121'}
              itemColor={'#B4B4B4'}
            />
            </View>



            <TouchableOpacity
            style={styles.submitButton}
            onPress={()=>{
              this.RBSheet.close()
            }}


            >
            <Text style={styles.submitText}>{I18n.t('select').toUpperCase()}</Text>
            </TouchableOpacity>




             </View>
         </RBSheet>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },

    submitButton:{

      marginBottom:5,
      marginTop:10,
      width:"90%",
      height:50,
      backgroundColor:colors.color_primary,
      borderRadius:10,
      borderWidth: 1,
      borderColor: 'black',


   },
   submitText:{
     color:'black',
     textAlign:'center',
     fontSize :18,
     padding:10

   },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },

 bid_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',

},


bidButton:{


  width:"100%",
  height:50,
  alignSelf:'center',
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:colors.color_secondary,
  borderRadius:8,
  borderWidth: 1,
  borderColor: 'black',
  marginLeft:40


},
bidText:{
 color:'white',
 textAlign:'center',
 fontSize :16,
 fontWeight : 'bold'
}
,
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
