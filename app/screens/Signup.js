import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,AsyncStorage,ToastAndroid,StatusBar,CheckBox,TextInput,StyleSheet,ImageBackground,TouchableOpacity,ActivityIndicator} from 'react-native';
//import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import { ScrollView } from 'react-native-gesture-handler';
//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
//import RadioGroup,{Radio} from "react-native-radio-input";
import { RadioButton } from 'react-native-paper';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";
import TwitterAuth from 'tipsi-twitter';

import { colors,urls,fonts} from './Variables';


TwitterAuth.init({
  twitter_key: "EKQbVGlRMwOTmjQAQ3NsScLPK",
  twitter_secret: "t5utUVmkDiAmqH3IfcRZRmY3NPjsUMUEo6hMLqzevgJvFl2ZaU"
})



export default class Signup extends Component {
    constructor(props) {
        super(props);
        this.state ={
            countries:[],
            states:[],
            cities:[],
            country_id:0,
            state_id:0,
            city_id:0,
            checked:true,
            loading_status:false,
            email:'',
            username:'',
            confirm_password:'',
            password:'',
           first_checked:'male',
           
        };

    }

    
    fetchCountries = async () =>{
      this.setState({loading_status:true})

                  
                      fetch('http://webmobril.org/dev/d8bid/api/api_country', {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }
    

    getStates(countryId){
      this.setState({loading_status:true,states:[]})


                      fetch("http://webmobril.org/dev/d8bid/api/state?country_id="+countryId, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].country_id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], country_id: country_id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({states : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }


    getCity(stateId){
      this.setState({loading_status:true,states:[]})


                      fetch("http://webmobril.org/dev/d8bid/api/city?state_id"+stateId, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({cities: temp_arr,loading_status:false});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                            this.setState({loading_status:false})
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    componentDidMount() {
    this.fetchCountries()
      
    }


    _fb(){

    

      LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
        function (result) {
          
          if (result.isCancelled) {
            console.log('Login cancelled')
            ToastAndroid.show('Login cancelled ' ,ToastAndroid.LONG)
          } else {
              AccessToken.getCurrentAccessToken().then(
             (data) => {
               let accessToken = data.accessToken
                //Alert.alert(accessToken.toString())
  
               const responseInfoCallback = (error, result) => {
                 if (error) {
                   console.log(error)
                  
                  // ToastAndroid.show('Error  fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
                 } else {
                   //console.log(result)
                    // Alert.alert('Success fetching data: ' + JSON.stringify(result));
                    //when success this will call
                   // ToastAndroid.show('Success fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
                     let id = result["id"]
                     let name = result["name"]
                     let email = result["email"]
                     //this.loginwithfb(id,name,email)
                     // ToastAndroid.show('Success fetching data: ' + id+"...."+"name"+name ,ToastAndroid.LONG)
                  //    var formData = new FormData();
  
                  //    formData.append('name', name);
                  //    formData.append('email',email);
                  //    formData.append('fb_id',id)
  
                  //    fetch('http://dev.webmobrilmedia.com/fulex_app/apis/api_fblogin', {
                  //    method: 'POST',
                  //    headers: {
                  //      'Accept': 'application/json',
                  //      'Content-Type':  'multipart/form-data',
                  //    },
                  //    body: formData
  
                  //  }).then((response) => response.json())
                  //        .then((responseJson) => {
                  //          //this.setState({loading_status:false})
                  //          // if(this.state.loading_status){
                  //          //   this.setState({loading_status:false})
                  //          // }
                  //        if(!responseJson.error){
                  //              //success in inserting data
                  //            ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                  //              var id = responseJson.result.User.id
                  //              AsyncStorage.setItem('uname', id)
                  //              //.then(() => )
                  //               //.catch(err => ToastAndroid.show("nottt----written", ToastAndroid.LONG));
                  //              this.props.navigation.navigate('Home');
                  //              LoginManager.logout()
  
                  //      }else{
  
                  //                //this.setState({error:responseJson.message,showProgress:false})
                  //                ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                  //        }
  
  
                  //        }).catch((error) => {
                  //          console.error(error);
                  //        });
                 }
               }
  
               const infoRequest = new GraphRequest(
                 '/me',
                 {
                   accessToken: accessToken,
                   parameters: {
                     fields: {
                       string: 'email,name,id'
                     }
                   }
                 },
                 responseInfoCallback
               );
  
               // Start the graph request.
               let a = new GraphRequestManager().addRequest(infoRequest).start()
               //ToastAndroid.show("Result ..."+ a.toString(),ToastAndroid.LONG)
  
             }
           )
          }
  
    }.bind(this),
    function (error) {
      console.log('Login fail with error: ' + error)
     // ToastAndroid.show('Login fail with error: ' + JSON.stringify(error),ToastAndroid.LONG)
    }
  )
    }

    _twitter = async () =>{
    
  
   try{
        const result = await TwitterAuth.login()
         Alert.alert("ID",result.userId)
        //  Alert.alert("ID",result.authToken)
        //  Alert.alert("ID",result.authTokenSecret)
         Alert.alert("ID",result.userName)
  
        //   this.setState({username : result.userId})
  
  
        //   var formData = new FormData();
  
  
        //   formData.append('twitter_id', result.userId);
        //   formData.append('email', result.userName);
        // //  formData.append('agree',check_status)
  
        //   fetch('http://dev.webmobrilmedia.com/fulex_app/apis/api_twitter_login', {
        //   method: 'POST',
        //   headers: {
        //     'Accept': 'application/json',
        //     'Content-Type':  'multipart/form-data',
        //   },
        //   body: formData
  
        // }).then((response) => response.json())
        //       .then((responseJson) => {
        //       //  this.setState({loading_status:false})
  
        //           // if(this.state.loading_status){
        //           //   this.setState({loading_status:false})
        //           // }
        //       if(!responseJson.error){
        //             //success in inserting data
        //             ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
        //             var id = responseJson.userid
        //             AsyncStorage.setItem('uname', id);
        //             this.props.navigation.navigate('Home');
  
        //     }else{
  
        //               this.setState({error:responseJson.message,showProgress:false})
        //             //  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
        //       }
  
  
        //       }).catch((error) => {
        //         console.error(error);
        //       });
  
      }
      catch(error){
          Alert.alert("Error Occured !")
      }
    }
  


    onSelect(index, value){
      this.setState({fuel_id:value})
         
       
    }

    render() {

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }
     
              let makecountries =this.state.countries.map((country) => {
                return (
                  <Item label={country.name} value={country.id} key={country.id}/>
                )
            })

                let makestates =this.state.states.map((state) => {
                  return (
                    <Item label={state.name} value={state.id} key={state.id}/>
                  )
              })


                let makecities =this.state.cities.map((city) => {
                  return (
                    <Item label={city.name} value={city.id} key={city.id}/>
                  )
              })

        return (
        <ScrollView showsHorizontalScrollIndicator={false}>
         <View 
         style={styles.container}>

        <Text style={styles.headerText}>SIGN UP</Text>

         <Text style={styles.input_text}>App User Name</Text>
          <TextInput
            value={this.state.username}
            onChangeText={(username) => this.setState({ username})}
            style={styles.input}
            placeholderTextColor={'black'}
          />

          <Text style={styles.input_text}>Email</Text>
          <TextInput
            value={this.state.email}
            onChangeText={(email) => this.setState({ email })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

          <Text style={styles.input_text}>Password</Text>
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({ password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />


          <Text style={styles.input_text}>Confirm Password</Text>
          <TextInput
            value={this.state.confirm_password}
            onChangeText={(confirm_password) => this.setState({ confirm_password })}
            style={styles.input}
            placeholderTextColor={'black'}
          />

       <Text style={styles.input_text}>Country</Text>
        <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black'}}>
                    <Picker
                          style={styles.picker}
                          mode="dropdown"
                          selectedValue={this.state.country_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              if(itemIndex > 0){
                                this.setState({country_id: itemValue})
                                this.getStates(itemValue)
                              }  
                            }}>
                          <Item label="Select Country" value="key0" />
                             {makecountries}
                  </Picker>
            </View>

            <Text style={styles.input_text}>State</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black'}}>
                        <Picker
                              style={styles.picker}
                              mode="dropdown"
                              selectedValue={this.state.state_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {
                                  if(itemIndex > 0){
                                    this.setState({state_id: itemValue})
                                    this.getCity(itemValue)
                                  }  
                                }}>
                              <Item label="Select State" value="key0" />
                                {makestates}
                      </Picker>
                </View>


                <Text style={styles.input_text}>City</Text>
            <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black'}}>
                        <Picker
                              style={styles.picker}
                              mode="dropdown"
                              selectedValue={this.state.city_id}
                              onValueChange={(itemValue, itemIndex) =>
                                {
                                  if(itemIndex > 0){
                                    this.setState({city_id: itemValue})
                                    
                                  }  
                                }}>
                              <Item label="Select City" value="key0" />
                                {makecities}
                      </Picker>
                </View>


    <Text style={{width:'100%',marginTop:20,marginBottom:10,fontSize:fonts.font_size}}>Gender</Text> 
       <View style={{width:'100%'}}>
       <RadioButton.Group
        onValueChange={value => this.setState({ value })}
        value={this.state.value}
      >

      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

        <View style={{flex:1}}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton 
         value="male"
         color={colors.color_primary}
         status={this.state.first_checked === 'male' ? 'checked' : 'unchecked'}
         onPress={() => { this.setState({ first_checked: 'male' }); }}/>
          <Text>Male</Text>
        </View>
        </View>


        <View  style={{flex:1,marginRight:10}}>
           <View style={{flexDirection:'row',alignItems:'center'}}>
          <RadioButton
           value="female"
           color={colors.color_primary}
           status={this.state.first_checked === 'female' ? 'checked' : 'unchecked'}
           onPress={() => { this.setState({ first_checked: 'female' }); }} />
          <Text>Female</Text>
        </View>
        </View>

        </View>



      </RadioButton.Group>
       </View>

          {/** end 1st radio */}   


          <View style={{ flexDirection: 'row' }}>
                <CheckBox
                  value={this.state.checked}
                  onValueChange={() => this.setState({ checked: !this.state.checked })}
                />
                <Text style={{marginTop: 5}}>Accept terms and condition</Text>
            </View>

         

          <TouchableOpacity
                onPress={()=> this.props.navigation.navigate("ModelProfile")}
                 style={styles.loginButton}
               
                 underlayColor='#fff'>
                 <Text style={styles.loginText}>SIGN UP</Text>
         </TouchableOpacity>

         <Text style={{fontSize:fonts.fontSize,marginBottom:10}}>or SignUp with</Text>

         
         
             

         <View style={styles.socialView}>
         <View style={{flex:1}}>
              <TouchableOpacity onPress={this._fb.bind(this)}>
              <Image source={require('../assets/fb.png')} style={styles.social_image} resizeMode='contain'/>
              </TouchableOpacity>
         </View>
         <View style={{flex:1}}>
         <TouchableOpacity onPress={this._twitter.bind(this)}>
         <Image source={require('../assets/twitter.png')} style={styles.social_image} resizeMode='contain'/>
         </TouchableOpacity>
         </View>
         </View>

         <View style={{flexDirection:'row',marginTop:10}}>
          <Text onPress={()=> this.props.navigation.navigate("Login")} style={{fontSize:fonts.font_size}}>Already have an account ? </Text>
          <Text  onPress={()=> this.props.navigation.navigate("Login")} style={{fontWeight:'bold',fontSize:fonts.font_size}}>Log In</Text>
         </View>


         </View>
         </ScrollView>

       

        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    padding:20,

    
  },
  headerText:{
  color:'black',
  fontWeight:'bold',
  fontSize:20,
  marginBottom:20
  },
 logo_image:{
  width:'100%',
 // height:Dimensions.get('height').height * 0.2
 height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size
 },
 input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 10,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{
  
    marginTop:10,
    marginBottom:15,
    width:"100%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   paddingTop:10,
   textAlign:'center',
   color:'black'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',
   marginBottom:10,
   marginTop:10
   


 },
 social_image:{
  height:50, 
  width:'100%'
  
  
},
picker:{
  marginBottom:10
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
  

}
)
