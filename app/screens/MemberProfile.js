import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,Platform,ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
//import {  } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { Header } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';



export default class MemberProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          name:'',
          about:null,
          height:'',
          weight:'',
          age:'',
          user_image:null,
          live:0,
          state:'',
          location:''


      };


    }


    fetch(member_id){
      this.setState({loading_status:true})

      var formData = new FormData();


      formData.append('member_id', member_id);

     // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)
     let url = urls.base_url +'api/memberdetail'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type':  'multipart/form-data',
                      },
                      body: formData

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var id = responseJson.Notifications.id
                             var name = responseJson.Notifications.name
                             var location = responseJson.Notifications.country.name
                             var about = responseJson.Notifications.about
                             var user_image = responseJson.Notifications.user_image


                             this.setState({
                              name:name,
                              about:about,
                             location:location,
                             user_image:user_image



                            })

                              // var interest = responseJson.result.interest_tag
                              // var length = interest.length

                              // for(var i = 0 ; i < length ; i++){

                              //   var name_temp = interest[i].name   //interest name

                              //         // Create a new array based on current state:
                              //         let interests = [...this.state.chips];
                              //        // Add item to it
                              //         interests.push( name_temp );



                              //         this.setState({ chips:interests});




                              //  }


                            }


                          else{
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Profile not found!", ToastAndroid.SHORT)
                            : Alert.alert("Profile not found!")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          });



    }


    componentWillMount() {

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    }




    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     var result  = this.props.navigation.getParam('result')
       var member_id = result["member_id"]
       this.fetch(member_id)


    }


    render() {


        return (

         <View
         style={styles.container}>


         {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('member_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />







       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.45,
           width:'100%',borderBottomColor:'black',borderBottomWidth:1,backgroundColor:'grey'}}>


            {
              this.state.user_image == null
              ?  <Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}}  />
              : <Image source={{uri:urls.base_url+this.state.user_image}}
              resizeMode='contain'
               style={{alignSelf:'center',height:'100%',width:'100%'}}  />
            }


           </View>


        {/*middle f;oating element */}

 {/*
              <View style={{flexDirection:'row',justifyContent:'space-around',width:'40%',alignSelf:'flex-end',marginLeft:'50%',marginTop:-25}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("MemberProtest")}>
                              <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/accept.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("MemberProtest")}>
                      <Image style={{width: 55, height: 55,margin:1}}  source={require('../assets/reject.png')} />
                      </TouchableOpacity>


              </View>
         */}
          {/*end */}

           <View style={{flex:0.5,padding:20,marginTop:15,width:'100%'}}>

                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight: "bold"}}>{this.state.name}</Text>





                    </View>

                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight:'bold',color:'black'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ?  <Text style={{color:'black'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'black'}}>{this.state.about}</Text>
                      }

                      </View>


                  </View>



                   {/*weight */}


                  {/*interest */}

                 <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight:'bold',color:'black'}}>{I18n.t('location').toUpperCase()}</Text>

                      </View>



                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>{this.state.location}</Text>
                      </View>


                  </View>


           </View>



         </View>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  coin_text:{
   fontSize:19,
    marginBottom:10,
    alignSelf:'center'
   },

   indicator: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     height: 80
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
