import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,
  ToastAndroid,StatusBar,StyleSheet,ImageBackground,
  TouchableOpacity,TextInput,ActivityIndicator,TouchableWithoutFeedback,KeyboardAvoidingView,ScrollView} from 'react-native';
import { colors,fonts,urls} from './Variables';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {Header} from 'react-native-elements';



export default class EnterOtp extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          otp:'',
          loading_status:false,
          resend_counter:0

      };


    }


    validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

    isValid() {


      let valid = false;
      var isnum = /^\d+$/.test(this.state.otp);

      if (this.state.otp.trim().length > 0 ) {
         valid = true;
      }

      if (this.state.otp.trim().length === 0) {

       // ToastAndroid.show('Enter otp', ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter OTP')



        return false;
      }
      if(this.state.otp.toString().includes(".")){
        //ToastAndroid.show("Enter Valid Otp", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Valid OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter Valid OTP')



        return false;
      }

      if(!isnum){
       // ToastAndroid.show("Enter Valid Otp", ToastAndroid.SHORT);


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Valid OTP', ToastAndroid.SHORT)
      : Alert.alert('Enter Valid OTP')



        return false;
      }




      return valid;
  }

  connect(otp,email){
    var formData = new FormData();


    formData.append('email', email);
    formData.append('otp', otp);


                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/otp_validation'
                    //ToastAndroid.show(url, ToastAndroid.SHORT);
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                   body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                this.setState({loading_status:false,otp:''})
              // ToastAndroid.show(JSON.stringify(), ToastAndroid.SHORT);
                             if(!responseJson.error){
                              Platform.OS === 'android'
                              ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                              : Alert.alert(responseJson.message)


                           // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                              var result  = this.props.navigation.getParam('result')

                              result["otp"] = this.state.otp
                              var email = result["email"]


                            this.props.navigation.navigate('OtpChangePassword',{result : result});

                          }
                          else{

                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Entered OTP is incorrect", ToastAndroid.SHORT)
                            : Alert.alert("Entered OTP is incorrect")
    
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})

                          Platform.OS === 'android'
                          ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                          : Alert.alert("Connection Error !")
                        });

  }


    onSubmit(){


        if(this.isValid()){

          var result  = this.props.navigation.getParam('result')

          result["otp"] = this.state.otp
          var email = result["email"]
          this.connect(this.state.otp,email);


            //ToastAndroid.show("name  ..."+this.state.address+"LAtit..."+this.state.latitude, ToastAndroid.LONG);
           // this.props.navigation.navigate('OtpChangePassword',{result : result});

        }
}


onForgot(email){

  var formData = new FormData();


formData.append('email', email);

                this.setState({loading_status:true})
                let url = urls.base_url +'api/api_forgot_password'

                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
            body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
            this.setState({loading_status:false})
          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                         if(!responseJson.error){

                       // ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT);
                        Platform.OS === 'android'
                        ?  ToastAndroid.show("Successfully sent OTP to Email", ToastAndroid.SHORT)
                        : Alert.alert("Successfully sent OTP to Email")
                      //  /// this.props.navigation.navigate("Welcome");
                      //   let obj = {
                      //     "email" : this.state.email,

                      //   }
                      //  this.props.navigation.navigate("EnterOtp",{result : obj})
                      }
                      else{

                       

                        Platform.OS === 'android'
                        ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                        : Alert.alert(responseJson.message)
                      }
                    }).catch((error) => {
                      this.setState({loading_status:false})
                      //ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);

                      Platform.OS === 'android'
                      ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")
                    });



}



componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

}


    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     if (this.state.loading_status) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }

    }

    render() {



        return (

          <KeyboardAvoidingView style={styles.container}  behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>

 <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>



          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}

            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <ScrollView
          //contentContainerStyle={{justifyContent:'center',alignItems:'center'}}
          style={{padding:20,flex:1,marginTop:30}}>



         <View style={{marginTop:-20,width:'100%'}}>
         <Image source={require('../assets/logo.png')}
          style={{height:120,width:120,margin:25,alignSelf
          :'center'}} resizeMode='contain'/>
         <Text style={{alignSelf:'center',color:'black',fontSize:21,marginBottom:50}}>{I18n.t('password_recovery')}</Text>

         {/*
          <Text style={{alignSelf:'center',color:'black',fontSize:21,marginBottom:10}}>Forgot Password?</Text>
          <Text  style={{alignSelf:'center',marginBottom:50,fontSize:fonts.font_size}}>Enter the email address you used to sign in.</Text>
          */}

          <Text style={styles.input_text}>{I18n.t('enter_otp')}</Text>
          <TextInput
            value={this.state.otp}
            style={styles.input}
            maxLength={6}
            keyboardType = 'numeric'
            textContentType='telephoneNumber'
            onChangeText={ (otp) => {
              var a = otp.replace(/[^0-9.]/g, '')
              this.setState({otp:a})
            } }
            placeholderTextColor={'black'}
          />


          <Text onPress={()=>{
            var result  = this.props.navigation.getParam('result')
            var email = result["email"]
            let count = this.state.resend_counter
            if(count < 3){
              count = count + 1
              this.setState({resend_counter:count,otp:''})
              this.onForgot(email);
            }
            else{
              Platform.OS == 'android' ?
                ToastAndroid.show("OTP can be send only three times",ToastAndroid.SHORT)
                : Alert.alert("OTP can be send only three times")
            }

          }} style={{textDecorationStyle:'dotted',marginTop:20,marginBottom:5}}>{I18n.t('resend_otp')}</Text>
          <TouchableOpacity
          onPress={this.onSubmit.bind(this)}
                style={styles.forgotButton}>
                <Text style={styles.forgotText}>{I18n.t('enter_otp')}</Text>
          </TouchableOpacity>

          </View>

         

</ScrollView>

      {this.state.loading_status &&
            <View pointerEvents="none" style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>
        }

         </KeyboardAvoidingView>


        )
    }
}

let styles = StyleSheet.create({
  container:{

    flex:1,



  } ,
 input_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size
 },

 input: {
  width: "100%",
  height: 50,
padding:5,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

forgotButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  forgotText:{
    color:'black',
    textAlign:'center',
    fontSize :19,


  },

  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }




}
)
