import React, {Component} from 'react';
import {Text, View,Platform,Alert,Image,Dimensions,ToastAndroid,WebView,ScrollView,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import { Chip } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckBox } from 'react-native-elements'
import I18n from '../i18n';
import {Header} from 'react-native-elements';


export default class TermsWebview extends React.Component {
  constructor(props) {
    super(props);
    this.state ={

      language:1,

    };

}


componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
             if(item == 'en'){
               //english
                  this.setState({language:1})
             }
             else if(item == 'hi'){
              //hindi
                 this.setState({language:1})
            }
            else if(item == 'th'){
              //thai
                 this.setState({language:1})
            }
            else if(item == 'zh'){
              //chineses
                 this.setState({language:1})
            }
            else if(item == 'de'){
              //germany
                 this.setState({language:1})
            }
            else if(item == 'ru'){
              //russian
                 this.setState({language:1})
            }
            else if(item == 'es'){
              //spanish
                 this.setState({language:1})
            }
            else if(item == 'fr'){
              //french
                 this.setState({language:1})
            }
            else if(item == 'ja'){
              //jaapanses
                 this.setState({language:1})
            }
            else if(item == 'ar'){
              //arabic
                 this.setState({language:1})
            }
          }
          else {
               I18n.locale = 'en'
            }
  });
}

  next(){
    if(this.state.checked){
      AsyncStorage.setItem('privacy_status',"false");
      this.props.navigation.navigate("Welcome")
    }
    else{


      Platform.OS === 'android'
      ?  ToastAndroid.show('You must agree to privacy policy !', ToastAndroid.SHORT)
      : Alert.alert('You must agree to privacy policy !')




    }

  }

  render() {
    return (
       <View style = {styles.container}>

<Header

barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

leftComponent={
 <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomePage")}>
           <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
     </TouchableWithoutFeedback>
}


rightComponent={

 <View></View>

 }


statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

//centerComponent={{ text: I18n.t('buy_coins'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
containerStyle={{

  backgroundColor: colors.color_primary,
  justifyContent: 'space-around',
  alignItems:'center',
  shadowOpacity:1.7,
  shadowOffset: {
     width: 0,
     height: 3,
 },
 shadowOpacity: 0.3,
 shadowRadius: 2,
  elevation:7
}}
/>



         <WebView
         style={{flex:1,margin:7}}
         source = {{ uri:
          urls.base_url+'privacy-policy.html' }}
         />



    </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      height: '100%',


   },
   privacyButton:{
  alignSelf:'center',
    marginBottom:15,
    marginTop:10,
    width:"90%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',


 },
 privacyText:{
   color:'black',
   textAlign:'center',
   fontSize :18,
   paddingTop:10

 },
})
