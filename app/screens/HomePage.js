import React, {Component} from 'react';
import {Text, View,Alert, Image,Dimensions,ScrollView,Switch,ToastAndroid,StatusBar,Platform,BackHandler,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import { withNavigationFocus } from 'react-navigation';
import { Chip } from 'react-native-paper';
import { DrawerActions } from 'react-navigation';
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { PermissionsAndroid } from 'react-native';
import { Slider,Overlay } from 'react-native-elements';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
import VideoPlayer from 'react-native-video-controls';
import Axios from 'axios';
import ImagePicker from 'react-native-image-picker';
import {Header} from 'react-native-elements';

 class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          available_status:true,
          loading_status:false,
          gender:'',
          chips:[],
          chips_boolean:[],
          bodytype_chips:[],
          bodytype_chips_boolean:[],
          ethnicity_chips:[],
          ethnicity_chips_boolean:[],
          gallery:[],
          name:null,
          age:null,
          height:null,
          weight:null,
          user_image:null,
          about:null,
          state_name:null,
          city_name:null,
          user_image:null,
          body_type:null,
          ethnicities:null,
          banner_image:null,
          videoSource:null,
          overlay_video:false




      };


    }
    xyz = async () =>{
      //make sure to save every item in string in asycnncstoarage  otheriws it will not store i it
      AsyncStorage.multiGet(["user_id","roles_id","state","user_name","user_image"]).then(response => {

        if(response
           && response[0][1] != null
            &&response[1][1] != null
             && response[2][1] != null
             && response[3][1] != null
             && response[4][1] != null){
         this.setState({

           user_image:urls.base_url+response[4][1],
         })
        }
        else{
          this.setState({
          user_image:null,
          })
        }


        console.log(response[0][0]) // Key1
        console.log(response[0][1]) // Value1
        console.log(response[1][0]) // Key2
        console.log(response[1][1]) // Value2
      })

    }


    uploadVideo = (videoUri) => {
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

          var video={
            name: 'name.mp4',
            uri: videoUri.uri,
            type: 'video/mp4'
          }




             formData.append('video', video);
             formData.append('user_id', item);


             console.log("para-----",JSON.stringify(formData))

                          this.setState({loading_status:true,
                            overlay_video:false})

                   let url = urls.base_url +'api/video_upload'
                         //let url = 'http://webmobril.org/dev/cannabis/api/create_post'




                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                          body: formData

                        }, (progressEvent) => {
                          const progress = progressEvent.loaded / progressEvent.total;
                          console.log("progeessss",progress);
                      }).then((response) =>response.json())
                              .then((responseJson) => {
                                console.log("RESONSE---",responseJson)
                         this.setState({loading_status:false})

                                 if(!responseJson.error){

                                    Platform.OS === 'android'
                                  ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
                                  : Alert.alert('Video added successfully')


                                }
                                else{

                                  Platform.OS === 'android'
                                  ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                console.log("RESONSE---",error)

                                Platform.OS === 'android'
                                ?   ToastAndroid.show("Upload failed ! Try Again", ToastAndroid.SHORT)
                                : Alert.alert("Upload failed ! Try Again")
                              });


                      //RN FETCH BLOB



      //   var tempParam =
      //   [{
      //     name: 'video', filename: 'vid.mp4', data: RNFetchBlob.wrap(videoUri.uri), type:'video/mp4'
      //   },
      //   {
      //     name:'user_id',data:'1060'
      //   }]

      //  console.log(JSON.stringify(tempParam))

      //    RNFetchBlob.config({timeout:300000}).fetch('POST', url, {
      //     'Content-Type' : 'multipart/form-data'
      //   },
      //   [{
      //     name: 'video',
      //     filename: 'vid.mp4',
      //     data: RNFetchBlob.wrap(videoUri.uri),
      //      type:'video/mp4'
      //   },
      //   {
      //     name:'user_id',
      //     data:'1060'
      //   }]
      //   )
      //   .uploadProgress((written, total) => {
      //     var perc = ((((written / total)*100).toFixed(2))*1) +  "%"
      //     console.log('uploaded', perc)

      //     Platform.OS === 'android'
      //     ?  ToastAndroid.show('Uploaded..'+perc.toString(), ToastAndroid.SHORT)
      //     : Alert.alert('Uploaded..'+perc.toString())
      //   })
      //   .then((response) => response.json())
      //   .then((responseJson) => {
      //     if(!responseJson.error){
      //       this.setState({loading_status:false})

      //                   Platform.OS === 'android'
      //                 ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
      //                 : Alert.alert('Video added successfully')

      //               }
      //               else{

      //                 Platform.OS === 'android'
      //                 ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
      //                 : Alert.alert(responseJson.message)
      //               }
      //   })
      //   .catch((err) => {
      //     this.setState({loading_status:false})
      //     Platform.OS === 'android'
      //                           ?   ToastAndroid.show("Try Again", ToastAndroid.SHORT)
      //                           : Alert.alert("Try Again !")
      //     console.log(err);
      //   })



        //AXIOS


    //     Axios.post(url, formData, {onUploadProgress: progressEvent => console.log(progressEvent.loaded)},{
    //       headers: {

    //           'accept': 'application/json',
    //           'Content-Type': `multipart/form-data`
    //       }
    //   }
    // ).then(res => {
    //   this.setState({loading_status:false})
    //   console.log("ERRRORR",res.data.error)
    //       if(!res.data.error){


    //                     Platform.OS === 'android'
    //                   ?  ToastAndroid.show('Video added successfully', ToastAndroid.SHORT)
    //                   : Alert.alert('Video added successfully')

    //                 }
    //                 else{

    //                   Platform.OS === 'android'
    //                   ? ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
    //                   : Alert.alert(responseJson.message)
    //                 }


    // })
    // .catch(err => {
    //   this.setState({loading_status:false})
    //   console.log("error1234567",err);
    //         Platform.OS === 'android'
    //                             ?   ToastAndroid.show("Try Again", ToastAndroid.SHORT)
    //                             : Alert.alert("Try Again !")
    // });


    //XMLREQUESY

    //   var xhr = new XMLHttpRequest();


    //   xhr.onreadystatechange = (e) => {
    //     if (xhr.readyState !== 4) {
    //       return;
    //     }

    //     if (xhr.status === 200) {
    //       console.log('SUCCESS', xhr.responseText);
    //       callback(JSON.parse(xhr.responseText));
    //     } else {
    //       console.warn('request_error0',xhr.status.toString());
    //     }
    //   };

    //   xhr.open('POST', url);
    //   console.log('OPENED', xhr.status);
    //   xhr.send(formData);



    //   xhr.onprogress = function () {
    //       console.log('LOADING', xhr.status);
    //   };

    //   xhr.onload = function () {
    //       console.log('DONE', xhr.status);
    //   };

    //  // xhr.setRequestHeader('authorization', this.state.token);

    //   if (xhr.upload) {
    //   xhr.upload.onprogress = ({ total, loaded }) => console.log("PPP",loaded / total);
    //   }
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });


     }



     addVideo() {
       const options = {
         title: 'Video Picker',
         mediaType: 'video',
         videoQuality: 'medium',
         durationLimit: 30,
         takePhotoButtonTitle: 'Take Video...',
         allowsEditing: true,
        // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
         storageOptions: {
           skipBackup: true
         }
       };

       ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
           console.log('User cancelled photo picker');
          // ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
         }
         else if (response.error) {
           console.log('ImagePicker Error: ', response.error);

          // ToastAndroid.show(JSON.stringify(response),ToastAndroid.LONG)
         }
         else if (response.customButton) {
           console.log('User tapped custom button: ', response.customButton);
         }
         else {
          //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
           let source = { uri: response.uri};
           console.log("VIDEO>>>>",JSON.stringify(response))

           this.setState({
             videoSource: source ,
             overlay_video:true

           });

         //  this.uploadVideo(response.uri)




         }
       });
    }


    onFetch(){
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
         // ToastAndroid.show("IDD>>>>>>"+item, ToastAndroid.LONG);
          this.setState({loading_status:true})

          let url = urls.base_url +'api/api_user_profile?id='+item
          fetch(url, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            //'Content-Type': 'multipart/form-data',
          },


        }).then((response) => response.json())
              .then((responseJson) => {
              //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                  this.setState({loading_status:false})
              //Alert.alert(responseJson);
              console.log("Response--",responseJson)
              if(!responseJson.error){
               // ToastAndroid.show(responseJson.interest[0], ToastAndroid.LONG);

                  //success in inserting data
                    //ToastAndroid.show(JSON.stringify(responseJson.gallery), ToastAndroid.LONG);
                    var id = responseJson.result.id
                    var name = responseJson.result.name
                    var about = responseJson.result.about
                    var roles_id = responseJson.result.roles_id
                    var height = responseJson.result.height
                    var age = responseJson.result.age
                    var weight = responseJson.result.weight
                    var user_image = responseJson.result.user_image
                    var state = responseJson.result.state.name
                    var city = responseJson.result.city.name
                    var interest = responseJson.interest
                    var gender = responseJson.result.gender
                    var live = responseJson.result.live

                    var ethnicities = responseJson.ethnicities[0].name == null ? '' : responseJson.ethnicities[0].name
                    var body_type = responseJson.body_type[0].name == null ? '' : responseJson.body_type[0].name
                  // ToastAndroid.show(user_image, ToastAndroid.LONG);


                  var gallery = responseJson.gallery
                  var len = gallery.length
                  var temp_arr=[]



                  for(var i = 0 ; i < 5 ; i++){
                    var id = responseJson.gallery[0].id
                    var profile_pic = responseJson.gallery[0].profile_pic

                    var image_one = responseJson.gallery[0].image_one
                    var image_two = responseJson.gallery[0].image_two
                    var image_three = responseJson.gallery[0].image_three
                    var image_four = responseJson.gallery[0].image_four

                    if(i == 0 && profile_pic != null ){
                      var image = responseJson.gallery[0].profile_pic
                      const array = [...temp_arr];
                      array[i] = { ...array[i], id: id };
                      array[i] = { ...array[i], image: image};
                      temp_arr = array
                    }
                    else if(i == 1 && image_one != null){
                      var image = responseJson.gallery[0].image_one
                      const array = [...temp_arr];
                      array[i] = { ...array[i], id: id };
                      array[i] = { ...array[i], image: image};
                      temp_arr = array
                    }
                    else if(i == 2 && image_two != null){
                      var image = responseJson.gallery[0].image_two
                      const array = [...temp_arr];
                      array[i] = { ...array[i], id: id };
                      array[i] = { ...array[i], image: image};
                      temp_arr = array
                    }
                    else if(i == 3 && image_three != null){
                      var image = responseJson.gallery[0].image_three
                      const array = [...temp_arr];
                      array[i] = { ...array[i], id: id };
                      array[i] = { ...array[i], image: image};
                      temp_arr = array
                    }
                    else if(i == 4 && image_four != null){
                      var image = responseJson.gallery[0].image_four
                      const array = [...temp_arr];
                      array[i] = { ...array[i], id: id };
                      array[i] = { ...array[i], image: image};
                      temp_arr = array
                    }



                    }

                    this.setState({gallery : temp_arr});

                    // var gender = gender.charAt(0).toUpperCase() + gender.substring(1,gender.length)
                    this.setState({
                      name:name,
                      about:about,
                      height:height,
                      weight:weight,
                      age:age,
                      user_image:user_image,
                      city_name:state,
                      state_name:state,
                      body_type:body_type,
                      ethnicities:ethnicities,
                      available_status:live == 1 ? true : false,
                      gender:gender != null ? gender.charAt(0).toUpperCase() + gender.substring(1,gender.length) : null


                    })


                    //
                    var length = interest.length

                    for(var i = 0 ; i < length ; i++){
                   //   var id = responseJson.result.interest[i].id  //interest id
                      var name_temp = responseJson.interest[i]   //interest name

                            // Create a new array based on current state:
                            let interests = [...this.state.chips];
                           // Add item to it
                            interests.push( name_temp );


                            let interests_bool = [...this.state.chips_boolean];
                            interests_bool.push( false );

                            // Set state
                            this.setState({ chips:interests,chips_boolean:interests_bool });




                      }


//                       //making chips for ethnicity
//                       var length = responseJson.ethnicities.length
// //ToastAndroid.show(length,ToastAndroid.LONG)
//                       for(var i = 0 ; i < length ; i++){
//                      //   var id = responseJson.result.interest[i].id  //interest id
//                         var name_temp = responseJson.ethnicities[i]   //interest name

//                               // Create a new array based on current state:
//                               let ethnicitys = [...this.state.ethnicity_chips];
//                              // Add item to it
//                              ethnicitys.push( name_temp );


//                               let ethnicitys_bool = [...this.state.ethnicity_chips_boolean];
//                               ethnicitys_bool.push( false );

//                               // Set state
//                               this.setState({ ethnicity_chips:ethnicitys,ethnicity_chips_boolean:ethnicitys_bool });




//                         }


//                          //making chips for bodytype
//                       var length =  responseJson.body_type.length

//                       for(var i = 0 ; i < length ; i++){
//                      //   var id = responseJson.result.interest[i].id  //interest id
//                         var name_temp = responseJson.body_type[i]   //interest name

//                               // Create a new array based on current state:
//                               let bodytypes = [...this.state.bodytype_chips];
//                              // Add item to it
//                              bodytypes.push( name_temp );


//                               let bodytypes_bool = [...this.state.bodytype_chips_boolean];
//                               bodytypes_bool.push( false );

//                               // Set state
//                               this.setState({ bodytype_chips:bodytypes,bodytype_chips_boolean:bodytypes_bool });


//                         }




                     //ToastAndroid.show(JSON.stringify(this.state.bodytype_chips), ToastAndroid.LONG);




                    //for interest array

                   // AsyncStorage.setItem('uname', id);


                  }else{


                      ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                      Platform.OS === 'android'
                      ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                      : Alert.alert(responseJson.message)



                    }

              }).catch((error) => {
                this.setState({loading_status:false})

                Platform.OS === 'android'
                ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                : Alert.alert("Connection Error !")
              });

        }
        else {
           //do something else means go to login



          //  Platform.OS === 'android'
          //  ?   ToastAndroid.show("User not found !", ToastAndroid.SHORT)
          //  : Alert.alert("User not found !")

         }
        });

        //ToastAndroid.show("JSON.stringify(responseJson)", ToastAndroid.LONG)

 }


 fetchBanner = async () =>{
  this.setState({loading_status:true})
  var formData = new FormData();
  formData.append('role_id', 3);//our id


                let url = urls.base_url +'api/advert_list'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                  },
              body: formData

                  }).then((response) => response.json())
                      .then((responseJson) => {

                      // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                        this.setState({loading_status:false})
                        if(!responseJson.error){

                          var image = responseJson.result.advert_image
                          this.setState({banner_image:urls.base_url+image})

                        }


                      else{
                        // Platform.OS === 'android'
                        // ?  ToastAndroid.show("Empty!", ToastAndroid.SHORT)
                        // : Alert.alert("Empty!")
                      }

                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
                        // Platform.OS === 'android'
                        // ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                        // : Alert.alert("Connection Error!")
                      });



}

 handleBackWithAlert = () => {
  // const parent = this.props.navigation.dangerouslyGetParent();
    // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
  // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)

  if (this.props.isFocused) {

            if(this.state.loading_status){
                   this.setState({loading_status:false})
            }

          else{
                  Alert.alert(
                  'Exit App',
                  'Exiting the application?',
                  [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                  },
                  {
                    text: 'OK',
                    onPress: () => BackHandler.exitApp()
                  }
                  ],
                  {
                  cancelable: false
                  }
                );
          }

return true;
}
}




    sendLocation(latitude,longtitude){

      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();

         var text = this.state.message
          formData.append('user_id', item);//our id
          formData.append('latitude',latitude);
          formData.append('longitude', longtitude);


                          // this.setState({loading_status:true})
                          let url = urls.base_url +'api/user_location'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})

                                   if(!responseJson.error){

                                       // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);





                                }
                                else{

                                       // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                // Platform.OS === 'android'
                                // ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                // : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });


    }


  async  requestLocationPermission(){
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'D8Bid App',
          'message': 'D8Bid App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {


        navigator.geolocation.getCurrentPosition(
          position => {
            this.setState({

             latitude: position.coords.latitude,
             longitude: position.coords.longitude,


            });
            this.sendLocation(position.coords.latitude,position.coords.longitude)
          },
          error => {
          //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

            Platform.OS === 'android'
            ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
            : Alert.alert("Check your internet speed connection")


            //this.setState({loading_status:false})
          },
          { enableHighAccuracy: false, timeout: 10000}
        );


      } else {
        console.log("location permission denied")
       Alert.alert("location permission denied");
        //alert("Location permission denied");
      }
    } catch (err) {
      console.warn(err)
        Alert.alert(JSON.stringify(err.message));
    }
  }


    async  requestLocationPermissionIOS(){


        navigator.geolocation.getCurrentPosition(
          position => {
            this.setState({

             latitude: position.coords.latitude,
             longitude: position.coords.longitude,


            });

            this.sendLocation(position.coords.latitude,position.coords.longitude)

          },
          error => {
          //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

            Platform.OS === 'android'
            ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
            : Alert.alert("Check your internet speed connection")


            this.setState({loading_status:false})
          },
          { enableHighAccuracy: false, timeout: 10000}
        );
  }



componentWillMount() {

  this.onFetch();

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
//this.props.navigation.dispatch(DrawerActions.closeDrawer());
this.props.navigation.closeDrawer();

}

componentWillUnmount() {
 BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
 //ToastAndroid.show("unmount callled " ,ToastAndroid.LONG);

}

async componentDidMount() {
  //this.props.navigation.dispatch(DrawerActions.closeDrawer());
  this.props.navigation.closeDrawer();

    this.fetchBanner()

    Platform.OS === 'android'
    ?  await this.requestLocationPermission()
    :  await this.requestLocationPermissionIOS()
     // this.xyz()


}

      changeAvailability(){


        AsyncStorage.getItem("user_id").then((item) => {
          if (item) {
            var formData = new FormData();


            formData.append('id',item);
            if(this.state.available_status){
              formData.append('status', 1);
            }
            else{
              formData.append('status', 0);
            }


                         // this.setState({loading_status:true})
                         let url = urls.base_url +'api/model_available'
                          fetch(url, {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData

                        }).then((response) => response.json())
                              .then((responseJson) => {
                      this.setState({loading_status:false})
                    //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                                  if(!responseJson.error){

                                }
                                else{


                                    // Platform.OS === 'android'
                                    // ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                    // : Alert.alert(responseJson.message)
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})

                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                  : Alert.alert("Connection Error !")
                              });
          }
          else{
            ToastAndroid.show("User not found !",ToastAndroid.LONG)
          }
        });
      }

      _renderDotIndicator() {
        return <PagerDotIndicator pageCount={this.state.gallery.length} />;
      }


    render() {

      let makeGallery =this.state.gallery.map((gallery) => {
        return (


          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'grey'
          }}>

          <Image
          source={{uri:urls.base_url+gallery.image}}
          resizeMode='contain'
          style={{alignSelf:'center',height:'100%',width:'100%'}} />

        </View>
        );
    })




      let chips = this.state.chips.map((r, i) => {

        return (
              <Chip
              style={{width:null,margin:4,backgroundColor:colors.color_primary}}
              mode={'outlined'}
              selected={this.state.chips_boolean[i]}
              selectedColor={'black'}
              onPress={() => {

                 {/*      let ids = [...this.state.interest_boolean];
                      ids[i] =!ids[i];
                      this.setState({interest_boolean: ids });
                      */}
              }}>
              {r}
              </Chip>
        );
})



let ethnicity_chips = this.state.ethnicity_chips.map((r, i) => {

  return (
        <Chip
        style={{width:null,margin:4,backgroundColor:colors.color_primary}}
        mode={'outlined'}
        selected={this.state.ethnicity_chips_boolean[i]}
        selectedColor={'black'}
        >
        {r}
        </Chip>
  );
})


let bodytype_chips = this.state.bodytype_chips.map((r, i) => {

  return (
        <Chip
        style={{width:null,margin:4,backgroundColor:colors.color_primary}}
        mode={'outlined'}
        selected={this.state.bodytype_chips_boolean[i]}
        selectedColor={'black'}
        >
        {r}
        </Chip>
  );
})




        return (

         <View
         style={styles.container}>

         <Overlay
         isVisible={this.state.overlay_video}
         windowBackgroundColor="rgba(0, 0, 0, .5)"
         overlayBackgroundColor="white"
         width={300}
         height='auto'

       >




         <View style={{height:400,width:null,backgroundColor:'white',alignItems:'center',justifyContent:'center',padding:25}}>

         <VideoPlayer source={this.state.videoSource}  // Can be a URL or a local file.
         disableBack
         disableFullscreen
         disableVolume
         disableSeekbar
         muted={true}
         autoplay={false}
         paused={false}
         onPause={()=> console.log("Pause")}
         onPlay={()=> console.log("Play..")}
         resizeMode='stretch'
         showOnStart={true}
         seekColor={colors.color_primary}
                                           // Store reference
        style={{height:'50%',width:'100%'}} />

          <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

          <View style={{marginTop:15,borderColor:'black',borderWidth:1,
        borderRadius:10,backgroundColor:colors.color_primary,flex:1,justifyContent:'center',alignItems:'center',padding:10}}>

        <Text onPress={()=> this.uploadVideo(this.state.videoSource)
        }>{I18n.t('upload')}</Text>
        </View>


          </View>

         </View>

         <View style={{position:'absolute',right:5,top:5}}>
         <TouchableWithoutFeedback onPress={()=> this.setState({
           overlay_video:false
         })}>
         <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:20,width:20}} />
         </TouchableWithoutFeedback>
         </View>



         </Overlay>


         {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=>this.props.navigation.toggleDrawer()}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/man-user.png')} />
                  </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}


         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />




       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.40}}>

           <IndicatorViewPager
           style={{ height: '100%' }}
           indicator={this._renderDotIndicator()}>
          {makeGallery}
         </IndicatorViewPager>
       {/*
                {
                  this.state.user_image == null
                  ?  <Text style={{fontSize:fonts.font_size,marginRight:7}}>Image Not Found</Text>
                  :    <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.user_image}} style={{alignSelf:'center',height:'100%',width:'100%'}} />
                }

                 */}



           </View>


           <View style={{padding:15,backgroundColor:'#F8F7E4'}}>


                {/*for avalaible unaailable part*/}
                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                          <View style={{flexDirection:'row',alignItems:'center'}}>

                          {
                            this.state.name == null
                            ?  <Text style={{fontSize:fonts.font_size,marginRight:7}}></Text>
                            :  <Text style={{fontSize:19,marginRight:7,color:'black'}}>{this.state.name}</Text>
                          }
                          {
                            this.state.age == null
                            ? <Text style={{fontSize:fonts.font_size,marginRight:7}}></Text>
                            :<Text style={{fontSize:fonts.font_size,marginRight:17,marginTop:3}}>, {this.state.age}</Text>
                          }





                                                {
                                                  this.state.available_status ?
                                                  <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />
                                                  :
                                                  <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/red.png')} />

                                                }


                          </View>




                          <View style={{borderColor:'black',borderRadius:10,borderWidth:1,width:null,alignSelf:'flex-start',margin:10,backgroundColor:colors.color_primary}}>
                          <Text style={{margin:10,fontWeight:'bold',size:18}} onPress={()=> this.addVideo()}>{I18n.t('add_video')}</Text>
                          </View>



                </View>



                 {/*locationspart*/}
                <Text style={{marginTop:6,color:'black'}}>{this.state.city_name}</Text>

               {/*for available status part*/}
               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                  <View>
                    {
                      this.state.available_status ?
                      <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black'}}>{I18n.t('available_now')} </Text>
                      :
                      <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black'}}>{I18n.t('available_later')}</Text>
                    }

                  </View>



                  <View>
                            <Switch
                                trackColor={{true: 'green', false: 'red'}}
                                thumbTintColor="white"
                                onValueChange={()=> {
                                  this.setState({available_status:!this.state.available_status})
                                  this.changeAvailability()

                                }}
                                value={this.state.available_status}/>
                  </View>





               </View>









                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.about}</Text>
                      }
                      </View>


                  </View>



                   {/*weigh */}

                   <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('weight').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.weight == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.weight} {I18n.t('kgs')}</Text>
                      }
                      </View>


                  </View>



                   {/*weigh */}

                   <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('gender').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.gender == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.gender} </Text>
                      }
                      </View>


                  </View>



                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('height').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.height == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.height} {I18n.t('cms')}</Text>
                      }

                      </View>


                  </View>



                  {/*interest */}

                {/*  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/grey.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold'}}>INTEREST</Text>

                      </View>



                        <View style={{flexDirection:'row',flexWrap: 'wrap'}}>
                              {chips}
                        </View>



                  </View>
                  */}


                  {/*ethniicty */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('ethnicity').toUpperCase()}</Text>

                      </View>



                      <View style={{marginLeft:30}}>
                      {
                        this.state.ethnicities == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.ethnicities} </Text>
                      }

                      </View>

                  </View>


                   {/*bodytype */}


                  <View style={{marginTop:35,marginLeft:20}}>
                  <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                      <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                      <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('body_type').toUpperCase()}</Text>

                  </View>



                  <View style={{marginLeft:30}}>
                  {
                    this.state.body_type == null
                    ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                    :  <Text style={{color:'grey'}}>{this.state.body_type} </Text>
                  }

                  </View>

              </View>


           </View>



         </View>

         </ScrollView>


              {/* banner */}
              {/*
                      {
                        this.state.banner_image == null
                        ? null
                        :
                        <View style={{position:'absolute',bottom:0,left:0,right:0,height:'5%',width:'100%',backgroundColor:'white'}}>
                      <Image style={{width:'100%',height:'100%'}} source={{uri:this.state.banner_image}}></Image>

                      </View>
                      }

                       */}


         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


         </View>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },

   indicator: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     height: 80
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)

export default withNavigationFocus(HomePage);
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
