import React, {Component} from 'react';
import {Text, View,Platform,Alert, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { RadioButton } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { colors,urls,fonts} from './Variables';
import {Header} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';



export default class LocationSocialMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          countries:[],
          states:[],
          cities:[],
          country_id:'key0',
          state_id:'key0',
          city_id:'key0',
          first_checked:'',
          second_checked:'',
      };


    }


    fetchCountries = async () =>{
      this.setState({loading_status:true})

      let url = urls.base_url +'api/api_country'
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                           // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({ countries : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }
    

    getStates(countryId){
      this.setState({loading_status:true,states:[]})

      let url = urls.base_url +"api/state?country_id="+countryId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].country_id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], country_id: country_id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({states : temp_arr});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }


    getCity(stateId){
      this.setState({loading_status:true,cities:[]})

      let url = urls.base_url +"api/city?state_id="+stateId
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id
                              var country_id = responseJson.result[i].id
                              var name = responseJson.result[i].name
                             
                                    const array = [...temp_arr];
                                    array[i] = { ...array[i], id: id };
                                    array[i] = { ...array[i], name: name };

                                    temp_arr = array
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                              this.setState({cities: temp_arr,loading_status:false});

                            }


                          else{
                            //Alert.alert("Cant Connect to Server");
                            this.setState({loading_status:false})
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                          });



    }

    isValid() {
      
      var isnum = /^\d+$/.test(this.state.phone);

      let valid = false;

      if (
          this.state.country_id > 0 &&
          this.state.first_checked.length > 0
          &&  this.state.second_checked.length > 0
          
         ) {
           valid = true;
           return true;
      }

   if (this.state.country_id < 0 || this.state.country_id ==="key0" || this.state.country_id == 0) {
       
          Platform.OS === 'android' 
          ?   ToastAndroid.show("Enter Country", ToastAndroid.SHORT)
          : Alert.alert("Enter Country")

        return false;
      }
      else if (this.state.first_checked < 0 || this.state.first_checked.length === 0) {
       
       
        Platform.OS === 'android' 
        ?  ToastAndroid.show('Enter gender', ToastAndroid.SHORT)
        : Alert.alert('Enter gender')


        return false;
      }

      else if (this.state.second_checked < 0 || this.state.second_checked.length === 0) {
       
       
        Platform.OS === 'android' 
        ?  ToastAndroid.show('Enter Interested gender', ToastAndroid.SHORT)
        : Alert.alert('Enter Interested gender')



        return false;
      }
    
     

      //return valid;
  }

    onSignup(){
   
      if (this.isValid()) {
        //ToastAndroid.show("JSON.stringify(responseJson)", ToastAndroid.LONG);

       this.setState({loading_status:true})
       var formData = new FormData();
          var result  = this.props.navigation.getParam('result')
          var uid = result["user_id"]
          var roles_id = result["roles_id"]


        formData.append('user_id',uid);
        formData.append('country_id', this.state.country_id);
        formData.append('roles_id', roles_id);
        formData.append('gender', this.state.first_checked);
        formData.append('interested_gender', this.state.second_checked);

        //ToastAndroid.show(JSON.stringify(formData), ToastAndroid.LONG);
       
        let url = urls.base_url +'api/update_location'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData
 
              }).then((response) => response.json())
                    .then((responseJson) => {
                    // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                        this.setState({loading_status:false})
                    
                    if(!responseJson.error){
                        //success in inserting data
                          //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                         
                        let star = 0
                        let coin = 0
                    var id = responseJson.result.id
                    var roles_id = responseJson.result.roles_id
                    var user_name = responseJson.result.user_name
                    var name = responseJson.result.name
                    var user_image = responseJson.result.user_image
                    var create_profile_status = responseJson.result.create_profile_status
                    // var state_name = responseJson.result.state.name;
                    // var country_name = responseJson.result.country.name;
                    var age = responseJson.result.age;
                    var country_id = responseJson.result.country_id
                    var city_id = responseJson.result.city_id
                    var state_id = responseJson.result.state_id
                    var stars = responseJson.result.stars
                    var coins = responseJson.result.coins

                   // ToastAndroid.show(country_name,ToastAndroid.LONG)
                    if(stars == null){
                      stars = star
                    }
                    else{
                      stars = stars
                    }


                    if(coins == null){
                      coins = coin
                    }
                    else{
                      coins = coins
                    }

                         let obj = {
                          "user_id" : id,
                          "user_name":user_name,
                        } ;

                        if(create_profile_status == 0){

                          Platform.OS === 'android' 
                          ?   ToastAndroid.show("Account Created Sucessfully and is under review", ToastAndroid.SHORT)
                          : Alert.alert("Account Created Sucessfully and is under review")

                          this.props.navigation.navigate('Login');



                          // if(roles_id == '1'){
                          //   //AsyncStorage.setItem('user_id', id.toString());
                          //   this.props.navigation.navigate('CreateProfileModel',{result : obj});
                          //  }
                          //  else if(roles_id == '2'){
                          //  // AsyncStorage.setItem('user_id', id.toString());
                          //   this.props.navigation.navigate('CreateProfileMember',{result : obj});
                          //  }
                         }
                         else{

                          if(roles_id == '2'){
                            // AsyncStorage.setItem('user_id', id.toString());
                            AsyncStorage.multiSet([
                              ["user_id",id.toString()], 
                              ["roles_id", roles_id.toString()],
                               ["state",responseJson.result.state.name.toString()],
                               ["user_name", name.toString()],
                               ["user_image", user_image.toString()],
                               ["user_age", age.toString()],
                               ["stars", stars.toString()]
                             ]);
                             this.props.navigation.navigate('HomeModel');
                            }
                            else if(roles_id == '3'){
                           // AsyncStorage.setItem('user_id', id.toString());
                              AsyncStorage.multiSet([
                                ["user_id",id.toString()],
                                ["roles_id", roles_id.toString()],
                                ["state", responseJson.result.country.name.toString()],
                                ["user_name", name.toString()],
                                 ["user_image", user_image.toString()],
                                 ["coins", coins.toString()]
                               ]);
                             this.props.navigation.navigate('HomeMember');
                            }

                         }
                        
 
                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);

                            Platform.OS === 'android' 
                            ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)
 
                          }
 
                    }).catch((error) => {
                      this.setState({loading_status:false})
                      Platform.OS === 'android' 
                            ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error !")
                    });
 
 
       }
 
 }


 componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });


}




    componentDidMount() {
      this.fetchCountries()
      var result  = this.props.navigation.getParam('result')
      var uid = result["user_id"]
      var roles_id = result["roles_id"]
      
    }
   
    render() {

   

      let makecountries =this.state.countries.map((country) => {
        return (
          <Item label={country.name} value={country.id} key={country.id}/>
        )
    })

     



        return (
          <View 
         style={styles.container}>

<Header
         
         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> {
          const { navigation } = this.props;
          navigation.goBack();
        }}>
              <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
        </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>
                  

                  
          }


           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            onPress={() => this.props.navigation.navigate("")}
           
            outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{

           backgroundColor: 'white',
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 1,
          },
          shadowOpacity: 0.1,
          shadowRadius: 1,
           elevation:1
         }}
       />
          <View style={{justifyContent:'center',alignItems:'center',padding:20,flex:1,marginTop:-30}}>



        <Text style={styles.headerText}>{I18n.t('profile_details').toUpperCase()}</Text>


       <Text style={styles.input_text}>{I18n.t('country')}</Text>
        <View style={{width:'100%',borderRadius:10,borderWidth:1,borderColor:'black',marginBottom:10}}>
                    <Picker
          
                          mode="dropdown"
                          selectedValue={this.state.country_id}
                          onValueChange={(itemValue, itemIndex) =>
                            {
                              this.setState({country_id: itemValue})
                              if(itemIndex > 0){
                               
                              
                              }  
                            }}>
                          <Item label="Select Country" value="key0" />
                             {makecountries}
                  </Picker>
            </View>

          

{/** start of 21st radio */}
<Text style={{width:'100%',marginTop:20,marginBottom:10,fontSize:fonts.font_size}}>{I18n.t('gender')}</Text> 
<View style={{width:'100%'}}>
<RadioButton.Group
 onValueChange={value => this.setState({ value })}
 value={this.state.value}
>

<View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

 <View style={{flex:1}}>
 <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android  
  value="male"
  color={colors.color_primary}
  status={this.state.first_checked === 'male' ? 'checked' : 'unchecked'}
  onPress={() => { this.setState({ first_checked: 'male' }); }}/>
   <Text>{I18n.t('male')}</Text>
 </View>
 </View>


 <View  style={{flex:1}}>
    <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android 
    value="female"
    color={colors.color_primary}
    status={this.state.first_checked === 'female' ? 'checked' : 'unchecked'}
    onPress={() => { this.setState({ first_checked: 'female' }); }} />
   <Text>{I18n.t('female')}</Text>
 </View>
 </View>


 <View style={{flex:1}}>
 <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android  
  value="transgender"
  color={colors.color_primary}
  status={this.state.first_checked === 'transgender' ? 'checked' : 'unchecked'}
  onPress={() => { this.setState({ first_checked: 'transgender' }); }}/>
   <Text>{I18n.t('transgender')}</Text>
 </View>
 </View>

 </View>







</RadioButton.Group>
</View>

   {/** end 1st radio */}   



{/** start of 2nd radio */}
<Text style={{width:'100%',marginTop:10,marginBottom:10,fontSize:fonts.font_size}}>{I18n.t('interested_gender')}</Text>
<View style={{width:'100%'}}>
<RadioButton.Group
 onValueChange={interested_value => this.setState({ interested_value })}
 value={this.state.interested_value}
>

<View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

 <View style={{flex:1}}>
 <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android 
  value="second_male"
  color={colors.color_primary}
  status={this.state.second_checked === 'male' ? 'checked' : 'unchecked'}
  onPress={() => { this.setState({ second_checked: 'male' }); }}/>
   <Text>{I18n.t('male')}</Text>
 </View>
 </View>


 <View  style={{flex:1,marginRight:10}}>
    <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android 
    value="second_female"
    color={colors.color_primary}
    status={this.state.second_checked === 'female' ? 'checked' : 'unchecked'}
    onPress={() => { this.setState({ second_checked: 'female' }); }} />
   <Text>{I18n.t('female')}</Text>
 </View>
 </View>


 <View style={{flex:1}}>
 <View style={{flexDirection:'row',alignItems:'center'}}>
   <RadioButton.Android 
  value="transgender"
  color={colors.color_primary}
  status={this.state.second_checked === 'transgender' ? 'checked' : 'unchecked'}
  onPress={() => { this.setState({ second_checked: 'transgender' }); }}/>
   <Text>{I18n.t('transgender')}</Text>
 </View>
 </View>



 </View>



</RadioButton.Group>
</View>
   {/** end 2nd radio */}
          
         

          <TouchableOpacity
                 onPress={this.onSignup.bind(this)}
                 style={styles.loginButton}
               
                 underlayColor='#fff'>
                 <Text style={styles.loginText}>{I18n.t('sign_up').toUpperCase()}</Text>
         </TouchableOpacity>

       

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }
         
             
</View>
         

         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
  
    flex:1,
  
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'09%',
    backgroundColor: '#D2AC45'

  },
  body:{
    width:'100%',
    flex:1,
   
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{
  
    marginTop:50,
    marginBottom:15,
    width:"100%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   paddingTop:10,
   textAlign:'center',
   color:'black'
 },
 headerText:{
    color:'black',
    fontWeight:'bold',
    fontSize:20,
    marginBottom:20
 },
 input_text:{
   width:'100%',
   marginBottom:10,
   marginLeft:7
 },
 indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}

  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}