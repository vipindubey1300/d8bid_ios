import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,StatusBar,ScrollView,StyleSheet,FlatList,Platform,TouchableWithoutFeedback, BackHandler,TextInput,ImageBackground,TouchableOpacity,ActivityIndicator} from 'react-native';
import { colors,fonts,urls } from './Variables';
import {Card} from 'native-base';
import { withNavigationFocus } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {Header} from 'react-native-elements';


export default class ChatList extends Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           chats:[],
           role:'',
           isFetching:false

                  };

    }





    fetchChats(uid){
      this.setState({loading_status:true})

                       let url = urls.base_url +'api/chat_list?member_id='+uid
                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {
                           // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                          // ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false,isFetching:false})
                            if(!responseJson.error){

                              var length = responseJson.chat_lists.length.toString();

                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){

                                    var chat_id = responseJson.chat_lists[i].id
                                    var form_id = responseJson.chat_lists[i].form_id
                                    var to_id = responseJson.chat_lists[i].to_id

                                    var message = responseJson.chat_lists[i].message
                                    var flinks = responseJson.chat_lists[i].flinks
                                    var attachment = responseJson.chat_lists[i].attachment
                                    var address = responseJson.chat_lists[i].address
                                    var lat = responseJson.chat_lists[i].lat
                                    var lng = responseJson.chat_lists[i].lng

                                    var updated_at = responseJson.chat_lists[i].updated_at
                                    var time = updated_at.substring(10, 16);



                                    var name = responseJson.user[i] == null ? '' : responseJson.user[i].name
                                    var user_image = responseJson.user[i] == null ? '' : responseJson.user[i].user_image



                                    // //var message = responseJson.User[i].message
                                    // var user_id = responseJson.User[i].models[0].id
                                    // var name = responseJson.User[i].models[0].name
                                    // var user_image = responseJson.User[i].models[0].user_image
                                    // var updated_at = responseJson.User[i].updated_at



                                          const array = [...temp_arr];
                                          array[i] = { ...array[i], chat_id: chat_id };
                                          array[i] = { ...array[i], time: time };
                                          array[i] = { ...array[i], form_id:form_id };
                                          array[i] = { ...array[i], to_id:to_id };

                                          array[i] = { ...array[i], name:name };
                                          array[i] = { ...array[i], user_image: user_image };

                                          array[i] = { ...array[i], message:message};
                                          array[i] = { ...array[i], flinks:flinks};
                                          array[i] = { ...array[i], attachment:attachment};
                                          array[i] = { ...array[i], address:address};
                                          array[i] = { ...array[i], lat:lat};
                                          array[i] = { ...array[i], lng:lng};



                                          temp_arr = array

                              }
                              this.setState({ chats : temp_arr});

                            }


                          else{
                            // Platform.OS === 'android'
                            // ?  ToastAndroid.show("No Chats Found", ToastAndroid.SHORT)
                            // : Alert.alert("No Chats Found")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          });



    }


  async componentDidMount(){
    const token = await AsyncStorage.getItem('roles_id');
    const uid = await AsyncStorage.getItem('user_id');
    // ToastAndroid.show("Token is "+uid,ToastAndroid.LONG)
    this.setState({ role : token });
    this.fetchChats(uid)

    // if(token == 2){
    //   //model
    //   this.fetchModelChats(uid)
    // }

    // else if (token == 3){
    //   //member
    //   this.fetchMemberChats(uid)
    // }
  }



	next(member_id,model_id){
    if(this.state.role == 3){
      //model
            if(this.state.chats.length > 0){
              //var jobs = this.state.jobs
              for(var i = 0 ; i < this.state.chats.length ; i++){
                if(this.state.chats[i].model_id == model_id){
                  result={}
                  result["model_id"] = this.state.chats[i].model_id
                  result["image"] = this.state.chats[i].user_image
                  result["name"] = this.state.chats[i].name

                  this.props.navigation.navigate("ChatBoxModel",{result : result});
                }
              }
            }
    }

    else if(this.state.role == 2 ){
      //member
            if(this.state.chats.length > 0){
              //var jobs = this.state.jobs
              for(var i = 0 ; i < this.state.chats.length ; i++){
                if(this.state.chats[i].from_id == from_id){
                  result={}
                  result["member_id"] = this.state.chats[i].member_id
                  result["image"] = this.state.chats[i].user_image
                  result["name"] = this.state.chats[i].name
                  //model chattng to member
                  this.props.navigation.navigate("ChatBoxMember",{result : result});
                }
              }
            }
    }

  }


  end(id){
    //ToastAndroid.show("end cht",ToastAndroid.LONG)
   // ToastAndroid.show('feetchhh meessage',ToastAndroid.SHORT)
     AsyncStorage.getItem("user_id").then((item) => {
       if (item) {
         var formData = new FormData();


//ToastAndroid.show(item+'...'+this.state.user_id,ToastAndroid.LONG)


         formData.append('form_id', item);//our id
          formData.append('to_id',id);
          formData.append('type_status',0);
         //formData.append('form_id', 1096);
         //formData.append('to_id',961);



                         // this.setState({loading_status:true})
                         let url = urls.base_url +'api/chat_message'

                       //  / ToastAndroid.show(url+".."+JSON.stringify(formData),ToastAndroid.LONG)

                         fetch(url, {
                         method: 'POST',
                         headers: {
                           'Accept': 'application/json',
                           'Content-Type': 'multipart/form-data',
                         },
                     body: formData

                       }).then((response) => response.json())
                             .then((responseJson) => {
                            // this.setState({loading_status:false,message:this.state.message})
               // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)

                         console.log("Ressss",responseJson)
                                  if(!responseJson.error){



                                  // ToastAndroid.show('ddddddddddddddddddd',ToastAndroid.LONG)




                               }
                               else{


                                 //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                               }
                             }).catch((error) => {
                               this.setState({loading_status:false})

                               // Platform.OS === 'android'
                               // ?   ToastAndroid.show('gefetch message', ToastAndroid.SHORT)
                               // : Alert.alert("Connection Error !")

                               //ToastAndroid.show(error.message, ToastAndroid.SHORT)
                             });
       }
       else{
        // ToastAndroid.show("User not found !",ToastAndroid.LONG)
       }
     });


   }

  endChat = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
  // ToastAndroid.show("Recieved data is "+JSON.stringify(data) ,ToastAndroid.LONG);
    //this.setState(data);

    if(data == null || data == undefined){

    }
    else{
      this.end(data['to_id'])
    }


  };


	nextNew(form_id){
    if(this.state.chats.length > 0){
      //var jobs = this.state.jobs
      for(var i = 0 ; i < this.state.chats.length ; i++){
        if(this.state.chats[i].form_id == form_id){
          result={}
          result["to_id"] = this.state.chats[i].form_id
          result["image"] = this.state.chats[i].user_image
          result["name"] = this.state.chats[i].name

          this.props.navigation.navigate("ChatBox",{result : result,endChat:this.endChat});
        }
      }
    }

  }


  componentWillMount() {

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });


  }

  onSelect = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
   // ToastAndroid.show("Recieved data is "+ data['selected'],ToastAndroid.LONG);
    //this.setState(data);
  };

        onRefresh() {
          this.setState({ isFetching: true }, function() { this.fetchChats() });
      }



      renderItem = ({item}) =>  {

        if(item.sent_flag){
           if(item.message != null ){
             return (

               <TouchableOpacity onLongPress={()=> this.copyText(item.message)}>
               <View style={{width:Dimensions.get('window').width * 0.7,margin:3,alignSelf:'flex-end'}}>

                       <View style={{ backgroundColor:'#d0d0d0',width:null,height:null,
                       alignSelf:'flex-end',flexDirection:'row',alignItems:'center'}}>

                         <Text style={{fontSize:15,marginRight:4,color:'black'
                        ,padding:10,width:'auto',
                         alignSelf:'flex-start',borderRadius:5,alignSelf:'flex-end'}}>{item.message}</Text>

                         <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10}}>

                         <Text style={{marginRight:10,fontSize:12}}>{item.created_at.toString().substring(11,16)}</Text>

                         {
                           item.reading_status ==  0
                           ? <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                           : <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                         }

                         </View>

                         </View>

               </View>
               </TouchableOpacity>
             );
           }

           else  if(item.flinks != null ){

             return (

               <View style={{width:'100%',margin:3,height:null,padding:5}}>

               <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-end'}}>
               <TouchableWithoutFeedback>
                 <View style={{width:'100%',justifyContent:'space-between'}}>
                     <View></View>

                 <Image style={{height:120,width:120,alignSelf:'flex-end'}} source={{uri: urls.base_url+item.flinks}} />


                 <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-end'}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                         {
                           item.reading_status ==  0
                           ? <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                           : <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                         }

                         </View>



                 </View>




                   </TouchableWithoutFeedback>
                   </View>

               </View>
             );

           }

           else if(item.address != null){
             return (


               <TouchableWithoutFeedback onPress={()=> this.seeLocation(item.address,item.lat,item.lng)}>

                 <View style={{width:null,padding:10,alignSelf:'flex-end',backgroundColor:'white',margin:5}}>


                 <Image style={{width:60, height: 60,margin:10}}  source={require('../assets/location-view.png')} />

                 <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                         {
                           item.reading_status ==  0
                           ? <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                           : <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                         }

                         </View>


                 </View>


                   </TouchableWithoutFeedback>

             );
           }



           else  if(item.attachment != null ){

             return (

               <View style={{width:'100%',margin:3}}>

               <Card style={{width:null,height:null,padding:5,alignSelf:'flex-end'}}>
               <TouchableWithoutFeedback>
                 <View style={{width:'100%'}}>


                 <Image style={{height:180,width:180}} source={{uri: urls.base_url +item.attachment}} />

                 <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10,marginTop:10}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>

                         {
                           item.reading_status ==  0
                           ? <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/read.png')} />
                           : <Image
                           style={{width: 15, height: 15,alignSelf:'flex-end'}}  source={require('../assets/seen.png')} />


                         }

                         </View>


                 </View>


                   </TouchableWithoutFeedback>
               </Card>

               </View>
             );

           }
        }




        else{
         if(item.message != null ){
           return (


             <TouchableOpacity onLongPress={()=> this.copyText(item.message)}>
             <View style={{width:Dimensions.get('window').width * 0.7,margin:3,alignSelf:'flex-start'}}>

                     <View style={{ backgroundColor:colors.color_primary,
                     width:null,height:null,alignSelf:'flex-start',flexDirection:'row',alignItems:'center'}}>

                       <Text style={{fontSize:15,marginRight:4,color:'black'
                      ,padding:10,width:'auto',
                       alignSelf:'flex-start',borderRadius:5,alignSelf:'flex-start'}}>{item.message}</Text>

                       <View style={{flexDirection:'row',
                       justifyContent:'space-between',alignItems:'center',marginLeft:10,
                       marginRight:10}}>

                       <Text style={{marginRight:10,fontSize:12}}>{item.created_at.toString().substring(11,16)}</Text>



                       </View>

                       </View>

             </View>
             </TouchableOpacity>
           );
         }


         else if(item.address != null){
           return (


             <TouchableWithoutFeedback onPress={()=> this.seeLocation(item.address,item.lat,item.lng)}>

             <View style={{width:null,padding:10,alignSelf:'flex-start',backgroundColor:'white',margin:5}}>


             <Image style={{width:60, height: 60,margin:10}}  source={require('../assets/location-view.png')} />

             <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-end'}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>


                         </View>





             </View>


               </TouchableWithoutFeedback>
           );
         }

         else  if(item.flinks != null ){

           return (

             <View style={{width:'100%',margin:3,height:null,padding:5}}>

             <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-start'}}>
             <TouchableWithoutFeedback>
               <View style={{width:'100%',justifyContent:'space-between'}}>
               <Image style={{height:120,width:120}} source={{uri: urls.base_url+item.flinks}} />
               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10,marginTop:10,alignSelf:'flex-start'}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>


                         </View>




               </View>


                 </TouchableWithoutFeedback>
                 </View>

             </View>
           );

         }



         else  if(item.attachment != null ){

           return (

             <View style={{width:'100%',margin:3}}>

             <Card style={{width:null,height:null,padding:5,alignSelf:'flex-start'}}>
             <TouchableWithoutFeedback>
               <View style={{width:'100%'}}>


               <Image style={{height:180,width:180}} source={{uri: urls.base_url +item.attachment}} />

               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginLeft:10,
                         marginRight:10,marginBottom:10,marginTop:10}}>

                         <Text style={{marginRight:10}}>{item.created_at.toString().substring(11,16)}</Text>


                         </View>



               </View>


                 </TouchableWithoutFeedback>
             </Card>

             </View>
           );

         }
        }
       }



    render() {





        return (

         <View
         style={styles.container}>

         {/* headrer */}
         <Header

          barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

         leftComponent={
           <TouchableWithoutFeedback onPress={() =>this.props.navigation.toggleDrawer()}>
                      <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/man-user.png')} />
                   </TouchableWithoutFeedback>
         }


          rightComponent={

           <View></View>

           }


          statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

         centerComponent={{ text: I18n.t('chat_list'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
          outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
          containerStyle={{

            backgroundColor: colors.color_primary,
            justifyContent: 'space-around',
            alignItems:'center',
            shadowOpacity:1.7,
            shadowOffset: {
               width: 0,
               height: 3,
           },
           shadowOpacity: 0.3,
           shadowRadius: 2,
            elevation:7
          }}
        />

                     {/* body */}

          <ScrollView style={{width:'100%',padding:10,backgroundColor:'#F5F0F0',flex:1}}>


          {
            this.state.chats.length == 0
            ?
            <Image style={{width: 200, height: 200,marginTop:100,alignSelf:'center'}}
            resizeMode='contain'
                    source={require('../assets/no_chat.png')} />
            :

            <FlatList
							  style={{marginBottom:10}}
                data={this.state.chats}
                 numColumns={1}
                 inverted
							  showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.isFetching}
                //renderItem={this.renderItem}
							  renderItem={({item}) =>


                 <View style={{width:'100%',margin:3}}>

									  <Card style={{width:'97%',height:null,padding:5}}>
                    <TouchableWithoutFeedback onPress={()=> this.nextNew(item.form_id)}>




                      <View style={{width:'100%',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                              <View style={{flexDirection:'row',alignItems:'center'}}>
                                   <Image style={{width: 60, height: 60,borderRadius:30,overflow:'hidden',marginRight:10}}  source={{uri:urls.base_url+item.user_image}} />

                                    <View>

                                        <Text style={{fontSize:15,marginRight:4}}>{item.name}</Text>

                                    </View>

                             </View>

                                  <View style={{alignItems:'center'}}>

                                       <Text>{item.time}</Text>
                                  </View>


                      </View>


                        </TouchableWithoutFeedback>
                    </Card>

                    </View>

							  }
							  keyExtractor={item => item.id}
							/>

          }



            </ScrollView>

            {this.state.loading_status &&
              <View pointerEvents="none" style={styles.loading}>
                <ActivityIndicator size='large' />
              </View>
          }


         </View>



        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',

    flex:1,



  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,

 },
 input: {
  width: "100%",
  height: 44,
  textAlign: 'left',


},
loginButton:{

    margin:6,
    width:"100%",
    height:40,
    backgroundColor:'#FFC300',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',


 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',



 },
 social_image:{
  height:50,
  width:'100%'


},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}


}
)
