import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,Switch,ToastAndroid,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

//import {  } from 'react-native-gesture-handler';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";



export default class ModelProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          available_status:false,
          
         
      };


    }




    componentDidMount() {
   // StatusBar.setBackgroundColor('#0040FF')
      
    }
  

    render() {
        return (
      
         <View 
         style={styles.container}>


         {/*for header*/}
          <View style = {styles.header}>

                  <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                        <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
                  </TouchableWithoutFeedback>

                  <View>
                    <Text style={{fontSize: 21,fontWeight: 'bold', color: "black",paddingRight:25}}>Model Profile</Text>
                  </View>

                  <View>
                  </View>

        </View>

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.45}}>
          
         

                <Image source={require('../assets/female.png')} style={{alignSelf:'center',height:'100%',width:'100%'}} />


           </View>


           <View style={{padding:15}}>
                

                {/*for avalaible unaailable part*/}
                <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                          <View style={{flexDirection:'row',alignItems:'center'}}>
                                            <Text style={{fontSize:fonts.font_size,marginRight:7}}>Freda Jones</Text>
                                                <Text style={{fontSize:fonts.font_size,marginRight:7}}>,</Text>
                                                <Text style={{fontSize:fonts.font_size,marginRight:17}}>23</Text>
                                                {
                                                  this.state.available_status ?
                                                  <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />
                                                  :
                                                  <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/red.png')} />

                                                }
                                                
                                                
                          </View>





                </View>


               {/*for available status part*/}
               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                  <View>
                    {
                      this.state.available_status ?
                      <Text style={{fontSize:fonts.font_size,marginRight:7}}>Availble</Text>
                      :
                      <Text style={{fontSize:fonts.font_size,marginRight:7}}>Unavailable</Text>
                    }
                                       
                  </View>

                  <View>
                            <Switch 
                                trackColor={{true: 'green', false: 'red'}}
                                thumbTintColor="white"
                                onValueChange={()=> this.setState({available_status:!this.state.available_status})}
                                value={this.state.available_status}/>
                  </View>


               
               
               
               </View>








            
                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>ABOUT</Text>
                      
                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>sdfjksbdf sdfjsdfh sdfjsdhf rtetoi sdfj qwt sfd jsf sdfjis fsjofos rj sdfofnsdhwer hwoerh bhd hworh hsoaidh qwoehq asdhoadas oashdasd hiasdoias hoasdihasd</Text>
                      </View>
                     

                  </View>



                   {/*status */}

                   <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>STATUS</Text>
                      
                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>Single , Looking for Hookups</Text>
                      </View>
                     

                  </View>



                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>HEIGHT</Text>
                      
                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>5'6''</Text>
                      </View>
                     

                  </View>



                  {/*interest */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7}}>INTEREST</Text>
                      
                      </View>



                      <View style={{marginLeft:30}}>
                      <Text style={{color:'black'}}>5'6''</Text>
                      </View>
                     

                  </View>
            

           </View>

               
    
         </View>

         </ScrollView>
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'07%',
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
   
    
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}