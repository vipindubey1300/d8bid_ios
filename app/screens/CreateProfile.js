import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,ScrollView,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import { Chip } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import {Header} from 'react-native-elements';



export default class CreateProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
         imageSource:null,
         imageData:null,
          interest_boolean: [],
          interest_id:[],
          interest_names:[],
          photo:null,
          name:'',
          about:'',
      };


    }

    isValid() {
      

      let valid = false;

      if (this.state.imageSource != null &&
          this.state.name.trim().length > 0 &&
          this.state.interest_boolean.includes(true)
          //this.state.interest_boolean.filter(Boolean).length > 0 
           ) {
        valid = true;
      }

      if (this.state.imageSource === null) {
        
        ToastAndroid.show('Upload Display Picture', ToastAndroid.SHORT);
        return false;
      }
      else if(this.state.name.trim().length === 0){

        ToastAndroid.show('Enter name', ToastAndroid.SHORT);
        return false;
      } 
      else if (this.state.interest_boolean.filter(Boolean).length < 0 || this.state.interest_boolean.filter(Boolean).length == 0  ) {
       
        ToastAndroid.show('Select Interest tags!', ToastAndroid.SHORT);
        return false;
      }

      return valid;
  }



    onCreate(){
   
      if (this.isValid()) {

        //making paramters for the interest tag
        var length = this.state.interest_boolean.length;
        var final= "";
        for(var i = 0 ; i < length ; i++){
          if(this.state.interest_boolean[i] === false){
           //do nothing
  
          }
          else{
            let temp = this.state.interest_id[i].toString()
            final = final + temp
            final = final + ","
          }
        }
        var final_interest = final.substring(0, final.length-1);
        //ToastAndroid.show("ToastItems"+final, ToastAndroid.SHORT);


       this.setState({loading_status:true})
       var formData = new FormData();
        
        formData.append('user_id', this.state.username);
        formData.append('display_name', this.state.name);
        formData.append('about', this.state.about);
        formData.append('interest_tag', final_interest);
        formData.append('profile_pic', this.state.photo);
       
 
                fetch('http://webmobril.org/dev/d8bid/api/create_profile', {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData
 
              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false,username:'',password:''})
                    //Alert.alert(responseJson);
                    if(!responseJson.error){
                          //success in inserting data
                          //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                          var id = responseJson.result.User.id
                          AsyncStorage.setItem('uname', id);
                           this.props.navigation.navigate('Home');
 
                        //   this.props.navigation.dispatch(NavigationActions.reset({
                        //     index: 0,
                        //     key: null,
                        //     actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })]
                        // }))
 
 
                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({error:responseJson.message})
                            ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
 
                          }
 
                    }).catch((error) => {
                      console.error(error);
                      //this.props.navigation.navigate("NoNetwork")
                      return;
                    });
 
 
       }
 
 }

    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };
   
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
   
        if (response.didCancel) {
          console.log('User cancelled photo picker');
          ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: response.fileName,
          };


          this.setState({
            imageSource: source ,
            imageData: response.data,
            photo:photo,
   
          });
         

          {/*photo is a file object to send to parameters */}
        }
      });
   }


    fetchInterest = async () =>{
      this.setState({loading_status:true})


                      fetch('http://webmobril.org/dev/d8bid/api/api_interest', {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name 

                                    // Create a new array based on current state:
                                    let interests = [...this.state.interest_names];
                                    // Add item to it
                                    interests.push( name );


                                    let interests_ids = [...this.state.interest_id];  
                                    interests_ids.push(id );


                                    let interests_bool = [...this.state.interest_boolean];  
                                    interests_bool.push( false );

                                    // Set state
                                    this.setState({ interest_id:interests_ids,interest_names:interests,interest_boolean:interests_bool });
                             
                                 
                                    
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                             // this.setState({ countries : temp_arr});

                            }


                          else{
                            Alert.alert("Cant Connect to Server");
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            ToastAndroid.show("Connection Error!", ToastAndroid.SHORT);
                          });



    }

    next(){
      var length = this.state.interest_boolean.length;
      var final= "";
      for(var i = 0 ; i < length ; i++){
        if(this.state.interest_boolean[i] === false){
         //do nothing

        }
        else{
          let temp = this.state.interest_id[i].toString()
          final = final + temp
          final = final + ","
        }
      }
      ToastAndroid.show("Final Items"+final, ToastAndroid.SHORT);
   

    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')
     this.fetchInterest()
      
    }

    check(){

      if(this.state.imageSource === null){
        return ( <Image source={require('../assets/avatar.png')}
        style={styles.image} resizeMode='contain'/>
         )
      }
    
      else{
          return (<Image source={this.state.imageSource}
             style={styles.image} resizeMode='contain'/>
             )
      }
  
  
    }
 

    render() {

      let imageResult = this.check()

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }


      let chips = this.state.interest_names.map((r, i) => {

        return (
              <Chip 
              style={{width:null,margin:4}}
              mode={'outlined'}
              selected={this.state.interest_boolean[i]}
              selectedColor={'black'}
              onPress={() => {
            
                      let ids = [...this.state.interest_boolean];    
                      ids[i] =!ids[i];                  
                      this.setState({interest_boolean: ids });         
              }}>
              {r}
              </Chip>
        );
})


        return (
      
         <View 
         style={styles.container}>


         {/*for header*/}
			 <View style = {styles.header}>

              <TouchableWithoutFeedback onPress={()=> this.props.navigation.goBack()}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>

              <View>
                <Text style={{fontSize: 21,fontWeight: 'bold', color: "black",paddingRight:25}}>Create Profile</Text>
              </View>

              <View>
              </View>

     </View>

       {/*for main content*/}

       <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>



           <TouchableWithoutFeedback onPress={this.selectPhotoTapped.bind(this)}>
                   {imageResult}
         </TouchableWithoutFeedback>     


                 <Text style={styles.name_text}>Member Display Name</Text>
                <TextInput
                  value={this.state.name}
                  onChangeText={(name) => this.setState({ name })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />

                   <Text style={styles.about_text}>About Bio (Optional)</Text>
                <TextInput
                  value={this.state.about}
                  onChangeText={(about) => this.setState({ about })}
                  style={styles.about_input}
                  placeholderTextColor={'black'}
                />

                <View style={{flexDirection:'row',flexWrap: 'wrap'}}>
                     {chips}
                </View>

            

                <TouchableOpacity
                onPress={this.next.bind(this)}
                      style={styles.saveButton}>
                      <Text style={styles.saveText}>SAVE</Text>
                </TouchableOpacity>
    
    </View>

       </ScrollView>  
         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height:'09%',
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:30,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
 },

 about_input: {
  width: "100%",
  height: 120,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  image:{
    height:140,
    width:140,
    alignSelf:'center',
    marginBottom:30
  },
  
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
 
 
  

}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View> 
*/}