import React, {Component} from 'react';
import {Text, View,Platform,Alert,Image,Dimensions,ToastAndroid,WebView,ScrollView,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import { Chip } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckBox } from 'react-native-elements'


export default class TermsWebview extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
       
        checked:false,

    };

}

  next(){
    if(this.state.checked){
      AsyncStorage.setItem('privacy_status',"false");
      this.props.navigation.navigate("ChooseLanguage")
    }
    else{

       
      Platform.OS === 'android' 
      ?  ToastAndroid.show('You must agree to privacy policy !', ToastAndroid.SHORT)
      : Alert.alert('You must agree to privacy policy !')



     
    }
    
  }

  render() {
    return (
       <View style = {styles.container}>
         <WebView
         style={{flex:1}}
         source = {{ uri:
          urls.base_url+'privacy-policy.html'  }}
         />

         <View style={{width:'100%'}}>
         

         <View style={{ flexDirection: 'row',marginLeft:-10 ,alignSelf:'center'}}>
                <CheckBox
                center
                checkedColor={colors.color_primary}
                onPress={() => this.setState({ checked: !this.state.checked })}
                checked={this.state.checked}
              />
                <Text style={{marginTop: 18,marginLeft:-15}}>Are you above 18+ ? </Text>
     </View>
         <TouchableOpacity
          onPress={this.next.bind(this)}
                 style={styles.privacyButton}
            >
                 <Text style={styles.privacyText}>Accept</Text>
         </TouchableOpacity>
         </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      height: '100%',
      padding:10,
     
   },
   privacyButton:{
  alignSelf:'center',
    marginBottom:15,
    marginTop:10,
    width:"90%",
    height:50,
    backgroundColor:colors.color_primary,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 privacyText:{
   color:'black',
   textAlign:'center',
   fontSize :18,
   paddingTop:10
   
 },
})
