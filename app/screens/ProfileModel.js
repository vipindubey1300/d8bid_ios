import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ScrollView,ToastAndroid,StatusBar,StyleSheet,Platform,ActivityIndicator,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import AsyncStorage from '@react-native-community/async-storage';
//import {  } from 'react-native-gesture-handler';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import {IndicatorViewPager,  PagerDotIndicator,} from 'rn-viewpager';
import { Header, Avatar, Icon } from 'react-native-elements';
import { Chip } from 'react-native-paper';
import I18n from '../i18n';
import RBSheet from "react-native-raw-bottom-sheet";
import VideoPlayer from 'react-native-video-controls';



export default class ProfileModel extends React.Component {
    constructor(props) {
        super(props);
        this.state ={

         loading_status : false,
         name:'',
         about:null,
         age:'',
         gallery:[],
         bodytype_chips:[],
         bodytype_chips_boolean:[],
         ethnicity_chips:[],
         ethnicity_chips_boolean:[],
         user_id:0,
         user_image:null,
         location:'',
          live:'',
          age:'',
          height:'',
          weight:'',
          country_id:'',
          state_id:'',
          city_id:'',
          body_type:null,
          ethnicities:null ,
          ethnicities_id:'',
          body_type_id:'',
          available_status:false,
          reviews:null,
          user_video:null

      };


    }


    next(){
      result={}
      result["name"] = this.state.name
      result["image"] = this.state.user_image
      result["about"] = this.state.about
      result["user_id"] = this.state.user_id
      result["height"] = this.state.height
      result["weight"] = this.state.weight
      result["age"] = this.state.age
      result['gallery'] = this.state.gallery
      result['country_id'] = this.state.country_id
      result['state_id'] = this.state.state_id
      result['city_id'] = this.state.city_id
      result['ethnicity_id'] = this.state.ethnicities_id
      result['body_type_id'] = this.state.body_type_id

      this.props.navigation.navigate('EditProfileModel',{result : result});


    }






    fetch(){


      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {

                        this.setState({loading_status:true})
                        //763
                        let url = urls.base_url +'api/api_user_profile?id='+ item
                        fetch(url, {
                        method: 'GET',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'multipart/form-data',
                        },


                      }).then((response) => response.json())
                            .then((responseJson) => {
                    this.setState({loading_status:false})
                   // ToastAndroid.show(JSON.stringify(responseJson.gallery), ToastAndroid.SHORT);

                                if(!responseJson.error){

                                  var uid = responseJson.result.id
                                 // ToastAndroid.show(JSON.stringify(uid), ToastAndroid.SHORT);
                                  var name = responseJson.result.name
                                 var live = responseJson.result.live
                                 var age = responseJson.result.age
                                  var user_image = responseJson.result.user_image
                                  var about = responseJson.result.about
                                  var location = responseJson.result.state.name
                                  var height = responseJson.result.height
                                  var weight = responseJson.result.weight
                                  var gender = responseJson.result.gender
                                  var user_video = responseJson.result.user_video


                                  var reviews = responseJson.reviews


                                  var ethnicities = responseJson.ethnicities[0].name
                                  var body_type = responseJson.body_type[0].name

                                  var ethnicities_id = responseJson.ethnicities[0].id
                                  var body_type_id = responseJson.body_type[0].id

                                  var country_id = responseJson.result.country.id
                                  var state_id = responseJson.result.state.id
                                  var city_id = responseJson.result.city.id


                                  var gallery = responseJson.gallery
                                  var len = gallery.length
                                  //ToastAndroid.show(JSON.stringify(id), ToastAndroid.SHORT);
                                  var temp_arr=[]



                                 if(len > 0){
                                  for(var i = 0 ; i < 5 ; i++){
                                    var id = responseJson.gallery[0].id
                                    var profile_pic = responseJson.gallery[0].profile_pic

                                    var image_one = responseJson.gallery[0].image_one
                                    var image_two = responseJson.gallery[0].image_two
                                    var image_three = responseJson.gallery[0].image_three
                                    var image_four = responseJson.gallery[0].image_four

                                    if(i == 0 && profile_pic != null ){
                                      var image = responseJson.gallery[0].profile_pic
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], id: id };
                                      array[i] = { ...array[i], image: image};
                                      temp_arr = array
                                    }
                                    else if(i == 1 && image_one != null){
                                      var image = responseJson.gallery[0].image_one
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], id: id };
                                      array[i] = { ...array[i], image: image};
                                      temp_arr = array
                                    }
                                    else if(i == 2 && image_two != null){
                                      var image = responseJson.gallery[0].image_two
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], id: id };
                                      array[i] = { ...array[i], image: image};
                                      temp_arr = array
                                    }
                                    else if(i == 3 && image_three != null){
                                      var image = responseJson.gallery[0].image_three
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], id: id };
                                      array[i] = { ...array[i], image: image};
                                      temp_arr = array
                                    }
                                    else if(i == 4 && image_four != null){
                                      var image = responseJson.gallery[0].image_four
                                      const array = [...temp_arr];
                                      array[i] = { ...array[i], id: id };
                                      array[i] = { ...array[i], image: image};
                                      temp_arr = array
                                    }

                                  }
                                 }

                                    this.setState({gallery : temp_arr});


                             this.setState({
                              name:name,
                              about:about,
                              user_id:uid,
                              user_image:user_image,
                              location:location,
                              live:live,
                              age:age,
                              height:height,
                              weight:weight,
                              gender:gender,
                              country_id:country_id,
                              city_id:city_id,
                              state_id:state_id,
                              body_type:body_type,
                              ethnicities:ethnicities,
                              ethnicities_id:ethnicities_id,
                              body_type_id:body_type_id,
                              available_status:live == 1 ? true : false,
                              reviews:reviews,
                              user_video:user_video



                            })


                      //making chips for ethnicity
                      var length = responseJson.ethnicities.length
                      //ToastAndroid.show(length,ToastAndroid.LONG)
                                            for(var i = 0 ; i < length ; i++){
                                           //   var id = responseJson.result.interest[i].id  //interest id
                                              var name_temp = responseJson.ethnicities[i]  //interest name

                                                    // Create a new array based on current state:
                                                    let ethnicitys = [...this.state.ethnicity_chips];
                                                   // Add item to it
                                                   ethnicitys.push( name_temp );


                                                    let ethnicitys_bool = [...this.state.ethnicity_chips_boolean];
                                                    ethnicitys_bool.push( false );

                                                    // Set state
                                                    this.setState({ ethnicity_chips:ethnicitys,ethnicity_chips_boolean:ethnicitys_bool });




                                              }


                                               //making chips for bodytype
                                            var length =  responseJson.body_type.length

                                            for(var i = 0 ; i < length ; i++){
                                           //   var id = responseJson.result.interest[i].id  //interest id
                                              var name_temp = responseJson.body_type[i] //interest name

                                                    // Create a new array based on current state:
                                                    let bodytypes = [...this.state.bodytype_chips];
                                                   // Add item to it
                                                   bodytypes.push( name_temp );


                                                    let bodytypes_bool = [...this.state.bodytype_chips_boolean];
                                                    bodytypes_bool.push( false );

                                                    // Set state
                                                    this.setState({ bodytype_chips:bodytypes,bodytype_chips_boolean:bodytypes_bool });


                                              }



                              }
                              else{


                                  Platform.OS === 'android'
                                  ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                                  : Alert.alert(responseJson.message)
                              }
                            }).catch((error) => {
                              this.setState({loading_status:false})

                                Platform.OS === 'android'
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                            });
        }
        else{
          //ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
    }


    componentWillMount(){
      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });

    }



    componentDidMount() {
   // StatusBar.setBackgroundColor('#0040FF')
         this.fetch()

    }


    _renderDotIndicator() {
      return <PagerDotIndicator pageCount={this.state.gallery.length} />;
    }

    render() {


let ethnicity_chips = this.state.ethnicity_chips.map((r, i) => {

  return (
        <Chip
        style={{width:null,margin:4,backgroundColor:colors.color_primary}}
        mode={'outlined'}
        selected={this.state.ethnicity_chips_boolean[i]}
        selectedColor={'black'}
        >
        {r}
        </Chip>
  );
})


let bodytype_chips = this.state.bodytype_chips.map((r, i) => {

  return (
        <Chip
        style={{width:null,margin:4,backgroundColor:colors.color_primary}}
        mode={'outlined'}
        selected={this.state.bodytype_chips_boolean[i]}
        selectedColor={'black'}
        >
        {r}
        </Chip>
  );
})




      let makeGallery =this.state.gallery.map((gallery) => {
        return (


          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor:'grey'
          }}>

          <Image source={{uri:urls.base_url+gallery.image}}
          resizeMode='contain'
           style={{alignSelf:'center',height:'100%',width:'100%'}}/>

        </View>
        );
    })





        return (

         <View
         style={styles.container}>




         <RBSheet
         ref={ref => {
          this.RBSheet = ref;
      }}
            height={300}
            duration={250}
            customStyles={{
                container: {
                justifyContent: "center",
                alignItems: "center"
                }
            }}

       >

         <View style={{height:'100%',width:'100%',backgroundColor:'white',alignItems:'center',
         justifyContent:'center',padding:25,backgroundColor:'white'}}>



         <View style={{position:'absolute',right:10,top:10}}>
        <TouchableWithoutFeedback onPress={()=> {
          this.RBSheet.close()
          this.setState({
            overlay_video:false
          })
        }}>
        <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:30,width:30}} />
        </TouchableWithoutFeedback>
        </View>


        {
          this.state.user_video == null
          ?
           <Text>{I18n.t('no_video_found')}</Text>
           :
           <VideoPlayer source={{uri:urls.base_url+this.state.user_video}}  // Can be a URL or a local file.
                                 // Store reference
          style={{height:'50%',width:'95%',marginTop:10}} />
        }


         </View>



         </RBSheet>



         {/*for header*/}
         <Header

barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

leftComponent={
 <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomeScreen")}>
           <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
     </TouchableWithoutFeedback>
}


rightComponent={

 <View></View>

 }


statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

centerComponent={{ text: I18n.t('profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
containerStyle={{

  backgroundColor: colors.color_primary,
  justifyContent: 'space-around',
  alignItems:'center',
  shadowOpacity:1.7,
  shadowOffset: {
     width: 0,
     height: 3,
 },
 shadowOpacity: 0.3,
 shadowRadius: 2,
  elevation:7
}}
/>

       {/*for main content*/}

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>

           <View style={{height:Dimensions.get('window').height * 0.45}}>

           <IndicatorViewPager
           style={{ height: '100%' }}
           indicator={this._renderDotIndicator()}>
          {makeGallery}
         </IndicatorViewPager>
{/*
           {
            this.state.user_image == null
            ?  <Image source={require('../assets/appimage.png')} style={{alignSelf:'center',height:'100%',width:'100%'}} />
            :  <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.user_image}} style={{alignSelf:'center',height:'100%',width:'100%'}} />
          }

          */}

           </View>


           <View style={{padding:15}}>

               {/*for above part*/}
               <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginTop:20}}>


                  <View style={{flexDirection:'row',alignItems:'center'}}>
                                    <Text style={{fontSize:19,marginRight:7,color:'black'}}>{this.state.name}</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:7}}>,</Text>
                                        <Text style={{fontSize:fonts.font_size,marginRight:17,marginTop:3}}>{this.state.age}</Text>


                                        {
                                          this.state.available_status ?
                                          <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/green.png')} />
                                          :
                                          <Image style={{width: 15, height: 15,margin:1}}  source={require('../assets/red.png')} />

                                        }

                  </View>



                {/* <TouchableWithoutFeedback onPress={()=> this.next()} >*/}
                  <TouchableWithoutFeedback  onPress={()=> this.next()}>
                  <View style={{borderColor:'black',borderRadius:8,borderWidth:2,backgroundColor:colors.color_primary}}>

                  <Text style={{color:'black',fontSize:17,marginLeft:20,marginRight:20,marginBottom:5,marginTop:5,fontWeight:'bold'}}>{I18n.t('edit')}</Text>

                  </View>
                  </TouchableWithoutFeedback>


               </View>

               <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
               {
                 this.state.reviews == null
                 ? <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>0</Text>
                 : <Text style={{fontSize:22,color:'black',fontWeight:'bold'}}>{this.state.reviews.toString().substring(0,3)}</Text>
               }
               <Image style={{width: 20, height: 20,marginLeft:7}}  source={require('../assets/star.png')} />

               </View>






                  {/*about */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('about').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.about == null
                        ?  <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        : <Text style={{color:'grey'}}>{this.state.about}</Text>
                      }
                      </View>


                  </View>



                   {/*status */}

                   <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('weight').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'grey'}}>{this.state.weight} {I18n.t('kgs')}</Text>
                      </View>


                  </View>

                      {/*gendert */}

                      <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('gender').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      {
                        this.state.weight == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.gender}</Text>
                      }
                      </View>


                  </View>



                  {/*height */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('height').toUpperCase()}</Text>

                      </View>
                      <View style={{marginLeft:30}}>
                      <Text style={{color:'grey'}}>{this.state.height} {I18n.t('cms')}</Text>
                      </View>


                  </View>



                  {/*interest */}

                     <View style={{marginTop:35,marginLeft:20}}>
                          <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                              <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                              <Text style={{fontSize:fonts.font_size,marginRight:7,color:'black',fontWeight:'bold'}}>{I18n.t('location').toUpperCase()}</Text>

                          </View>



                          <View style={{marginLeft:30}}>
                          <Text style={{color:'grey'}}>{this.state.location}</Text>
                          </View>


                  </View>



                  {/*ethniicty */}

                  <View style={{marginTop:35,marginLeft:20}}>
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                          <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                          <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('ethnicity').toUpperCase()}</Text>

                      </View>



                      <View style={{marginLeft:30}}>
                      {
                        this.state.ethnicities == null
                        ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                        :  <Text style={{color:'grey'}}>{this.state.ethnicities} </Text>
                      }

                      </View>

                  </View>


                   {/*bodytype */}


                  <View style={{marginTop:35,marginLeft:20}}>
                  <View style={{flexDirection:'row',alignItems:'center',marginBottom:10}}>
                      <Image style={{width: 15, height: 15,marginRight:10}}  source={require('../assets/golden.png')} />
                      <Text style={{fontSize:fonts.font_size,marginRight:7,fontWeight: 'bold',color:'black'}}>{I18n.t('body_type').toUpperCase()}</Text>

                  </View>





                  <View style={{marginLeft:30}}>
                  {
                    this.state.body_type == null
                    ? <Text style={{color:'grey'}}>{I18n.t('na')}</Text>
                    :  <Text style={{color:'grey'}}>{this.state.body_type} </Text>
                  }

                  </View>


                  <Text style={{alignSelf:'flex-end',fontWeight:'bold',color:'blue',fontSize:15}}
                  onPress={()=> {

                    this.RBSheet.open()

                  }}>{I18n.t('see_video')}</Text>

              </View>


           </View>



         </View>

         </ScrollView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


         </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 45,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"60%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
   indicator: {
     flex: 1,
     alignItems: 'center',
     justifyContent: 'center',
     height: 80
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
