import React, {Component} from 'react';
import {Text, View,Alert, Image,Dimensions,ScrollView,ToastAndroid,Platform,
  StatusBar,ActivityIndicator,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colors,fonts,urls } from './Variables';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { CheckBox } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';

import {Header} from 'react-native-elements';

export default class ContactUs extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status: false ,
          complain_status : false,
          message:'',
          description:'',
          username:'',
          complain_detail:'',
          accept_bid_status:false,
          paid_coin_status:false,
          not_meet_status:false,
          backgroundColor:'#ffffff',
          user_id:''


      };


    }

    isContactValid() {


      let valid = false;

      if (this.state.message.toString().trim().length > 0 && this.state.description.toString().trim().length > 0 ) {
        valid = true;
      }

      if (this.state.message.trim().length === 0) {


      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Message Title', ToastAndroid.SHORT)
      : Alert.alert('Enter Message Title')

        return false;
      }
      else if(this.state.description.trim().length === 0){

        Platform.OS === 'android'
      ?  ToastAndroid.show('Enter description', ToastAndroid.SHORT)
      : Alert.alert('Enter description')


        return false;
      }


      return valid;
  }


    onContact(){

      var formData = new FormData();


    formData.append('user_id', this.state.user_id);
    formData.append('title', this.state.message);
    formData.append('description', this.state.description);




    if(this.isContactValid()){
                    this.setState({loading_status:true})
                    let url = urls.base_url +'api/contact_us'

                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                   this.setState({loading_status:false})
              // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                             if(!responseJson.error){


                              Platform.OS === 'android'
                            ?  ToastAndroid.show('Sent successfully ', ToastAndroid.SHORT)
                            : Alert.alert('Sent successfully')

                            this.props.navigation.navigate("HomeScreen")


                          }
                          else{

                            Platform.OS === 'android'
                            ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)

                            //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})

                          Platform.OS === 'android'
                          ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                          : Alert.alert("Connection Error !")
                        });


    }
}




isComplainValid() {


  let valid = false;

  if (this.state.username.toString().trim().length > 0 && this.state.complain_detail.toString().trim().length > 0 ) {
    valid = true;
  }

  if (this.state.username.trim().length === 0) {


  Platform.OS === 'android'
  ?  ToastAndroid.show('Enter username', ToastAndroid.SHORT)
  : Alert.alert('Enter username')

    return false;
  }

  else if (this.state.accept_bid_status == false && this.state.paid_coin_status == false && this.state.not_meet_status == false){
    Platform.OS === 'android'
    ?  ToastAndroid.show('Choose Complain reason', ToastAndroid.SHORT)
    : Alert.alert('Choose Complain  reason')

    return false
  }

  else if(this.state.complain_detail.trim().length === 0){

    Platform.OS === 'android'
  ?  ToastAndroid.show('Enter Complain description', ToastAndroid.SHORT)
  : Alert.alert('Enter Complain  description')


    return false;
  }


  return valid;
}


onComplain(){

  var response = '';
  if(this.state.accept_bid_status ){
    response = response + "Model Accepted bid" + ","
  }
  if(this.state.paid_coin_status){
    response = response + "Model paid coin" + ","
  }
  if(this.state.not_meet_status){
    response = response + "Model did not meet" + ","
  }

  response  = response.substring(0, response.length - 1);

var formData = new FormData();


formData.append('form_username', this.state.user_id);
formData.append('user_id', this.state.username);
formData.append('reason', response);
formData.append('message', this.state.complain_detail);

//ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)


if(this.isComplainValid()){
                this.setState({loading_status:true})
                let url = urls.base_url +'api/complain_us'


                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
            body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
               this.setState({loading_status:false})
          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                         if(!responseJson.error){

                          this.props.navigation.navigate("HomeScreen")

                          Platform.OS === 'android'
                        ?  ToastAndroid.show('Sent successfully ', ToastAndroid.SHORT)
                        : Alert.alert('Sent successfully')


                      }
                      else{

                        Platform.OS === 'android'
                        ?   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                        : Alert.alert(responseJson.message)


                      }
                    }).catch((error) => {
                      this.setState({loading_status:false})

                      Platform.OS === 'android'
                      ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")
                    });


}
}



componentWillMount() {

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

}



    componentDidMount() {
   // StatusBar.setBackgroundColor('#0040FF')

   AsyncStorage.getItem('user_id')
    .then((item) => {
              if (item) {

                      this.setState({user_id:item})

            }
            else {
              ToastAndroid.show("Error !")
              }
    });

    }


    render() {
        return (

       <View style={{flex:1}}>


         {/*for header*/}

         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("HomePage")}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('contact_us'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />


       {/*for main content*/}
       <KeyboardAvoidingView style={styles.container}
          behavior= {(Platform.OS === 'ios') ? "padding" : null} enabled>

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}>

          


           <View style={styles.body}>



           <View style={{padding:15}}>

               {/*for above part*/}
               <Text style={styles.name_text}>{I18n.t('message_title')}</Text>
                <TextInput
                  value={this.state.message}
                  editable={!this.state.complain_status}
                  onChangeText={(message) => this.setState({ message })}
                  style={styles.name_input}
                  placeholderTextColor={'black'}
                />

                <Text style={styles.about_text}>{I18n.t('description')}</Text>
                <TextInput
                  value={this.state.description}
                  editable={!this.state.complain_status}
                  onChangeText={(description) => this.setState({description })}
                  style={styles.about_input}
                  multiline={true}
                  placeholderTextColor={'black'}
                />
{


  this.state.complain_status
  ? null
  :
  <TouchableOpacity
  onPress={()=> this.onContact()}
        style={styles.saveButton}>
        <Text style={styles.saveText}>{I18n.t('submit').toUpperCase()}</Text>
  </TouchableOpacity>

}


                <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:-10}}>


                      <CheckBox
                         center
                         checkedColor={colors.color_primary}
                          onPress={() => this.setState({ complain_status: !this.state.complain_status })}
                          checked={this.state.complain_status}
                        />
                      <Text style={{marginTop: 18,marginLeft:-15,color:'black'}}
                       >{I18n.t('model_member_complain')}</Text>
                  </View>


             {/*en of first part*/}


                  {/*second part  */}
            {
              !this.state.complain_status
              ?
              null
              :
              <View style={{width:'100%'}}>
              <Text style={styles.name_text}>{I18n.t('username')}</Text>
              <TextInput
                value={this.state.username}
                editable={this.state.complain_status}
                onChangeText={(username) => this.setState({username })}
                style={styles.name_input}
                placeholderTextColor={'black'}
              />


              <Text style={{marginTop: 10,marginLeft:10,color:'black'}}
                   >{I18n.t('model_member_noshow')}</Text>



                   <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:10}}>


                   <CheckBox
                      center
                      checkedColor={colors.color_primary}
                       onPress={() => this.setState({ accept_bid_status: !this.state.accept_bid_status })}
                       checked={this.state.accept_bid_status}
                     />
                   <Text style={{marginTop: 18,marginLeft:-15,color:'black'}}
                    >{I18n.t('model_accepted_bid')}</Text>
               </View>

               <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:10}}>


               <CheckBox
                  center
                  checkedColor={colors.color_primary}
                   onPress={() => this.setState({ paid_coin_status: !this.state.paid_coin_status })}
                   checked={this.state.paid_coin_status}
                 />
               <Text style={{marginTop: 18,marginLeft:-15,color:'black'}}
                >{I18n.t('member_send_bid')}</Text>
           </View>

           <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:10}}>


           <CheckBox
              center
              checkedColor={colors.color_primary}
               onPress={() => this.setState({ not_meet_status: !this.state.not_meet_status })}
               checked={this.state.not_meet_status}
             />
           <Text style={{marginTop: 18,marginLeft:-15,color:'black'}}
            >{I18n.t('objectional_content')}</Text>
       </View>


       <Text style={styles.about_text}>{I18n.t('please_detail_complain')}</Text>
       <TextInput
         value={this.state.complain_detail}
         editable={this.state.complain_status}
         onChangeText={(complain_detail) => this.setState({complain_detail })}
         style={styles.about_input}
         multiline={true}
         placeholderTextColor={'black'}
       />


       <TouchableOpacity
       onPress={()=> this.onComplain()}
             style={styles.saveButton}>
             <Text style={styles.saveText}>{I18n.t('submit').toUpperCase()}</Text>
       </TouchableOpacity>
       </View>
            }




                   {/*end second part  */}




                  {/*height */}




                  {/*interest */}




           </View>



         </View>
        

         </ScrollView>
         </KeyboardAvoidingView>

         {this.state.loading_status &&
          <View pointerEvents="none" style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
      }


       </View>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',


  },
 name_text:{
  width:'100%',
  marginBottom:10,

 },

 name_input: {
  width: "100%",
  height: 50,
padding:6,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
 // backgroundColor:this.state.complain_status ? "#D3D3D3" : "#ffffff"
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 120,
  textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
  padding:6
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
