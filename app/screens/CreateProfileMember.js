import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,ScrollView,StatusBar,Platform,
  StyleSheet,ImageBackground,BackHandler,TouchableOpacity,TextInput,TouchableWithoutFeedback,
  ActivityIndicator,Alert,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { fonts,colors,urls } from './Variables';
import { Chip } from 'react-native-paper';
import { withNavigationFocus } from 'react-navigation';

import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import {Header} from 'react-native-elements';


 class CreateProfileMember extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         loading_status:false,
         imageSource:null,
         imageData:null,
          photo:null,
          name:'',
          about:null,
      };


    }

    isValid() {


      let valid = false;

      if (
          this.state.name.trim().length > 0
          ) {
        valid = true;
      }

     if(this.state.name.trim().length === 0){

      Platform.OS === 'android'
      ?  ToastAndroid.show('Enter Name', ToastAndroid.SHORT)
      : Alert.alert('Enter name')


        return false;
      }


      return valid;
  }

  handleBackWithAlert = () => {
    // const parent = this.props.navigation.dangerouslyGetParent();
      // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
    // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)

    if (this.props.isFocused) {

              if(this.state.loading_status){
                     this.setState({loading_status:false})
              }

            else{
              this.props.navigation.navigate("Welcome")
            }

  return true;
  }
  }



  componentWillMount() {

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });

  BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
  //this.props.navigation.dispatch(DrawerActions.closeDrawer());

  }

  componentWillUnmount() {
   BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);

  }



    onCreate(){

      if (this.isValid()) {




       this.setState({loading_status:true})
       var formData = new FormData();
         var result  = this.props.navigation.getParam('result')
        formData.append('user_id',result["user_id"]);
        formData.append('display_name', this.state.name);
        if(this.state.about !=null){
          formData.append('about', this.state.about);
        }

        formData.append('profile_pic', this.state.photo);
        //Alert.alert(JSON.stringify(formData));

                let url = urls.base_url +'api/create_profile'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
                body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({loading_status:false})
                    //Alert.alert(responseJson);
                    //ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.LONG);
                    if(!responseJson.error){
                          //success in inserting data
                         //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);

                            var id = responseJson.result.id
                            var roles_id = responseJson.result.roles_id
                            var country = responseJson.result.country.name;
                            var name = responseJson.result.name
                          //  var city = responseJson.result.city.name;
                            var user_image = responseJson.result.user_image
                            var coins = responseJson.result.coins
                            var abc = 0



                            AsyncStorage.multiSet([
                              ['user_id',id.toString()],
                              ['roles_id', roles_id.toString()],
                              ["state", country],
                              ["user_name", name],
                              ["user_image", user_image],
                              ["coins", abc.toString()]

                            ]);
                            //ToastAndroid.show("saved all", ToastAndroid.LONG);
                             this.props.navigation.navigate('HomeMember');


                        }else{
                            //errror in insering data
                            //ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({error:responseJson.message})

                            Platform.OS === 'android'
                            ?  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT)
                            : Alert.alert(responseJson.message)




                          }

                    }).catch((error) => {
                      console.error(error);
                      //this.props.navigation.navigate("NoNetwork")

                      Platform.OS === 'android'
                      ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")


                      return;
                    });


       }

 }

    selectPhotoTapped() {

      const DEFAULT_OPTIONS = {
        title: 'Select a Photo',
        cancelButtonTitle: 'Cancel',
        takePhotoButtonTitle: 'Take Photo…',
        chooseFromLibraryButtonTitle: 'Choose from Library…',
        quality: 1.0,
        allowsEditing: false,
        permissionDenied: {
          title: 'Permission denied',
          text:
            'To be able to take pictures with your camera and choose images from your library.',
          reTryTitle: 're-try',
          okTitle: "I'm sure",
        },
      };


      var options_two = {
        title: 'Select Image',
        customButtons: [
          { name: 'customOptionKey', title: 'Choose Photo from Custom Option'  },//if custome button used hede
        ],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };



      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled photo picker');
          //ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg",
          };


          this.setState({
            imageSource: source ,
            imageData: response.data,
            photo:photo,

          });


          {/*photo is a file object to send to parameters */}
        }
      });
   }


    fetchInterest = async () =>{
      this.setState({loading_status:true})

            let url = urls.base_url +'api/api_interest'

                      fetch(url, {
                      method: 'GET',

                      }).then((response) => response.json())
                          .then((responseJson) => {

                          //  ToastAndroid.show(responseJson.message, ToastAndroid.LONG);
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                              var length = responseJson.result.length.toString();
                              var temp_arr=[]
                              for(var i = 0 ; i < length ; i++){
                              var id = responseJson.result[i].id  //interest id
                              var name = responseJson.result[i].name   //interest name

                                    // Create a new array based on current state:
                                    let interests = [...this.state.interest_names];
                                    // Add item to it
                                    interests.push( name );


                                    let interests_ids = [...this.state.interest_id];
                                    interests_ids.push(id );


                                    let interests_bool = [...this.state.interest_boolean];
                                    interests_bool.push( false );

                                    // Set state
                                    this.setState({ interest_id:interests_ids,interest_names:interests,interest_boolean:interests_bool });



                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                              }
                             // this.setState({ countries : temp_arr});

                            }


                          else{
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          }

                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
                            Platform.OS === 'android'
                            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
                            : Alert.alert("Connection Error!")
                          });



    }

    next(){
      var length = this.state.interest_boolean.length;
      var final= "";
      for(var i = 0 ; i < length ; i++){
        if(this.state.interest_boolean[i] === false){
         //do nothing

        }
        else{
          let temp = this.state.interest_id[i].toString()
          final = final + temp
          final = final + ","
        }
      }
     // ToastAndroid.show("Final Items"+final, ToastAndroid.SHORT);


    }

    componentDidMount() {
     // StatusBar.setBackgroundColor('#0040FF')


    }

    check(){

      if(this.state.imageSource === null){
        return ( <Image source={require('../assets/avatar.png')}
        style={styles.image} resizeMode='contain'/>
         )
      }

      else{
          return (<Image source={this.state.imageSource}
             style={styles.image}/>
             )
      }


    }


    render() {

      let imageResult = this.check()






        return (
          <KeyboardAvoidingView style={styles.container}  behavior={Platform.OS === 'android' ? "height" : 'padding'} enabled>


         {/*for header*/}


         <Header

         barStyle = {Platform.OS === 'ios' ? "dark-content" : "light-content" }// or light-content

        leftComponent={
          <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate("Welcome")}>
                    <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/backbtn.png')} />
              </TouchableWithoutFeedback>
        }


         rightComponent={

          <View></View>

          }


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}

        centerComponent={{ text: I18n.t('create_profile'), style: { fontSize: 21,fontWeight: 'bold', color: "black"} }}
         outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 30}}
         containerStyle={{

           backgroundColor: colors.color_primary,
           justifyContent: 'space-around',
           alignItems:'center',
           shadowOpacity:1.7,
           shadowOffset: {
              width: 0,
              height: 3,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
           elevation:7
         }}
       />





       {/*for main content*/}

       <ScrollView style={{width:'100%',flex:1,height:'100%'}}>
           <View style={styles.body}>



           <TouchableWithoutFeedback onPress={this.selectPhotoTapped.bind(this)}>
                   {imageResult}
         </TouchableWithoutFeedback>



                 <Text style={styles.name_text}>{I18n.t('member_display_name')}</Text>
                <TextInput
                  value={this.state.name}
                  keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
                  onChangeText={(name) => this.setState({ name })}
                  style={styles.name_input}
                  maxLength={30}
                  placeholderTextColor={'black'}
                />

                   <Text style={styles.about_text}>{I18n.t('about_bio')}</Text>
                <TextInput
                  value={this.state.about}
                  onChangeText={(about) => this.setState({ about })}
                  keyboardType={Platform.OS === 'android' ? 'email-address' : 'ascii-capable'}
                  style={styles.about_input}
                  multiline={true}
                  placeholderTextColor={'black'}
                />




                <TouchableOpacity
                onPress={this.onCreate.bind(this)}
                      style={styles.saveButton}>
                      <Text style={styles.saveText}>{I18n.t('save').toUpperCase()}</Text>
                </TouchableOpacity>

    </View>

       </ScrollView>

       {this.state.loading_status &&
        <View pointerEvents="none" style={styles.loading}>
          <ActivityIndicator size='large' />
        </View>
    }


         </KeyboardAvoidingView>


        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.color_primary,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
    padding:30,
    margin:5
  },
 name_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size

 },

 name_input: {
  width: "100%",
  height: 50,
padding:6,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
about_text:{
  width:'100%',
  marginBottom:10,
  fontSize:fonts.font_size,
 },

 about_input: {
  width: "100%",
  height: 120,
  textAlignVertical:'top',
  padding:3,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:colors.color_primary,
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  image:{
    height:140,
    width:140,
    alignSelf:'center',
    marginBottom:30,
    borderRadius: 140 / 2,
    overflow: "hidden",
  },

indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
},
loading: {
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor:'rgba(128,128,128,0.5)'
}




}
)

export default withNavigationFocus(CreateProfileMember);
{/**

  import MultiSlider from '@ptomasroos/react-native-multi-slider';

  this.state ={
          password:'',
          values: [3, 7],
      };




    multiSliderValuesChange = (values) => {
      this.setState({
          values,
      });
  }



         <View style={{width:'100%'}}>
                <MultiSlider
                    values={[this.state.values[1], this.state.values[1]]}
                    sliderLength={280}
                    onValuesChange={this.multiSliderValuesChange}
                    min={0}
                    max={10}
                    step={1}
                />
                <Text style={styles.text}>Two Markers:</Text>
                <Text style={styles.text}>{this.state.values[0]}</Text>
                <Text style={styles.text}>{this.state.values[1]}</Text>
            </View>
*/}
