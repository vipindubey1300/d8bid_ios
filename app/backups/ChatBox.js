import React, {Component} from 'react';
import {Text, View, Image,Alert,Dimensions,ToastAndroid,StatusBar,ScrollView,StyleSheet,FlatList,Platform,TouchableWithoutFeedback, BackHandler,TextInput,ImageBackground,TouchableOpacity,ActivityIndicator} from 'react-native';
import { colors,fonts,urls } from './Variables';
import {Card} from 'native-base';
import { withNavigationFocus } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';


 class ChatBox extends Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          loading_status:false,
           user_image:null,
           user_name:'',
           user_id:0,
           message:'',
           chats:[],
           flinks:[],
           active_flinks:[],
           flink_status : false,
           show_flings:true


           
                  };

    }

    fetchFlinks(){

     // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/flink_list', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                     
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                             // this.setState({loading_status:false,message:''})
                 //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                                   if(!responseJson.error){
                                  
                                    var length = responseJson.result.length.toString();

                                    var temp_arr=[]
                                    for(var i = 0 ; i < length ; i++){
                                   
                                          var id = responseJson.result[i].id
                                          var name = responseJson.result[i].name
                                          var image = responseJson.result[i].image
                                          var status = responseJson.result[i].status
                                          
                                         
      
                                       
                                          
                                         
                                                const array = [...temp_arr];
                                                array[i] = { ...array[i], id: id };
                                                array[i] = { ...array[i], name:name };
                                                array[i] = { ...array[i], image:image };
                                                array[i] = { ...array[i], status:status };
                                              
                                               
                                               
            
                                                temp_arr = array
                                                
                                    }
                                    this.setState({ flinks : temp_arr});
                                                  
                                   
      
                                }
                                else{
          
                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
       
  
    }

    fetchActiveFlinks(){



      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
         var text = this.state.message
         formData.append('user_id', item);//our id
         formData.append('model_id',this.state.user_id);

        //  formData.append('user_id', 801);//our id
        //  formData.append('model_id',800);
          
        // ToastAndroid.show(JSON.stringify(formData),ToastAndroid.LONG)
          fetch('http://webmobril.org/dev/d8bid/api/perchased_flink', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
            },
       
            body:formData
          }).then((response) => response.json())
                .then((responseJson) => {
               // this.setState({loading_status:false,message:''})
   //ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                     if(!responseJson.error){
                     // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                      var length = responseJson.result.length.toString();

                      var temp_arr=[]
                      for(var i = 0 ; i < length ; i++){
                     
                            var id = responseJson.result[i].flinks.id
                            var name = responseJson.result[i].flinks.name
                            var image = responseJson.result[i].flinks.image
                            var status = responseJson.result[i].flinks.status
                            
                           

                         
                            
                           
                                  const array = [...temp_arr];
                                  array[i] = { ...array[i], id: id };
                                  array[i] = { ...array[i], name:name };
                                  array[i] = { ...array[i], image:image };
                                  array[i] = { ...array[i], status:status };
                                
                                 
                                 

                                  temp_arr = array
                                  
                      }
                      this.setState({ active_flinks : temp_arr});


                      this.makeFLings()
                       // ToastAndroid.show(JSON.stringify(this.state.active_flinks),ToastAndroid.LONG)            
                     

                  }
                  else{

                    ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                  }
                }).catch((error) => {
                  this.setState({loading_status:false})
                  
                  Platform.OS === 'android' 
                  ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                  : Alert.alert("Connection Error !")
                });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });

      // this.setState({loading_status:true})
                          
        
   
     }
 


    fetchMessage(){
     // ToastAndroid.show("fetching",ToastAndroid.LONG)
   
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
  
          formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
          //formData.append('form_id', 693);
          //formData.append('to_id',715);
         
      
          
                          // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/chat_message', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                             // this.setState({loading_status:false,message:this.state.message})
                // ToastAndroid.show(JSON.stringify(responseJson),ToastAndroid.LONG)
                                   if(!responseJson.error){
                                  
                                    var length = responseJson.Chat.length.toString();

                                    var temp_arr=[]
                                    for(var i = 0 ; i < length ; i++){
                                   
                                          var chat_id = responseJson.Chat[i].id
                                          var form_id = responseJson.Chat[i].form_id
                                          var to_id = responseJson.Chat[i].to_id
                                          var message = responseJson.Chat[i].message
                                          var attachment = responseJson.Chat[i].attachment
                                          var flinks = responseJson.Chat[i].flinks
                                          var sent_flag = true
                                          if(form_id == item){

                                          }
                                          else{
                                            sent_flag = false
                                          }
                                         
                                                const array = [...temp_arr];
                                                array[i] = { ...array[i], chat_id: chat_id };
                                                array[i] = { ...array[i], sent_flag:sent_flag };
                                                array[i] = { ...array[i], message:message };
                                                array[i] = { ...array[i], attachment:attachment };
                                                array[i] = { ...array[i], flinks:flinks };
                                              
                                               
                                               
            
                                                temp_arr = array
                                                
                                    }
                                   
                                  //  ToastAndroid.show(text,ToastAndroid.LONG)
                                    this.setState({ chats : temp_arr});
                                    
                                  
                                                  
                                   
      
                                }
                                else{
          
                                  //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                // Platform.OS === 'android' 
                                // ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                                // : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     
  
    }

   purchaseFlings(gif,status,id){
 
   // ToastAndroid.show("shiow", ToastAndroid.SHORT);
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
         var text = this.state.message
          formData.append('user_id', item);//our id
          formData.append('model_id',this.state.user_id);
          formData.append('flink_id', id);
          formData.append('coins', 1);
      
         // ToastAndroid.show(JSON.stringify(formData), ToastAndroid.SHORT);
                          // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/flink_buy', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})
                  
                                   if(!responseJson.error){
                                  
                                    ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                 
      
                                  var l = this.state.flinks.length
                                  for(var i = 0 ; i < l ; i++){
                           
                                      if(this.state.flinks[i].id == id){
                                        //status 0 means true....byued...theflinkss
                                        let flink = [...this.state.flinks];    
                                        flink[i].status = 0;                  
                                        this.setState({flinks: flink},);  
                                        
                                      }
                                    
                                  }
                                  
                                 
                                }
                                else{
          
                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     
  
    }

    sendFlings(gif,status,flink_id){
     // ToastAndroid.show(gif, ToastAndroid.SHORT);
     if(status == 1){
        //purchase flings
        this.purchaseFlings(gif,status,flink_id)


     }else{
     // ToastAndroid.show("sendd", ToastAndroid.SHORT);
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
         var text = this.state.message
         formData.append('form_id', item);//our id
         formData.append('to_id',this.state.user_id);
          formData.append('flinks',gif);
      
          
                          // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/chat', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({flink_status:false,message:''})
                  
                                   if(!responseJson.error){
                                  
                                   // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                  var l = this.state.chats.length
                                  
                                  const array = [...this.state.chats];
                                  array[l] = { ...array[l], chat_id: 0 };
                                  array[l] = { ...array[l], sent_flag:true };
                                  array[l] = { ...array[l], message:null };
                                  array[l] = { ...array[l], attachment:null };
                                  array[l] = { ...array[l], flinks:gif };
                                
                                 
                                  
                      
                                       this.setState({ chats : array});
                                    
                                                  
                                   
      
                                }
                                else{
          
                                  ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     }
     
  
    }

    sendAttachment(image){
      // ToastAndroid.show(gif, ToastAndroid.SHORT);
       AsyncStorage.getItem("user_id").then((item) => {
         if (item) {
           var formData = new FormData();
   
          var text = this.state.message
          formData.append('form_id', item);//our id
           formData.append('to_id',this.state.user_id);
           formData.append('attachment',image);
       
           
                           // this.setState({loading_status:true})
                           fetch('http://webmobril.org/dev/d8bid/api/chat', {
                           method: 'POST',
                           headers: {
                             'Accept': 'application/json',
                             'Content-Type': 'multipart/form-data',
                           },
                       body: formData
           
                         }).then((response) => response.json())
                               .then((responseJson) => {
                             //  this.setState({flink_status:false,message:''})
                           //  ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                   
                                    if(!responseJson.error){
                                   
                                 
                                   var l = this.state.chats.length
                                   
                                   const array = [...this.state.chats];
                                   array[l] = { ...array[l], chat_id: 0 };
                                   array[l] = { ...array[l], sent_flag:true };
                                   array[l] = { ...array[l], message:null };
                                   array[l] = { ...array[l], attachment:image };
                                   array[l] = { ...array[l], flinks:null};
                                 
                                  
                                   
                       
                                        this.setState({ chats : array});
                                     
                                                   
                                    
       
                                 }
                                 else{
           
                                   ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                 }
                               }).catch((error) => {
                                 this.setState({loading_status:false})
                                 
                                 Platform.OS === 'android' 
                                 ?   ToastAndroid.show(error.message, ToastAndroid.SHORT)
                                 : Alert.alert("Connection Error !")
                               });
         }
         else{
           ToastAndroid.show("User not found !",ToastAndroid.LONG)
         }
       });
      
   
     }

    sendMessage(){

     if(this.state.message.length > 0){
      AsyncStorage.getItem("user_id").then((item) => {
        if (item) {
          var formData = new FormData();
  
         var text = this.state.message
          formData.append('form_id', item);//our id
          formData.append('to_id',this.state.user_id);
          formData.append('message', this.state.message);
      
          
                          // this.setState({loading_status:true})
                          fetch('http://webmobril.org/dev/d8bid/api/chat', {
                          method: 'POST',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'multipart/form-data',
                          },
                      body: formData
          
                        }).then((response) => response.json())
                              .then((responseJson) => {
                              this.setState({loading_status:false,message:''})
                  
                                   if(!responseJson.error){

                                   // this.setState({loading_status:false,message:''})
                  
                                  
                                 // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                  var l = this.state.chats.length
                                  
                                  const array = [...this.state.chats];
                                  array[l] = { ...array[l], chat_id: 0 };
                                  array[l] = { ...array[l], sent_flag:true };
                                  array[l] = { ...array[l], message:text };
                                  array[l] = { ...array[l], flinks:null };
                                  array[l] = { ...array[l], attachment:null };
                                
                                 
                                  
                      
                            this.setState({ chats : array});
                                    
                                                  
                                   
      
                                }
                                else{
          
                                 // ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                                }
                              }).catch((error) => {
                                this.setState({loading_status:false})
                                
                                Platform.OS === 'android' 
                                ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                                : Alert.alert("Connection Error !")
                              });
        }
        else{
          ToastAndroid.show("User not found !",ToastAndroid.LONG)
        }
      });
     }
     
  
    }

    selectPhotoTapped() {
      const options = {
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };
   
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
   
        if (response.didCancel) {
          console.log('User cancelled photo picker');
          ToastAndroid.show('User cancelled photo picker',ToastAndroid.SHORT);
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
         //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
          let source = { uri: response.uri};

          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg",
          };

          this.sendAttachment(photo)


          // this.setState({
          //   imageSource: source ,
          //   imageData: response.data,
          //   photo:photo,
   
          // });
         

          {/*photo is a file object to send to parameters */}
        }
      });
   }


    handleBackWithAlert = () => {
      // const parent = this.props.navigation.dangerouslyGetParent();
        // const isDrawerOpen = parent && parent.state && parent.state.isDrawerOpen;
      // ToastAndroid.show(JSON.stringify(isDrawerOpen),ToastAndroid.LONG)
    
      if (this.props.isFocused) {
    
                if(this.state.flink_status){
                       this.setState({flink_status:false})
                }
                
              else{
                    //   Alert.alert(
                    //   'Exit App',
                    //   'Exiting the application?',
                    //   [
                    //   {
                    //     text: 'Cancel',
                    //     onPress: () => console.log('Cancel Pressed'),
                    //     style: 'cancel'
                    //   },
                    //   {
                    //     text: 'OK',
                    //     onPress: () => BackHandler.exitApp()
                    //   }
                    //   ],
                    //   {
                    //   cancelable: false
                    //   }
                    // );
              }
    
    return true;
    }
    }
    
    
    
   async componentWillMount() {

      await this.fetchFlinks()
      await this.fetchActiveFlinks()
     
    
    BackHandler.addEventListener('hardwareBackPress',this.handleBackWithAlert);
    //this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.navigation.closeDrawer();
    
    }
    
    componentWillUnmount() {
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackWithAlert);
     clearInterval(this.timer)
    
    }


    makeFLings(){
      var length = this.state.flinks.length.toString();
      
     
      if(this.state.active_flinks.length > 0){
        // /ToastAndroid.show("GH",ToastAndroid.LONG)
        for(var i = 0 ; i < length ; i++){
          for(var j = 0 ; j < this.state.active_flinks.length.toString() ; j++){
            if(this.state.flinks[i].id == this.state.active_flinks[j].id){
              //status 0 means true....byued...theflinkss
              let flink = [...this.state.flinks];    
              flink[i].status = 0;                  
              this.setState({flinks: flink},);  

            }
          }
        }
      }

      
    }

   
//means this was started y model to chat 
  async componentDidMount(){

    setTimeout(() => {
      this.myFlatListRef.scrollToEnd({animated:true})
    }, 10);


    var result  = this.props.navigation.getParam('result')
   // ToastAndroid.show("ID>>>>"+JSON.stringify(result),ToastAndroid.LONG)
    this.setState({
      user_id : result['to_id'],
      user_name : result['name'],
      user_image : result['image'],
    
    })

    AsyncStorage.getItem('roles_id')
    .then((item) => {
              if (item) {
                    if(item == 2){
                      //means model
                      this.setState({show_flings:false})
                    }
                    else{
                      this.setState({show_flings:true})
                    }
            }
            else {
              ToastAndroid.show("Error !")
              }
    });
   


    this.timer = setInterval(()=> this.fetchMessage(), 4000)
     this.fetchMessage()
    // 
   

  }


  
	// next(model_id){
	// 	if(this.state.models.length > 0){
	// 		//var jobs = this.state.jobs
	// 		for(var i = 0 ; i < this.state.models.length ; i++){
	// 			if(this.state.models[i].key == model_id){
	// 				result={}
	// 				result["model_id"] = this.state.models[i].id
					
	// 				this.props.navigation.navigate("ProfileDetails",{result : result});
	// 			}
	// 		}
	// 	}
  // }


  next(){
   
		if(this.state.show_flings){
     // ToastAndroid.show("FDSfsdfsdf",ToastAndroid.LONG)
      result= {}
      result["model_id"] = this.state.user_id
      this.props.navigation.navigate("ModelRating",{result:result})
    }
    else{
      //go to profile member
      let obj ={
        "member_id":this.state.user_id
      }
      this.props.navigation.navigate("MemberProfile",{result:obj})
    }
  }
  
  renderItem = ({item}) =>  {
   if(item.sent_flag){
      if(item.message != null ){
        return (
      
          <View style={{width:'100%',margin:3}}>
                 
          <Card style={{width:'70%',height:null,padding:5,alignSelf:'flex-end'}}>
          <TouchableWithoutFeedback>
            <View style={{width:'100%'}}>
                  
    
                    <Text style={{fontSize:15,marginRight:4,color:'black'}}>{item.message}</Text>
                   
    
            </View>
           
    
              </TouchableWithoutFeedback> 
          </Card>
          
          </View>
        );
      }

      else  if(item.flinks != null ){

        return (
      
          <View style={{width:'100%',margin:3,height:null,padding:5}}>
                 
          <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-end'}}>
          <TouchableWithoutFeedback>
            <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between'}}>
                <View></View>  
   
            <Image style={{height:120,width:120}} source={{uri: 'http://webmobril.org/dev/d8bid/'+item.flinks}} />
                   
    
            </View>
           
    
              </TouchableWithoutFeedback> 
              </View>
          
          </View>
        );

      }



      else  if(item.attachment != null ){

        return (
      
          <View style={{width:'100%',margin:3}}>
                 
          <Card style={{width:null,height:null,padding:5,alignSelf:'flex-end'}}>
          <TouchableWithoutFeedback>
            <View style={{width:'100%'}}>
                  
    
            <Image style={{height:180,width:180}} source={{uri: 'http://webmobril.org/dev/d8bid/'+item.attachment}} />
                   
    
            </View>
           
    
              </TouchableWithoutFeedback> 
          </Card>
          
          </View>
        );

      }
   }



   
   else{
    if(item.message != null ){
      return (
    
        <View style={{width:'100%',margin:3}}>
               
        <Card style={{width:'70%',height:null,padding:5,alignSelf:'flex-start'}}>
        <TouchableWithoutFeedback>
          <View style={{width:'100%'}}>
                
  
                  <Text style={{fontSize:15,marginRight:4,color:'black'}}>{item.message}</Text>
                 
  
          </View>
         
  
            </TouchableWithoutFeedback> 
        </Card>
        
        </View>
      );
    }

    else  if(item.flinks != null ){

      return (
    
        <View style={{width:'100%',margin:3,height:null,padding:5}}>
               
        <View style={{width:'70%',height:null,padding:5,alignSelf:'flex-start'}}>
        <TouchableWithoutFeedback>
          <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between'}}>
          <Image style={{height:120,width:120}} source={{uri: 'http://webmobril.org/dev/d8bid/'+item.flinks}} />
              <View></View>  
 
         
                 
  
          </View>
         
  
            </TouchableWithoutFeedback> 
            </View>
        
        </View>
      );

    }



    else  if(item.attachment != null ){

      return (
    
        <View style={{width:'100%',margin:3}}>
               
        <Card style={{width:null,height:null,padding:5,alignSelf:'flex-start'}}>
        <TouchableWithoutFeedback>
          <View style={{width:'100%'}}>
                
  
          <Image style={{height:180,width:180}} source={{uri: 'http://webmobril.org/dev/d8bid/'+item.attachment}} />
                 
  
          </View>
         
  
            </TouchableWithoutFeedback> 
        </Card>
        
        </View>
      );

    }
   }
  }

  onSelect = data => {
    //this function is used heere as a callback ...
    //using this function we get the data from filters page
   // ToastAndroid.show("Recieved data is "+ data['selected'],ToastAndroid.LONG);
    //this.setState(data);
  };

    render() {

     

      if (this.state.loading_status) {
        return (
          <ActivityIndicator
            animating={true}
            style={styles.indicator}
            size="large"
          />
        );
      }

    
        return (
        
         <View 
         style={styles.container}>

         {/* headrer */}
          <View style = {{flexDirection:'row',justifyContent:'space-between',alignItems: 'center',width:'100%',height:Platform.OS === 'android'  ? '07%':'09%',backgroundColor:'#262626'}}>

                  <TouchableWithoutFeedback onPress={() =>this.props.navigation.goBack()}>
                     <Image style={{width: 25, height: 25,margin:10}}  source={require('../assets/back2.png')} />
                  </TouchableWithoutFeedback>

                  <View>
                  <Text style={{fontSize: 19, color: "white",paddingRight:25}}>{this.state.user_name}</Text>
                </View>

                  <View style={{height:40,width:40,borderRadius:20,borderColor:'white',borderWidth:2}}>
                  <TouchableWithoutFeedback onPress={() => this.next()}>
                  <Image source={{uri:"http://webmobril.org/dev/d8bid/"+this.state.user_image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:20}} />
                  </TouchableWithoutFeedback>
                  </View>

            </View>

                     {/* body */}

                     <View style={{width:'100%',paddingBottom:0,backgroundColor:'#F5F0F0',flex:1}}>

      
         

                     <FlatList
                           style={{marginBottom:70,flex:1}}
                           data={this.state.chats}
                            numColumns={1}
                            ref={ (ref) => { this.myFlatListRef = ref } }
                            onContentSizeChange={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }
                            onLayout={ () => { this.myFlatListRef.scrollToEnd({animated:true}) } }
                            showsVerticalScrollIndicator={false}
                           renderItem={this.renderItem}
           
                         />
                       
                       </View>


         

            <View style={{position:'absolute',bottom:0,left:0,right:0,height:70,backgroundColor:'white',flexDirection:'row',padding:5,alignItems:'center'}}>
                  <View style={{flex:5}}>
                  <TextInput
                      value={this.state.message}
                      style={styles.input}
                      
                     
                      onChangeText={ (message) => {
                     
                        this.setState({message:message})
                      } }
                      placeholderTextColor={'black'}
             />
                  </View>

                 
                  {
                    this.state.show_flings
                    ?
                    <View style={{flex:1}}>
                  <TouchableWithoutFeedback onPress={()=> this.setState({
                    flink_status:true
                  })}>
                  <Image source={require('../assets/fling.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </TouchableWithoutFeedback>
                  </View>
                  :
                  null


                  }


                  <View style={{flex:1}}>
                  <TouchableWithoutFeedback onPress={()=> this.selectPhotoTapped()}>
                  <Image source={require('../assets/cam.png')} style={{alignSelf:'center',height:35,width:35}} />
                  </TouchableWithoutFeedback>
                  </View>


                 {
                   this.state.message.length > 0
                   ?  <View style={{flex:1}}>
                   <TouchableWithoutFeedback onPress={()=> this.sendMessage()}>
                   <Image source={require('../assets/send.png')} style={{alignSelf:'center',height:35,width:35}} />
                   </TouchableWithoutFeedback>
                   </View>
                   :null
                 }
            </View>
            {
              this.state.flink_status
              ?

             
               <View style={{position:'absolute',bottom:0,left:0,right:0,height:250,backgroundColor:'white'}}>
            
               <ScrollView style={{width:'100%',padding:10,flex:1}}>

               <View style={{flex:1,alignSelf:'flex-end'}}>
               <TouchableWithoutFeedback onPress={()=> this.setState({
                 flink_status:false
               })}>
               <Image source={require('../assets/error.png')} style={{alignSelf:'center',height:30,width:30}} />
               </TouchableWithoutFeedback>
               </View>


               <FlatList
               style={{marginBottom:10}}
               data={this.state.flinks}
                numColumns={3}
               showsVerticalScrollIndicator={false}
               scrollEnabled={false}
               renderItem={({item}) =>
   
                   
               <View style={{width:'32%',margin:3}}>

               
                  <Card style={{width:null,height:null,padding:5}}>

               {
                item.status == 1 //means not buyied
                ?
              <View style={{position:'absolute',top:0,bottom:0,right:0,left:0,backgroundColor:'grey'}}>
              
              </View>

              :

              null
              }


                  <TouchableWithoutFeedback onPress={()=> this.sendFlings(item.image,item.status,item.id)}>
                 
                  <Image style={{height:90,width:90}} source={{uri: 'http://webmobril.org/dev/d8bid/'+item.image}} resizeMode="contain"/>
                  </TouchableWithoutFeedback> 
                  </Card>

                  
                  
                  </View>
             
              }      
             />
             </ScrollView>
               
               </View>
              
               :
               null

            }

         </View>

       

        )
    }
}

let styles = StyleSheet.create({
  container:{
    alignItems: 'center',
   
    flex:1,
  

    
  },
 logo_image:{
  width:'100%',
  //height:Dimensions.get('window').height * 0.2
  height:'20%'
 },
 input_text:{
  width:'100%',
  marginBottom:10,

 } ,input: {
  width: "100%",
  height: 45,
  
  borderRadius: 19,
  borderWidth: 1,
  borderColor: 'black',
  backgroundColor:'#E4E4EE'
  
},
loginButton:{
  
    margin:6,
    width:"100%",
    height:40,
    backgroundColor:'#FFC300',
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
  
   
 },
 loginText:{
   color:'black',
   textAlign:'center',
   fontSize :20,
   fontWeight : 'bold'
 },
 socialView:{
   flexDirection:'row',
   alignItems:'center',
   width:'100%',
   justifyContent:'space-between',
   


 },
 social_image:{
  height:50, 
  width:'100%'
  
  
},
indicator: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  height: 80
}
  

}
)

export default withNavigationFocus(ChatBox);