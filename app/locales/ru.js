export default {
  //welcome
  welcome_sign_in: 'войти в систему',
  welcome_sign_up: 'зарегистрироваться',

//login
  email_id: 'Email ID',
  password:'пароль',
  forgot_password:"Забыли пароль",
  login_with:'или войдите с',
  dont_have_account:"Нет аккаунта",
  sign_up:'Подписаться',
  login:'Авторизоваться',

//forgot password
  enter_registered_email:'Введите зарегистрированный адрес электронной почты',
  send_my_password:'Отправить мой пароль',

//enter otp
  password_recovery: 'Восстановление пароля',
  enter_otp:'Введите OTP',
  resend_otp:'Повторно отправить OTP',

//otpshcnagepassword
  please_change_your_password:'Пожалуйста, измените свой пароль',
  new_password:'новый пароль',
  confirm_password:'Подтвердите Пароль',
  change:'+ Изменить',

  //location /location Socailamember
  profile_details:'Детали профиля',
  country:'Страна',
  state:'государственный',
  city:'город',
  gender:'Пол',
  interested_gender:'Интересует пол',
  male:'мужчина',
  female:'женский',
  transgender:'Трансгендер',


  //createprofiel /both
  create_profile:'Создать профиль',
  add_images:'Добавить изображения',
  model_display_name:'Отображаемое имя модели',
  member_display_name:'Отображаемое имя участника',
  age:'Возраст',
  height_cms:'Высота (в смс)',
  weight_kgs:'Вес (в кг)',
  about_bio:'О био (опционально)',
  ethnicity:'Этнос',
  body_type:'Телосложение',
  save:'Save',

  //choose member type both
  who_you_are:'Кто ты',
  please_select_type:'Пожалуйста, выберите ваш тип',
  enter_as_model:'Введите в качестве модели',
  enter_as_member:'Войти в качестве члена',

  //signup member and sign up model
  app_user_name:'Имя пользователя приложения',
  email:'Эл. адрес',
  phone_no:'Номер телефона.',
  password:'пароль',
  confirm_password:'Подтвердите Пароль',
  accept_terms_condition:'Принять условия и условия',
  or_signup_with:'или зарегистрируйтесь с',
  already_have_account :'Уже есть аккаунт',
  log_in:'авторизоваться',

  //instagram email
  enter_your_email:'Введите адрес электронной почты',
  submit:'Отправить',

  //homepasge model/member
  na:'N / A',
  height:'Рост',
  weight:'Вес',
  cms:'КМВ',
  kgs:'килограммы',
  about:'Около',
  available_now:'Доступен сейчас',
  available_later:'Доступно позже',

  //profile_details
  model_profile:'Профиль модели',
  member_profile:'Профиль участника',
  set_bid_amount:'Установить сумму ставки',
  bid_now:'Ставка сейчас',
  choose_bid_amount_dollars:'Выберите сумму ставки (US $)',
  select:'Выбрать',
  your_previous_bid_amount:'Ваша предыдущая сумма ставки',
  raised_bid_amount:'Повышенная сумма ставки',


  //reset password
  reset_password:'Сброс пароля',
  please_update_your_password:'Пожалуйста, обновите ваш пароль',
  old_password:'Прежний пароль',
  new_password:'новый пароль',
  confirm_password:'Подтвердите Пароль',
  reset:'Сброс',

  //profile model
  profile:'Профиль',
  edit:'редактировать',
  location:'Место нахождения',

  //payment
  payment_mode:'Режим оплаты',
  select_payment_mode:'Выберите способ оплаты',
  credit_debit:'Кредитная / дебетовая карта',
  paypal:'Paypal',
  credit_card:'Кредитная карта',

  //model rating
  rate:'Темп',

  //model profile chat.
  redeem_one_coin_chat:'Обменять 1 монету на чат',
  redeem_one_coin_chat_to_lock:'Активируйте 1 монету, чтобы зафиксировать принятую ставку и получить личные контактные данные моделей.',
  redeem_now:'Выкупить сейчас',
  no_coins_left:'Монет не осталось',
  buy_coins_to_connect:'Пожалуйста, купите монеты для подключения',
  buy_coins:'Купите монеты',


  //filters
  choose_desired_location_for_models:'Выберите нужные города, чтобы получить список доступных моделей',
  cities:'Города',
  filters:'фильтры',
  change:'+ Изменить',
  search_by_stars:'Поиск по звездам',
  close:'близко',


  //edit profile both
  edit_profile:'Редактировать профиль',
  name:'название',
  save:'Сохранить',

  //contact us
  contact_us:'Связаться с нами',
  message_title:'Заголовок сообщения',
  description:'Описание',
  model_member_complain:'Модель / Участник Пожаловаться',
  username:'имя пользователя',
  model_member_noshow:'Модель / Участник Нет Показать',
  
  model_accepted_bid:'Принятая модель не соответствует',
  member_send_bid:'Участник Отправить ставку не встретил',
  bad_behaviour:'Плохое поведение или насилие',
  model_paid_coin:'Модель платной монеты',
  model_didnot_meet:'Модель не встречалась',
  please_detail_complain:'Пожалуйста, подробно опишите вашу жалобу',
  objectional_content:'Неприемлемое содержание, злоупотребление или насилие',


  //buy coins and stars
  buy_coins:'Купите монеты',
  buy_stars:'Купить Звезды',
  buy_coins_to_unlock_conversation:'Покупайте монеты, чтобы разблокировать разговоры и идти на первые свидания!',
  choose_number_of_coins:'Выберите количество монет.',
  buy_now:'купить сейчас',
  total_price:'Итоговая цена',
  buy_star_rating_visible_partner:'Покупайте рейтинг звезд для более заметного для ваших партнеров!',
  choose_number_of_stars:'Выберите количество звезд.',


//notifcaiton
  enter_raise_amount:'Введите сумму повышения',
  enter_amount_dollars:'Введите сумму в долларах',
  raise:'поднимать',
  raised:'поднятый',
  accepted:'Принято',
  rejected:'Отклонено',
  bid_price:'Цена предложения',
  pending:'в ожидании',

  //cchat lit
  chat_list : "Списки чата",

  //choose language
  choose_language:'Выберите язык',
  language:'язык',

  //drawer both
  home:'Главная',
  chats:'Чаты',
  coins:'монеты',
  stars:'Звезды',
  notifications:'Уведомления',
  update_profile:'Обновить профиль',
  change_password:'Изменить пароль',
  legal:'легальный',
  privacy_policy:'политика конфиденциальности',
  terms_and_condition:'Условия использования',
  logout:'Выйти',
  bids:'Инфо',

  //header
  lists : 'Списки',
  activity:'Деятельность',

  //drawer
  coins:'монеты',
  stars:'Звезды',

  //location modal overlay
  choose:'выберите',
  distance:'Расстояние',
  choose_distance:'Выберите Расстояние (км)',

  //block model
  block :'блок',
  unblock:'Разблокировать',
  choose_block_reason:'Введите причину для блокировки (необязательно)',
  block_this_model:'Заблокировать эту модель',

  //
  enter_otp_email:'Пожалуйста, подтвердите ваш адрес электронной почты с OTP, который мы вам отправили. Проверьте нежелательную почту, если нет в папке "Входящие"',


  //video
  add_video:'Добавить видео',
  see_video:'Смотреть видео',
  upload:'Загрузить',
  no_video_found:'Видео не найдено',

    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',



};
