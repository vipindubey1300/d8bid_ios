export default {
  //welcome
  welcome_sign_in: 'تسجيل الدخول',
  welcome_sign_up: 'سجل',

//login
  email_id: 'عنوان الايميل',
  password:'كلمه السر',
  forgot_password:"هل نسيت كلمة المرور",
  login_with:'أو تسجيل الدخول مع',
  dont_have_account:"ليس لديك حساب",
  sign_up:'	سجل',
  login:'	تسجيل الدخول',

//forgot password
  enter_registered_email:'أدخل البريد الإلكتروني المسجل',
  send_my_password:'	أرسل كلمة المرور الخاصة بي',

//enter otp
  password_recovery: 'استعادة كلمة السر',
  enter_otp:'أدخل OTP',
  resend_otp:'إعادة إرسال OTP',

//otpshcnagepassword
  please_change_your_password:'يرجى تغيير كلمة المرور الخاصة بك',
  new_password:'كلمة السر الجديدة',
  confirm_password:'تأكيد كلمة المرور',
  change:'يتغيرون',

  //location /location Socailamember
  profile_details:'تفاصيل الملف الشخصي',
  country:'بلد',
  state:'حالة',
  city:'مدينة',
  gender:'جنس',
  interested_gender:'الجنس المهتم',
  male:'الذكر',
  female:'إناثا',
  transgender:'المتحولين جنسيا',


  //createprofiel /both
  create_profile:'إنشاء ملف تعريف',
  add_images:'	إضافة الصور',
  model_display_name:'اسم العرض النموذجي',
  member_display_name:'اسم عرض العضو',
  age:'عمر',
  height_cms:'الإرتفاع بالسنتيمير)',
  weight_kgs:'الوزن (بالكيلوغرام)',
  about_bio:'حول السيرة الذاتية (اختياري)',
  ethnicity:'	الأصل العرقي',
  body_type:'نوع الجسم',
  save:'حفظ',

  //choose member type both
  who_you_are:'من أنت',
  please_select_type:'يرجى اختيار النوع الخاص بك',
  enter_as_model:'أدخل كنموذج',
  enter_as_member:'أدخل كعضو',

  //signup member and sign up model
  app_user_name:'اسم المستخدم التطبيق',
  email:'البريد الإلكتروني',
  phone_no:'رقم الهاتف.',
  password:'كلمه السر',
  confirm_password:'تأكيد كلمة المرور',
  accept_terms_condition:'قبول الشروط والأحكام',
  or_signup_with:'أو الاشتراك مع',
  already_have_account :'هل لديك حساب',
  log_in:'تسجيل الدخول',

  //instagram email
  enter_your_email:'أدخل معرف البريد الإلكتروني الخاص بك',
  submit:'خضع',

  //homepasge model/member
  na:'N/A',
  height:'ارتفاع',
  weight:'وزن',
  cms:'سم',
  kgs:'	كلغ',
  about:'حول',
  available_now:'متوفر في الوقت الحاضر',
  available_later:'	متاح لاحقا',

  //profile_details
  model_profile:'الملف الشخصي النموذج',
  member_profile:'الملف الشخصي للعضو',
  set_bid_amount:'تعيين مبلغ العطاء',
  bid_now:'اشتري الآن',
  choose_bid_amount_dollars:'اختر مبلغ العطاء (بالدولار)',
  select:'تحديد',
  your_previous_bid_amount:'مبلغ العطاء السابق',
  raised_bid_amount:'أثارت مبلغ العطاء',


  //reset password
  reset_password:'إعادة تعيين كلمة المرور',
  please_update_your_password:'يرجى تحديث كلمة المرور الخاصة بك',
  old_password:'كلمة المرور القديمة',
  new_password:'كلمة السر الجديدة',
  confirm_password:'تأكيد كلمة المرور',
  reset:'إعادة تعيين',

  //profile model
  profile:'الملف الشخصي',
  edit:'تصحيح',
  location:'موقعك',

  //payment
  payment_mode:'طريقة الدفع',
  select_payment_mode:'حدد طريقة الدفع',
  credit_debit:'بطاقة الائتمان / الخصم',
  paypal:'باي بال',
  credit_card:'بطاقة ائتمان',

  //model rating
  rate:'معدل',

  //model profile chat.
  redeem_one_coin_chat:'استبدل عملة واحدة للدردشة',
  redeem_one_coin_chat_to_lock:'استبدل عملة واحدة بقفل العطاء المقبول وتلقي الطرز تفاصيل الاتصال الخاصة',
  redeem_now:'استبدال الآن',
  no_coins_left:'لا النقود اليسار',
  buy_coins_to_connect:'يرجى شراء العملات المعدنية للاتصال',
  buy_coins:'شراء العملات',


  //filters
  choose_desired_location_for_models:'اختيار المدن المطلوبة للحصول على قائمة النماذج المتاحة',
  cities:'	مدن',
  filters:'	مرشحات',
  change:'يتغيرون',
  search_by_stars:'البحث عن طريق النجوم',
  close:'قريب',


  //edit profile both
  edit_profile:'تعديل الملف الشخصي',
  name:'اسم',
  save:'حفظ',

  //contact us
  contact_us:'اتصل بنا',
  message_title:'عنوان الرسالة',
  description:'	وصف',
  model_member_complain:'نموذج / عضو يشكو',
  username:'	اسم المستخدم',
  model_member_noshow:'نموذج / عضو لا تظهر',
  model_accepted_bid:'نموذج العطاء المقبول لم يلتق',
  member_send_bid:'العضو المرسلة',
  bad_behaviour:'سوء السلوك أو العنف',
  model_paid_coin:'نموذج العملة المدفوعة',
  model_didnot_meet:'	نموذج لم يجتمع',
  please_detail_complain:'يرجى تقديم تفاصيل شكواك',
  objectional_content:'المحتوى الموضوعي ، سوء المعاملة أو العنف',


  //buy coins and stars
  buy_coins:'شراء العملات',
  buy_stars:'شراء نجوم',
  buy_coins_to_unlock_conversation:'شراء عملات معدنية لفتح المحادثات والذهاب في التواريخ الأولى!',
  choose_number_of_coins:'اختيار عدد من العملات المعدنية.',
  buy_now:'اشتري الآن',
  total_price:'السعر الكلي',
  buy_star_rating_visible_partner:'شراء تصنيف النجوم لأكثر وضوحًا لشركائك!',
  choose_number_of_stars:'اختيار عدد من النجوم.',


//notifcaiton
  enter_raise_amount:'أدخل زيادة المبلغ',
  enter_amount_dollars:'أدخل المبلغ بالدولار',
  raise:'ربى',
  raised:'رفع',
  accepted:'وافقت',
  rejected:'مرفوض',
  bid_price:'سعر',
  pending:'قيد الانتظار',

  //cchat lit
  chat_list : "قوائم الدردشة",

  //choose language
  choose_language:'اختر اللغة',
  language:'لغة',

  //drawer both
  home:'الصفحة الرئيسية',
  chats:'دردشات',
  coins:'عملات معدنية',
  stars:'نجوم',
  notifications:'إخطارات',
  update_profile:'تحديث الملف',
  change_password:'غير كلمة السر',
  legal:'قانوني',
  privacy_policy:'سياسة خاصة',
  terms_and_condition:'لشروط والأحكام',
  logout:'الخروج',
  bids:'العروض',

  //header
  lists : 'قوائم',
  activity:'نشاط',

  //drawer
  coins:'عملات معدنية',
  stars:'نجوم',

  //location modal overlay
  choose:'العروض',
  distance:'مسافه: بعد',
  choose_distance:'(اختيار المسافة (كم',
  

  //block model
  block :'منع',
  unblock:'رفع الحظر',
  choose_block_reason:'أدخل سبب الحظر (اختياري)',
  block_this_model:'منع هذا النموذج',

  //
  enter_otp_email:'يرجى التحقق من بريدك الإلكتروني باستخدام OTP التي أرسلناها إليك. تحقق من البريد غير الهام إن لم يكن في صندوق الوارد',

    //video
    add_video:'أضف فيديو',
    see_video:'انظر الفيديو',
    upload:'رفع',
    no_video_found:'لم يتم العثور على فيديو',

      //location
  send:'Send',
  your_current_location:'Your Current location :',
  nearby_favourite_places:'Nearby Favourite Places :',
  send_location:'Send Location',


};
