export default {
  //welcome
  welcome_sign_in: 'Registrarse',
  welcome_sign_up: 'Regístrate',

//login
  email_id: 'Identificación de correo',
  password:'Contraseña',
  forgot_password:"Se te olvidó tu contraseña",
  login_with:'o inicia sesión con',
  dont_have_account:"No tienes cuenta",
  sign_up:'Regístrate',
  login:'Iniciar sesión',

//forgot password
  enter_registered_email:'Ingrese el correo electrónico registrado',
  send_my_password:'Enviar mi contraseña',

//enter otp
  password_recovery: 'Recuperación de contraseña',
  enter_otp:'Entrar en OTP',
  resend_otp:'Reenviar OTP',

//otpshcnagepassword
  please_change_your_password:'Por favor cambia tu contraseña',
  new_password:'Nueva contraseña',
  confirm_password:'Confirmar contraseña',
  change:'Cambio',

  //location /location Socailamember
  profile_details:'detalles del perfil',
  country:'País',
  state:'Estado',
  city:'Ciudad',
  gender:'Género',
  interested_gender:'Género interesado',
  male:'Masculino',
  female:'Hembra',
  transgender:'Transgénero',


  //createprofiel /both
  create_profile:'Crear perfil',
  add_images:'Añadir imágenes',
  model_display_name:'Nombre para mostrar modelo',
  member_display_name:'Nombre para mostrar del miembro',
  age:'Años',
  height_cms:'Altura en (centímetros)',
  weight_kgs:'Peso (en kgs)',
  about_bio:'Sobre Bio (Opcional)',
  ethnicity:'Etnicidad',
  body_type:'Tipo de cuerpo',
  save:'Salvar',

  //choose member type both
  who_you_are:'Quien eres',
  please_select_type:'Por favor seleccione su tipo',
  enter_as_model:'Entrar como modelo',
  enter_as_member:'Entrar como miembro',

  //signup member and sign up model
  app_user_name:'Nombre de usuario de la aplicación',
  email:'Email',
  phone_no:'Telefono no.',
  password:'Contraseña',
  confirm_password:'Confirmar contraseña',
  accept_terms_condition:'Aceptar términos y condiciones',
  or_signup_with:'o Regístrate con',
  already_have_account :'Ya tienes una cuenta',
  log_in:'iniciar sesión',

  //instagram email
  enter_your_email:'Ingrese su ID de correo electrónico',
  submit:'Enviar',

  //homepasge model/member
  na:'N/A',
  height:'Altura',
  weight:'Peso',
  cms:'cms',
  kgs:'kgs',
  about:'Acerca de',
  available_now:'Disponible ahora',
  available_later:'Disponible mas tarde',

  //profile_details
  model_profile:'Perfil del modelo',
  member_profile:'Perfil de miembro',
  set_bid_amount:'Establecer el monto de la oferta',
  bid_now:'Haga su oferta',
  choose_bid_amount_dollars:'Elija el monto de la oferta (US $)',
  select:'Seleccionar',
  your_previous_bid_amount:'Su monto de puja anterior',
  raised_bid_amount:'Cantidad aumentada de la oferta',


  //reset password
  reset_password:'Restablecer la contraseña',
  please_update_your_password:'Por favor actualice su contraseña',
  old_password:'Contraseña anterior',
  new_password:'Nueva contraseña',
  confirm_password:'Confirmar contraseña',
  reset:'Reiniciar',

  //profile model
  profile:'Perfil',
  edit:'Editar',
  location:'Ubicación',

  //payment
  payment_mode:'Modo de pago',
  select_payment_mode:'Seleccione el modo de pago',
  credit_debit:'Tarjeta de crédito / débito',
  paypal:'Paypal',
  credit_card:'Tarjeta de crédito',



  //model rating
  rate:'Tarifa',

  //model profile chat.
  redeem_one_coin_chat:'Canjear 1 moneda por chat',
  redeem_one_coin_chat_to_lock:'Canjee 1 moneda para bloquear la oferta aceptada y recibir modelos de datos de contacto privados',
  redeem_now:'Canjear ahora',
  no_coins_left:'No quedan monedas',
  buy_coins_to_connect:'Por favor, compre monedas para conectar',
  buy_coins:'Comprar monedas',


  //filters
  choose_desired_location_for_models:'Elija las ciudades deseadas para obtener la lista de modelos disponibles',
  cities:'Ciudades',
  filters:'Filtros',
  change:'Cambio',
  search_by_stars:'Búsqueda por estrellas',
  close:'Cerca',


  //edit profile both
  edit_profile:'Editar perfil',
  name:'Nombre',
  save:'Salvar',

  //contact us
  contact_us:'Contáctenos',
  message_title:'Título del mensaje',
  description:'Descripción',
  model_member_complain:'Modelo / Miembro Quejarse',
  username:'Nombre de usuario',
  model_member_noshow:'Modelo / Miembro No Show',
  objectional_content:'Contenido objetable, abuso o violencia',

  
  model_accepted_bid:'La oferta aceptada del modelo no se cumplió',
  member_send_bid:'La oferta de envío del miembro no se cumplió',
  bad_behaviour:'Mal comportamiento o violencia',
  model_paid_coin:'Oferta de modelo aceptada',
  model_didnot_meet:'Modelo no se reunió',
  please_detail_complain:'Por favor detalle su queja',

  //buy coins and stars
  buy_coins:'Comprar monedas',
  buy_stars:'Comprar estrellas',
  buy_coins_to_unlock_conversation:'¡Compra monedas para desbloquear conversaciones e ir a las primeras citas!',
  choose_number_of_coins:'Elija el número de monedas.',
  buy_now:'Compra ahora',
  total_price:'Precio total',
  buy_star_rating_visible_partner:'¡Compre estrellas de calificación para que sean más visibles para sus socios!',
  choose_number_of_stars:'Elija número de estrellas.',


//notifcaiton
  enter_raise_amount:'Ingrese la cantidad de aumento',
  enter_amount_dollars:'Ingrese el monto en dólares',
  raise:'Aumento',
  raised:'Elevado',
  accepted:'Aceptada',
  rejected:'Rechazado',
  bid_price:'Precio de oferta',
  pending:'Pendiente',

  //cchat lit
  chat_list : "Listas de chat",

  //choose language
  choose_language:'Elige lengua',
  language:'Idioma',

  //drawer both
  home:'Casa',
  chats:'Chats',
  coins:'Monedas',
  stars:'Estrellas',
  notifications:'Notificaciones',
  update_profile:'Actualización del perfil',
  change_password:'Cambia la contraseña',
  legal:'Legal',
  privacy_policy:'Política de privacidad',
  terms_and_condition:'Términos y Condiciones',
  logout:'Cerrar sesión',
  bids:'Ofertas',

  //header
  lists : 'Liza',
  activity:'Actividad',

  //drawer
  coins:'Monedas',
  stars:'Estrellas',

  //location modal overlay
  choose:'Escoger',
  distance:'Distancia',
  choose_distance:'Elegir distancia (km)',

  //block model
  block :'Bloquear',
  unblock:'Desatascar',
  choose_block_reason:'Ingrese el motivo para bloquear (opcional)',
  block_this_model:'Bloquear este modelo',

  //
  enter_otp_email:'Verifique su correo electrónico con OTP que le hemos enviado. Revise el correo no deseado si no está en la bandeja de entrada',


    //video
    add_video:'Añadir video',
    see_video:'Ver video',
    upload:'Subir',
    no_video_found:'No se encontró video',



      //location
  send:'Send',
  your_current_location:'Your Current location :',
  nearby_favourite_places:'Nearby Favourite Places :',
  send_location:'Send Location',





};
