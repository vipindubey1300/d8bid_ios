export default {
  //welcome
  welcome_sign_in: 'Sign in',
  welcome_sign_up: 'Sign up',

//login
  email_id: 'Email ID',
  password:'Password',
  forgot_password:"Forgot Password",
  login_with:'or login with',
  dont_have_account:"Don't have an account",
  sign_up:'Sign Up',
  login:'Login',

//forgot password
  enter_registered_email:'Enter Registered Email',
  send_my_password:'Send my Password',

//enter otp
  password_recovery: 'Password Recovery',
  enter_otp:'Enter OTP',
  resend_otp:'Resend OTP',

//otpshcnagepassword
  please_change_your_password:'Please change your password',
  new_password:'New Password',
  confirm_password:'Confirm Password',
  change:'Change',

  //location /location Socailamember
  profile_details:'Profile Details',
  country:'Country',
  state:'State',
  city:'City',
  gender:'Gender',
  interested_gender:'Interested Gender',
  male:'Male',
  female:'Female',
  transgender:'Transgender',


  //createprofiel /both
  create_profile:'Create Profile',
  add_images:'Add Images',
  model_display_name:'Model Display Name',
  member_display_name:'Member Display Name',
  age:'Age',
  height_cms:'Height (in cms)',
  weight_kgs:'Weight (in kgs)',
  about_bio:'About Bio (Optional)',
  ethnicity:'Ethnicity',
  body_type:'Body type',
  save:'Save',

  //choose member type both
  who_you_are:'Who you are',
  please_select_type:'Please Select Your Type',
  enter_as_model:'Enter as a Model',
  enter_as_member:'Enter as a Member',

  //signup member and sign up model
  app_user_name:'App User Name',
  email:'Email',
  phone_no:'Phone No.',
  password:'Password',
  confirm_password:'Confirm Password',
  accept_terms_condition:'Accept terms & Condition',
  or_signup_with:'or SignUp with',
  already_have_account :'Already have an account',
  log_in:'Log In',

  //instagram email
  enter_your_email:'Enter Your Email ID',
  submit:'Submit',

  //homepasge model/member
  na:'N/A',
  height:'Height',
  weight:'Weight',
  cms:'cms',
  kgs:'kgs',
  about:'About',
  available_now:'Available Now',
  available_later:'Available Later',

  //profile_details
  model_profile:'Model Profile',
  member_profile:'Member Profile',
  set_bid_amount:'Set Bid Amount',
  bid_now:'Bid Now',
  choose_bid_amount_dollars:'Choose Bid Amount (US $)',
  select:'Select',
  your_previous_bid_amount:'Your previous Bid Amount',
  raised_bid_amount:'Raised Bid Amount',


  //reset password
  reset_password:'Reset Password',
  please_update_your_password:'Please update your password',
  old_password:'Old Password',
  new_password:'New Password',
  confirm_password:'Confirm Password',
  reset:'Reset',

  //profile model
  profile:'Profile',
  edit:'Edit',
  location:'Location',

  //payment
  payment_mode:'Payment Mode',
  select_payment_mode:'Select Payment Mode',
  credit_debit:'Credit / Debit Card',
  paypal:'Paypal',
  credit_card:'Credit card',

  //model rating
  rate:'Rate',

  //model profile chat.
  redeem_one_coin_chat:'Redeem 1 coin for chat',
  redeem_one_coin_chat_to_lock:'Redeem 1 coin to lock in accepted bid and recieve models private contact details',
  redeem_now:'Redeem Now',
  no_coins_left:'No Coins Left',
  buy_coins_to_connect:'Please Buy coins to connect',
  buy_coins:'Buy Coins',


  //filters
  choose_desired_location_for_models:'Choose desired cities to get list of available models',
  cities:'Cities',
  filters:'Filters',
  change:'Change',
  search_by_stars:'Search By Stars',
  age:'Age',
  close:'Close',


  //edit profile both
  edit_profile:'Edit Profile',
  name:'Name',
  save:'Save',

  //contact us
  contact_us:'Contact Us',
  message_title:'Message Title',
  description:'Description',
  model_member_complain:'Model / Member Complaint',
  username:'Username',
  model_member_noshow:'Model / Member No Show',
  model_accepted_bid:'Model Accepted Bid Didn’t Meet',
  member_send_bid:'Member Sent Bid Didn’t Meet',
  bad_behaviour:'Bad Behaviour or Violence',
  model_paid_coin:'Model Paid Coin',
  model_didnot_meet:'Model Did not Meet',
  please_detail_complain:'Please detail your complaint',
  objectional_content:'Objectional Content ,Abuse or Violence',

  //buy coins and stars
  buy_coins:'Buy Coins',
  buy_stars:'Buy Stars',
  buy_coins_to_unlock_conversation:'Buy Coins to unlock conversations and go on first dates !',
  choose_number_of_coins:'Choose number of coins.',
  buy_now:'Buy Now',
  total_price:'Total Price',
  buy_star_rating_visible_partner:'Buy stars rating for more visible to your partners !',
  choose_number_of_stars:'Choose number of stars.',


//notifcaiton
  enter_raise_amount:'Enter Raise Amount',
  enter_amount_dollars:'Enter amount in dollars',
  raise:'Raise',
  raised:'Raised',
  accepted:'Accepted',
  rejected:'Rejected',
  bid_price:'Bid Price',
  pending:'Pending',

  //cchat lit
  chat_list : "Chat Lists",

  //choose language
  choose_language:'Choose Language',
  language:'Language',

  //drawer both
  home:'Home',
  chats:'Chats',
  coins:'Coins',
  stars:'Stars',
  notifications:'Notifications',
  update_profile:'Update Profile',
  change_password:'Change Password',
  legal:'Legal',
  privacy_policy:'Privacy Policy',
  terms_and_condition:'Terms & Condition',
  logout:'Logout',
  bids:'Bids',

  //header
  lists : 'Lists',
  activity:'Activity',

  //drawer
  coins:'Coins',
  stars:'Stars',

  //location modal overlay
  choose:'Choose',
  distance:'Distance',
  choose_distance:'Choose Distance(km)',

  //block model
  block :'Block',
  unblock:'Unblock',
  choose_block_reason:'Enter reason to block(optional)',
  block_this_model:'Block this Model',

  //
  enter_otp_email:'Please verify your email with OTP we have sent to you. Check junk mail if not in inbox',


  //video
  add_video:'Add Video',
  see_video:'See Video',
  upload:'Upload',
  no_video_found:'No Video Found',


  //location
  send:'Send',
  your_current_location:'Your Current location :',
  nearby_favourite_places:'Nearby Favourite Places :',
  send_location:'Send Location',


  






};
