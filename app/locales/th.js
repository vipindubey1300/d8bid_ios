export default {
  //welcome
  welcome_sign_in: 'ลงชื่อเข้าใช้',
  welcome_sign_up: 'ลงชื่อ',

//login
  email_id: 'ชื่ออีเมล์',
  password:'รหัสผ่าน',
  forgot_password:"ลืมรหัสผ่าน",
  login_with:'หรือเข้าสู่ระบบด้วย',
  dont_have_account:"ไม่ได้มีบัญชี",
  sign_up:'ลงชื่อ',
  login:'เข้าสู่ระบบ',

//forgot password
  enter_registered_email:'ใส่อีเมลลงทะเบียน',
  send_my_password:'ส่งรหัสผ่านของฉัน',

//enter otp
  password_recovery: 'รหัสผ่าน',
  enter_otp:'ป้อน OTP',
  resend_otp:'ส่ง OTP อีกครั้ง',

//otpshcnagepassword
  please_change_your_password:'กรุณาเปลี่ยนรหัสผ่านของคุณ',
  new_password:'รหัสผ่านใหม่',
  confirm_password:'ยืนยันรหัสผ่าน',
  change:'เปลี่ยนแปลง',

  //location /location Socailamember
  profile_details:'รายละเอียดโปรไฟล์',
  country:'ประเทศ',
  state:'อำเภอ',
  city:'เมือง',
  gender:'เพศ',
  interested_gender:'เพศที่สนใจ',
  male:'ชาย',
  female:'หญิง',
  transgender:'เพศ',


  //createprofiel /both
  create_profile:'สร้างโปรไฟล์',
  add_images:'เพิ่มรูปภาพ',
  model_display_name:'ชื่อที่แสดงของแบบจำลอง',
  member_display_name:'ชื่อที่แสดงของสมาชิก',
  age:'อายุ',
  height_cms:'ความสูง (ซม.)',
  weight_kgs:'น้ำหนัก (กิโลกรัม)',
  about_bio:'เกี่ยวกับไบโอ (ไม่จำเป็น)',
  ethnicity:'เชื้อชาติ',
  body_type:'ประเภทของร่างกาย',
  save:'บันทึก',

  //choose member type both
  who_you_are:'คุณเป็นใคร',
  please_select_type:'กรุณาเลือกประเภทของคุณ',
  enter_as_model:'ลงชื่อเป็นนางแบบ',
  enter_as_member:'ลงชื่อเป็นสมาชิก',

  //signup member and sign up model
  app_user_name:'ชื่อผู้ใช้แอป',
  email:'อีเมล์',
  phone_no:'เบอร์โทรศัพท์',
  password:'รหัสผ่าน',
  confirm_password:'ยืนยันรหัสผ่าน',
  accept_terms_condition:'ยอมรับข้อกำหนดและเงื่อนไข',
  or_signup_with:'หรือสมัครสมาชิกกับ',
  already_have_account :'มีบัญชีแล้ว',
  log_in:'เข้าสู่ระบบ',

  //instagram email
  enter_your_email:'ป้อนรหัสอีเมลของคุณ',
  submit:'เสนอ',

  //homepasge model/member
  na:'N/A',
  height:'ความสูง',
  weight:'น้ำหนัก',
  cms:'ซ.ม.',
  kgs:'กิโลกรัม',
  about:'เกี่ยวกับ',
  available_now:'ใช้ได้ในขณะนี้',
  available_later:'มีจำหน่ายในภายหลัง',

  //profile_details
  model_profile:'โปรไฟล์โมเดล',
  member_profile:'รายละเอียดสมาชิก',
  set_bid_amount:'กำหนดจำนวนการเสนอราคา',
  bid_now:'ประมูลตอนนี้',
  choose_bid_amount_dollars:'เลือกจำนวนราคาเสนอ (US $)',
  select:'เลือก',
  your_previous_bid_amount:'จำนวนราคาเสนอก่อนหน้าของคุณ',
  raised_bid_amount:'จำนวนราคาเสนอที่เพิ่มขึ้น',


  //reset password
  reset_password:'รีเซ็ตรหัสผ่าน',
  please_update_your_password:'โปรดอัปเดตรหัสผ่านของคุณ',
  old_password:'รหัสผ่านเก่า',
  new_password:'รหัสผ่านใหม่',
  confirm_password:'ยืนยันรหัสผ่าน',
  reset:'รีเซ็ต',

  //profile model
  profile:'ข้อมูลส่วนตัว',
  edit:'แก้ไข',
  location:'ที่ตั้ง',

  //payment
  payment_mode:'โหมดการชำระเงิน',
  select_payment_mode:'เลือกโหมดการชำระเงิน',
  credit_debit:'บัตรเครดิต / เดบิต',
  paypal:'Paypal',
  credit_card:'บัตรเครดิต',

  //model rating
  rate:'อัตรา',

  //model profile chat.
  redeem_one_coin_chat:'แลก 1 เหรียญสำหรับการแชท',
  redeem_one_coin_chat_to_lock:'แลก 1 เหรียญเพื่อล็อคในการเสนอราคาที่ยอมรับและรับรายละเอียดการติดต่อส่วนตัว',
  redeem_now:'แลกเลย',
  no_coins_left:'ไม่มีเหรียญเหลือ',
  buy_coins_to_connect:'กรุณาซื้อเหรียญเพื่อเชื่อมต่อ',
  buy_coins:'ซื้อเหรียญ',


  //filters
  choose_desired_location_for_models:'เลือกเมืองที่ต้องการเพื่อรับรายการรุ่นที่มี',
  cities:'เมือง',
  filters:'ฟิลเตอร์',
  change:'เปลี่ยนแปลง',
  search_by_stars:'ค้นหาตามดาว',
  close:'ปิด',


  //edit profile both
  edit_profile:'แก้ไขโปรไฟล์',
  name:'ชื่อ',
  save:'บันทึก',

  //contact us
  contact_us:'ติดต่อเรา',
  message_title:'ชื่อข้อความ',
  description:'ลักษณะ',
  model_member_complain:'รูปแบบ / การร้องเรียนของสมาชิก',
  username:'ชื่อผู้ใช้',
  model_member_noshow:'นางแบบ / สมาชิกไม่แสดง',
  
  model_accepted_bid:'แบบจำลองการเสนอราคาที่ยอมรับไม่ตอบสนอง',
  member_send_bid:'สมาชิกส่งการประมูลไม่พบ',
  bad_behaviour:'พฤติกรรมที่ไม่ดีหรือความรุนแรง',
  model_paid_coin:'รุ่นจ่ายเหรียญ',
  model_didnot_meet:'แบบจำลองไม่พบ',
  please_detail_complain:'โปรดระบุรายละเอียดการร้องเรียนของคุณ',
  objectional_content:'เนื้อหาเชิงวัตถุประสงค์การละเมิดหรือความรุนแรง',


  //buy coins and stars
  buy_coins:'ซื้อเหรียญ',
  buy_stars:'ซื้อดวงดาว',
  buy_coins_to_unlock_conversation:'ซื้อเหรียญเพื่อปลดล็อคการสนทนาและเริ่มในวันแรก!',
  choose_number_of_coins:'เลือกจำนวนเหรียญ',
  buy_now:'ซื้อเลย',
  total_price:'ราคารวม',
  buy_star_rating_visible_partner:'ซื้อคะแนนดาวเพื่อให้พันธมิตรของคุณมองเห็นได้ชัดเจนยิ่งขึ้น !',
  choose_number_of_stars:'เลือกจำนวนดาว',


//notifcaiton
  enter_raise_amount:'ป้อนจำนวนเงินเพิ่ม',
  enter_amount_dollars:'ป้อนจำนวนเงินเป็นดอลลาร์',
  raise:'ยก',
  raised:'ยก',
  accepted:'ได้รับการยืนยัน',
  rejected:'ปฏิเสธ',
  bid_price:'ราคาเสนอซื้อ',
  pending:'รอดำเนินการ',

  //cchat lit
  chat_list : "รายการแชท",

  //choose language
  choose_language:'เลือกภาษา',
  language:'ภาษา',

  //drawer both
  home:'บ้าน',
  chats:'แชท',
  coins:'เหรียญ',
  stars:'ดาว',
  notifications:'การแจ้งเตือน',
  update_profile:'อัปเดตโปรไฟล์',
  change_password:'เปลี่ยนรหัสผ่าน',
  legal:'ถูกกฎหมาย',
  privacy_policy:'นโยบายความเป็นส่วนตัว',
  terms_and_condition:'ข้อกำหนดและเงื่อนไข',
  logout:'ออกจากระบบ',
  bids:'การเสนอราคา',

  //header
  lists : 'รายการ',
  activity:'กิจกรรม',

  //drawer
  coins:'เหรียญ',
  stars:'ดาว',

  //location modal overlay
  choose:'เลือก',
  distance:'ระยะทาง',
  choose_distance:'เลือกระยะทาง (กม.)',

  //block model
  block :'กลุ่ม',
  unblock:'เลิกบล็อก',
  choose_block_reason:'ป้อนเหตุผลในการบล็อก (ไม่บังคับ)',
  block_this_model:'บล็อกรุ่นนี้',

  //
  enter_otp_email:'โปรดยืนยันอีเมลของคุณด้วย OTP ที่เราได้ส่งถึงคุณ ตรวจสอบเมลขยะหากไม่อยู่ในกล่องจดหมาย',


  //video
  add_video:'เพิ่มวิดีโอ',
  see_video:'ดูวิดีโอ',
  upload:'ที่อัพโหลด',
  no_video_found:'ไม่พบวิดีโอ',

    //location
    send:'Send',
    your_current_location:'Your Current location :',
    nearby_favourite_places:'Nearby Favourite Places :',
    send_location:'Send Location',



};
