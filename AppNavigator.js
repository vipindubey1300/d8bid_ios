import { createStackNavigator , createSwitchNavigator ,createAppContainer ,createDrawerNavigator, createMaterialTopTabNavigator,DrawerItems} from 'react-navigation';
import React from 'react';
import { Text, View, Button, StyleSheet,SafeAreaView, Dimensions, ScrollView, Image, FlatList, TouchableOpacity,ToastAndroid,AsyncStorage } from 'react-native';
import { colors,urls,fonts} from './app/screens/Variables';
import I18n from './app/i18n';


//screens

import Splash from './app/screens/Splash';
import Login from './app/screens/Login';
import Welcome from './app/screens/Welcome';
import Terms from './app/screens/Terms';
import Signup from './app/screens/Signup';
import SignupMember from './app/screens/SignupMember';
import SignupModel from './app/screens/SignupModel';
import MemberType from './app/screens/MemberType';
import MemberTypeSocial from './app/screens/MemberTypeSocial';
import LocationSocial from './app/screens/LocationSocial';
import LocationSocialMember from './app/screens/LocationSocialMember';
import ResetPassword from './app/screens/ResetPassword';
import ForgotPassword from './app/screens/ForgotPassword';
import CreateProfileModel from './app/screens/CreateProfileModel';
import CreateProfileMember from './app/screens/CreateProfileMember';
import ProfileMember from './app/screens/ProfileMember';
import EditProfileMember from './app/screens/EditProfileMember';
import ProfileModel from './app/screens/ProfileModel';
import EditProfileModel from './app/screens/EditProfileModel';
import DrawerModel from './app/screens/DrawerModel';
import DrawerMember from './app/screens/DrawerMember';
import MemberHomePage from './app/screens/MemberHomePage';
import BuyStars from './app/screens/BuyStars';
import ModelType from './app/screens/ModelType';
import MemberProtest from './app/screens/MemberProtest';
import MemberProfile from './app/screens/MemberProfile';
import MemberProfileBlock from './app/screens/MemberProfileBlock';
import ModelProfile from './app/screens/ModelProfile';
import NotificationActivity from './app/screens/NotificationActivity';
//import PaymentMode from './app/screens/PaymentMode';
import HomePage from './app/screens/HomePage';
import Filters from './app/screens/Filters';
import PrivacyPolicy from './app/screens/PrivacyPolicy';
import EnterOtp from './app/screens/EnterOtp';
import OtpChangePassword from './app/screens/OtpChangePassword';
import ModelProfileChat from './app/screens/ModelProfileChat';
import BuyCoins from './app/screens/BuyCoins';
import ProfileDetails from './app/screens/ProfileDetails';
import ModelProtest from './app/screens/ModelProtest';
import ModelBidding from './app/screens/ModelBidding';
import NotificationsActivityMember from './app/screens/NotificationsActivityMember';
import NotificationsListMember from './app/screens/NotificationsListMember';
import NotificationsActivityModel from './app/screens/NotificationsActivityModel';
import NotificationsListModel from './app/screens/NotificationsListModel';
import ChatList from './app/screens/ChatList';
import ChatBox from './app/screens/ChatBox';
import ChatBoxModel from './app/screens/ChatBoxModel';
import ChatBoxMember from './app/screens/ChatBoxMember';
import UpdateBid from './app/screens/UpdateBid';
import ContactUs from './app/screens/ContactUs';
import InstagramEmail from './app/screens/InstagramEmail';
import ModelRating from './app/screens/ModelRating';
import PrivacyPolicyDrawer from './app/screens/PrivacyPolicyDrawer';
import TermsDrawer from './app/screens/TermsDrawer';
import ChooseLanguage from './app/screens/ChooseLanguage';
import ChooseLanguageDrawer from './app/screens/ChooseLanguageDrawer';
import OTPEmail from './app/screens/OTPEmail';
import ModelBlock from './app/screens/ModelBlock';
import SendLocation from './app/screens/SendLocation';
import ImageView from './app/screens/ImageView';


console.disableYellowBox = true;






const memberprofileStack = createStackNavigator({

 ProfileMember : { screen: ProfileMember},
 EditProfileMember : { screen: EditProfileMember},


}, {
	 headerMode: 'none',
     initialRouteName: 'ProfileMember'

});


const modelprofileStack = createStackNavigator({

  ProfileModel : { screen: ProfileModel},
  EditProfileModel : { screen: EditProfileModel},


 }, {
    headerMode: 'none',
      initialRouteName: 'ProfileModel'

 });



const forgotPasswordStack = createStackNavigator({

  ForgotPassword : { screen: ForgotPassword},
  EnterOtp : { screen: EnterOtp},
  OtpChangePassword : { screen: OtpChangePassword},



 }, {
    headerMode: 'none',
      initialRouteName: 'ResetPassword'

 });

 const chatStack= createStackNavigator({

  ChatList : { screen: ChatList},
  ChatBoxMember : { screen: ChatBoxMember},
  ChatBoxModel : { screen: ChatBoxModel},
  ChatBox : { screen: ChatBox},
  ModelRating : { screen:ModelRating},
  MemberProfileBlock : { screen:MemberProfileBlock},
  ModelBlock : { screen:ModelBlock},
    SendLocation : { screen:SendLocation},
    ImageView : { screen:ImageView},





 }, {
    headerMode: 'none',
      initialRouteName: 'ChatList'

 });




 const NotificationTabModel = createMaterialTopTabNavigator({
  Activity: NotificationActivity,
  List: NotificationsListModel,

},
{
  initialRouteName: 'Activity',
  labelStyle: {
    fontSize: 12,
  },
  tabStyle: {
    width: 100,
  },
  tabBarOptions: {
    activeTintColor:colors.color_primary,
    inactiveTintColor:'black',
      style: {
          backgroundColor: 'white'
      }
  },
  tabBarPosition: 'top',
  // swipeEnabled: true,
  // animationEnabled: true,

})

const NotificationTabMember = createMaterialTopTabNavigator({
  Activity: NotificationActivity,
  List: NotificationsListMember,

},
{
  initialRouteName: 'Activity',
  labelStyle: {
    fontSize: 12,
  },
  tabStyle: {
    width: 100,
  },
  tabBarOptions: {
    activeTintColor:colors.color_primary,
    inactiveTintColor:'black',
      style: {
          backgroundColor: 'white'
      }
  },
  tabBarPosition: 'top',
  // swipeEnabled: true,
  // animationEnabled: true,

})



 const nStackmodel = createStackNavigator({

  Page : { screen: NotificationTabModel,
    navigationOptions: ({ navigation }) => ({  //navigation is used for setting custiom title from prev
      headerTitleStyle: {  textAlign:"center",  flex:1 ,marginLeft:-20 },
      headerLayoutPreset: 'center',
      title: I18n.t('notifications'),
      headerLeft: (

        <TouchableOpacity onPress={() => navigation.navigate("HomeScreen")}>
        <Image style={{width: 25, height: 25,margin:10}}  source={require('./app/assets/backbtn.png')} />
        </TouchableOpacity>


      ),
      headerStyle:{
        backgroundColor:colors.color_primary
      }



    }),
  },
  MemberProfile:{
    screen:MemberProfile,
    navigationOptions: {

      header: null //this will hide the header
    },
  }


}, {
  headerMode:'screen',

     initialRouteName: 'Page'

});

const nStackmember = createStackNavigator({

  Page : { screen: NotificationTabMember,
    navigationOptions: ({ navigation }) => ({  //navigation is used for setting custiom title from prev
      headerTitleStyle: {  textAlign:"center",  flex:1 ,marginLeft:-20 },
      headerLayoutPreset: 'center',
      title: I18n.t('notifications'),
      headerLeft: (

        <TouchableOpacity onPress={() => navigation.navigate("HomePage")}>
        <Image style={{width: 25, height: 25,margin:10}}  source={require('./app/assets/backbtn.png')} />
        </TouchableOpacity>


      ),
      headerStyle:{
        backgroundColor:colors.color_primary
      }


    }),
  },
  ModelProfileChat:{
    screen:ModelProfileChat,
    navigationOptions: {

      header: null //this will hide the header
    },

  },
  BuyCoins:{
    screen:BuyCoins,
    navigationOptions: {

      header: null //this will hide the header
    },

  },
  ChatBoxMember : {
    screen: ChatBoxMember,
    navigationOptions: {

      header: null //this will hide the header
    },
  },
  UpdateBid:{
    screen:UpdateBid,
    navigationOptions: {

      header: null //this will hide the header
    },

  },



}, {
  headerMode:'screen',

     initialRouteName: 'Page'

});



 const notificationStack = createStackNavigator({


  MemberProfile : { screen: MemberProfile},
  MemberProtest : { screen: MemberProtest},



 }, {
    headerMode: 'none',
      initialRouteName: 'MemberProfile'

 });



 const homeStack = createStackNavigator({

  HomePage : { screen: HomePage},


}, {
	 headerMode: 'none',
     initialRouteName: 'HomePage'

});

const resetStack = createStackNavigator({

  ResetPassword : { screen: ResetPassword},


}, {
	 headerMode: 'none',
     initialRouteName: 'ResetPassword'

});


const chooseLanguageDrawerStack = createStackNavigator({

  ChooseLanguageDrawer : { screen: ChooseLanguageDrawer},


}, {
	 headerMode: 'none',
     initialRouteName: 'ChooseLanguageDrawer'

});

const StarStack = createStackNavigator({

  BuyStars : { screen: BuyStars},
 // PaymentMode : { screen: PaymentMode},

 }, {
    headerMode: 'none',
    initialRouteName: 'BuyStars'

 });



const coinStack = createStackNavigator({

  BuyCoins : { screen: BuyCoins},
  //PaymentMode : { screen: PaymentMode},



}, {
	 headerMode: 'none',
     initialRouteName: 'BuyCoins'

})


const contactStack = createStackNavigator({

  ContactUs : { screen: ContactUs},




}, {
	 headerMode: 'none',
     initialRouteName: 'ContactUs'

})




const memberhomeStack = createStackNavigator({

  HomePage : { screen: MemberHomePage},
  Filters : { screen: Filters},
  ChatList : { screen: chatStack},
  ModelBidding : { screen: ModelBidding},
  ProfileDetails : { screen: ProfileDetails},
  ModelProfileChat : { screen: ModelProfileChat},
  ModelProtest : { screen: ModelProtest},


}, {
	 headerMode: 'none',
     initialRouteName: 'HomePage'

})

const stackModelBids = createStackNavigator({


  NotificationsListModel : { screen: NotificationsListModel},
  MemberProfile:{
    screen:MemberProfile,
    navigationOptions: {

      header: null //this will hide the header
    },
  }


}, {
	 headerMode: 'none',
     initialRouteName: 'NotificationsListModel'

})

const stackMemberBids = createStackNavigator({

  NotificationsListMember : { screen: NotificationsListMember},
  ModelProfileChat:{
    screen:ModelProfileChat,
    navigationOptions: {

      header: null //this will hide the header
    },

  },
  BuyCoins:{
    screen:BuyCoins,
    navigationOptions: {

      header: null //this will hide the header
    },

  },
  ChatBoxMember : {
    screen: ChatBoxMember,
    navigationOptions: {

      header: null //this will hide the header
    },
  },
  UpdateBid:{
    screen:UpdateBid,
    navigationOptions: {

      header: null //this will hide the header
    },

  },


}, {
	 headerMode: 'none',
     initialRouteName: 'NotificationsListMember'

})

const stackNotifications = createStackNavigator({


  NotificationActivity : { screen: NotificationActivity},


}, {
	 headerMode: 'none',
     initialRouteName: 'NotificationActivity'

})


const modeldrawerNavigator = createDrawerNavigator(
  {
  HomeScreen: { screen: homeStack},
  EditProfile : { screen: modelprofileStack},
  BuyStars : { screen: StarStack},
  ChatList : { screen: chatStack},
  Notifications : { screen: stackNotifications},
  Bids : { screen: stackModelBids},
  ResetPassword : { screen:resetStack},
  Terms : { screen: TermsDrawer},
  Privacy : { screen:PrivacyPolicyDrawer},
  Contact : { screen:contactStack},
  ChooseLanguageDrawer : {screen:chooseLanguageDrawerStack},
  Home : { screen:homeStack},


  },
  {initialRouteName: 'HomeScreen',
  gesturesEnabled: true,
    contentComponent: props => <DrawerModel {...props} />
  },
  {
		contentOptions: {
       // activeTintColor: '#e91e63',
        //activeBackgroundColor:'#A4A4DD',

			}
  }

);

const memberdrawerNavigator = createDrawerNavigator(
  {
  HomeScreen: { screen: memberhomeStack},
  EditProfile : { screen: memberprofileStack},
  BuyCoins : { screen: coinStack},
  ChatList : { screen: chatStack},
  Notifications : { screen: stackNotifications},
  Bids : { screen: stackMemberBids},
  ResetPassword : { screen: resetStack},
  Terms : { screen: TermsDrawer},
  Privacy : { screen:PrivacyPolicyDrawer},
  Contact : { screen:contactStack},
  ChooseLanguageDrawer : {screen:chooseLanguageDrawerStack},
  Home : { screen:memberhomeStack},

  },
  {initialRouteName: 'HomeScreen',
  gesturesEnabled: true,
    contentComponent: props => <DrawerMember {...props} />
  },
  {
		contentOptions: {
       // activeTintColor: '#e91e63',
        //activeBackgroundColor:'#A4A4DD',

			}
  }

);




// const RootStack = createStackNavigator({
//   Splash : { screen: Splash },
//   PrivacyPolicy : { screen: PrivacyPolicy},
//   Welcome : { screen: Welcome },
//   Login : { screen: Login },
//   Terms : { screen: Terms },
//   SignupModel : { screen: SignupModel},
//   SignupMember : { screen: SignupMember},
//   MemberType : { screen: MemberType},
//   MemberTypeSocial : { screen: MemberTypeSocial},
//   LocationSocial : { screen: LocationSocial},
//   LocationSocialMember : { screen: LocationSocialMember},
//   ModelType : { screen: ModelType},
//   ForgotPassword : { screen: ForgotPassword},
//   CreateProfileModel : { screen: CreateProfileModel},
//   CreateProfileMember : { screen: CreateProfileMember},
//   ModelProfile: { screen: ModelProfile},
//   EnterOtp : { screen: EnterOtp},
//   HomeMember: {screen: memberdrawerNavigator},
//   HomeModel: {screen: modeldrawerNavigator},
//   OtpChangePassword : { screen: OtpChangePassword},
//   InstagramEmail : { screen: InstagramEmail},
//   ChooseLanguage : { screen: ChooseLanguage},


// }, {
// 	 headerMode: 'none',
//      initialRouteName: 'Splash',
//      navigationOptions: {
//       gesturesEnabled: false
//     }

// })

const signInStack  = createStackNavigator({

  Welcome : { screen: Welcome },
  Login : { screen: Login },
  Terms : { screen: Terms },
  SignupModel : { screen: SignupModel},
  SignupMember : { screen: SignupMember},
  MemberType : { screen: MemberType},
  MemberTypeSocial : { screen: MemberTypeSocial},
  LocationSocial : { screen: LocationSocial},
  LocationSocialMember : { screen: LocationSocialMember},
  ModelType : { screen: ModelType},
  ForgotPassword : { screen: ForgotPassword},
  CreateProfileModel : { screen: CreateProfileModel},
  CreateProfileMember : { screen: CreateProfileMember},
  ModelProfile: { screen: ModelProfile},
  EnterOtp : { screen: EnterOtp},
  OTPEmail : { screen: OTPEmail},

  OtpChangePassword : { screen: OtpChangePassword},
  InstagramEmail : { screen: InstagramEmail},
  ChooseLanguage : { screen: ChooseLanguage},




}, {
	 headerMode: 'none',
     initialRouteName: 'Welcome'

})



const AppStack = createSwitchNavigator({
  Splash : { screen: Splash },
  Welcome : { screen: signInStack },
  HomeMember: {screen: memberdrawerNavigator},
  HomeModel: {screen: modeldrawerNavigator},
  ChooseLanguage : { screen: ChooseLanguage},
  PrivacyPolicy : { screen: PrivacyPolicy},



}, {
	 headerMode: 'none',
     initialRouteName: 'Splash',
     navigationOptions: {
      gesturesEnabled: false
    }

})

const AppNavigator = createAppContainer(AppStack)
export default AppNavigator;
